var editor = document.getElementById('editor');


// editor.addEventListener('keydown', editorKeyDown);
function editorKeyDown(event) {
    const activeSpan = document.activeElement;

    editor.className = 'editor default';
    if (event.key === 'Enter' && !event.shiftKey && !event.ctrlKey) {
        event.preventDefault(); // Prevent new line in the span
        addNewSpan(activeSpan);
    } if (event.key === 'Enter' && !event.shiftKey && event.ctrlKey) {
        event.preventDefault(); // Prevent new line in the span
        // addNewSpan(activeSpan);
            // loop through all the spans and store the data into an array
            // that will then get stored onto currentHighlightNode.notes = ["", ""] etc
        let notes = resetSpan(true, activeSpan, false);
        // noteHighlightNode.notes = notes;
        thisNoteAtTime.notes = notes;
        editor.className = 'editor saved';

    } else if (event.key === 'Backspace' && activeSpan.innerText.trim() === '') {
        event.preventDefault(); // Prevent removing characters if already empty
        removeSpan(activeSpan);
    } else if (event.key === 'ArrowUp') {
        setCaretToEnd(activeSpan.previousElementSibling);
    }
    else if (event.key === 'ArrowDown') {
        setCaretToEnd(activeSpan.nextElementSibling);
    }

}

function resetSpan(takeNotes, activeSpan, toClearSpan) {
    let currentSpan = activeSpan;
    while (currentSpan.previousElementSibling) {
        currentSpan = currentSpan.previousElementSibling;
    }
    let notes = [];
    let lastSpan;
    while (currentSpan) {
        if (takeNotes) {
            notes.push(currentSpan.innerText);
        }
        currentSpan = currentSpan.nextElementSibling;
        if (currentSpan)
            lastSpan = currentSpan;
    }
    console.log("NOTES: ", notes);

    if (toClearSpan) {
        while (lastSpan.previousElementSibling) {
            let tmp = lastSpan;
            lastSpan = lastSpan.previousElementSibling;
            removeSpan(tmp);
        }
        if (lastSpan) {
            lastSpan.innerText = '';
        }
    }
    return notes;
}

function unicodeReplaceSymbols(activeSpan) {
    replaceSymbol(activeSpan, /\\\uI/g, " ∫ ", 3);
    replaceSymbol(activeSpan, /sqrt/g, "√", 4);
    replaceSymbol(activeSpan, /theta/g, "θ", 5);
    replaceSymbol(activeSpan, /beta/g, "β", 4);
    replaceSymbol(activeSpan, /\\\utick/g, "✅", 6);
    replaceSymbol(activeSpan, /\\\ucross/g, "❌", 7);
    replaceSymbol(activeSpan, /\\\uquestion/g, "❓", 10);

}

// editor.addEventListener('input', editorInput);
function editorInput(event) {
    const activeSpan = document.activeElement;
    let previousSpan = activeSpan.previousElementSibling;

    unicodeReplaceSymbols(activeSpan);
    // if (previousSpan) {
    if (!previousSpan) {
        previousSpan = activeSpan;
    }
        // let replaceStr = activeSpan.innerText.replace(/\\\uI/g, " ∫ ");
        // if (replaceStr !== activeSpan.innerText) {
        //     activeSpan.innerText = replaceStr;
        //     setCaretToEnd(activeSpan); // Move caret to the end of the previous span
        // }

        let prevVal = previousSpan.innerText.trim();
        let currVal = activeSpan.innerText.trim();
        if (!(prevVal === "" || currVal === "" )) {
          validateLine(prevVal, currVal, activeSpan);
        }
        else {
            if (prevVal === "" && currVal !== "") {
                console.log("CLEAR LINE COLOR: ")
                applyValidationClass(activeSpan, '');
                // activeSpan.className = 'line';

            }
        }

        // validateLine(previousSpan.innerText.trim(), activeSpan.innerText.trim(), activeSpan);
    // }
}

function replaceSymbol(span, regexFrom, to, replacedLength) {
    let replaceStr = span.innerText.replace(regexFrom, to);

    let originalCaretPosition = getCaretPosition(span);
    adjustCaretAfterReplacement(span, span.innerText, replaceStr, originalCaretPosition)
}

function getCaretPosition(span) {
    const selection = window.getSelection();
    let caretPosition = 0;

    if (selection.rangeCount > 0) {
        const range = selection.getRangeAt(0);
        if (range.commonAncestorContainer === span.firstChild) {
            caretPosition = range.startOffset;
        }
    }

    return caretPosition;
}

// Exampl

function replaceExponentsForPow(formulaStr) {
    let operators = [ "+", "-", "/", "*", "√", "(", ")"];
    let functions = [
        "arcsin", "arccos", "arctan",
        "cosec", "csc", "sec", "cot",
        "sin", "cos", "tan",
        "sqrt", "pow", "abs" ]

    let brackets = ["(", ")",
        // "|" // for absolutes but similar to brackets
    ];
    let str = formulaStr;

    for (let i=0; i < str.length; i++) {
        // if (i === 0 ) {
        //     console.log("LatestStr: " + str);
        // }
        if (str[i] === '^') {
            // then loop backwards and extract the value
                // track start pos
                let startIndex = i;
                let firstFoundBracket = (startIndex - 1 >= 0 && str[startIndex - 1] === ")");
                let bracketDepth = 0;
                // let absDepth = 0;

                // findStartIndex for term:
                while (startIndex - 1 >= 0) {
                    --startIndex;
                    // if not first found bracket
                    if (startIndex === 0 || str[startIndex] === '=') {
                        break;
                    }
                    if (firstFoundBracket) {
                        if (str[startIndex] === ')') {
                            bracketDepth += 1;
                        }
                        if (str[startIndex] === '(') {
                            bracketDepth -= 1;
                        }
                        if (bracketDepth === 0) {
                            break;
                        }
                    } else {
                        if (operators.includes(str[startIndex])) {
                            ++startIndex;
                            break;
                        }

                    }
                }

                // findEndIndex for exponent:
                let endIndex = i;
                firstFoundBracket = (endIndex + 1 < str.length && str[endIndex + 1] === "(");
                bracketDepth = 0;
                while (endIndex + 1 <= str.length) {
                    ++endIndex;
                    // if not first found bracket
                    if (endIndex === str.length || str[endIndex] === '=') {
                        --endIndex;
                        break;
                    }
                    if (firstFoundBracket) {
                        if (str[endIndex] === '(') {
                            bracketDepth += 1;
                        }
                        if (str[endIndex] === ')') {
                            bracketDepth -= 1;
                        }
                        if (bracketDepth === 0) {
                            break;
                        }
                    } else {
                        if (operators.includes(str[endIndex])) {
                            --endIndex;
                            break;
                        }
                    }
                }

                let term = str.substring(startIndex, i);
                let exponent = str.substring(i + 1, endIndex + 1);
                let mathPowStr = "(pow(" + term + "," + exponent + "))";
                str = str.replace(term + "^" + exponent, mathPowStr);

                // console.log("TERM: ", term, startIndex, i);
                // console.log("EXPONENT: ", exponent, i, endIndex);

                i = -1;// reset to start of string to search for more '^' symbols
            // then loop forwards and extract the value
                // track end pos

            // replaceSubstring with new value
        }
    }
    return str;
}

function replaceAbsoluteSymbolForAbs(formulaStr) {
    let operators = [ "+", "-", "/", "*", "√"];// DO NOT INCLUDE THESE FOR ABS: "(", ")"
    // let functions = [
    //     "arcsin", "arccos", "arctan",
    //     "cosec", "csc", "sec", "cot",
    //     "sin", "cos", "tan",
    //     "sqrt", "pow", "abs" ]

    let brackets = ["(", ")"];
    let str = formulaStr;

    for (let i=0; i < str.length; i++) {
        // if (i === 0 ) {
        //     console.log("LatestStr: " + str);
        // }
        if (str[i] === '|') {
            // then loop backwards and extract the value
                // track start pos

                let startIndex = i;// will remain i
                let endIndex = i;// will increment to where it should be
                // let firstFoundBracket = (startIndex - 1 >= 0 && str[startIndex - 1] === ")");
                let bracketDepth = 0;
                let absDepth = 0;

                let innerStartIndex = null, innerEndIndex = null;
                // findEndIndex for exponent:
                while (endIndex + 1 <= str.length) {
                    ++endIndex;
                    // if not first found bracket
                    if (endIndex === str.length) {
                        endIndex = -1; // there was no closing brace (so actually just exit out of the function altogether and don't process the str result):
                        console.log("ABS RETURNED EARLY on: ", str, startIndex, str.length);
                        return formulaStr;// Don't bother touching the formulaStr as it is currently/temporarily malformed whilst the user is still typing it out.
                        // break;
                    }
                    if (str[endIndex] === '(') {
                        bracketDepth += 1;
                    }
                    if (str[endIndex] === ')') {
                        bracketDepth -= 1;
                    }
                    if (str[endIndex] === '|') {
                        // if (operators.includes(str[endIndex - 1]))
                        // then must do the inner bracket thing first:
                        let isImproperAbsSection = operators.includes(str[endIndex - 1]);
                        let isStartIndex = false;
                        if (bracketDepth > 0 || isImproperAbsSection || absDepth > 0) {// or if ended with an operator thus not properly closed:
                            if (absDepth === 0 || isImproperAbsSection) {// now we will need these positions to process this internal one first
                                isStartIndex = true;
                                innerStartIndex = endIndex;
                                absDepth += 1; // +1 if is a startIndex

                            }
                            if (!isStartIndex) {

                                // --absDepth;// get it back to the same layer it is supposedly to be on.
                                // let isEven = (absDepth % 2 == 0);
                                // if (!isEven) {// if not is even
                                //     console.log("ABS RETURNED EARLY on: ", str, startIndex, str.length);
                                //     return formulaStr;
                                // }
                                isStartIndex = false;
                                innerEndIndex = endIndex;
                                // THEN: process this inner one and reset indexes on initial abs() loop and endIndex:
                                let absInternalFunction = str.substring(innerStartIndex + 1, innerEndIndex);
                                let mathAbsStr = "abs(" + absInternalFunction + ")";
                                str = str.replace("|" + absInternalFunction + "|", mathAbsStr);

                                console.log("ABS REACHED ENDING_INNER_ABS on: ", str, innerStartIndex, innerEndIndex, absInternalFunction);

                                // reset currentLoop variables:
                                bracketDepth = 0;
                                absDepth = 0;
                                endIndex = i;
                                innerStartIndex = innerEndIndex = null;
                                continue;
                            }
                            // REMOVE: NO LONGER: changes between 1's and 0's and if bracketDepth ever hits 0 and absDepth === 0 then we've found the matching abs
                        }
                        if (bracketDepth === 0 && absDepth === 0) {
                            break;
                        }
                    }
                }

                // let term = str.substring(startIndex, i);
                let absInternalFunction = str.substring(startIndex + 1, endIndex);
                let mathAbsStr = "abs(" + absInternalFunction + ")";
                str = str.replace("|" + absInternalFunction + "|", mathAbsStr);

                console.log("ABS INTERNAL FUNCTION: ", absInternalFunction, startIndex, endIndex);
                console.log("ABS RESULT: ", mathAbsStr, startIndex, endIndex);
                // console.log("EXPONENT: ", exponent, i, endIndex);

                i = -1;// reset to start of string to search for more '^' symbols
            // then loop forwards and extract the value
                // track end pos

            // replaceSubstring with new value
        }
    }
    return str;
}


function addNewSpan(afterSpan = null) {
    const newSpan = document.createElement('span');
    newSpan.classList.add('line');
    newSpan.contentEditable = true;

    if (afterSpan) {
        editor.insertBefore(newSpan, afterSpan.nextSibling);
    } else {
        editor.appendChild(newSpan);
    }

    newSpan.focus(); // Focus on the new span
    return newSpan;
}

function removeSpan(span) {
    const previousSpan = span.previousElementSibling;

    if (previousSpan) {
        setCaretToEnd(previousSpan); // Move caret to the end of the previous span
        span.remove(); // Remove the current span
    }
    
    // Ensure at least one span remains
    if (editor.children.length === 0) {
        addNewSpan();
    }
}

function setCaretToEnd(element) {
    const range = document.createRange();
    const selection = window.getSelection();
    range.selectNodeContents(element);
    range.collapse(false); // Collapse to the end of the element's contents
    selection.removeAllRanges();
    selection.addRange(range);
}

function adjustCaretAfterReplacement(span, oldText, newText, originalCaretPosition) {
    const selection = window.getSelection();
    const range = document.createRange();

    // Calculate the difference in length between the old and new text
    const lengthDifference = newText.length - oldText.length;

    // Adjust the new caret position based on the length difference
    const newCaretPosition = originalCaretPosition + lengthDifference;

    // Update the span's content with the new text
    // span.innerText = newText;
    if (newText !== span.innerText) {
        span.innerText = newText;
        // moveCaretNpositions(-replacedLength + to.length);// setCaretToEnd(span); // Move caret to the end of the previous span

        // Ensure that the new caret position is within the valid bounds
        const validCaretPosition = Math.min(Math.max(newCaretPosition, 0), newText.length);

        // Move the caret to the new position
        range.setStart(span.firstChild, validCaretPosition);
        range.setEnd(span.firstChild, validCaretPosition);
        selection.removeAllRanges();
        selection.addRange(range);
    }
}

function validateLine(prevLine, currLine, currentSpan) {
    let isValid = true;

    let testCases = [ -2, -1, 0, 1, 2 ];// maybe use 0 to 2pi to cover most scenarios?
    let prevResults = [];
    let currResults = [];

    let prevVariables = [];
    let currVariables = [];
    // getVariablesFromLine(currentLine);
    // getVariablesFromLine(previousLine);

    // TODO: optimise this to only find the variables from the list below as was found in str:
    let acceptedVariables = ["x", "y", "C", "u", "N", "β", "θ" ];
    // for () loop and find all ascii within range of A-Z or a-z
    prevVariables = acceptedVariables;// hardcoded REMOVE
    currVariables = acceptedVariables;// hardcoded REMOVE and add functionality to get whatever is found instead

    // "//" caters for comments (NOTE: is a quick fix - but not thorough):
    previousLine = (prevLine + "//").split("//")[0]
    currentLine = (currLine + "//").split("//")[0]

    let contains = (arr, target) => target.every(v => arr.includes(v));

    if (contains(prevVariables, currVariables) && prevVariables.length === currVariables.length) {
        prevResults = unitTest(previousLine, prevVariables, testCases);
        currResults = unitTest(currentLine, currVariables, testCases);
        let validationResult = validateEqual(prevResults, currResults);
        if (validationResult === 1) {
            // currentSpan.className = '';
            // currentSpan.style.color = '';
            // currentSpan.style.attributes = '';
            // currentSpan.style.attribute = '';

            applyValidationClass(currentSpan, "line-valid");
            editor.className = 'editor valid';
            return 1;
        } else if (validationResult === 0) {
            // currentSpan.className = '';
            // currentSpan.style.color = '';
            // currentSpan.style.attributes = '';
            // currentSpan.style.attribute = '';

            applyValidationClass(currentSpan, "line-error");
            editor.className = 'editor invalid';
            return 0;
        }
        // else let it become a line caution as per what follows below:
    }
    // } else {
        applyValidationClass(currentSpan, "line-caution");
        editor.className = 'editor caution';
        //    return 0;// make it orange
    // }

    // return -1; then is invalid
    return -1;// then is valid
    
    // Dummy validation: check if line lengths match
    // if (previousLine.length !== currentLine.length) {
    //     isValid = false;
    // }

    // // Apply appropriate class
    // if (isValid) {
    //     currentSpan.className = 'line line-valid';
    //     editor.className = 'editor valid';
    // } else {
    //     currentSpan.className = 'line line-error';
    //     editor.className = 'editor invalid';
    // }
}

// editor.addEventListener('copy', editorCopy);
function editorCopy() {
    event.preventDefault();  // Prevent default copy behavior

    // Get the text content of the current selection
    const selectedText = window.getSelection().toString();
    console.log("TO BE COPIED: ", selectedText);
    
    // Set the copied text to the clipboard
    event.clipboardData.setData('text/plain', selectedText);
}

function applyValidationClass(span, className) {
    cleanUpSpan(span);
    span.style.color = '';
    span.className = 'line';  // Reset the class list
    span.classList.add(className);

}

function cleanUpSpan(span) {        
    if (span.firstChild) {
        // Loop through child nodes and only remove span elements
        let foundInnerSpan = false;
        let child = span.firstChild;
        while (child) {
            const nextChild = child.nextSibling; // Store the next sibling before removal
            if (child.nodeName === "SPAN") {
                span.removeChild(child);
                foundInnerSpan = true;
            }
            child = nextChild;
        }
        if (!foundInnerSpan) {
            return ;// then don't process any changes to the current span.
        }

        // Save the current selection (caret position)
        const selection = window.getSelection();
        const range = document.createRange();
        const originalCaretPosition = selection.anchorOffset;

        // Get the current innerText
        const text = span.innerText;


        // Reset the span's innerText to ensure it only contains plain text
        span.innerText = text;

        // Restore the caret position
        range.setStart(span.firstChild || span, originalCaretPosition);
        range.setEnd(span.firstChild || span, originalCaretPosition);
        selection.removeAllRanges();
        selection.addRange(range);
    }
}


function unitTest(line, variables, testCases) {
    let results = [];
    let lineStr = line;

    lineStr = lineStr.replace(/\s/g, "");// removes whitespaces
    lineStr = replaceAbsoluteSymbolForAbs(replaceExponentsForPow(lineStr));
    for (let i=0; i < testCases.length; i++) {
        let formula = lineStr;

        // replace symbol related stuff:
        formula = formula.replace(/∫/g, "");
        formula = formula.replace(/dx/g, "");
        formula = formula.replace(/√/g, "sqrt");
        
        for (let j=0; j < variables.length; j++) {
            let variableIteration = j / 10000.0;// to make 0.0000n difference in the value to handle each variable difference so that is unique per variable
            let variable = variables[j];
            var replace = new RegExp(variable, 'g');
            var replaceWithMultiply = new RegExp("\\\*" + variable, 'g');

            // replace variable related stuff:
            formula = formula.replace(replaceWithMultiply, variable);
            formula = formula.replace(replace, "*("+ (testCases[i] + variableIteration + 0.0).toString() + ")");
        }

        // correction bug fixes:
        var correctionMultiply = new RegExp("\\\(\\\*", 'g');
        formula = formula.replace(correctionMultiply, "(");
        var correctionMultiplyFront = new RegExp(/^\\\*/g);

        // replace equal sign: 
        if (formula.includes("=")) {
            // then split and swap signs of lhs because its like moving it over to the rhs:
            let splitFormula = formula.split("=")
            formula = "-("+ splitFormula[0] + ")" + "+" + splitFormula[1];
            console.log("BUG HELP: ", formula, splitFormula)
        }

        // replace multiplication-with-symbol (bug fix related):
        formula = formula.replace(/^(\*)/, "");
        formula = formula.replace(/(\=\*)/, "=");
        formula = formula.replace(/(\+\*)/, "+");
        formula = formula.replace(/(\-\*)/, "-");
        formula = formula.replace(/(\/\*)/, "/");
        formula = formula.replace(/(\*\*)/, "*");
        
        formula = formula.replace(/(\,\*)/, ",");
        formula = formula.replace(/(\,\*)/, ",");// BUG: needed to replace 2 times it seems...
        // formula = formula.replace(/(\.)/, "*");// don't do this as won't work well with floats: unless check if lhs and rhs is not both a number (that would fix that)

        // replace bracket related stuff:
        formula = formula.replace(/(\)\()/, ")*(");

        console.log("FORM: ", formula); // should now be calculateable
        // let result = eval(`let tan=Math.tan, sin=Math.sin, cos=Math.cos; ${formula}`);

        let result;
        try {
            result = evaluate(`${formula}`);
        }
        catch(err) {
            result = null;
        }

        console.log("RESULT: ", result);

        // To handle inequalities - would need to acknowledge  && swap entire symbol: <= or >= or == but take calculation of each side separately
            // and answer must still equate to both sides individually, but also the boolean check must check out.
        // result = (result === true ? 1 : result === false ? 0 : result);

        results.push(result);
    }
    return results;
}
function validateEqual(result1, result2) {
    if (result1.length === result2.length) {
        for (let i=0; i < result1.length; i++) {
            try {
                if (result1[i] === null || result2[i] === null) {
                    return -1;// error in validating (meaning the mathString is likely malformed)
                }
                else if (result1[i].toFixed(9) != result2[i].toFixed(9)) {

                    if (isFinite(result1[i]) === false && isFinite(result2[i]) === false ) {
                        // then is equal because infinity and not a number is essentually the same thing. ('but we just don't know which one gets there quicker')
                        continue;
                    } else {
                        console.log("NOT EQUAL : ", result1[i], result2[i])
                        console.log("NOT SO EQUAL : ", isFinite(result1[i]), isFinite(result2[i]))
                        return 0;// successful error validation
                    }
                }
            } catch (e) {
                console.log("ERROR: validateEqual(): ", e);
                return -1;// error in validating
            }
        }
    } else {
        console.log("ERROR: LENGTHS DONT MATCH: ", result1, result2)
        return -1;//error in validating
    }
    console.log("ARE EQUAL : ", result1, result2)

    return 1;// successful positive validation
}

// Add an initial span to start editing
// addNewSpan();


// DONE: if equal sign: replace with a + but multiply left side with a minus sign  -(lhs)
    // so get: -(lhs) + rhs
// DONE: first use these results and then later will adapt to get permutations of 
// more variables like being inclusive of y

// DONE: if save: ctrl+s: then box shadow must go blue for saved

// TODO: add let variables like: dy/dx or d/dx for derivatives and derivative formulas then
    // then replace with these formula's in terms of x in following lines

// TODO: if startsWith: "let ": then make replacements of those variables

// NEW: 
// TODO: n( i.e 1(, 2(, 3(, x( .etc should become 1*(, 2*(, 3*( x*(
// TODO: arctan(x)^2 or sqrt(2)^2 should take into account the function being used not just as the brackets. (so once reached '(' check infront for a functionality like sin, cos, tan etc)



// TODO fix copy paste: (by copying into buffer only after removing the colours (and the span) if possible)
        // - (but also when pasting to remove that too)


// TODO: sqrt(pow(anything, 2)) and pow(sqrt(anything), 2), is the same as anything^(2 * 1/2) which is == anything NOT just |anything|.

// NICE TO HAVE: (colour in the different colours the differences between the 2 lines - thus the previous line and the curr line will be affected in terms of color based off whatever has changed)
