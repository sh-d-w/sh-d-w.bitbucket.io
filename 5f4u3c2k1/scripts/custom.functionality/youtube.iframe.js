async function ytplay() {
  const iframe = document.getElementById('yt-iframe');
  iframe.contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}', '*');

  // global.player;
  function onYouTubeIframeAPIReady() {
    global.player = new YT.Player('player', {
      height: '390',
      width: '640',
      videoId: 'M7lc1UVf-VE',
      playerVars: {
        'playsinline': 1
      },
      events: {
        'onReady': onPlayerReady,
        'onStateChange': onPlayerStateChange
      }
    });
  }
  onYouTubeIframeAPIReady();


}

// TODO add this iframe to the source code - but with display: none (by default):
// TODO make above function capable of taking in the src video url that will be played by the user.
// <!-- <iframe
// id="yt-iframe" style="position:fixed; top:0; left:0; bottom:0; right:0; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:-999999;"
// src="https://www.youtube.com/embed/6XGzJQKbV40?si=BkY_7acIGG2YlYoK&enablejsapi=1" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen>
// </iframe> -->

