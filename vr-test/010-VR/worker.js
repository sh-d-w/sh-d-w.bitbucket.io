// worker.js
// This thread aims to process the slow optimisation of MarchingCubes to blender object files.

importScripts('three.min.js');
// importScripts('jsm/objects/MarchingCubes.js');
importScripts('jsm/exporters/OBJExporter.js');

function exportVrDrawBlobFaces(marchingCube) {
	// Export the geometry to a Wavefront OBJ format
	const exporter = new OBJExporter();
	const objData = exporter.parse(marchingCube);// vrDrawBlob);

	// Process the OBJ data to remove duplicate vertices and faces
	const lines = objData.split('\n');
	const vertexMap = new Map();
	const uniqueVertices = [];
	const uniqueFaces = [];

	const originalVerticesMapping = [];

	for (let i = 0; i < lines.length; i++) {
	const line = lines[i];

	if (line.startsWith('v ')) {
		// Vertex line
		const vertexCoords = line.split(' ').slice(1).map(parseFloat);
		const vertexKey = vertexCoords.join(',');

		let vertexIndex;
		if (!vertexMap.has(vertexKey)) {
		// Add new unique vertex to the map and array
		vertexIndex = uniqueVertices.length / 3;
		vertexMap.set(vertexKey, vertexIndex);
		uniqueVertices.push(...vertexCoords);
		} else {
		vertexIndex = vertexMap.get(vertexKey);
		}
		originalVerticesMapping.push(vertexIndex);

	} else if (line.startsWith('f ')) {
		// Face line
		const faceIndices = line.split(' ').slice(1).map(index => {
		const subIndices = index.split('/').map(subIndex => {
			const vertexIndex = parseInt(subIndex) - 1; // Subtract 1 since OBJ indices start from 1
			// let vertexKey = uniqueVertices.slice(vertexIndex * 3, vertexIndex * 3 + 3).join(',');
			// let mappedIndex = vertexMap.get(vertexKey);

			// if (mappedIndex === undefined) {//TODO: TRY USING ONLY THIS METHOD INSTEAD OF MAPPING IT IN 2 ways
			let newVertexIndex = originalVerticesMapping[vertexIndex/* +1 here */];//TODO: TRY + 1 as might be distorting some faces because using wrong index
			vertexKey = uniqueVertices.slice(newVertexIndex * 3, newVertexIndex * 3 + 3).join(',');
			mappedIndex = vertexMap.get(vertexKey);// TODO: could even be this being +1 'd when returned??? but less likely
			// }
			return mappedIndex + 1; // Add 1 since OBJ indices start from 1
		});

		return subIndices.join('/');
		});

		// Check if the face is already in the unique faces array
		const faceLine = faceIndices.join(' ');
		if (!uniqueFaces.includes(faceLine) && !faceIndices.includes(NaN)) {
		// Add new unique face to the array
		uniqueFaces.push(faceLine);
		}
	}
	}

	// Reconstruct the OBJ data with the unique vertices and faces
	let reconstructedOBJ = '';

	for (let i = 0; i < uniqueVertices.length; i += 3) {
	const vertexLine = `v ${uniqueVertices[i]} ${uniqueVertices[i + 1]} ${uniqueVertices[i + 2]}`;
	reconstructedOBJ += vertexLine + '\n';
	}

	for (let i = 0; i < uniqueFaces.length; i++) {
		const faceLine = `f ${uniqueFaces[i]}`;
		const processedFaceLine = faceLine
		.split(' ')
		.map(face => {
		  const faceIndices = face.split('/').map(index => (isNaN(index) ? '' : index));
		  return faceIndices.join('/');
		})
		.join(' ');
		reconstructedOBJ += 'f ' + processedFaceLine + '\n'
		// reconstructedOBJ += faceLine + '\n';
		// reconstructedOBJ += faceLine.replace("NaN", "") + '\n';
	}

	// Save the reconstructed OBJ data to a file or further process it as needed
	return reconstructedOBJ;

}

self.addEventListener('message', (event) => {

    const data = event.data;
    const functionName = data.functionName;
    
    // const serializedData = event.data;
    const serializedData = data.vrDrawBlob;
    // Deserialize the serializedData
    const deserializedData = JSON.parse(serializedData);
    // Reconstruct the vrDrawBlob object with its methods
    const vrDrawBlob = new THREE.ObjectLoader().parse(deserializedData);
    //   const vrDrawBlob = event.data;


    // Perform the slow computation here
    let result;
    if (functionName === "exportVrDrawBlobFaces") {
        result = exportVrDrawBlobFaces(vrDrawBlob);
    }
    // add more functionName functions here...

    // Send the result back to the main thread
    self.postMessage(result);
});