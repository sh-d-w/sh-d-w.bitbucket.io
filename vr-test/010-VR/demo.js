//TODO OTHER:
// 1. find suitable location for this raycasting of camera and vrheadset collision object, move from objects/ascii.js
// 1. move css3d code into its file css3dRenderer.js
// 3. Removed buildings objects until such time as you get them to work - get them to work.
// 2. Delete the speechRecognition.js code as couldn't get it to work on oculus.

// DONE: 1. Headset must move to camera position on init
// DONE: 2. Camera must adopt headset rotation values
// DONE: 3. Headset must update the camera position when it moves with analog stick
// DONE: 4. Right controller analog stick must rotate the headset view
// DONE: 5. Fix the camera aimer to work on the VR headset
// DONE: 6. Fix up the terrain collision algorithm
// DONE: 7. Add (basic) visible controllers in the UI

//TODO: non-vr:
// 1. Make moving objects doable in non-vr too - perhaps make it work with the camera aimer object.
// 2. MOVE THIS WORKER STUFF TO APPROPRIATE LOCATION/function-names etc AND CLEANUP

//TODO: important - planning
// 1. port objects meta data and their current position, rotation and scale states into a firebase
//		json file. Should be able to move objects and then save them back here and thus everytime
//		we reload the application, the objects are in their correct locations.
// 2. port all .obj files from the server aswell as we want to build a collection of useful upload
//		objects without having to struggle with the current way of porting them.

//TODO: VR:
// 2. Test original video texture worked or not in vr without the stream, just a blatent video? Will indicate possible causes of the non animating issue...
// 1. mode switching: allow for on button combo click with analog direction to signify different
//	modes, modes namely:
		// A. DONE: object move mode
		// B. save current objects states - ON THE SERVER
		// C. Keyboard mode - First plan out the the key combo mapping
		// D. Draw mode
				// 1. - (BUT FIRST conclude whether can save it as a blender object file)
					//TODO: Below is how you can convert a threejs object into a wavefront.obj file:
						// var exporter = new OBJExporter();
						// console.log(exporter.parse(sphere.object));

		// E. object texture switching mode
				// 1. traverse the selected object if no instance of emissive
					// 2. else change its colour
					// 3. or change its texture directly
		// F. object scaling mode
		// G. charecter control mode
		// H. kinematics (edit) mode
			// rotate, translate, center, relative
			// relativeTo, switch to part,
		// I. NPC instructions recorder mode
		// J. Decal mode
				// 1. would be good to first implement it then later get it working on animations but link it solely to the instance object meta data
				// 2. but should be kept for saving purposed: shouldn't need its meta data as new instance object should reset/remove the decals on them and start a fresh
				// 3. pre render at where aimer is aiming - what it would look like and delete etc
				// 4. once placed it shouldn't move unless animating but should adopt that of its parents meta-data thereafter
		// K. object internal layer select (makes outer object layer semi-transparent and the inner layers clickable - and go more and more in depth wise)
		// L. terrain mode //12
		// M. Diagram mode
		// N. switch objects - 2d array sorting of object options -
				// 1. the selected object must be swappable with a new instance object of any object from the root objects list
				// 2. it must get removed but first its position, rotation and scale values must be applied to the new instance object that is cloned off the new object.

//Current goals:
				// DONE: mode change identification,
				// DONE: mode switching,
				// DONE: isolate object select mode,
				// DONE: scale mode (in scale mode),
				// SEMI: texture swap (in texture mode),
				// object swap (in objectSwitch mode) - should include create newObject too - and object delete if enough controls
				// decal mode (in decal mode)
				// draw mode (integrate if possible to save as blender object file)
				// kinematics mode (in kinematics mode)

// 3. datagram mode to make datagrams and save them as jsons (preferably on the server)
// 4. typing formula, without keyboard make bindings possible with analog stick.
// 5. Datagram nodes rendering 3d ascii.js - pull current formula and place in 3d.
// 6. Headset must thereafter update the camera by its change in position (dynamically)
// 7. Complete all the '// TODO' items in this code.
// 8. Test selenium video texture inclusion of a web object ported to threejs using selenium and phantomJS to run url in background

// FAILED:
// 3. integrate speech recognition to change cube color as a signifier of which mode it is in.


/**
 * Based on http://www.emagix.net/academic/mscs-project/item/camera-sync-with-css3-and-webgl-threejs
 * @author mrdoob / http://mrdoob.com/
 */

// THREE.CSS3DObject = function ( element ) {

// 	THREE.Object3D.call( this );

// 	this.element = element;
// 	this.element.style.position = 'absolute';

// 	this.remove = function () {

// 		if ( this.element.parentNode !== null ) {

// 			this.element.parentNode.removeChild( this.element );

// 		}

// 	};

// };
class CSS3DObject extends THREE.Object3D {

	constructor( element = document.createElement( 'div' ) ) {

		super();

		this.isCSS3DObject = true;

		this.element = element;
		this.element.style.position = 'absolute';
		this.element.style.pointerEvents = 'auto';
		this.element.style.userSelect = 'none';

		this.element.setAttribute( 'draggable', false );

		this.addEventListener( 'removed', function () {

			this.traverse( function ( object ) {

				if ( object.element instanceof Element && object.element.parentNode !== null ) {

					object.element.parentNode.removeChild( object.element );

				}

			} );

		} );

	}

	copy( source, recursive ) {

		super.copy( source, recursive );

		this.element = source.element.cloneNode( true );

		return this;

	}

}
THREE.CSS3DObject = CSS3DObject;
THREE.CSS3DObject.prototype = Object.create( THREE.Object3D.prototype );
THREE.CSS3DObject.prototype.constructor = THREE.CSS3DObject;

THREE.CSS3DSprite = function ( element ) {

	THREE.CSS3DObject.call( this, element );

};

THREE.CSS3DSprite.prototype = Object.create( THREE.CSS3DObject.prototype );

//

THREE.CSS3DRenderer = function (domElement, cameraElement) {

	console.log( 'THREE.CSS3DRenderer', THREE.REVISION );

	var _width, _height;
	var _widthHalf, _heightHalf;

	var matrix = new THREE.Matrix4();
	
	var cache = {
		camera: { fov: 0, style: '' },
		objects: {}
	};

	var domElement = domElement || document.createElement( 'div' );
	this.domElement = domElement;

	domElement.style.overflow = 'hidden';

	domElement.style.WebkitTransformStyle = 'preserve-3d';
	domElement.style.MozTransformStyle = 'preserve-3d';
	domElement.style.oTransformStyle = 'preserve-3d';
	domElement.style.transformStyle = 'preserve-3d';

	var cameraElement = cameraElement || document.createElement( 'div' ) && domElement.appendChild(cameraElement);

	cameraElement.style.WebkitTransformStyle = 'preserve-3d';
	cameraElement.style.MozTransformStyle = 'preserve-3d';
	cameraElement.style.oTransformStyle = 'preserve-3d';
	cameraElement.style.transformStyle = 'preserve-3d';

	this.setClearColor = function () {

	};

	this.setSize = function ( width, height ) {

		_width = width;
		_height = height;

		_widthHalf = _width / 2;
		_heightHalf = _height / 2;

		domElement.style.width = width + 'px';
		domElement.style.height = height + 'px';

		cameraElement.style.width = width + 'px';
		cameraElement.style.height = height + 'px';

	};

	var epsilon = function ( value ) {

		return Math.abs( value ) < 0.000001 ? 0 : value;

	};

	var getCameraCSSMatrix = function ( matrix ) {

		var elements = matrix.elements;

		return 'matrix3d(' +
			epsilon( elements[ 0 ] ) + ',' +
			epsilon( - elements[ 1 ] ) + ',' +
			epsilon( elements[ 2 ] ) + ',' +
			epsilon( elements[ 3 ] ) + ',' +
			epsilon( elements[ 4 ] ) + ',' +
			epsilon( - elements[ 5 ] ) + ',' +
			epsilon( elements[ 6 ] ) + ',' +
			epsilon( elements[ 7 ] ) + ',' +
			epsilon( elements[ 8 ] ) + ',' +
			epsilon( - elements[ 9 ] ) + ',' +
			epsilon( elements[ 10 ] ) + ',' +
			epsilon( elements[ 11 ] ) + ',' +
			epsilon( elements[ 12 ] ) + ',' +
			epsilon( - elements[ 13 ] ) + ',' +
			epsilon( elements[ 14 ] ) + ',' +
			epsilon( elements[ 15 ] ) +
		')';

	};

	var getObjectCSSMatrix = function ( matrix, camera, parentMetaData ) {

		var elements = matrix.elements;

		/* Adding to calculate its rotation angle: */
		let cssObject = new THREE.Object3D();//create a temp object instance with the rotates and translates
		cssObject.position.set(parentMetaData.position[0], parentMetaData.position[1], parentMetaData.position[2]);
		cssObject.rotation.set(parentMetaData.angle[0], parentMetaData.angle[1], parentMetaData.angle[2]);
		cssObject.updateMatrixWorld(true);//TODO: test if this line is needed or not - by commenting it out.

		function    objectDeltaAngleFromCameraPerspective(object, cameraClone) {
		 // let points = [ [ -1.0 + object.position.x - camera.position.x, 0.0, 0.0 + object.position.z - camera.position.z ], [ 1.0 + object.position.x - camera.position.x , 0.0, 0.0 + object.position.z - camera.position.z ] ]; // [[x, z][x, z]] initial
			let p1 = [ -1.0, 0.0, 0.0 ], p2 = [ 1.0, 0.0, 0.0 ]; // [[x, z][x, z]] initial

			// cameraClone.rotation.order = "ZYX"
			// cameraClone.rotation.x = 0.0;
			// cameraClone.rotation.z = 0.0;
			// let degrees = cameraClone.rotation.y * (180 / Math.PI);
			// if (degrees < 0) {
			// 	degrees += 360;
			// }
			// let quadrant = Math.floor(degrees / 90) + 1;
			// let quadrant = Math.floor((degrees + 45) / 90) % 4 + 1;
			// // console.log(degrees);

			// console.log(cameraClone.rotation.y * (180 / Math.PI));
			// console.log(quadrant);

			// let degrees = cameraClone.rotation.y * (180 / Math.PI);
			// // let quadrant = Math.floor((degrees + 45) / 90) % 4 + 1;
			// let quadrant = Math.floor((degrees + 90) / 90) % 4 + 1;
			// // let quadrant = Math.floor(degrees / 90) + 1;// 0 1
			// console.log(quadrant);
			// console.log(degrees);
			// let degrees = Math.atan2(directionVector.x, directionVector.z) * (180 / Math.PI);// 0deg is infront of camera
			// let degrees = Math.atan2(directionVector.z, directionVector.x) * (180 / Math.PI);//90deg is infront of camera and 0 on lhs
			// let quadrant = Math.floor((degrees) / 90) + 1;

			// Y axis degrees of rotation:
			cameraClone.updateMatrixWorld();
			let directionVector = new THREE.Vector3();
			cameraClone.getWorldDirection(directionVector);
			// let degrees = Math.atan2(directionVector.x, directionVector.z) * (180 / Math.PI);// 0deg is infront of camera
			let cameraRotationY = Math.atan2(directionVector.x, directionVector.z) * (180 / Math.PI);// 0deg is infront of camera
			// let cameraRotationY = Math.atan2(directionVector.z, -directionVector.x) * (180 / Math.PI);//90deg is infront of camera and 0 on rhs
			// cameraRotationY = (cameraRotationY + 360) % 360;
			// let quadrant = Math.floor((cameraRotationY) / 90) + 1;
			// console.log(cameraRotationY);// is now correct in degree form
			// console.log(quadrant);

			p1[0] = -1.0 + object.position.x - cameraClone.position.x;
			p1[2] = 0.0 + object.position.z - cameraClone.position.z;
			p2[0] = 1.0 + object.position.x - cameraClone.position.x;
			p2[2] = 0.0 + object.position.z - cameraClone.position.z;
			// console.log("p1: " + p1);	// p1: -1,0,15		
			// console.log("p2: " + p2);	// p2: 1,0,15	is now correct

			// rotatezyx(p1, [0, 0, 0], 0, -cameraRotationY * 180 / Math.PI, 0);//TODO: get degrees or radians right for these function calls
			// rotatezyx(p2, [0, 0, 0], 0, -cameraRotationY * 180 / Math.PI, 0);
			rotatezyx(p1, [0, 0, 0], 0, cameraRotationY * Math.PI / 180, 0);//TODO: is now correct and by rot of cameraRotationY is correct too as object is always correctly infront of camera now when it is supposed to be.
			rotatezyx(p2, [0, 0, 0], 0, cameraRotationY * Math.PI / 180, 0);

			// console.log("p2p1::: " + p2[2] + " " + p1[2] + " - " + cameraRotationY);// is now definitely correct.
			if (p1[2] > 0.0 || p2[2] > 0.0) { // if z > 0 then object is somewhere on camera view half
				// console.log("p1: " + p1);	// p1: -1,0,15 before rotated by cameraY		
				// console.log("p2: " + p2);	// p2: 1,0,15 before	//I believe is now correct and when off screen it doesn't print this thus correctly detects it would appear
	
				let midpoint = [ (p1[0] + p2[0]) / 2, 0.0, (p1[2] + p2[2]) / 2 ];
				// console.log(midpoint);// seems correct
				let angleToFrontOfCamera = Math.atan2(midpoint[0], midpoint[2]);//Seems correct now. //90.0 - Math.tan(midpoint[2] / midpoint[0]); // TODO: get radians or degrees correct here too.
				// console.log("O1: " + Math.tan(midpoint[2] / midpoint[0]));// angle to midpoint is incorrect
				// console.log("O2: " + Math.tan(midpoint[2] / midpoint[0]) * (180 / Math.PI));// angle to midpoint is incorrect
				// console.log("O3: " + Math.atan2(midpoint[2], midpoint[0]) * (180 / Math.PI));// angle to midpoint is incorrect.
				// console.log("O4: " + Math.atan2(midpoint[0], midpoint[2]) * (180 / Math.PI));//this is the correct one! // 93.0 to -93.0 angle to midpoint is it correct? This one seems correct to me.
				// console.log(angleToFrontOfCamera);

				rotatezyx(p1, [0, 0, 0], 0, angleToFrontOfCamera, 0);//TODO: get degrees or radians right for these function calls
				rotatezyx(p2, [0, 0, 0], 0, angleToFrontOfCamera, 0);
				// midpoint = [ (p1[0] + p2[0]) / 2, 0.0, (p1[2] + p2[2]) / 2 ];// seems correct to me. new midpoint after rotation

				// console.log("angle-to mid now O5: " + Math.atan2(midpoint[0], midpoint[2]) * (180 / Math.PI));// 93.0 to -93.0 angle to midpoint is it correct? This one seems correct to me.
				// console.log("p-1: " + p1);
				// console.log("p-2: " + p2);
		
				midpoint = [ (p1[0] + p2[0]) / 2, 0.0, (p1[2] + p2[2]) / 2 ];// new midpoint after rotation
				p1[0] -= midpoint[0];
				p1[2] -= midpoint[2];
				p2[0] -= midpoint[0];
				p2[2] -= midpoint[2];

				// console.log("p-1: " + p1);
				// console.log("p-2: " + p2);// seems correct now.

				//TODO: finally rotate the points by the object.rotation.y value to be in its exact rotation angle.
				// console.log("DeltaAngle: " + ((Math.atan2(p2[0], p2[2]) * (180 / Math.PI)) - 90) );//seems correct after subtracting 90deg as when straight it is 90deg
				return [0, ((Math.atan2(p2[0], p2[2]) * (180 / Math.PI)) - 90.0), 0];//inshallah is correct now.
			}
			// return [0, 0, 0]; // else no rotations need to take place as object is not on screen/camera.
			return false;
		}

		//intantiate meta object:
		// cssObject = parentMetaData.object.clone();
		// cssObject.position.set(parentMetaData.position[0], parentMetaData.position[1], parentMetaData.position[2]);
		// cssObject.rotation.set(parentMetaData.angle[0], parentMetaData.angle[1], parentMetaData.angle[2]);
		let cssObjectDeltaAngles = objectDeltaAngleFromCameraPerspective(cssObject, camera.clone());

		// var originalDirectionVector = new THREE.Vector3(0, 0, 1);//direction vector
		// originalDirectionVector.applyQuaternion(cssObject.quaternion);//sets the correct direction into the directionVector

		// cssObject.lookAt( camera.position );//look at the camera position
		// cssObject.updateMatrixWorld(true);//TODO: test if this line is needed or not - by commenting it out.
		// var newDirectionVector = new THREE.Vector3(0, 0, 1);//direction vector
		// newDirectionVector.applyQuaternion(cssObject.quaternion);//sets the correct direction into the directionVector

		// // var angle = originalDirectionVector.angleTo(newDirectionVector);//how to get angle between original direction and new direction
		// // css: transform: rotate3d(1, 1, 1, angleAwayFromCamera * 180 / Math.PI);
		// // console.log(angleAwayFromCamera * 180 / Math.PI);		
		// // var angleAwayFromCamera = newDirectionVector.angleTo(originalDirectionVector);

		// // calculates angle to camera - but not change in angle based on camera rotations:
		// var angleAwayFromCamera = newDirectionVector.angleTo(originalDirectionVector);//needed to swap around as css object always faces camera not direction we want it to
		// var axis = new THREE.Vector3().crossVectors(originalDirectionVector, newDirectionVector).normalize();
		// var quaternion = new THREE.Quaternion().setFromAxisAngle(axis, angleAwayFromCamera);
		// var euler = new THREE.Euler().setFromQuaternion(quaternion);//gives the angleAwayFromCamera in x, y and z format
		
		// // calculate change in rotation
		// var originalDirectionVector = new THREE.Vector3(0, 0, -1);
		// var newDirectionVector = cssObject.getWorldDirection(new THREE.Vector3());
		// var angleAwayFromCamera = newDirectionVector.angleTo(originalDirectionVector);
		// var axis = new THREE.Vector3().crossVectors(originalDirectionVector, newDirectionVector).normalize();
		// var quaternion = new THREE.Quaternion().setFromAxisAngle(axis, angleAwayFromCamera);
		// var euler = new THREE.Euler().setFromQuaternion(quaternion);

		// // get the camera's rotation and subtract it from the object's rotation
		// var cameraRotation = camera.getWorldQuaternion(new THREE.Quaternion());
		// var objectRotation = cssObject.getWorldQuaternion(new THREE.Quaternion());
		// var inverseCameraRotation = cameraRotation.clone().inverse();
		// var relativeObjectRotation = inverseCameraRotation.multiply(objectRotation);
		// relativeObjectRotation.multiply(quaternion);

		// var quaternionXYZW = new THREE.Quaternion(relativeObjectRotation.x, relativeObjectRotation.y, relativeObjectRotation.z, relativeObjectRotation.w);
		// var euler = new THREE.Euler().setFromQuaternion(quaternionXYZW);
		// console.log("e-X: " + (euler.x * (180 / Math.PI)) + " e-Y: " + (euler.y * (180 / Math.PI))); // output: e-X: -45 e-Y: 0
		// var euler = new THREE.Euler().setFromQuaternion(relativeObjectRotation);
		// // console.log("X: " + (relativeObjectRotation.x * (180 / Math.PI)) + " Y: "  + (relativeObjectRotation.y * (180 / Math.PI))); // output: -45		console.log(euler.x * (180 / Math.PI)); // output: -45
		// console.log("e-X: " + (euler.x * (180 / Math.PI)) + " e-Y: " + (euler.y * (180 / Math.PI))); // output: X: -45 Y: 0
		// console.log("ANGLE: " + euler.x + ", " + euler.y + ", " + euler.z);

		//angle between object and camera positions not rotations:
		// var cameraPosition = new THREE.Vector3().copy(camera.position);
		// var objectPosition = new THREE.Vector3().copy(cssObject.position);
		
		// var cameraToObject = new THREE.Vector3().subVectors(objectPosition, cameraPosition).normalize();
		// var cameraUp = new THREE.Vector3(0, 1, 0);
		
		// var right = new THREE.Vector3().crossVectors(cameraUp, cameraToObject).normalize();
		// var up = new THREE.Vector3().crossVectors(cameraToObject, right).normalize();
		
		// var matrix = new THREE.Matrix4();
		// matrix.set(
		// 	right.x, up.x, cameraToObject.x,
		// 	right.y, up.y, cameraToObject.y,
		// 	right.z, up.z, cameraToObject.z
		// );
		
		// var euler = new THREE.Euler();
		// euler.setFromRotationMatrix(matrix);
		// console.log("e-X: " + (euler.x * (180 / Math.PI)) + " e-Y: " + (euler.y * (180 / Math.PI))); // output: e-X: -45 e-Y: 0	

		// var cameraRotation = new THREE.Euler().copy(camera.rotation);
		// var objectRotation = new THREE.Euler().copy(cssObject.rotation);
		
		// var relativeRotation = new THREE.Vector3().subVectors(objectRotation, cameraRotation);
		// console.log("relativeRotation: ", relativeRotation);
		
		// var quaternionXYZ = new THREE.Quaternion().setFromEuler(relativeRotation);
		// console.log("quaternionXYZ: ", quaternionXYZ);
		
		// var quaternionXYZW = new THREE.Quaternion(quaternionXYZ.x, quaternionXYZ.y, quaternionXYZ.z, 1);
		// console.log("quaternionXYZW: ", quaternionXYZW);
		
		// var euler = new THREE.Euler().setFromQuaternion(quaternionXYZW);
		// console.log("e-X: " + (euler.x * (180 / Math.PI)) + " e-Y: " + (euler.y * (180 / Math.PI))); // output: e-X: -45 e-Y: 0

		/* Adding end */


		// rotateY('+ camera.rotation.y * 180 / Math.PI +'deg)
		// let translate3d = 'translate3d(-50%,-50%,0) rotateZ(180deg) rotateY(180deg) matrix3d(';
		let display = '';
		if (!cssObjectDeltaAngles) {
			cssObjectDeltaAngles = [0.0, 0.0, 0.0];
			display = 'scaleX(0)';
		}
		let translate3d = 'translate3d(-50%,-50%,0) rotateZ(180deg) rotateY(' + (180.0 + cssObjectDeltaAngles[1]) + 'deg) ' + display + ' matrix3d(';
		// let translate3d = 'translate3d(-50%,-50%,0) rotateZ(180deg) rotateY(180deg) rotate3d(1,1,1,'+angleAwayFromCamera * 180 / Math.PI+'deg) matrix3d(';
		// if (parentMetaData && camera) {
		// 	var vector = new THREE.Vector3(parentMetaData.position[0], parentMetaData.position[1], parentMetaData.position[2]);
		// 	vector.project(camera);
	
		// 	var x = (vector.x + 1) / 2 * window.innerWidth;
		// 	var y = -(vector.y - 1) / 2 * window.innerHeight;	
		// 	translate3d = 'translate3d(calc(' + x + 'px - 50% - ' + ((window.innerWidth / 2) + 2 * (x - (window.innerWidth / 2))) + 'px),calc(50% - ' + y + 'px),0) matrix3d(';
		// 	console.log(x)
		// }
		// return 'translate3d(-50%,-50%,0) matrix3d(' +
		return translate3d +
			epsilon( elements[ 0 ] ) + ',' +
			epsilon( elements[ 1 ] ) + ',' +
			epsilon( elements[ 2 ] ) + ',' +
			epsilon( elements[ 3 ] ) + ',' +
			epsilon( - elements[ 4 ] ) + ',' +
			epsilon( - elements[ 5 ] ) + ',' +
			epsilon( - elements[ 6 ] ) + ',' +
			epsilon( - elements[ 7 ] ) + ',' +
			epsilon( elements[ 8 ] ) + ',' +
			epsilon( elements[ 9 ] ) + ',' +
			epsilon( elements[ 10 ] ) + ',' +
			epsilon( elements[ 11 ] ) + ',' +
			epsilon( elements[ 12 ] ) + ',' +
			epsilon( elements[ 13 ] ) + ',' +
			epsilon( elements[ 14 ] ) + ',' +
			epsilon( elements[ 15 ] ) +
		')';

	};

	var renderObject = function ( object, camera, parentMetaData ) {

		if ( object instanceof THREE.CSS3DObject ) {
			// console.log(isChildMeta);

			var style;

			if ( object instanceof THREE.CSS3DSprite ) {

				// http://swiftcoder.wordpress.com/2008/11/25/constructing-a-billboard-matrix/

				matrix.copy( camera.matrixWorldInverse );
				matrix.transpose();

				matrix.copyPosition( object.matrixWorld );
				matrix.scale( object.scale );
				// matrix.rotation.z = -1;
				// matrix.rotat( object.rotation )
				matrix.elements[ 3 ] = 0;
				matrix.elements[ 7 ] = 0;
				matrix.elements[ 11 ] = 0;
				matrix.elements[ 15 ] = 1;

				style = getObjectCSSMatrix( matrix );// object, camera not implemented for this Sprites logic as currently don't use sprites...

			} else {
				style = getObjectCSSMatrix( object.matrixWorld, camera, parentMetaData);
				// style = getObjectCSSMatrix( object.matrixWorld, object, camera );
				// style = getObjectCSSMatrix( fixedObject.matrixWorld );
			}

			var element = object.element;
			/* Added to align object to correct 3d position in space: */
			element.style.position = 'fixed';
			var vector = new THREE.Vector3(parentMetaData.position[0], parentMetaData.position[1], parentMetaData.position[2]);
			vector.project(camera);
	
			var x = (vector.x + 1) / 2 * window.innerWidth;
			var y = -(vector.y - 1) / 2 * window.innerHeight;	

			element.style.left = x+'px';
			element.style.top = y+'px';
			/* Added end */

			var cachedStyle = cache.objects[ object.id ];

			if ( cachedStyle === undefined || cachedStyle !== style ) {

				element.style.WebkitTransform = style;
				element.style.MozTransform = style;
				element.style.oTransform = style;
				element.style.transform = style;

				cache.objects[ object.id ] = style;

			}

			if ( element.parentNode !== cameraElement ) {

				cameraElement.appendChild( element );

			}

		}

		for ( var i = 0, l = object.children.length; i < l; i ++ ) {
			renderObject( object.children[ i ], camera, object.meta );// parse meta data of angle and position from parent object if is instance object.
		}

	};

	this.render = function ( scene, camera ) {

		var fov = 0.5 / Math.tan( THREE.MathUtils.degToRad( camera.fov * 0.5 ) ) * _height;

		if ( cache.camera.fov !== fov ) {

			domElement.style.WebkitPerspective = fov + "px";
			domElement.style.MozPerspective = fov + "px";
			domElement.style.oPerspective = fov + "px";
			domElement.style.perspective = fov + "px";

			cache.camera.fov = fov;

		}

		scene.updateMatrixWorld();

		if ( camera.parent === undefined ) camera.updateMatrixWorld();

		// const inverseMatrix = new THREE.Matrix4();
		// inverseMatrix.getInverse(camera.matrixWorld);//Still needed?
		// camera.matrixWorldInverse.getInverse( camera.matrixWorld );

		// var style = "translate3d(0,0," + fov + "px)" + getCameraCSSMatrix( camera.matrixWorldInverse ) +
		// 	" translate3d(" + _widthHalf + "px," + _heightHalf + "px, 0)";
		var style = "translate3d(0,0,0px)" + //getCameraCSSMatrix( gss.matrixWorldInverse ) +
			" translate3d(0px,0px, 0)";

		if ( cache.camera.style !== style ) {

			cameraElement.style.WebkitTransform = style;
			cameraElement.style.MozTransform = style;
			cameraElement.style.oTransform = style;
			cameraElement.style.transform = style;
			
			cache.camera.style = style;

		}

		renderObject( scene, camera );

	};

};
// import { CSS3DRenderer } from 'three/addons/renderers/CSS3DRenderer.js';
/////////////END OF css3DRENDERER.js////////////////////////

// const gpu = new GPU();// instantiate to use GPU.js

const cssSceneElem = document.querySelector('#scene');
const cssCameraElem = document.querySelector('#camera');
// const cssWindowElem = document.querySelector(window);
const cssMapElem = document.querySelector('#map');
var cssScene, cssRenderer;
var cssGroup, cssDiv;//group of css dom items

var scene, camera, renderer, mesh, sphere;
var meshFloor, ambientLight, light;

//LUMO SCENE objects renderer:
// var renderTarget, composer, lumoScene;
// var cubeMeshShadow2, cubeMeshShadow, cubeMesh;

var crate, crate2, crateTexture, crateNormalMap, crateBumpMap;
var mitsubishi, mitsubishiBody;


// VR VARIABLES:
	// OBJECT change mode variables:
	var selectableObjects = [];

//ANDROID ANALOG STICK VARIABLES:
var		g_joystick = false;
var		g_analogdown = false;

//3d KINEMATICS ONLINE TOOL:
var		g_kinematicsToolState = "input";
var 	g_parts = [];
var		g_part;//currently selected part on the UI
var		g_center;// = sphere;

//3D OBJECT VARIABLES
var cuber;
var obj_loader;

var camera_pointer;
var	camera_state = 0;	/* 0 = FPS		1 = TPS		2 = BEV */
var	camera_switch_iter = 0;
var	camera_position;	// temp variable to be able to calculate object collisions and movement prevention

var keyboard = {};
var player = { height:1.8, speed:0.3, turnSpeed:Math.PI*0.03 };
var USE_WIREFRAME = false;


//SHAD TEST VARIABLES
// 	var g_angle = 0
	// var texturer;
	var textures = [];
	var objects = new object_("", 0);
	var	images = [];
	
	var images_loaded = 0;
	var no_of_images = 0;

	var	animation_id = 1;
	var	animation_index = 0;
	var animate_timer_start = new Date();
	var animate_timer_end;
	var animate_speed = 75;
	
	var	screen_text = [];

	//GLOBALS:
	var g_vertices;				//storage of vertices from traverse function
	var collision_triangles		// the triangles got from g_vertices for platform object

	var collision_triangles_translation		// a duplicate of collision_triangles but with translated values


//heightmap variables:
var	heightmap_vertices = []
var	height_map = []
	
var	height_offset = [0, 0, 0]       //# The output vector representing the height to drop map by due to terrain differences


// debug tool
function	ft_write(p_fd, p_line, p_size) {
	let	i;
	
	i = 0;
	while (i < p_line.length && i < p_size) {
		document.getElementById(p_fd).innerHTML += p_line[i]
		i++;
	}
}

function	len(p_vector) {
	return (p_vector.length);
}

function	abs(p_number) {
	return Math.abs(p_number);
}

function	straight_line(p_p1, p_p2, p_time) {
    let l_direction_vector = [p_p1[0] - p_p2[0], p_p1[1] - p_p2[1], p_p1[2] - p_p2[2]];

    return [p_p1[0] - l_direction_vector[0] * p_time, p_p1[1] - l_direction_vector[1] * p_time, p_p1[2] - l_direction_vector[2] * p_time];
}

//# SETTING HEIGHTMAP ARRAY:
function	heightmap_load(p_file, p_heightmap, p_heightmap_vertices) {
    // <!-- l_fd = open(p_file, "r") -->

    // <!-- l_file = l_fd.read() -->
	let	l_file = p_file.replaceAll("\\n", '\n')//temp until fix the object file_data
    let i = 0
    let l_f = 0 // # for faces section:
    let l_fc = 0

    l_file = l_file.split("\n")


	for (const [l_key, line] of Object.entries(l_file)) {
		<!-- document.getElementById("demo").innerHTML += l_key + " " + line + "<br>" -->
        if (line && line[0] == 'v' && line[1] == ' ') {
            let vertice = line.split(" ");
            p_heightmap_vertices.push([parseFloat(vertice[1]), parseFloat(vertice[2]), parseFloat(vertice[3])])
            i += 1;
		}
        // # sort_faces()
        if (line && line[0] == 'f' && line[1] == ' ') {
            l_fc += 1
            //# GET NUMERIC VERTEX NUMBERS FOR EACH FACE:
            l_face = line.split(" ");
			l_face.splice(0, 1);

            l_face_len = len(l_face)
			for (let i = 0; i < l_face_len; i++)
                l_face[i] = parseInt(l_face[i].split("/")[0]);
            //# print(l_face)

            //# STORE IN APPROPRIATE ORDER:
            //# [4, 1, 3]     [ [ , ], [ ] ]
            let l_arr = [];
            //# SORTS ORDER OF FACE, MAKE READY TO STORE IN HEIGHTMAP VECTOR:
            if (abs(p_heightmap_vertices[l_face[0] - 1][2] - p_heightmap_vertices[l_face[1] - 1][2]) < 0.02) {        // # if same y's
                if (p_heightmap_vertices[l_face[0] - 1][0] < p_heightmap_vertices[l_face[1] - 1][0])     // # if p1 x < p2 x    then 
                    l_arr.push([ l_face[0], l_face[1] ])
                else    //# else store in any horizontal order
                    l_arr.push([ l_face[1], l_face[0] ])
                if (p_heightmap_vertices[l_face[0] - 1][2] > p_heightmap_vertices[l_face[2] - 1][2])     // # then append at top
                    l_arr.splice(0, 0, [ l_face[2] ])
                else       // # else append at bottom
                    l_arr.push([ l_face[2] ])
            } else if (abs(p_heightmap_vertices[l_face[0] - 1][2] - p_heightmap_vertices[l_face[2] - 1][2]) < 0.02) {
                if (p_heightmap_vertices[l_face[0] - 1][0] < p_heightmap_vertices[l_face[2] - 1][0])     // # if p1 x < p2 x    then 
                    l_arr.push([ l_face[0], l_face[2] ])
                else    // # else store in any horizontal order
                    l_arr.push([ l_face[2], l_face[0] ])
                if (p_heightmap_vertices[l_face[0] - 1][2] > p_heightmap_vertices[l_face[1] - 1][2])     // # then append at top
                    l_arr.splice(0, 0, [ l_face[1] ])
                else      // # else append at bottom
                    l_arr.push([ l_face[1] ])
            } else if (abs(p_heightmap_vertices[l_face[1] - 1][2] - p_heightmap_vertices[l_face[2] - 1][2]) < 0.02) {
                if (p_heightmap_vertices[l_face[1] - 1][0] < p_heightmap_vertices[l_face[2] - 1][0])     // # if p1 x < p2 x    then 
                    l_arr.push([ l_face[1], l_face[2] ])
                else    // # else store in any horizontal order
                    l_arr.push([ l_face[2], l_face[1] ])
                if (p_heightmap_vertices[l_face[1] - 1][2] > p_heightmap_vertices[l_face[0] - 1][2])     // # then append at top
                    l_arr.splice(0, 0, [ l_face[0] ])
                else       // # else append at bottom
                    l_arr.push([ l_face[0] ])
            } else
                console.log("HEIGHTMAP ERROR: NOT TRIANGULATED OBJECT OR OBJECT FACES NOT SYMETRICAL. ", p_heightmap_vertices[l_face[0] - 1][2], " : ", p_heightmap_vertices[l_face[1] - 1][2], " : ", p_heightmap_vertices[l_face[2] - 1][2])

            // # STORE AT APPROPRIATE LOCATION IN MEMEORY (&& merge in some instances):
            if (l_f == 0) {                                                      // # first element in heightmap array
                p_heightmap.push( [ l_arr ])    // # store in height_map[0]
            }
			else {   // # else if > or smaller than, store appropriately
                // # # thereafter
                if (p_heightmap_vertices[l_arr[0][0] - 1][2] < p_heightmap_vertices[p_heightmap[0][0][0][0] - 1][2] && abs(p_heightmap_vertices[l_arr[0][0] - 1][2] - p_heightmap_vertices[p_heightmap[0][0][0][0] - 1][2]) > 0.02)
                    p_heightmap.splice(0, 0, [ l_arr ]) //# then append at beginning of height_map array
                else if (p_heightmap_vertices[l_arr[0][0] - 1][2] > p_heightmap_vertices[p_heightmap[len(p_heightmap) - 1][0][0][0] - 1][2] && abs(p_heightmap_vertices[l_arr[0][0] - 1][2] - p_heightmap_vertices[p_heightmap[len(p_heightmap) - 1][0][0][0] - 1][2]) > 0.02)
                    p_heightmap.push([ l_arr ]) //# then append at end of height_map array
                else {
                    let j = 0
                    let found = 1
                    while (j < len(p_heightmap) && found) {                    // # search through heights in height_maps
                        if (abs(p_heightmap_vertices[l_arr[0][0] - 1][2] - p_heightmap_vertices[p_heightmap[j][0][0][0] - 1][2]) < 0.02) {   // # if same y value
                            // # p_heightmap[j].push(p_p_heightmap_vertices[len(p_p_heightmap_vertices) - 1])                   # then add vertice to y layer
                            // # print("SAME LINE REACHED: FOUND ", l_arr)
                            
                            // # loop through width wise
                            let l_i = 0
                            let l_found = 1
                            let p_heightmap_len = len(p_heightmap[j])
                            while (l_i < p_heightmap_len && l_found) {    // # loop through width
                                // # if 2 points match then check if diagnals cause square then merge vertices
                                // # else if no face fits && found correct storage position (then insert)

                                // # IF 2 VERTICES MATCH:
                                let l_count = 0
								let l_matrix;
                                if (abs(p_heightmap_vertices[p_heightmap[j][l_i][0][0] - 1][0] - p_heightmap_vertices[p_heightmap[j][l_i][1][0] - 1][0]) < 0.02) {
                                    l_matrix = [0, 0]
                                    if (p_heightmap[j][l_i][0][len(p_heightmap[j][l_i][0]) - 1] == l_face[0]) {
                                        l_matrix[0] = l_face[0]
                                        l_count += 1
									}
                                    else if (p_heightmap[j][l_i][0][len(p_heightmap[j][l_i][0]) - 1] == l_face[1]) {
                                        l_matrix[0] = l_face[1]
                                        l_count += 1
									}
                                    else if (p_heightmap[j][l_i][0][len(p_heightmap[j][l_i][0]) - 1] == l_face[2]) {
                                        l_matrix[0] = l_face[2]
                                        l_count += 1
									}

                                    if (p_heightmap[j][l_i][1][len(p_heightmap[j][l_i][1]) - 1] == l_face[0]) {
                                        l_matrix[1] = l_face[0]
                                        l_count += 1
									}
                                    else if (p_heightmap[j][l_i][1][len(p_heightmap[j][l_i][1]) - 1] == l_face[1]) {
                                        l_matrix[1] = l_face[1]
                                        l_count += 1
									}
                                    else if (p_heightmap[j][l_i][1][len(p_heightmap[j][l_i][1]) - 1] == l_face[2]) {
                                        l_matrix[1] = l_face[2]
                                        l_count += 1
									}
                                }
								if (l_count == 2) {    // # if 2 vertices match (&& on edge of 1 triangle) then select id of face && merge into this face
                                    l_face_len = len(l_face)
                                    l_id = -1
									for (let t_i = 0; t_i < l_face_len; t_i++)
                                        if (l_face[t_i] != l_matrix[0] && l_face[t_i] != l_matrix[1])
                                            l_id = l_face[t_i]
                                    // # use l_id && store at
                                    if (len(p_heightmap[j][l_i][0]) < 2) {         // # if not max len
                                        p_heightmap[j][l_i][0].push(l_id)
                                        l_found = 0
									}
                                    else if (len(p_heightmap[j][l_i][1]) < 2) {
                                        p_heightmap[j][l_i][1].push(l_id)
                                        l_found = 0
									}
                                }
								else { // # else check front end of triangle for connection
                                    l_count = 0
                                    if (abs(p_heightmap_vertices[p_heightmap[j][l_i][0][len(p_heightmap[j][l_i][0]) - 1] - 1][0] - p_heightmap_vertices[p_heightmap[j][l_i][1][len(p_heightmap[j][l_i][1]) - 1] - 1][0]) < 0.02) {
                                        l_matrix = [0, 0]
                                        if (p_heightmap[j][l_i][0][0] == l_face[0]) {
                                            l_matrix[0] = l_face[0]
                                            l_count += 1
										}
                                        else if (p_heightmap[j][l_i][0][0] == l_face[1]) {
                                            l_matrix[0] = l_face[1]
                                            l_count += 1
										}
                                        else if (p_heightmap[j][l_i][0][0] == l_face[2]) {
                                            l_matrix[0] = l_face[2]
                                            l_count += 1
										}
										
                                        if (p_heightmap[j][l_i][1][0] == l_face[0]) {
                                            l_matrix[1] = l_face[0]
                                            l_count += 1
										}
                                        else if (p_heightmap[j][l_i][1][0] == l_face[1]) {
                                            l_matrix[1] = l_face[1]
                                            l_count += 1
										}
                                        else if (p_heightmap[j][l_i][1][0] == l_face[2]) {
                                            l_matrix[1] = l_face[2]
                                            l_count += 1
										}
                                    }
									if (l_count == 2) {    // # if 2 vertices match (&& on edge of 1 triangle) then select id of face && merge into this face
                                        l_face_len = len(l_face)
                                        l_id = -1
										for (let t_i = 0; t_i < l_face_len; t_i++)
                                            if (l_face[t_i] != l_matrix[0] && l_face[t_i] != l_matrix[1])
                                                l_id = l_face[t_i]
                                        // # use l_id && store at
                                        if (len(p_heightmap[j][l_i][0]) < 2) {
                                            p_heightmap[j][l_i][0].push(l_id)
                                            l_found = 0
										}
                                        else if (len(p_heightmap[j][l_i][1]) < 2) {
                                            p_heightmap[j][l_i][1].push(l_id)
                                            l_found = 0
										}
									}
                                }
								l_i += 1
							}
                            if (l_found == 1) // # if not found: then add at end of p_heightmap[j].push()
                                p_heightmap[j].push( l_arr )
                            found = 0
                        }
						else if (p_heightmap_vertices[l_arr[0][0] - 1][2] - p_heightmap_vertices[p_heightmap[j][0][0][0] - 1][2] < 0) {          //# if y value now smaller then search through value
                            p_heightmap.splice(j, 0, [ l_arr ])                 // # insert new vector at location x in height_maps
                            found = 0
						}
                        j += 1
					}
				}
			}
            l_f += 1
		}
	}
    // <!-- print("HEIGHTMAP LOAD END REACHED!!") -->
}

function	terrain_bounds(p_xy_position, p_heightmap, p_heightmap_vertices) {
    // # IF WITHIN BASIC BOUNDS:
    // # print("YESS: ",  heightmap_vertices[p_heightmap[0][0][0][0] - 1][2], " : ", heightmap_vertices[p_heightmap[len(p_heightmap) - 1][0][1][0] - 1][2])

    if (p_xy_position[2] >= p_heightmap_vertices[p_heightmap[0][0][0][0] - 1][2] && p_xy_position[2] <= p_heightmap_vertices[p_heightmap[len(p_heightmap) - 1][0][1][0] - 1][2]) {
        //# THEN FIND WHICH BOUNDS ITS IN
        let l_x = -1
        let l_y = 0
        let l_h_leny = len(p_heightmap)
        //# LOOP THROUGH UNTIL FOUND ROW FOR DECENT Y VALUE RANGE
        while (l_y + 1 < l_h_leny && p_xy_position[2] > p_heightmap_vertices[p_heightmap[l_y + 1][0][1][0] - 1][2])       // # until Y value is within range of a quadrant
            l_y += 1
        //# LOOP THROUGH X AXIS AND COMPARE IF IS A SQUARE AND IF X POSITION WITHIN X RANGES:
        let l_h_lenx = len(p_heightmap[l_y])
        let l_found = 1
        let l_i = 0
        while (l_i < l_h_lenx && l_found) {
            //# CHECK IF SQUARE
            if (len(p_heightmap[l_y][l_i][0]) == 2 && len(p_heightmap[l_y][l_i][1]) == 2) {
                //# CHECK IF xposition WITHIN RANGE  THEN SET l_x to l_i && l_found = 0 
                if (p_xy_position[0] >= p_heightmap_vertices[p_heightmap[l_y][l_i][0][0] - 1][0] - 0.02 && p_xy_position[0] <= p_heightmap_vertices[p_heightmap[l_y][l_i][0][1] - 1][0] + 0.02) {
                    l_x = l_i
                    l_found = 0
				}
			}
            else {
                //# CHECK IF IS A TRIANGLE:
                if (len(p_heightmap[l_y][l_i][0]) == 2 && len(p_heightmap[l_y][l_i][1]) == 1) {
                    if (p_xy_position[0] >= p_heightmap_vertices[p_heightmap[l_y][l_i][0][0] - 1][0] - 0.02 && p_xy_position[0] <= p_heightmap_vertices[p_heightmap[l_y][l_i][0][1] - 1][0] + 0.02)
                        l_found = 0
                    //pass
				}
                else if (len(p_heightmap[l_y][l_i][0]) == 1 && len(p_heightmap[l_y][l_i][1]) == 2) {
                    if (p_xy_position[0] >= p_heightmap_vertices[p_heightmap[l_y][l_i][1][0] - 1][0] - 0.02 && p_xy_position[0] <= p_heightmap_vertices[p_heightmap[l_y][l_i][1][1] - 1][0] + 0.02)
                        l_found = 0
				}
                    //pass
                //pass
			}
            l_i += 1
		}
        if (l_found == 0)    // # if found could be triangle if either l_x == -1 or l_y == -1
            return [l_x, l_y]
        else if (l_x == -1 || l_y == -1)
            return (0)
        else
            return [l_x, l_y]
	}
    return (0)
}

function	point_terrain(p_xy_position, p_xy, p_heightmap, p_heightmap_vertices, p_line_xxy) {
    //# x plane 1:
    time_x = p_xy_position[0] / abs(p_heightmap_vertices[p_heightmap[p_xy[1]][p_xy[0]][0][0] - 1][0] + p_heightmap_vertices[p_heightmap[p_xy[1]][p_xy[0]][0][1] - 1][0])
    time = time_x

    p_line_xxy[0] = straight_line(p_heightmap_vertices[p_heightmap[p_xy[1]][p_xy[0]][0][0] - 1], p_heightmap_vertices[p_heightmap[p_xy[1]][p_xy[0]][0][1] - 1], time)

    //# # x plane 2:
    time_x = p_xy_position[0] / abs(p_heightmap_vertices[p_heightmap[p_xy[1]][p_xy[0]][1][0] - 1][0] + p_heightmap_vertices[p_heightmap[p_xy[1]][p_xy[0]][1][1] - 1][0])
    time = time_x

    p_line_xxy[1] = straight_line(p_heightmap_vertices[p_heightmap[p_xy[1]][p_xy[0]][1][0] - 1], p_heightmap_vertices[p_heightmap[p_xy[1]][p_xy[0]][1][1] - 1], time)

    //# # x plane 2:
    time_y = p_xy_position[2] / abs(p_line_xxy[0][2] + p_line_xxy[1][2])
    time = time_y

    p_line_xxy[2] = straight_line(p_line_xxy[0], p_line_xxy[1], time)

    //# DEBUG: PRINT OUTPUTS:
	// document.getElementById("stdout").innerHTML += "<br>" + p_line_xxy[0] 
	// document.getElementById("stdout").innerHTML += "<br>" + p_line_xxy[1]
	// document.getElementById("stdout").innerHTML += "<br>" + p_line_xxy[2]

    return p_line_xxy[2]
}

function	load_obj(p_obj_file, p_texture, p_shadow, p_material, p_wireframe, p_dontCullFaces) {
	let	l_location
	let	l_objstring = p_obj_file.replaceAll("\\n", '\n')//temp until fix the object file_data

	l_location = obj_loader.parse(String(l_objstring));

	let textureIter = 0;
	let c = 0;
	l_location.traverse( function ( child ) {
		if ( child instanceof THREE.Mesh ) {
			child.castShadow = p_shadow;
			child.receiveShadow = p_shadow;
			// // child.material = new THREE.MeshBasicMaterial();
			if (p_material){
				// https://stackoverflow.com/questions/16200082/assigning-materials-to-an-objloader-model-in-three-js
				child.material = p_material;
			}
			child.name = ++c;
			if (p_texture.length > 0)
				child.material.map = p_texture[textureIter];
			textureIter = (textureIter + 1) * (textureIter + 1 < p_texture.length);//iterate through
			// child.material.map = new THREE.MeshBasicMaterial();
			child.material.side = p_dontCullFaces ? THREE.DoubleSide : THREE.FrontSide;

			// child.material.transparent = true;
			// child.material.opacity = 0.5;
			// child.material.needsUpdate = true;

			child.material.wireframe = p_wireframe;
			// child.material.transparent = true;
			// // set opacity to 50%
			// child.material.opacity = 0.5; 

			//get vertices data:
			g_vertices = child.geometry.attributes.position.array;
			// console.log(child);
			// console.log("geo ", child.geometry.attributes.position.array);
			// console.log( (new THREE.Geometry().fromBufferGeometry( child.geometry )).geometry);
			// geometry = new THREE.Geometry().fromBufferGeometry( object.children["0"].geometry );
		}
	} );
	l_location.vertices = [1, 2, 3];

	// p_location.position.set(-5, 0, 4);
	// cuber.rotation.x = 90 * Math.PI / 180;
	// p_location.rotation.x = -90 * Math.PI / 180;
	// cuber.rotation.z = 90 * Math.PI / 180;

	// cuber.material.map = textures[0];
	// scene.add( p_location );
	return l_location;
}

function init(){
	// var dataurl = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2ZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYxIDY0LjE0MDk0OSwgMjAxMC8xMi8wNy0xMDo1NzowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpERUY4NEY4NjY2ODhFNDExODgxNkNGMzk4MzBFNzE1MCIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpFMzc0MTg1MTg4NjYxMUU0QUQ3REM0MEJFQThCMjdCOSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpFMzc0MTg1MDg4NjYxMUU0QUQ3REM0MEJFQThCMjdCOSIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M1LjEgV2luZG93cyI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkRGRjg0Rjg2NjY4OEU0MTE4ODE2Q0YzOTgzMEU3MTUwIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkRFRjg0Rjg2NjY4OEU0MTE4ODE2Q0YzOTgzMEU3MTUwIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+87tNGwAAByJJREFUeNpcl1tIl3cYx3/+/Vt5yDxUeGjaIJWyiAWtBVZSMYeIbEYMhK5GsbZBdbHbbbDLwViDLgbrorbb2C4kpEiYQ6OahVmzYWJaauWxg2nmYc/n6f/98+oLL+/7/n7P8fucfm/KgwcPQkpKSujt7Q2vX78Oqamp4fHjx2Fqaio8f/48rFixInR0dISGhoZgV1lfX9+Htl++atWq+MaNG/tzc3PbX7169c/CwsLMypUrw/T0dOjp6QmbN28ODx8+dFkFBQWhsLAwlJWVhbGxsdDf3x9GR0d9L47yxcVFV8wVj8f9nWcsFvOnXWlm4Lf37t07YUZmpaWluWGzs7OhpKQkbNiw4a7R/TIxMfGr0U4neJw/es/Pz7tsjOHKyckJTokRMPEUMYQJAzLT09PPGwoNKLX3oOvly5fBjArj4+OVFRUVP2dkZDROTk4eM76uubm5EL0MoWD7Ye3atf6+bdu28ObNm7cIsCAEUIqHbPL+6NGjHywcrhyIo8hYGISQw71u3boPsrKymox2r8nslzOSywUPiKMXPc6thSgx98zMzHsWr2NAh3I8kAG8c2MYTzzmtv2S4uLiH+39UFQeTvJED3SEz+nxHG8xQt4glDUjOm7ep65evTpkZ2e7IjwQjZQrLBjJZfQNJu+jFy9eNMsZoTo8POyJigHmYIgp8UgIWZoQmG2xrREzNNE76gU8KM/MzPRQwp+fn3/cFRg/hsqAJ0+eeAWYcZ6U7rKYgIfFBHGZZfU7KGMNYQjiQjHrFu+Ql5fnqMCDHGQkQrPXjCs28kElInvogV55F49mKgsIxxsjeNfeU1jjmxAoDBiDcgTRKyzzQxRqDDUDc0zk+3b/sTz+OKTcS5ahSlHfpjhVtQtcGAZ0GME6DYxv4olQVRHNzEoyNDY2UnIVFsYlpUgYvPwSZb8EARnANTQ0lHf//v1gXc7XYRYTXmIExrAuj3jHgMHBwTAwMIBh1WZEn/FdsO05hRhn8N6RkfcKgSeGKbLGU4eHEAta9vBYlQAy7JHAhIU9Ysw6xls11Fj7/c3kFSkEGA09MryfCHIZoNq1jJ6M9ghgZj7wbq3Xjbx165YrBFKMw/s1a9aEPXv2hOrqajfIsn4miizdExnkCXkUk4JoI+IqLy+/KkRgQphqHYG7d+/2dopCaBC2ZcuWUFNTE/bt2+ey4DNPRw3qMSm1JuXes/f06dMQR7AaiBKR2GzduvWCGfG9teIcDKHG8VZzAsj3798frFQ9TzCgtLTUyzKRxL5udMyFKTUvZgEIgii648RYDNEQmLLhHTt2tJqH9SjHapRSdsRw/fr1nhcgyB43/BjPGqiwZvLbNJi0JzTcIRFjjQaT+rcJ6In2cxTDpEGE9+qGoIhQGcGNtyb3L4149QrlHLpjOgsoDOpm3Bavy8Y8p1GKIFCgn1uZJrsi+xixvJc8e/bsksW5Qx0UGhI2WnkxWaIOFbXOxuslC8F/Gr+UJeugYLnhHZDYG11SgXv1FrVxa1afm4HJgwHyCaEaW7IRwSRPQEBNwt4XTUG7eVKZGLXOpO4HD4mmASbjE6GcMJqhaGUhl8yXQ6Aa4wX4lx9M9LSM/d3WF1CoNiyhCKCslJBKtATEscS1pNOS0FGdMZoHtypBDGo+Bu/fNlpbgA7h1D3MjFVqWXGFV6M5MROyjSc3mnQKsw4yfrgRNLJ++SnGCBetJ5wxgYvkgE4+XMBPPElMbt4JD+8mPN9y5IzJSFXvQCFIQUdXdT2Kt4RKcTJL7dtQaLG1EZKOG8Q4JZEDTMXu7m4/1qssMcyO62HTpk0f37lz5ycAVdxBD+TkcDIJ+dDA0RgW09mzZ7+z6ZbPcRrhGKDpqORUs5IcKgN++0f4yhI59fDhw19Ah5HqOy5fI1Iwqh8ghMxubm7+sr29/RRtliTk9Cv4iaViqvzBGX5A+Dbvfa2zs/O45UNpVVXVn9acrloe/Wt6FnyuSBnClSQkiMWpwDw/df369a+xnJhZMvrfDBCyBhIqJ7okPaGystK/iTN/VMovC1Ht7du3a82gWaucThta53ft2tWc0tXV5Z6qLKzmw8WLFz87d+7cNwZXiRDBsLq6OlfIrEcBhhYVFbkDNrhCfX29OwIPo/rkyZMeFng1A8gRhddou+MqPyNIMXj3nj59+kRbW9snMMgoGUCiHTlyJOzcudNjefPmTV/n96y2tnbJX1NTU5OXJzKQFR37aveGajrjuNBgPtTS0vLptWvXqoBa8dS/nBoTP5XATjJahvvN0Wv79u2ehJqAyLhx40ayN8gJNTr1DkNzPn706NHLIyMjlfox0YiNtk9ZTewpOfsrTp6eUXzlypVw8OBBRwIZIMVfME6gTL1FsiKzISNuyseNoHe5QnUwwac9YD9w4IDXP4iggFbMQZTDCIna2trq6+SITszqLzoTJPQM/C/AACIm/vZ9H964AAAAAElFTkSuQmCC";

	var imageElement = document.createElement('img');

	imageElement.src = g_images[0][2]//dataurl;

	// initialise images array:
	for (let i = 0; i < g_images.length; i++) {
		images[i] = [];
		for (let j = 0; j < g_images[i].length; j++) {
			images[i][j]= document.createElement('img');
			images[i][j].src = g_images[i][j]//dataurl;
			images[i][j].onload = function(e) {
				let	m = 0;
				images_loaded += 1;
				if (images_loaded === no_of_images) {	// IF ALL IMAGES ARE LOADED:
					for (let k = 0; k < g_images.length; k++) {
						let n = 0;
						textures[m] = [];
						for (let l = 0; l < g_images[k].length; l++) {
							textures[m][n] = new THREE.Texture( images[k][l] );
							textures[m][n].needsUpdate = true;
							n += 1;
						}
						m += 1;
					}
					initialize();
				}
			};
			no_of_images += 1;
		}
	}

	imageElement.onload = function(e) {
		let	m = 0;
		images_loaded += 1;
		if (images_loaded === no_of_images) {	// IF ALL IMAGES ARE LOADED:
			for (let k = 0; k < g_images.length; k++) {
				let n = 0;
				textures[m] = [];
				for (let l = 0; l < g_images[k].length; l++) {
					textures[m][n] = new THREE.Texture( images[k][l] );
					textures[m][n].needsUpdate = true;
					n += 1;
				}
				m += 1;
			}
			initialize();
		}
	};
}

function	initialize() {	
	cssScene = new THREE.Scene();
	// document.addEventListener('DOMContentLoaded', () => {
	// 	// let map = document.querySelector('#map');
	// 	// if (map) {
	// 	//   var mapObj = new THREE.CSS3DObject(map[0]);
	// 	//   mapObj.position.set(0,0,0);
	// 	//   mapObj.rotation.x = -Math.PI/2;
	// 	//   cssScene.add(mapObj);
	// 	// }
	//   });

	scene = new THREE.Scene();
	// scene.fog = new THREE.FogExp2( 0xffffff/*0xefd1b5*/, 0.08 );

	// lumoScene = new THREE.Scene();

	camera = new THREE.PerspectiveCamera(90, 1280/720, 0.1, 1000);

	// console.log("CAMERA_FOV" + camera.fov); //default is 90
	// camera.fov = 120;
	// camera.updateProjectionMatrix();

	selectableObjects.push(new object_("mesh", 0))
	mesh = selectableObjects[selectableObjects.length - 1];
	selectableObjects[selectableObjects.length - 1].object = new THREE.Mesh(
		new THREE.BoxGeometry(1,1,1),
		new THREE.MeshPhongMaterial({color:0x444444, wireframe:USE_WIREFRAME})
	);
	// mesh.position.y += 1;
	mesh.object.name = mesh.name;
	mesh.object.receiveShadow = true;
	mesh.object.castShadow = true;
	mesh.object.position.set(2.0, 2.3, 10);
	scene.add(mesh.object);


	selectableObjects.push(new object_("sphere", 0))
	sphere = selectableObjects[selectableObjects.length - 1];
	sphere.object = new THREE.Mesh( 
		new THREE.SphereGeometry( 0.2, 32, 16 ),//SphereGeometry(radius : Float, widthSegments : Integer, heightSegments : Integer, phiStart : Float, phiLength : Float, thetaStart : Float, thetaLength : Float)
		new THREE.MeshBasicMaterial( { color: 0xffff00 } )
	);
	sphere.object.position.x += 2;
	g_center = sphere.object;
	scene.add( sphere.object );

	// // Create a plane geometry for the waterfall
	// const waterfallGeometry = new THREE.PlaneGeometry(5, 5);
	// // Create a shader material for the waterfall
	// const waterfallMaterial = new THREE.ShaderMaterial({
	// vertexShader: `// Vertex shader

	// void main() {
	//   // Transform the vertex position
	//   gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
	// }
	// `,//document.getElementById('vertexShader').textContent,
	// fragmentShader: `// Waterfall fragment shader

	// uniform float time; // You can pass this uniform to control the animation over time
	// uniform vec2 resolution; // Pass the resolution as a uniform
	
	// void main() {
	//   // Define the coordinates of the fragment
	//   vec2 uv = gl_FragCoord.xy / resolution.xy;
	  
	//   // Create a noise effect using the fragment coordinates and time
	//   float noise = fract(sin(dot(uv + time, vec2(12.9898, 78.233))) * 43758.5453);
	  
	//   // Set the color of the waterfall
	//   vec3 color = vec3(0.0, 0.0, noise);
	  
	//   gl_FragColor = vec4(color, 1.0);
	// }
	// `,//document.getElementById('waterfallFragmentShader').textContent,
	// uniforms: {
	// 	time: 0.0// Define any required uniforms for the shader
	// 	// For example, you might want to pass the time or any other parameters
	// },
	// });
	// // Create the waterfall mesh
	// const waterfallMesh = new THREE.Mesh(waterfallGeometry, waterfallMaterial);

	// // Create a plane geometry for the river
	// const riverGeometry = new THREE.PlaneGeometry(2, 2, 2);
	// // Create a basic material for the river
	// const riverMaterial = new THREE.MeshBasicMaterial({ color: 0x0077ff });
	// // Create the river mesh
	// const riverMesh = new THREE.Mesh(riverGeometry, riverMaterial);

	// // Create a glow material for the objects
	// const glowMaterial = new THREE.GlowMaterial({
	// 	color: 0x00ff00, // Set the desired glow color
	// 	coefficient: 0.5, // Adjust the glow intensity
	// });
	
	// // Apply the glow material to the waterfall and river meshes
	// waterfallMesh.material = glowMaterial;
	// riverMesh.material = glowMaterial;
  
	// scene.add(waterfallMesh);	
	// scene.add(riverMesh);	

		
	ambientLight = new THREE.AmbientLight(0xffffff, 0.2);
	scene.add(ambientLight);
	
	light = new THREE.PointLight(0xffffff, 0.8, 18);
	light.position.set(-3,6,-3);
	light.castShadow = true;
	light.shadow.camera.near = 0.1;
	light.shadow.camera.far = 25;
	scene.add(light);


	ambientLight = new THREE.AmbientLight(0xffffff, 0.2);
	scene.add(ambientLight);

	light = new THREE.PointLight(0xffFFff, 1.0, 18);
	light.position.set(40,6,-3);
	light.castShadow = true;
	light.shadow.camera.near = 0.1;
	light.shadow.camera.far = 25;
	scene.add(light);


	selectableObjects.push(new object_("crate", 0))
	crate = selectableObjects[selectableObjects.length - 1];
	crate.object = new THREE.Mesh(
		// new THREE.BoxGeometry(3,3,3),
		new THREE.PlaneGeometry(10, 5),
		new THREE.MeshPhongMaterial({
			color:0xffffff,
			// map: new THREE.MeshBasicMaterial(),
			map: textures[3][0],
			// side: THREE.DoubleSide,
			// map:crateTexture,
			// bumpMap:crateBumpMap,
			// normalMap:crateNormalMap
		})
	);
	scene.add(crate.object);
	crate.object.name = crate.name;
	crate.object.position.set(10, 3/2, 10);
	crate.object.scale.set(0.5, 0.5, 0.5);
	crate.object.receiveShadow = true;
	crate.object.castShadow = true;

	selectableObjects.push(new object_("crate-2", 0))
	crate2 = selectableObjects[selectableObjects.length - 1];
	crate2.object = new THREE.Mesh(
		// new THREE.BoxGeometry(3,3,3),
		new THREE.PlaneGeometry(10, 5),
		new THREE.MeshPhongMaterial({
			color:0xffffff,
			// map: new THREE.MeshBasicMaterial(),
			map: textures[3][0],
			// map:crateTexture,
			// bumpMap:crateBumpMap,
			// normalMap:crateNormalMap
		})
	);
	scene.add(crate2.object);
	crate2.object.name = crate2.name;
	crate2.object.rotation.y = Math.PI;//180 degrees opposite to crate
	crate2.object.position.set(10, 3/2, 10);
	crate2.object.scale.set(0.5, 0.5, 0.5);
	crate2.object.receiveShadow = true;
	crate2.object.castShadow = true;

// obj string below represents a cube


	// objString = g_objects.droid[1][1].replaceAll("\\n", '\n')
// console.log( objString );

	obj_loader = new THREE.OBJLoader();

	//LOAD ANIMATION OBJECTS:
	objects.droid = [];
	objects.droid[0] = [];//droid.standing
	objects.droid[1] = [];//droid.jogging
	objects.droid[2] = [];//droid.butterfly.twirl
	objects.droid[3] = [];//droid.jumping.over
	objects.droid[4] = [];//droid.T default object
	objects.droid[5] = [];//droid.T default object

	for (let i = 0; i < g_objects.droid.length; i++)
	{
		for (let j = 0; j < g_objects.droid[i].length; j++)
		{
			objects.droid[i][j] = load_obj(g_objects.droid[i][j], [ textures[0][5] ], true);	//load object	1
			objects.droid[i][j].rotation.x = -90 * Math.PI / 180;						//tranform object to correct direction
			// scene.add( objects.droid[i][j] );
		}
	}

	// objects.droid[4][0]
	// scene.add( objects.droid[4][0] );		//add object to the scene

	// LOAD PLATFORM PIECE:
	objects.platform = [];
	objects.platform[0] = load_obj(g_objects.platform, [ textures[1][6]/*textures[0][5]*/ ], true, new THREE.MeshBasicMaterial( { color: 0xFFFFFF } ));	//load object	1
	objects.platform[0].position.set(0, 3, 11);
	objects.platform[0].rotation.set(180 * Math.PI / 180, 0 * Math.PI / 180, 45 * Math.PI / 180);
	scene.add( objects.platform[0] );		//add object to the scene

	// objects.platform[0].rhs = [];
	// objects.platform[0].rhs[0] = load_obj(g_objects.platform, [ textures[1][5] ], true);	//load object	1
	// objects.platform[0].rhs[0].position.set(0, 3, 11)
	// objects.platform[0].rhs[0].rotation.set(180 * Math.PI / 180, 0 * Math.PI / 180, 45 * Math.PI / 180)
	//scene.add( objects.platform[0].rhs[0] );		//add object to the scene

	// 3D TRIANGLE COLLISION SETUP: 

	// console.log("TEST PRINT VERTICES: ");
	// console.log("vertices: ", g_vertices);
		
	objects.platform[0].triangles = [
		[ [g_vertices[0], g_vertices[1], g_vertices[2]], [g_vertices[3], g_vertices[4], g_vertices[5]], [g_vertices[6], g_vertices[7], g_vertices[8]] ],
		[ [g_vertices[9], g_vertices[10], g_vertices[11]], [g_vertices[12], g_vertices[13], g_vertices[14]], [g_vertices[15], g_vertices[16], g_vertices[17]] ]
	];

	// let l_translate = [ objects.platform[0].position.x, objects.platform[0].position.y , objects.platform[0].position.z ]
	// let	l_rotate = [0, 0, 0];
	// let l_tp = [ l_translate[0] + l_rotate[0], l_translate[1] + l_rotate[1], l_translate[2] + l_rotate[2]];
	//console.log("l translate position: ", l_tp);
	// collision_triangles_translation = [];	
	// for (let i = 0; i < objects.platform[0].triangles.length; i++) {
	// 	collision_triangles_translation[i] = [];
	// 	for (let j = 0; j < objects.platform[0].triangles[i].length; j++) {
	// 		collision_triangles_translation[i][j] = [];
	// 		//translation and rotation formulas here:
	// 		let l_ct = [ objects.platform[0].triangles[i][j][0], objects.platform[0].triangles[i][j][1], objects.platform[0].triangles[i][j][2] ]

	// 		rotatexyz(l_ct, [0, 0, 0], objects.platform[0].rotation.x, objects.platform[0].rotation.y, objects.platform[0].rotation.z )

	// 		for (let k = 0; k < objects.platform[0].triangles[i][j].length; k++) {

	// 			collision_triangles_translation[i][j][k] = l_translate[k] + l_ct[k];
	// 			//console.log("inner ", collision_triangles[i][j]);
	// 		}
	// 	}
	// }
	// //REMOVE:
	// 	scene.add( objects.platform[0].rhs[0] );
	// 	objects.platform[0].rhs[0].matrixAutoUpdate = false;
	
	// console.log("objects.platform[0]")
	// console.log(objects.platform[0].triangles);

	// console.log("inner ", collision_triangles_translation);
	
	//screen_text[0][0].object.position.x

	// objects.platform[0].rhs[1] = objects.platform[0].clone(true)
	// objects.platform[0].rhs[1].rotation.set(270 * Math.PI / 180, 0, (Math.random() * 10) * Math.PI / 180)
	// objects.platform[0].rhs[1].position.set(0, 0, 17)
	// scene.add( objects.platform[0].rhs[1] );		//add object to the scene

	// objects.platform[0].rhs[2] = objects.platform[0].clone(true)
	// objects.platform[0].rhs[2].rotation.set(270 * Math.PI / 180, 0, (Math.random() * 10) * Math.PI / 180)
	// objects.platform[0].rhs[2].position.set(0, 0, 20)
	// scene.add( objects.platform[0].rhs[2] );		//add object to the scene

	// objects.platform[0].rhs[3] = objects.platform[0].clone(true)
	// objects.platform[0].rhs[3].rotation.set(270 * Math.PI / 180, 0, (Math.random() * 10) * Math.PI / 180)
	// objects.platform[0].rhs[3].position.set(0, 0, 23)
	// scene.add( objects.platform[0].rhs[3] );		//add object to the scene



	//LOAD FLOORS
	objects.floor = [];
	objects.floor[0] = [];
	for (let i = 0; i < g_objects.floor.length; i++)
	{
		for (let j = 0; j < g_objects.floor[i].length; j++)
		{
			objects.floor[i][j] = load_obj(g_objects.floor[i][j], [ /*textures[2][1] textures[1][5] */ ], true,  new THREE.MeshBasicMaterial({ color: '#000000' }) );	//load object	3
			// objects.droid[i][j].rotation.x = -90 * Math.PI / 180;						//tranform object to correct direction
			// scene.add( objects.droid[i][j] );
		}
	}
	// objects.floor[0][0].position.set(-25, 0, 50);
	// objects.floor[0][0].position.set(0, 0, 0);
	objects.floor[0][0].position.set(100, 0, -100);
	scene.add( objects.floor[0][0] );

	// HEIGHTMAPS:
	// heightmap_load(g_objects.floor[0][0], height_map, heightmap_vertices)

	// objects.floor[0][0].rhs = [];
	// objects.floor[0][0].rhs[0] = objects.floor[0][0].clone(true);
	// objects.floor[0][0].position.set(25, 0, 0);
	// scene.add( objects.floor[0][0].rhs[0] );
	
	//LOAD trees:
	objects.tree = [];
	objects.tree[0] = [];
	
	objects.tree[0][0] = load_obj(g_objects.tree[0], [ textures[1][6]/*textures[1][5]*/ ], true, false, false, true);	//load object	7

	objects.tree[0][0].position.set(5, -3, 17);
	objects.tree[0][0].scale.x = 0.8;
	objects.tree[0][0].scale.y = 0.8;
	objects.tree[0][0].scale.z = 0.8;
	// objects.tree[0][0].scale.x = 0.5;
	// objects.tree[0][0].scale.y = 0.5;
	// objects.tree[0][0].scale.z = 0.5;

	scene.add( objects.tree[0][0] );
	objects.tree[0][0].rhs = [];
	objects.tree[0][0].rhs[0] = objects.tree[0][0].clone(true);
	objects.tree[0][0].rhs[0].position.set(-5, -3, 17);
	scene.add( objects.tree[0][0].rhs[0] );

	//LOAD BUILDINGS:
	objects.buildings = [];
	objects.buildings[0] = [];
	// console.log("texturesss: ", textures)
	let mallPillars;
	for (let i = 0; i < g_objects.buildings.length; i++)
	{
		let t = [];
		for (let k = 0; k < 151; k++)
			t.push(textures[1][3]);
		objects.buildings[0][i] = load_obj(g_objects.buildings[i], t/*[textures[1][3]]*//* [1][3] */, true, false, false, true);	//load object	3
		// textures[1][3]		// nice
	}
	objects.buildings[0][0].traverse( function ( child ) {
		if ( child instanceof THREE.Mesh && (child.name === 67 || child.name === 50 || child.name === 53 || child.name === 52) ) {
			child.material = new THREE.MeshBasicMaterial({ color: '#000000' });
			// child.material.map = textures[2][1];
		}
		if ( child.name === 47 || child.name === 3 ) {//above bottom pillars
			child.material = new THREE.MeshBasicMaterial({ color: '#0A0A0A' })
		}
		if ( child.name === 2 || child.name === 24 ) {//castle floors
			child.material = new THREE.MeshBasicMaterial({ color: '#0A0A0A' })
		}
		if ( child.name === 5 || child.name === 48 ) {//bottom pillars
			child.material = new THREE.MeshBasicMaterial({ color: '#00FFFF' })
		}
		if (child.name === 54) {//castle large corner dome pillars
			child.material = new THREE.MeshBasicMaterial({ color: '#000000' })
		}
		if ( child.name === 68 || child.name === 61 ) {//roof edges
			child.material = new THREE.MeshBasicMaterial({ color: '#00FFFF' })
		}
		if ( child.name === 9) {//top pillars
			child.material = new THREE.MeshBasicMaterial({ color: '#00FFFF' })
		}
	});
	// let obbj = objects.buildings[0][0].getObjectByName(67);
	// obbj.material.map = textures[2][0];
	objects.buildings[0][0].position.set(0, 0, 0);
	objects.buildings[0][0].scale.set(30.0, 30.0, 30.0);
	// console.log(objects.buildings[0][0]);
	scene.add(objects.buildings[0][0])

	// objects.buildings[0][1].position.set(28.0, 8.0, 0.0);
	// objects.buildings[0][1].rotation.set(0, 145 * Math.PI / 180, 0);
	// objects.buildings[0][1].scale.set(0.1, 0.1, 0.1);
	// scene.add(objects.buildings[0][1])


	//LOAD MALL COMPONENTS:
	objects.malls = [];
	objects.malls[0] = [];
	objects.malls[0][0] = load_obj(g_objects.mall[0], []/*[textures[1][3]]*//* [1][3] */, true, new THREE.MeshBasicMaterial({ color: '#00FFFF' }), false, false);	//load object	3
	objects.malls[0][1] = load_obj(g_objects.mall[1], []/*[textures[1][3]]*//* [1][3] */, true, new THREE.MeshBasicMaterial({ color: '#FFFFFF', opacity: 0.2, transparent: true }), false, false);	//load object	3
	objects.malls[0][2] = load_obj(g_objects.mall[2], [textures[1][3]]/*[textures[1][3]]*//* [1][3] */, true, new THREE.MeshBasicMaterial({ color: '#212121'}), false, true);	//load object	3

	objects.malls[0][0].scale.set(30.0, 30.0, 30.0);
	objects.malls[0][1].scale.set(30.0, 30.0, 30.0);
	objects.malls[0][2].scale.set(30.0, 30.0, 30.0);
	scene.add(objects.malls[0][0]);
	// scene.add(objects.malls[0][1]);
	scene.add(objects.malls[0][2]);


	// background for water:
	let groundGeometry = new THREE.PlaneGeometry( 1.95, 20, 50, 1 );
	let groundMaterial = new THREE.MeshStandardMaterial( { roughness: 0.8, metalness: 0.4, color: '#FFFFFF' } );
	let ground = new THREE.Mesh( groundGeometry, groundMaterial );
	ground.position.x = 9;
	ground.position.z = 18.01;
	ground.rotation.y = Math.PI;
	ground.rotation.x = 5 * Math.PI / 180;
	scene.add( ground );

	// add water:
	let params = {
		color: '#00FFFF',
		scale: 0.0,//50.0, // the commented values make a mirror
		flowSpeed: 0.5,//0.5,//0.0,
		flowX: 0,
		flowY: 1
	};
	const waterGeometry = new THREE.PlaneGeometry( 2, 20, 50, 1 );

	// include the 
	// const shaderCode = document.getElementById('colorspace_fragment').textContent;
	// THREE.ShaderChunk['colorspace_fragment'] = shaderCode;

	let water = new Water( waterGeometry, {
		color: params.color,
		scale: params.scale,
		flowDirection: new THREE.Vector2( params.flowX, params.flowY ),
		flowSpeed: params.flowSpeed,
		textureWidth: 1024,
		textureHeight: 1024
	} );

	// water.position.y = -2;
	water.position.x = 9;
	water.position.z = 18;
	water.rotation.y = Math.PI;
	water.rotation.x = 5 * Math.PI / 180;
	// water.rotation.x = Math.PI * - 0.5;
	scene.add( water );

	//2nd water element
	let ground2 = new THREE.Mesh( groundGeometry, groundMaterial );
	ground2.position.x = -8.5;
	ground2.position.z = 18.01;
	ground2.rotation.y = Math.PI;
	ground2.rotation.x = 5 * Math.PI / 180;
	scene.add( ground2 );

	let water2 = new Water( waterGeometry, {
		color: params.color,
		scale: params.scale,
		flowDirection: new THREE.Vector2( params.flowX, params.flowY ),
		flowSpeed: params.flowSpeed,
		textureWidth: 1024,
		textureHeight: 1024
	} );

	// water.position.y = -2;
	water2.position.x = -8.5;
	water2.position.z = 18;
	water2.rotation.y = Math.PI;
	water2.rotation.x = 5 * Math.PI / 180;
	scene.add( water2 );


	// vehicles/mitsubishi:
	const loader = new GLTFLoader();
	// loader.load( '../resources/gltf/mitsubishi.glb', function ( gltf ) {
	loader.load( '../resources/gltf/mitsubishi.texturing.glb', function ( gltf ) {
		// loader.load( '../resources/gltf/mitsubishi.texturing.gltf', function ( gltf ) {

		mitsubishi = gltf.scene;
		mitsubishi.scale.set(0.7, 0.7, 0.7);
		mitsubishi.position.z = 5.0;
		scene.add( mitsubishi );

		let partCount = 0;
		mitsubishi.traverse( function ( object ) {

			if ( object.isMesh ) {
				object.castShadow = true;
				object.name = ++partCount;
				// console.log("multi parts separated?" + partCount)
				if (partCount === 3) {// mitsubishi car body
					object.material = new THREE.MeshBasicMaterial({map: textures[4][0]})
					// console.log();
					// var exporter = new OBJExporter();
					// mitsubishiBody = load_obj(exporter.parse(object), [textures[1][3]]/*[textures[1][3]]*//* [1][3] */, true, false, false, true);	//load object	3
					// object = mitsubishiBody;
					// vrControllerSelectableObjectsgroup.add( mitsubishiBody );// TODO: is not currently in an object_ instance in the selectableObjects list

					// object.material.map = textures[1][3];
					// mitsubishi = object;

					// const texture = object.material.map;
					// console.log('Current encoding:', texture.encoding);
			
					// Change the encoding if needed
					// if (texture.encoding !== THREE.sRGBEncoding) {
					//   texture.encoding = THREE.sRGBEncoding;
					//   texture.needsUpdate = true;
					// }
				}
			}

		} );
	});



	//LOAD ASCII CHARECTERS
	objects.ascii = [];
	for (let i = 0; i < g_objects.ascii.length; i++)
	{
		objects.ascii[i] = load_obj(g_objects.ascii[i], [ textures[0][0] ], true, false, false, true);
		// function	load_obj(p_obj_file, [ p_texture ], p_shadow, p_material, p_wireframe, p_cullFaces) {

		objects.ascii[i].rotation.x = -90 * Math.PI / 180;
		objects.ascii[i].rotation.z = 180 * Math.PI / 180;
	}

		//SET AND RENDER SCREEN_TEXT OBJECT:
	ascii.construct[0]();
	
	//LOAD TEST OBJECT:
	// scene.remove( cuber );
	cuber = objects.droid[1][0];
	cuber.position.set(-5, 0, 4);
	scene.add( cuber );

	//CREATE CAMERA_POINTER OBJECT:
	camera_pointer = load_obj(g_objects.cube, [ textures[0][0] ], true);
	// scene.add( camera_pointer );
	
	camera.position.set(0 - 2, player.height, -5);
	camera.lookAt(new THREE.Vector3(0,player.height,0));
	camera.rotation.x = 270;	// for looking up and down

	
	renderer = new THREE.WebGLRenderer({
		alpha: true,
		antialias: true,// TODO: Testing with logarithmDepthBuffer
		// logarithmicDepthBuffer: true// TODO: testing this for precision rendering of close objects
		preserveDrawingBuffer: true // html2canvas seems to want this
	});

	// renderer.setSize(1280, 720);
	renderer.setPixelRatio(window.devicePixelRatio);
	renderer.setSize(window.innerWidth, window.innerHeight);

	initXR();

	renderer.shadowMap.enabled = true;
	renderer.shadowMap.type = THREE.BasicShadowMap;
	renderer.domElement.style.position = 'absolute';
	renderer.domElement.style.top = 0;

	// renderer.domElement.addEventListener('keydown', keyDown);
	// renderer.domElement.addEventListener('keyup', keyUp);
	// // window.addEventListener('keyup', keyUp);

	renderer.domElement.addEventListener('mousedown', initMove);
	renderer.domElement.addEventListener('mousemove', move);
	renderer.domElement.addEventListener('mouseup', stopMove);

	renderer.domElement.addEventListener('touchstart', touchStart);
	renderer.domElement.addEventListener('touchmove', touchMove);
	renderer.domElement.addEventListener('touchend', touchEnd);

	// renderer.domElement.style.zIndex = "-1";
	document.querySelector("#container").appendChild(renderer.domElement);

	cssRenderer = new THREE.CSS3DRenderer(cssSceneElem[0], cssCameraElem);//THREE
	cssRenderer.setSize(window.innerWidth, window.innerHeight);
	cssRenderer.domElement.style.position = 'absolute';
	cssRenderer.domElement.style.top = 0;

	document.querySelector("#container").appendChild(cssRenderer.domElement);

	// const container = document.getElementById( 'container' );
	// document.body.appendChild(cssRenderer.domElement);

	// container.appendChild( cssRenderer.domElement );
	// document.body.appendChild(cssRenderer.domElement);

	cssGroup = new object_("", 0);
	cssGroup.name = "my first css3d";
	// fileObject.fileName = file.name;
	cssGroup.angle = [0.0, 0.0, 0.0];
	// mesh.position.set(0, 2.3, 10);
	cssGroup.position = [0, 2.3, 10];//translate position meta-data
	// fileObject.limits = [[0, 360], [0, 360], [0, 360]];
	// fileObject.rhs = [];
	// fileObject.lhs = null;
	cssGroup.center = [0.0, 0.0, 0.0];
	cssGroup.centerLength = 0;
	cssGroup.deltaAngle = [0.0, 0.0, 0.0]; // for lhs node combined calculations
	cssGroup.deltaCenter = [0.0, 0.0, 0.0]; // for lhs node combined calculations

	cssGroup.object = new THREE.Group();
	cssGroup.object.add( new cssElement( 'SJOz3qjfQXU', 0, 0, 240, 0 ) );
	// cssGroup.object.add( new cssElement( 'Y2-xZ-1HE-Q', 240, 0, 0, Math.PI / 2 ) );

	// group.add( new cssElement( 'IrydklNpcFI', 0, 0, - 240, Math.PI ) );
	// group.add( new cssElement( '9ubytEsCaS0', - 240, 0, 0, - Math.PI / 2 ) );
	// group.position.set( cssGroup.position[0], cssGroup.position[1], cssGroup.position[2]/ );// need to fix to work with css.
	// group.rotation.y = 50.0;
	// cssGroup.object = new cssElement( 'SJOz3qjfQXU', 0, 0, 240, 0 );
	cssScene.add( cssGroup.object );

	cssDiv = new object_("", 0);
	cssDiv.name = "my second css3d";
	cssDiv.angle = [0.0, 0.0, 0.0];
	cssDiv.position = [1.0, 2.3, 10];//translate position meta-data
	cssDiv.object = new THREE.Group();
	cssDiv.object.add( new cssDivElement( 'SJOz3qjfQXV', 0, 0, 240, 0 ) );
	cssDiv.object.meta = cssDiv;
	cssScene.add( cssDiv.object );


	// cssScene.add(new cssElement( 'SJOz3qjfQXU', 0, 0, 240, 0 ));

	window.addEventListener('mousedown', initMove);
	window.addEventListener('mousemove', move);
	window.addEventListener('mouseup', stopMove);

	cssRenderer.domElement.addEventListener('touchstart', touchStart);
	cssRenderer.domElement.addEventListener('touchmove', touchMove);
	cssRenderer.domElement.addEventListener('touchend', touchEnd);
	
	this.renderer.setAnimationLoop(	animate.bind(this) );

	// VRButton.button.addEventListener('sessionstart', function (event) {
	// 	const session = event.detail.session;
	// 	console.log("REACHED SESSION START");
	// 	session.addEventListener('frame', function (event) {
	// 	console.log("REACHED SESSION FRAME");
	// 	const frame = event.frame;
	// 	  xrRefSpace = session.requestReferenceSpace('local-floor');
	// 	  frame.session.requestAnimationFrame(onSessionFrame);
	// 	});
	// });
	// animate();

	//GLOWING OBJECTS RENDERER:
		// Create a render target for the glowing objects
	// const renderTargetParameters = {
	// 	minFilter: THREE.LinearFilter,
	// 	magFilter: THREE.LinearFilter,
	// 	format: THREE.RGBAFormat,
	// 	stencilBuffer: false
	// };
	// renderTarget = new THREE.WebGLRenderTarget(window.innerWidth, window.innerHeight, renderTargetParameters);
	
	// // Create a render pass for the scene
	// const renderPass = new RenderPass(scene, camera);
	
	// // Create a luminosity high pass shader pass
	// const highPassShader = LuminosityHighPassShader;
	// const highPassUniforms = THREE.UniformsUtils.clone(highPassShader.uniforms);
	// highPassUniforms.luminosityThreshold.value = 0.6; // Adjust the threshold to control the glow intensity
	// const highPassShaderPass = new ShaderPass(highPassShader, 'luminosityHighPass');
	// highPassShaderPass.uniforms = highPassUniforms;
	
	// // Create a copy shader pass to combine the glow with the scene
	// const copyShaderPass = new ShaderPass(CopyShader);
	// copyShaderPass.renderToScreen = true;
	
	// // Create an effect composer and add the passes
	// composer = new EffectComposer(renderer);
	// composer.addPass(renderPass);
	// composer.addPass(highPassShaderPass);
	// composer.addPass(copyShaderPass);

	// Create a cube geometry
	// const cubeGeometry = new THREE.BoxGeometry(1, 1, 1);
	// // Create a basic material with emissive color
	// const cubeMaterial = new THREE.MeshBasicMaterial({
	// 	color: 0xFFFFCC, // Set your desired color
	// 	// emissive: 0xffff00, // Set the emissive color to match the desired glow color
	// 	// emissiveIntensity: -1.0, // Adjust the intensity of the emissive glow
	// 	// transparent: true,
	// 	// opacity: 0.5
	// 	transparent: true,
	// 	opacity: 0.5
	// });
	// const cubeMaterialShadow = new THREE.MeshBasicMaterial({
	// 	color: 0xffff55, // Set your desired color
	// 	// emissive: 0xffff00, // Set the emissive color to match the desired glow color
	// 	// emissiveIntensity: -1.0, // Adjust the intensity of the emissive glow
	// 	transparent: true,
	// 	opacity: 0.2
	// });
	// const cubeMaterialShadow2 = new THREE.MeshBasicMaterial({
	// 	color: 0xffff55, // Set your desired color
	// 	// emissive: 0xffff00, // Set the emissive color to match the desired glow color
	// 	// emissiveIntensity: -1.0, // Adjust the intensity of the emissive glow
	// 	transparent: true,
	// 	opacity: 0.1
	// });

	// // Create the cube mesh
	// cubeMesh = new THREE.Mesh(cubeGeometry, cubeMaterial);
	// cubeMeshShadow = new THREE.Mesh(cubeGeometry, cubeMaterialShadow);
	// // const cubeMeshShadow2 = new THREE.Mesh(cubeGeometry, cubeMaterialShadow2);
	// cubeMeshShadow2 = new THREE.Mesh( 
	// 	new THREE.SphereGeometry( 0.7, 32, 16 ),//SphereGeometry(radius : Float, widthSegments : Integer, heightSegments : Integer, phiStart : Float, phiLength : Float, thetaStart : Float, thetaLength : Float)
	// 	cubeMaterialShadow2
	// );

	// cubeMesh.position.y = 1;
	// // cubeMesh.scale.set(0.8, 0.8, 0.8);
	// cubeMeshShadow.position.y = 1;
	// cubeMeshShadow.scale.set(1.2, 1.2, 1.2);
	// cubeMeshShadow2.position.y = 1;
	// cubeMeshShadow2.scale.set(1.5, 1.5, 1.5);
	// scene.add(cubeMesh);
	// scene.add(cubeMeshShadow);
	// scene.add(cubeMeshShadow2);
	// lumoScene.add(cubeMesh);

}

var vrControllers = {};//TODO: Move to top
var xrRefSpace;// TODO: Move to top  // think its the guardian boundery space set on the vr by the user.
// var vrCurrentPosition;//current position in 3d space of the headset after camera positional consideration changes
var xrCamInit;//todo: try use it as the referenced/ difference position

function initXR() {
    // console.log("rendererrr: " + this.renderer.xr);
	renderer.xr.enabled = true;
	//On session start move to camera objects offset location:
	renderer.xr.addEventListener('sessionstart', (event) => {
		const session = renderer.xr.getSession();
		const baseReferenceSpace = renderer.xr.getReferenceSpace();
		const offsetPosition = camera.position.clone();
		xrCamInit = { x: -offsetPosition.x, y: -offsetPosition.y, z: -offsetPosition.z };//initial camera position
		const offsetRotation = new THREE.Quaternion().setFromAxisAngle(new THREE.Vector3(0.0, 0.0, 0.0), 0.0);
		const transform = new XRRigidTransform({ x: -offsetPosition.x, y: -offsetPosition.y + player.height, z: -offsetPosition.z }, offsetRotation);
		const teleportSpaceOffset = baseReferenceSpace.getOffsetReferenceSpace(transform);
		// vrCurrentPosition = teleportSpaceOffset.transform.position;
		renderer.xr.setReferenceSpace(teleportSpaceOffset);

		console.log("REACHED SESSION STARRT");
		// session.requestReferenceSpace('local-floor').then((refSpace) => {
		// 	xrRefSpace = refSpace;
		// 	// session.requestAnimationFrame(onSessionFrame);
		// 	VRButton.xrSession.addEventListener( 'frame', onSessionFrame );

		// 	// navigator.xr.requestSession('immersive-vr').then((sess) => {
		// 	// 	sess.on('frame', (time, frame) => {
		// 	// 		console.log("REACHED SESSION FRAME");
		// 	// 		const ses = frame.session;
		// 	// 		onSessionFrame(time, frame);
		// 	// 	});
		// 	//   });
		// 	// session.on('frame', (time, frame) => {
		// 	// 	const session = frame.session;
		// 	// 	onSessionFrame(time, frame);
		// 	// });

		// });
	});

	// renderer.xr.addEventListener('sessionstart', (e) => {

	// 	// controls.update();

	// 	const baseReferenceSpace = renderer.xr.getReferenceSpace();
	// 	const offsetPosition = camera.position;
	// 	console.log(camera.position)
	// 	//const offsetRotation = camera.rotation;
	// 	const offsetRotation = camera.quaternion;
	// 	const transform = new XRRigidTransform( { x: offsetPosition.x, y: offsetPosition.y, z: offsetPosition.z }, { x: offsetRotation.x, y: (offsetRotation.y), z: offsetRotation.z, w: offsetRotation.w } ); 
	// 	//const transform = new XRRigidTransform( offsetPosition, { x: offsetRotation.x, y: -(offsetRotation.y - 0.85), z: offsetRotation.z, w: offsetRotation.w } ); 
	// 	const teleportSpaceOffset = baseReferenceSpace.getOffsetReferenceSpace( transform );
	// 	renderer.xr.setReferenceSpace( teleportSpaceOffset );
	// });
    document.body.appendChild(VRButton.createButton(renderer));

	buildVrControllers();


	// await renderer.context.makeXRCompatible();
	// VRButton.xrSession.baseLayer = new XRWebGLLayer(VRButton.xrSession, renderer.context);

	// vrControllers = buildVrControllers();

    // function onSelectStart() {
    //   // this refers to the controller
    // //   this.children[0].scale.z = 10;
    //   this.userData.selectPressed = true;
    // }
  
    // function onSelectEnd() {
    //   // this refers to the controller
    // //   this.children[0].scale.z = 0;
    //   this.userData.selectPressed = false;
    // }

    // vrControllers.forEach(controller => {
    //   controller.addEventListener('selectstart', onSelectStart);
    //   controller.addEventListener('selectend', onSelectEnd);
    // });
}

function getIntersections( controller ) {
	controller.updateMatrixWorld();

	tempMatrix.identity().extractRotation( controller.matrixWorld );

	vrControllerRaycaster.ray.origin.setFromMatrixPosition( controller.matrixWorld );
	vrControllerRaycaster.ray.direction.set( 0, 0, - 1 ).applyMatrix4( tempMatrix );

	return vrControllerRaycaster.intersectObjects( vrControllerSelectableObjectsgroup.children, true );
}

var vrController1, vrController2;//controller line object
var vrControllerGrip1, vrControllerGrip2;//controller object
var vrControllerObjectLeft, vrControllerObjectRight;//Could be opposite, dont know yet which hand is right or left
var vrControllerlineLeft, vrControllerlineRight;

var vrControllerRaycaster;
var vrControllerIntersected = [];
var vrControllerSelectableObjectsgroup;//will store the list of objects that can be selectable

const tempMatrix = new THREE.Matrix4();


var lastSelectedObject;// texureMode object selected

var vrDrawBlob;// VR SCULPT DRAWMODE
var vrDrawPoints = [];// VR SCULPT DRAWMODE

function buildVrControllers() {

	function onSelectStart( event ) {
		const controller = event.target;
		controller.userData.isSelecting = true;// VR SCULPT DRAWMODE

		const intersections = getIntersections( controller );
		if ( intersections.length > 0 ) {
			const intersection = intersections[ 0 ];

			const object = intersection.object;
			if (object.material.emissive)
				object.material.emissive.b = 1;
			if (ascii.modes["currentMode"].name == "objectMode") {//only in object select mode
				controller.attach( object );
			}
			console.log(object)
			lastSelectedObject = object;// stores the last selected object for other modes

			// console.log( object )// this is the latest selected object
			controller.userData.selected = object;
		}
		controller.userData.targetRayMode = event.data.targetRayMode;
	}

	function onSelectEnd( event ) {
		const controller = event.target;
		controller.userData.isSelecting = false;// VR SCULPT DRAWMODE

		if ( controller.userData.selected !== undefined ) {
			const object = controller.userData.selected;
			if (object.material.emissive)// OBJLoader files don't have the emmissive property 
				object.material.emissive.b = 0;
			vrControllerSelectableObjectsgroup.attach( object );
			controller.userData.selected = undefined;
		}
	}

	//ADD objects to make selectable by controller here:
	vrControllerSelectableObjectsgroup = new THREE.Group();
	scene.add( vrControllerSelectableObjectsgroup );


	selectableObjects.push(new object_("objects.platform[0].clone()", 0));
	selectableObjects[selectableObjects.length - 1].object = objects.platform[0].clone();
	selectableObjects[selectableObjects.length - 1].object.name = selectableObjects[selectableObjects.length - 1].name;

	vrControllerSelectableObjectsgroup.add( mesh.object );
	vrControllerSelectableObjectsgroup.add( sphere.object );
	vrControllerSelectableObjectsgroup.add( screen_text[0][0].object );
	// vrControllerSelectableObjectsgroup.add( objects.floor[0][0] );

	vrControllerSelectableObjectsgroup.add( selectableObjects[selectableObjects.length - 1].object );
	vrControllerSelectableObjectsgroup.add( crate.object );
	vrControllerSelectableObjectsgroup.add( crate2.object );

	vrControllerSelectableObjectsgroup.add( ascii.nodes[0].object );// TODO: is not currently stored in selectableObjects but testing it out
	vrControllerSelectableObjectsgroup.add( objects.buildings[0][0] );// TODO: is not currently in an object_ instance in the selectableObjects list
	// vrControllerSelectableObjectsgroup.add( objects.buildings[0][1] );// TODO: is not currently in an object_ instance in the selectableObjects list

	// myGroup.add(myObjectWrapper);

	// objects.platform[0].name = "platform.0"
	// screen_text[0][0].name = "screen_text0.O";
	// sphere.name = "sphere";
	// crate.name = 'crate';
	// console.log(mesh)
	// console.log(objects.platform)

	// controllers
	vrController1 = renderer.xr.getController( 0 );
	vrController1.addEventListener( 'selectstart', onSelectStart );
	vrController1.addEventListener( 'selectend', onSelectEnd );
	vrController1.userData.id = 0;// VR SCULPT DRAWMODE
	scene.add( vrController1 );

	vrController2 = renderer.xr.getController( 1 );
	vrController2.addEventListener( 'selectstart', onSelectStart );
	vrController2.addEventListener( 'selectend', onSelectEnd );
	vrController2.userData.id = 1;// VR SCULPT DRAWMODE
	scene.add( vrController2 );

	// const controllerModelFactory = new THREE.XRControllerModelFactory();
	// const controllerModelFactory = new XRControllerModelFactory();

	// Set the asset path for the left controller
	// controllerModelFactory.setAssetPath('left', 'http://localhost:8001/vr-test/010-VR/webxr/meta-quest-touch-pro/');
	// vrControllerGrip1.path = 'http://localhost:8001/vr-test/010-VR/webxr/meta-quest-touch-pro/';
	// vrControllerGrip1.add( controllerModelFactory.createControllerModel( vrControllerGrip1 ) );
	vrControllerGrip1 = renderer.xr.getControllerGrip( 0 );
	vrControllerObjectLeft = new THREE.Mesh(//sphere TODO: upload the oculus controller objects and use here in replacement
		new THREE.SphereGeometry( 0.1, 16, 8 ),//SphereGeometry(radius : Float, widthSegments : Integer, heightSegments : Integer, phiStart : Float, phiLength : Float, thetaStart : Float, thetaLength : Float)
		new THREE.MeshBasicMaterial( { color: 0x919191 } )
	)
	// vrControllerGrip1.add( vrControllerObjectLeft )
	scene.add( vrControllerGrip1 );

	// vrControllerGrip2.path = 'http://localhost:8001/vr-test/010-VR/webxr/meta-quest-touch-pro/';
	// vrControllerGrip2.add( controllerModelFactory.createControllerModel( vrControllerGrip2 ) );
	vrControllerGrip2 = renderer.xr.getControllerGrip( 1 );
	vrControllerObjectRight = new THREE.Mesh(//sphere TODO: upload the oculus controller objects and use here in replacement
		new THREE.SphereGeometry( 0.1, 16, 8 ),//SphereGeometry(radius : Float, widthSegments : Integer, heightSegments : Integer, phiStart : Float, phiLength : Float, thetaStart : Float, thetaLength : Float)
		new THREE.MeshBasicMaterial( { color: 0x919191 } )
	)
	// vrControllerGrip2.add( vrControllerObjectRight )
	scene.add( vrControllerGrip2 );
	//

	const geometry = new THREE.BufferGeometry().setFromPoints( [ new THREE.Vector3( 0, 0, 0 ), new THREE.Vector3( 0, 0, - 1 ) ] );

	let line = new THREE.Line( geometry );
	line.name = 'line';
	line.scale.z = 5;
	vrControllerlineLeft = line.clone();
	vrControllerlineRight = line.clone();
	
	// vrController1.add( vrControllerlineLeft );//is left and right correct here?
	// vrController2.add( vrControllerlineRight );

	vrControllerRaycaster = new THREE.Raycaster();
	//	

	//// VR SCULPT DRAWMODE BEGIN:

	const pivot = new THREE.Mesh( new THREE.IcosahedronGeometry( 0.01, 3 ) );
	pivot.name = 'pivot';
	pivot.position.z = - 0.05;

	const group = new THREE.Group();
	group.add( pivot );
	group.add(line);

	vrController1.add( group.clone() );
	vrController2.add( group.clone() );

	//// VR SCULPT DRAWMODE END

	const grid = new THREE.GridHelper( 2, 1, 0xFFFFFF, 0xFFFFFF);//0x111111, 0x111111 );
	grid.position.x -= 0.5;
	grid.position.z -= 0.5;
	grid.position.y += 0.1;
	scene.add( grid );

	initVrDrawBlob();// VR SCULPT DRAWMODE
  }

  function initVrDrawBlob() {// VR SCULPT DRAWMODE

	const material = new THREE.MeshStandardMaterial( {
		color: 0xff00ff,
		// envMap: reflectionCube,
		roughness: 0.9,
		metalness: 0.0,
		transparent: true
	} );

	vrDrawBlob = new MarchingCubes( 64 * 2, material, false, false, 500000);//500000 );
	vrDrawBlob.position.y = 1;
	scene.add( vrDrawBlob );

	initVrDrawPoints();
}

function initVrDrawPoints() {// VR SCULPT DRAWMODE
	vrDrawPoints = [
		{ position: new THREE.Vector3(), strength: 0.04, subtract: 10 },
		{ position: new THREE.Vector3(), strength: - 0.08, subtract: 10 }
		// { position: new THREE.Vector3(), strength: 10, subtract: 10 },
		// { position: new THREE.Vector3(), strength: 1.0, subtract: 10 }
	];
}

function transformVrDrawPoint( vector ) {// VR SCULPT DRAWMODE
	vector.x = ( vector.x + 1.0 ) / 2.0;
	vector.y = ( vector.y / 2.0 );
	vector.z = ( vector.z + 1.0 ) / 2.0;
}

function handleVRDrawController( controller ) {// VR SCULPT DRAWMODE
	const pivot = controller.getObjectByName( 'pivot' );

	if ( pivot ) {
		const id = controller.userData.id;
		const matrix = pivot.matrixWorld;

		vrDrawPoints[ id ].position.setFromMatrixPosition( matrix );
		transformVrDrawPoint( vrDrawPoints[ id ].position );


		if ( controller.userData.isSelecting ) {
			const strength = vrDrawPoints[ id ].strength / 2;

			const vector = new THREE.Vector3().setFromMatrixPosition( matrix );

			transformVrDrawPoint( vector );

			vrDrawPoints.push( { position: vector, strength: strength, subtract: 10 } );
		}
	}
}

function updateVrDrawBlob() {// VR SCULPT DRAWMODE
	vrDrawBlob.reset();

	for ( let i = 0; i < vrDrawPoints.length; i ++ ) {
		const point = vrDrawPoints[ i ];
		const position = point.position;

		vrDrawBlob.addBall( position.x, position.y, position.z, point.strength, point.subtract );
	}
	vrDrawBlob.update();

	// vrDrawBlob.removeAllBalls();

	// console.log(vrDrawPoints.length);
	// for ( let i = 0; i < vrDrawPoints.length; i ++ ) {
	// 	const point = vrDrawPoints[ i ];
	// 	const position = point.position;

	// 	var ballGeometry = new THREE.SphereGeometry( 1, 4, 4 );
	// 	var ballMaterial = new THREE.MeshLambertMaterial( { color: 0xff0000 } );
	// 	var ballMesh = new THREE.Mesh( ballGeometry, ballMaterial );
	// 	ballMesh.scale.set(point.strength, point.strength, point.strength);
	// 	ballMesh.position.set(position.x, position.y, position.z);
	// 	vrDrawBlob.add(ballMesh);
	// }
	// scene.remove(vrDrawBlob);
	// var material = new THREE.MeshLambertMaterial( { color: 0xff0000 } );
	// var vrDrawBlob = new MarchingCubes( 64, material, false, false, 500000 );
	// vrDrawBlob.position.set(0, 1, 0);

}


function cssElement( id, x, y, z, ry ) {

	const h2 = document.createElement( 'h2' );
	h2.innerHTML = "helloworld";

	const div = document.createElement( 'div' );
	div.style.width = '480px';
	div.style.height = '360px';
	div.style.backgroundColor = '#000';
	div.style.opacity = 0.8;
	div.style.borderRadius = "20%";

	const iframe = document.createElement( 'iframe' );
	iframe.style.width = '480px';
	iframe.style.height = '360px';
	iframe.style.border = '0px';
	iframe.style.opacity = 0.8;
	iframe.style.borderRadius = "20%";
	iframe.style.boxShadow = "0px 0px 1px rgb(151, 243, 255) inset," +
    	"0px 0px 2px rgb(151, 243, 255) inset," +
    	"0px 0px 10px rgb(151, 243, 255) inset," +
    	"0px 0px 40px rgb(151, 243, 255)," +
    	"0px 0px 100px rgb(151, 243, 255)," +
    	"0px 0px 5px rgb(151, 243, 255)";


	// iframe.src = [ 'https://www.youtube.com/embed/', id, '?rel=0' ].join( '' );
	// iframe.src = [ 'https://sh-d-w.bitbucket.io' ].join( '' );
	iframe.src = [ 'https://fingerprintsoft.co' ].join( '' );
	
	div.appendChild( h2 );
	div.appendChild( iframe );

	const object = new THREE.CSS3DObject( iframe );//div
	object.position.set( x, y, z );
	object.rotation.y = ry;
	object.onBeforeRender = function( renderer, scene, camera, geometry, material, group ) {
		// Your code here
	};
	object.onAfterRender = function( renderer, scene, camera, geometry, material, group ) {
		// Your code here
	};

	return object;

}

function cssDivElement( id, x, y, z, ry ) {

	const h2 = document.createElement( 'h2' );
	h2.innerHTML = "helloworld";

	const div = document.createElement( 'div' );
	div.style.width = '480px';
	div.style.height = '360px';
	div.style.backgroundColor = '#000';
	div.style.opacity = 0.8;
	div.style.borderRadius = "20%";
	div.style.color = "#212121";//"#FFFFFF";
	div.style.textShadow = "1px 1px 2px white, 0 0 1em white, 0 0 0.2em white";
	// "#FC0 1px 0 10px";

	// "2px 2px rgb(151, 243, 255)";
//"0px 0px 5px rgb(151, 243, 255);";

	div.appendChild( h2 );

	const object = new THREE.CSS3DObject( div );//div
	object.position.set( x, y, z );
	object.rotation.y = ry;
	object.onBeforeRender = function( renderer, scene, camera, geometry, material, group ) {
		// Your code here
	};
	object.onAfterRender = function( renderer, scene, camera, geometry, material, group ) {
		// Your code here
	};

	return object;

}

function onControllerUpdate(session, frame) { // this function will be called every frame, before rendering
	for(let inputSource of session.inputSources) { // we loop through every input source (controller) caught by our session
		if(inputSource.gripSpace) { // we check if our controllers actually have their space
			let gripPose = frame.getPose(inputSource.gripSpace, xrRefSpace); // we get controller's pose, by comparing our controller's space to our referance space
			if(gripPose) { // we check if our controller's pose was gotten correctly
				vrControllers[inputSource.handedness] = {pose: gripPose, gamepad: inputSource.gamepad}; // inputSource.handedness returns a string representing in which hand we have our controller - that is "left" or "right". Which means that controllers.left and controllers.right will contain two elements, one named "pose", which will simply be their corresponding XRPose, and the second named "gamepad", which will contain their corresponding Gamepad object. 
			}
		}
	}
}

const VR_CONTROLLER_TRIGGER = 0;
const VR_CONTROLLER_GRIP = 1;
const VR_LEFT_CONTROLLER_X = 4;
const VR_LEFT_CONTROLLER_Y = 5;
const VR_RIGHT_CONTROLLER_A = 4;
const VR_RIGHT_CONTROLLER_B = 5;
function modeSelectMode() {

	// console.log(vrControllers.right.gamepad.buttons[0]);// trigger
	// console.log(vrControllers.right.gamepad.buttons[1]);// grip
	// console.log("4: " + vrControllers.right.gamepad.buttons[4].value);// A
	// console.log("5: " + vrControllers.right.gamepad.buttons[5].value);// B

	// console.log(vrControllers.left.gamepad.buttons[0]);// trigger
	// console.log(vrControllers.left.gamepad.buttons[1]);// grip
	// console.log("4: " + vrControllers.left.gamepad.buttons[4].value);// X
	// console.log("5: " + vrControllers.left.gamepad.buttons[5].value);// Y

	let newMode;
	if (vrControllers.right.gamepad.buttons[VR_CONTROLLER_TRIGGER].value &&
		vrControllers.right.gamepad.buttons[VR_CONTROLLER_GRIP].value &&
		vrControllers.right.gamepad.buttons[VR_RIGHT_CONTROLLER_A].value &&
		vrControllers.right.gamepad.buttons[VR_RIGHT_CONTROLLER_B].value) {

		if (vrControllers.left.gamepad.axes[2] < 0 && vrControllers.left.gamepad.axes[3] < 0) {//NW
			newMode = "objectMode";
		}
		if (vrControllers.left.gamepad.axes[2] < 0 && vrControllers.left.gamepad.axes[3] > 0) {//SW
			newMode = "saveMode";
		}
		if (vrControllers.left.gamepad.axes[2] < 0 && vrControllers.left.gamepad.axes[3] == 0.0) {//W
			newMode = "keyboardMode";
		}
		if (vrControllers.left.gamepad.axes[2] == 0.0 && vrControllers.left.gamepad.axes[3] < 0) {//N
			newMode = "characterMode";
		}
		if (vrControllers.left.gamepad.axes[2] == 0.0 && vrControllers.left.gamepad.axes[3] > 0) {//S
			newMode = "kinematicsMode";
		}
		if (vrControllers.left.gamepad.axes[2] > 0 && vrControllers.left.gamepad.axes[3] < 0) {//NE
			newMode = "npcMode";
		}
		if (vrControllers.left.gamepad.axes[2] > 0 && vrControllers.left.gamepad.axes[3] > 0) {//SE
			newMode = "internalLayerMode";
		}
		if (vrControllers.left.gamepad.axes[2] > 0 && vrControllers.left.gamepad.axes[3] == 0) {//E
			newMode = "diagramMode";
		}
	}
	else if (vrControllers.left.gamepad.buttons[VR_CONTROLLER_TRIGGER].value &&
		vrControllers.left.gamepad.buttons[VR_CONTROLLER_GRIP].value &&
		vrControllers.left.gamepad.buttons[VR_LEFT_CONTROLLER_X].value &&
		vrControllers.left.gamepad.buttons[VR_LEFT_CONTROLLER_Y].value) {
		
		if (vrControllers.right.gamepad.axes[2] < 0 && vrControllers.right.gamepad.axes[3] < 0) {//NW
			newMode = "objectSwitchMode";
		}
		if (vrControllers.right.gamepad.axes[2] < 0 && vrControllers.right.gamepad.axes[3] > 0) {//SW
			newMode = "terrainMode";
		}
		if (vrControllers.right.gamepad.axes[2] < 0 && vrControllers.right.gamepad.axes[3] == 0.0) {//W
			newMode = "scaleMode";
		}
		if (vrControllers.right.gamepad.axes[2] == 0.0 && vrControllers.right.gamepad.axes[3] < 0) {//N
			newMode = "drawMode";
		}
		if (vrControllers.right.gamepad.axes[2] == 0.0 && vrControllers.right.gamepad.axes[3] > 0) {//S
			newMode = "textureMode";
		}
		if (vrControllers.right.gamepad.axes[2] > 0 && vrControllers.right.gamepad.axes[3] < 0) {//NE
			newMode = "vacant";
		}
		if (vrControllers.right.gamepad.axes[2] > 0 && vrControllers.right.gamepad.axes[3] > 0) {//SE
			newMode = "vacant";
		}
		if (vrControllers.right.gamepad.axes[2] > 0 && vrControllers.right.gamepad.axes[3] == 0) {//E
			newMode = "decalMode";
		}
	}
	if (newMode && newMode != "vacant") {
		lastSelectedObject = null; // reset last selected object when entering a new mode
		scene.remove(ascii.modes["currentMode"].object);
		ascii.modes["currentMode"] = ascii.modes[newMode];
		scene.add(ascii.modes["currentMode"].object)
	}
	return newMode;
}

var firstVrDrawBlobSaveInstantiation = true;
var canDownload = true;
var downloadButton;

// Main JavaScript file
// Create the Web Worker
const worker = new Worker('worker.js');

// Function to handle the response from the worker
function handleWorkerMessage(event) {//TODO MOVE THIS WORKER STUFF TO APPROPRIATE LOCATION AND CLEANUP
  const result = event.data;
  // Process the result as needed
//   console.log('Received result:', result);
}

// Attach the message event listener to the worker
worker.addEventListener('message', handleWorkerMessage);

// Function to initiate the slow computation
function computeSlowTask(functionName, vrDrawBlob) {//initiateTask
  return new Promise((resolve, reject) => {
    // Listen for the 'message' event from the worker
    const messageHandler = (event) => {
      const result = event.data;
      // Perform any additional processing if needed
      resolve(result); // Resolve the Promise with the result
    };

    // Attach the message event listener to the worker
    worker.addEventListener('message', messageHandler);

    // Send the data to the worker for processing
    // worker.postMessage(vrDrawBlob, [vrDrawBlob]);
    // worker.postMessage(JSON.stringify(vrDrawBlob));
	// worker.postMessage({ vrDrawBlob.traverse });

	const serializedData = JSON.stringify(vrDrawBlob.toJSON());	  // Serialize the vrDrawBlob object
	const data = {
		vrDrawBlob: serializedData,//vrDrawBlob
		functionName: functionName//functionName i.e "exportVrDrawBlobFaces"
	};
	worker.postMessage(data);	  // Send the serialized data to the worker for processing
	// worker.postMessage(serializedData);	  // Send the serialized data to the worker for processing
});
}

async function	modesLogicHandler() {

	if (ascii.modes["currentMode"].name == "saveMode") {

		if (vrControllers.right.gamepad.axes[3] < 0) {//N
			if (canDownload) {
				// Set the flag to false to prevent immediate subsequent downloads
				canDownload = false;

				computeSlowTask("exportVrDrawBlobFaces", vrDrawBlob)// converts the slow vrBlob to an optimised blender object file
				.then((result) => {
					console.log('Data processing completed: Ready to import object');//, result);
					//save the object as a new object instance:
					selectableObjects.push(new object_("drawModeBlob" /*increment here*/, 0))// TODO increment name so each object is unique name
					let blobObj = selectableObjects[selectableObjects.length - 1];
					blobObj.object = load_obj(result/* converts it to a wavefront object */, [ textures[0][5] ], true);
					blobObj.object.name = blobObj.name;
					scene.add(blobObj.object);
					vrControllerSelectableObjectsgroup.add( blobObj.object );
					// Perform the action you want to do with the completed data
				})
				.catch((error) => {
					console.error('Error occurred:', error);
					// Handle any errors that occurred during the computation
				});
				setTimeout(() => {
					canDownload = true;
				}, 5000);
			}
		}
		if (vrControllers.right.gamepad.axes[3] > 0) {//S
			async function downloadFile(fileData) {
				const blob = new Blob([ fileData ], { type: 'text/plain' });
				const url = URL.createObjectURL(blob);
		  
				const anchor = document.createElement('a');
				anchor.href = url;
				anchor.download = 'your_file_name.obj';
				anchor.click();
		  
				URL.revokeObjectURL(url);
				// component.remove();
			}
			if (firstVrDrawBlobSaveInstantiation) {
				const sceneElement = document.createElement('a-scene');
				// Set any attributes or properties of the scene element
				sceneElement.setAttribute('background', 'color: #ECECEC');
				sceneElement.setAttribute('shadow', 'enabled: true');
				// Append the scene element to the document body or any other desired parent element
				document.body.appendChild(sceneElement);

				downloadButton = document.createElement('a-entity');
				downloadButton.setAttribute('geometry', 'primitive: box; width: 1; height: 0.2; depth: 0.2');
				downloadButton.setAttribute('material', 'color: #00aaff');
				downloadButton.setAttribute('position', '0 1.6 -3');
				downloadButton.addEventListener('click', downloadFile);
		
				const scene = document.querySelector('a-scene');
				scene.appendChild(downloadButton);
		
				firstVrDrawBlobSaveInstantiation = false;
			}
			console.log(vrControllers.right.gamepad.axes[3])
			if (canDownload) {
				// Set the flag to false to prevent immediate subsequent downloads
				canDownload = false;

				// downloadFile();

				// Call the function to initiate the slow computation
				computeSlowTask("exportVrDrawBlobFaces", vrDrawBlob)
				.then((result) => {
					console.log('Data processing completed:');//, result);
					downloadFile(result);
					// Perform the action you want to do with the completed data
				})
				.catch((error) => {
					console.error('Error occurred:', error);
					// Handle any errors that occurred during the computation
				});

				setTimeout(() => {
					canDownload = true;
				}, 5000);
			}
			// downloadFile();
		}
		// downloadVrDrawBlobObject("draw_blob_object.obj", exportVrDrawBlobFaces(vrDrawBlob));
	} else if (ascii.modes["currentMode"].name == "objectSwitchMode") {
		if (vrControllers.right.gamepad.axes[3] < 0 && lastSelectedObject) {//N
			let objectsRow = Object.keys(objects)[Math.floor(Math.random() * Object.keys(objects).length)];//Math.floor(Math.random() * objects.length)
			let objectsCol = Math.floor(Math.random() * objects[objectsRow].length);//Object.keys(objects[objectsRow])[Math.floor(Math.random() * Object.keys(objects[objectsRow]).length)];
			let objectsInnerLen = Math.floor(Math.random() * objects[objectsRow][objectsCol].length);//Object.keys(objects[objectsRow])[Math.floor(Math.random() * Object.keys(objects[objectsRow]).length)];
			let currObject;
			let prevObject = lastSelectedObject;
			currObject = (objectsInnerLen != NaN) ? objects[objectsRow][objectsCol][objectsInnerLen] : objects[objectsRow][objectsCol];
			// console.log(Object.keys(objects.platform[0]))

			if (currObject) {
				//add to group
				vrControllerSelectableObjectsgroup.remove(lastSelectedObject);
				lastSelectedObject = currObject.clone(true);
				//add to group
				vrControllerSelectableObjectsgroup.add(lastSelectedObject);
				lastSelectedObject.position.set(prevObject.position.x, prevObject.position.y, prevObject.position.z);
				lastSelectedObject.rotation.set(prevObject.rotation.x, prevObject.rotation.y, prevObject.rotation.z);
				lastSelectedObject.scale.set(prevObject.scale.x, prevObject.scale.y, prevObject.scale.z);
				lastSelectedObject.material = prevObject.material;
			}
		}
		if (vrControllers.right.gamepad.axes[3] > 0 && lastSelectedObject) {//S
			let objectsRow = Object.keys(objects)[Math.floor(Math.random() * Object.keys(objects).length)];//Math.floor(Math.random() * objects.length)
			let objectsCol = Math.floor(Math.random() * objects[objectsRow].length);//Object.keys(objects[objectsRow])[Math.floor(Math.random() * Object.keys(objects[objectsRow]).length)];
			let objectsInnerLen = Math.floor(Math.random() * objects[objectsRow][objectsCol].length);//Object.keys(objects[objectsRow])[Math.floor(Math.random() * Object.keys(objects[objectsRow]).length)];
			let currObject;
			let prevObject = lastSelectedObject;
			currObject = (objectsInnerLen != NaN) ? objects[objectsRow][objectsCol][objectsInnerLen] : objects[objectsRow][objectsCol];
			// console.log(Object.keys(objects.platform[0]))

			if (currObject) {
				//add to group
				vrControllerSelectableObjectsgroup.remove(lastSelectedObject);
				lastSelectedObject = currObject.clone(true);
				// lastSelectedObject = new THREE.Group();
				// currObject.children.forEach((child) => {
				// 	const clonedChild = child.clone(true);
				// 	lastSelectedObject.add(clonedChild);
				// });
				//add to group
				vrControllerSelectableObjectsgroup.add(lastSelectedObject);
				lastSelectedObject.position.set(prevObject.position.x, prevObject.position.y, prevObject.position.z);
				lastSelectedObject.rotation.set(prevObject.rotation.x, prevObject.rotation.y, prevObject.rotation.z);
				lastSelectedObject.scale.set(prevObject.scale.x, prevObject.scale.y, prevObject.scale.z);
				lastSelectedObject.material = prevObject.material;
			}
		}
	}
	else if (ascii.modes["currentMode"].name == "scaleMode") {
		if (vrControllers.right.gamepad.axes[3] < 0 && lastSelectedObject) {//N
			lastSelectedObject.scale.x += 0.02;
			lastSelectedObject.scale.y += 0.02;
			lastSelectedObject.scale.z += 0.02;
		}
		if (vrControllers.right.gamepad.axes[3] > 0 && lastSelectedObject) {//S
			lastSelectedObject.scale.x -= 0.02;
			lastSelectedObject.scale.y -= 0.02;
			lastSelectedObject.scale.z -= 0.02;
		}
	} else if (ascii.modes["currentMode"].name == "textureMode") {
		if (vrControllers.right.gamepad.axes[3] < 0 && lastSelectedObject) {//N
			let textureRow = Math.floor(Math.random() * textures.length)
			let textureCol = Math.floor(Math.random() * textures[textureRow].length);

			const clonedTexture = lastSelectedObject.material.clone();
			clonedTexture.needsUpdate = true;
			const clonedMaterial = new THREE.MeshBasicMaterial({ map: textures[textureRow][textureCol], transparent: clonedTexture.transparent, opacity: clonedTexture.opacity });
			lastSelectedObject.material = clonedMaterial;
			// lastSelectedObject.material.map = textures[textureRow][textureCol];
			// lastSelectedObject.material.map = textures[textureRow][textureCol];
		}
		if (vrControllers.right.gamepad.axes[3] > 0 && lastSelectedObject) {//S
			let textureRow = Math.floor(Math.random() * textures.length)
			let textureCol = Math.floor(Math.random() * textures[textureRow].length);

			const clonedTexture = lastSelectedObject.material.clone();
			clonedTexture.needsUpdate = true;
			const clonedMaterial = new THREE.MeshBasicMaterial({ map: textures[textureRow][textureCol], transparent: clonedTexture.transparent, opacity: clonedTexture.opacity });
			lastSelectedObject.material = clonedMaterial;
			// lastSelectedObject.material.map = textures[textureRow][textureCol];
		}
	// console.log("4: " + vrControllers.right.gamepad.buttons[4].value);// A
	// console.log("5: " + vrControllers.right.gamepad.buttons[5].value);// B
	// console.log("4: " + vrControllers.left.gamepad.buttons[4].value);// X
		if (vrControllers.left.gamepad.buttons[5].value && lastSelectedObject) {// Y
			// const clonedTexture = lastSelectedObject.material.clone();
			lastSelectedObject.material.transparent = true;
			lastSelectedObject.material.opacity = (Math.random() * 5) * 0.2;
			lastSelectedObject.material.needsUpdate = true;
			// clonedTexture.needsUpdate = true;
			// const clonedMaterial = new THREE.MeshBasicMaterial({ map: textures[textureRow][textureCol] });
			// lastSelectedObject.material = clonedMaterial;

			// child.material.transparent = true;
			// child.material.opacity = 0.5;
			// child.material.needsUpdate = true;
		}

	}
}

var vrXDiff = 0.0, vrYDiff;//TODO: add to top with vr stuff AND change to more descriptive/specific name
var vrRightXDiff = 0.0;
var xrOffsetRotationY = 0.0;// TODO: move to top with vr stuff if works out and is still used.
async function onSessionFrame(t, frame) { // this function will happen every frame
	const session = frame.session; // frame is a frame handling object - it's used to get frame sessions, frame WebGL layers and some more things
	// session.requestAnimationFrame(onSessionFrame); // we simply set our animation frame function to be this function again
	// let pose = frame.getViewerPose(xrRefSpace); // gets the pose of the headset, relative to the previously gotten referance space

	onControllerUpdate(session, frame); // this function will be called every frame, before rendering

	// console.log("REACHED SESSION FRAME");

	vrXDiff = 0.0;
	vrYDiff = 0.0;
	vrRightXDiff = 0.0;
	if (!modeSelectMode()) {
		vrXDiff = vrControllers.left.gamepad.axes[2];
		vrYDiff = vrControllers.left.gamepad.axes[3];
		vrRightXDiff = vrControllers.right.gamepad.axes[2];
		modesLogicHandler();
	}
	// vrXDiff = vrControllers.left.gamepad.axes[0]
	// vrYDiff = vrControllers.left.gamepad.axes[1]

	// console.log(vrControllers.right.gamepad.buttons[0]);// trigger
	// console.log(vrControllers.right.gamepad.buttons[1]);// grip
	// console.log("4: " + vrControllers.right.gamepad.buttons[4].value);// A
	// console.log("5: " + vrControllers.right.gamepad.buttons[5].value);// B

	// console.log(vrControllers.left.gamepad.buttons[0]);// trigger
	// console.log(vrControllers.left.gamepad.buttons[1]);// grip
	// console.log("4: " + vrControllers.left.gamepad.buttons[4].value);// X
	// console.log("5: " + vrControllers.left.gamepad.buttons[5].value);// Y

	// gamepad.buttons[3] // Y button
	// gamepad.buttons[4] // X   ''
	// gamepad.buttons[1] // B   ''
	// gamepad.buttons[2] // A   ''

	// if(vrControllers.left.gamepad.axes[3] < 0.0){ // S key analog direction
	// 	camera_position[0] += Math.sin(camera.rotation.y) * player.speed;
	// 	camera_position[2] += -Math.cos(camera.rotation.y) * player.speed;
	// }
	// if(vrControllers.left.gamepad.axes[3] > 0.0){ // A key analog direction
	// 	camera_position[0] += Math.sin(camera.rotation.y + Math.PI/2) * player.speed;
	// 	camera_position[2] += -Math.cos(camera.rotation.y + Math.PI/2) * player.speed;
	// }


	let pose = frame.getViewerPose(xrRefSpace);// renderer.xr.getReferenceSpace());
	if (pose) {
		let vrPosition = pose.transform.position;
		let vrOrientation = pose.transform.orientation;
		let vrDirectionVector = new THREE.Vector3(0, 0, -1).applyQuaternion(vrOrientation);

		// console.log(vrOrientation);
		let camRotToHeadsetAngle = [0, 0, 0]
		//Y rotation:
		let p2 = [1.0, 0.0, 0.0]
		let cameraRotationY = Math.atan2(vrDirectionVector.x, vrDirectionVector.z) * (180 / Math.PI);// 0deg is infront of camera
		rotatezyx(p2, [0, 0, 0], 0, cameraRotationY * Math.PI / 180, 0);
		camRotToHeadsetAngle[1] = ((Math.atan2(p2[0], p2[2]) * (180 / Math.PI)) - 90.0 + xrOffsetRotationY * (180 / Math.PI));
		// camera.rotation.x = 0.0;
		// camera.rotation.y = ((Math.atan2(p2[0], p2[2]) * (180 / Math.PI)) - 90.0);
		// camera.rotation.z = 0.0;
		p2 = [0.0, 1.0, 0.0]
		let cameraRotationX = Math.atan2(vrDirectionVector.y, vrDirectionVector.z) * (180 / Math.PI);// 0deg is infront of camera
		rotatezyx(p2, [0, 0, 0], cameraRotationX * Math.PI / 180, 0, 0);
		camRotToHeadsetAngle[0] = ((Math.atan2(p2[1], p2[2]) * (180 / Math.PI)) - 90.0);//hopefully is the x-z axis angle to direction vector direction

		// console.log(camRotToHeadsetAngle);//hopefully is the x-z axis angle to direction vector direction

		// camera.rotation.x = ((Math.atan2(p2[1], p2[2]) * (180 / Math.PI)) - 90.0) * Math.PI / 180.0;
		camera.rotation.x = (camRotToHeadsetAngle[0]) * Math.PI / 180.0;
		camera.rotation.y = (camRotToHeadsetAngle[1]) * Math.PI / 180.0;
		camera.rotation.z = 0.0;

		// THIS IS HOW TO MOVE THE VR HEADSET:
		const baseReferenceSpace = renderer.xr.getReferenceSpace();

		//TESTING:
		let offsetRotationY = -player.turnSpeed * (vrRightXDiff < 0.0) + player.turnSpeed * (vrRightXDiff > 0.0);
		xrOffsetRotationY += offsetRotationY;
		const offsetRot = new THREE.Quaternion().setFromAxisAngle(new THREE.Vector3(0.0, 1.0, 0.0), offsetRotationY);
		let offsetReferenceSpace = baseReferenceSpace.getOffsetReferenceSpace(new XRRigidTransform ({x: -xrCamInit.x, y: -xrCamInit.y - player.height, z: -xrCamInit.z}, offsetRot));
		renderer.xr.setReferenceSpace(offsetReferenceSpace);//teleportSpaceOffset
		offsetReferenceSpace = renderer.xr.getReferenceSpace();

		//   const camera = renderer.xr.getCamera();
		// console.log(vrPosition.x - camera.position.x);
		// console.log(baseReferenceSpace.transform.position)
		// (camera.position.x - xrCamInit.x)
		xrCamInit.x += -(xrCamInit.x + camera.position.x);
		//thinking: headset:5 camera:-6  headset:-5,1 camera:6 | 5 + -6 = -1 | -5 + 6 = 1
		xrCamInit.y += -(xrCamInit.y + camera.position.y);
		xrCamInit.z += -(xrCamInit.z + camera.position.z);
		const offsetPosition = {x: xrCamInit.x, y: xrCamInit.y + player.height, z: xrCamInit.z};//camera.position;
		const offsetRotation = new THREE.Quaternion().setFromAxisAngle(new THREE.Vector3(0.0, 0.0, 0.0), 0.0 );

		// console.log(offsetRotation)
		const transform = new XRRigidTransform(offsetPosition, offsetRotation);
		const teleportSpaceOffset = offsetReferenceSpace.getOffsetReferenceSpace(transform);

		renderer.xr.setReferenceSpace(teleportSpaceOffset);//teleportSpaceOffset
	}
	// console.log("REACHED");
	// session.requestAnimationFrame(onSessionFrame);
	// VRButton.xrSession.requestAnimationFrame(onSessionFrame);
	// window.requestAnimationFrame(onSessionFrame);
	// setTimeout(onSessionFrame, 1000 / 60);
}

function intersectObjects( controller ) {//TODO: Rename these methods to vrControllerIntersectObjects() etc.
	// Do not highlight in mobile-ar
	if ( controller.userData.targetRayMode === 'screen' ) return;

	// Do not highlight when already selected
	if ( controller.userData.selected !== undefined ) return;

	const line = controller.getObjectByName( 'line' );
	const intersections = getIntersections( controller );

	if ( intersections.length > 0 ) {
		const intersection = intersections[ 0 ];

		const object = intersection.object;

		if (object.material.emissive)
			object.material.emissive.r = 0.03;
		vrControllerIntersected.push( object );

		line.scale.z = intersection.distance;
	} else {
		line.scale.z = 5;
	}
}

function cleanIntersected() {//VR Controller clears intersected
	while ( vrControllerIntersected.length ) {
		const object = vrControllerIntersected.pop();
		if (object.material.emissive)
			object.material.emissive.r = 0;
	}
}

function cameraMoveLogic() {
	////////////////////////////////////////////////////////////////
	let playerMoved = false;
	let onlyWClicked = false;
	let post_camera_position = [camera.position.x, camera.position.y, camera.position.z];
	camera_position = [camera.position.x, camera.position.y, camera.position.z];

	if(keyboard[87] || vrYDiff < 0.0 ){ // W key
		post_camera_position[0] -= Math.sin(camera.rotation.y) * player.speed;
		post_camera_position[2] -= -Math.cos(camera.rotation.y) * player.speed;
		playerMoved = true;
		onlyWClicked = true;
	}
	if(keyboard[83] || vrYDiff > 0.0){ // S key
		post_camera_position[0] += Math.sin(camera.rotation.y) * player.speed;
		post_camera_position[2] += -Math.cos(camera.rotation.y) * player.speed;
		playerMoved = true;
		onlyWClicked = false;
	}
	if(keyboard[65] || vrXDiff < 0.0){ // A key
		post_camera_position[0] += Math.sin(camera.rotation.y + Math.PI/2) * player.speed;
		post_camera_position[2] += -Math.cos(camera.rotation.y + Math.PI/2) * player.speed;
		playerMoved = true;
		onlyWClicked = false;
	}
	if(keyboard[68] || vrXDiff > 0.0){ // D key
		post_camera_position[0] += Math.sin(camera.rotation.y - Math.PI/2) * player.speed;
		post_camera_position[2] += -Math.cos(camera.rotation.y - Math.PI/2) * player.speed;
		playerMoved = true;
		onlyWClicked = false;
	}

	let moveDirection = new THREE.Vector3();
	moveDirection.subVectors(new THREE.Vector3(post_camera_position[0], post_camera_position[1], post_camera_position[2]), camera.position);

	let moveRaycaster = new THREE.Raycaster();//(camera.position, moveDirection);
	moveRaycaster.set(camera.position, moveDirection);
	scene.remove(ascii.sphere);//remove these objects just for the ray intersection else will collide not with floor
	// scene.remove(ascii.sphere2);
	scene.remove(cuber);
	scene.remove(vrController1);
	scene.remove(vrController2);
	scene.remove(vrControllerGrip1);
	scene.remove(vrControllerGrip2);
	let moveIntersects = moveRaycaster.intersectObjects(scene.children);
		// objects.floor[0][0].position.y = 0.0;
	// scene.add(ascii.sphere);//add back the temporary removed objects for the raycasting to prevent unwanted collisions with these objects
	// scene.add(ascii.sphere2);
	// scene.add(cuber);
	// scene.add(vrController1);
	// scene.add(vrController2);
	// scene.add(vrControllerGrip1);
	// scene.add(vrControllerGrip2);

	if (moveIntersects.length > 0 && moveIntersects[0].distance < 2.0) {// && intersects[0].distance > 0.01) {
		// console.log("COLLISION" + moveIntersects[0].distance)
		let cam_pos = new THREE.Vector3(camera_position[0], camera_position[1], camera_position[2]);

		// Move camera in direction of intersect face angle
		if (moveIntersects[0].face) {
			if (onlyWClicked) {
				let faceNormal = moveIntersects[0].face.normal.clone();
				faceNormal.applyQuaternion(camera.quaternion);
				let perpendicularDirection = new THREE.Vector3();
				perpendicularDirection.crossVectors(faceNormal, new THREE.Vector3(0, 1, 0));
				perpendicularDirection.normalize();
				let dotProduct = perpendicularDirection.dot(camera.getWorldDirection(new THREE.Vector3()));
				if (dotProduct < 0) {
					let distance = 0.25;
					perpendicularDirection.multiplyScalar(distance);
					cam_pos.add(perpendicularDirection);
				}
			} else {
				let camDirection = new THREE.Vector3();
				camera.getWorldDirection(camDirection);
				// direction = new THREE.Vector3(direction.x, direction.y, direction.z).normalize();

				moveRaycaster = new THREE.Raycaster();//(camera.position, moveDirection);
				moveRaycaster.set(camera.position, camDirection);
				let camIntersects = moveRaycaster.intersectObjects(scene.children);//moveIntersects[0].object);
				if (camIntersects.length > 0) {
					// console.log("cam intersects with line at: " + camIntersects[0].point.x + "," + camIntersects[0].point.y + "," + camIntersects[0].point.z + " ")
					//TODO cam_pos to move in the parallel direction of the camIntersects[0].point to moveIntersects[0].point
					let direction = new THREE.Vector3().subVectors(moveIntersects[0].point, camIntersects[0].point).normalize();
					// let speed = 0.1;

					// Move the camera in the direction of the vector by a certain amount
					cam_pos.addScaledVector(direction, player.speed);
					// Move the camera in the direction of the vector
					// cam_pos.add(direction);
				}
			}

			// only if cam_pos is within range of another wall:
			moveDirection = new THREE.Vector3();
			moveDirection.subVectors(cam_pos, camera.position);	
			moveRaycaster = new THREE.Raycaster();//(camera.position, moveDirection);
			moveRaycaster.set(camera.position, moveDirection);
			moveIntersects = moveRaycaster.intersectObjects(scene.children);
			if (moveIntersects.length > 0 && moveIntersects[0].distance > 0.5) {// && intersects[0].distance > 0.01) {
				camera_position = [cam_pos.x, cam_pos.y, cam_pos.z];
			}
		} else {
			console.log("WARNING: when hits here then object has no face: ", moveIntersects[0])
			// if (moveIntersects[0].object.name === "Cube.023_Cube.006")
			// scene.remove(moveIntersects[0].object);
			// var faceIndex = intersects[0].faceIndex;

			// camera_position = post_camera_position;

		}
	} else {
		camera_position = post_camera_position;
		// console.log("NO_COLLISION")
	}

	//FLOOR COLLISION DETECTION///////////////////////////////////////////////////////////////
	if (playerMoved) {

		let raycaster = new THREE.Raycaster();
		let direction = new THREE.Vector3(0, -1, 0);
		raycaster.set({x: camera.position.x, y: camera.position.y, z: camera.position.z }, direction);

		// scene.remove(ascii.sphere);//remove these objects just for the ray intersection else will collide not with floor
		// scene.remove(ascii.sphere2);
		// scene.remove(cuber);
		// scene.remove(vrController1);
		// scene.remove(vrController2);
		// scene.remove(vrControllerGrip1);
		// scene.remove(vrControllerGrip2);

		// objects.floor[0][0].position.y = 0.0;
		scene.add(objects.malls[0][1]);
		scene.updateMatrixWorld();// needed to account for temporarily adding the mall floors for collisions only
		let intersects = raycaster.intersectObjects(scene.children);
		scene.remove(objects.malls[0][1]);// mall floors are transparent so no need to keep them on the scene as blocks other visual effects
		scene.add(ascii.sphere);//add back the temporary removed objects for the raycasting to prevent unwanted collisions with these objects
		// scene.add(ascii.sphere2);
		scene.add(cuber);
		scene.add(vrController1);
		scene.add(vrController2);
		scene.add(vrControllerGrip1);
		scene.add(vrControllerGrip2);

		if (intersects.length > 0) {// if ray intersects with terrain:
			// Do something with the intersected object(s)
			camera_position[1] += -(camera_position[1] - intersects[0].point.y - player.height);//diff to point + playerHeight == where camera should be
			// objects.floor[0][0].position.y = -intersects[0].point.y;
		}

	}
	camera.position.set(camera_position[0], camera_position[1], camera_position[2]);
	///////////////////////////////////////////////////////////////////////////////////////

}

function animate(){
	// REALTIME: 		current:	0.09 collisions, 0.08 walking, 0.06 standard
		// SLOW CALC REMOVALS: updateVrDrawBlob()+ascii::raycaster2 (0.025 + 0.011) = 0.036 time saved
		// SLOW VR REMOVALS: raycaster+raycaster2 (0.011 + 0.011) = 0.022 time saved
		// SLOW: updateVrDrawBlob() ==	0.025													// 1. DONE: can make sure only occurs when in drawMode
		// cleanIntersected()+intesectObjects()+handleVrDrawController() == 0.002 
		// cameraMoveLogic()::if (playerMoved) == 0.011
		// SLOW: cameraMoveLogic()::moveRaycaster (collision detection) == 0.011 + 0.021 == 0.031	// if calculate properly, would not need second slow raycast + perhaps if limit the objects to fewer objects might reduce compute time.
		// MEDIUM: ascii::render[0]:: raycaster+raycaster2 == 0.011 + 0.011 == 0.022				// 2. DONE: can half the time by only casting 1 ray - and further by if statementing it to only use when not in VR mode

		// make vr Controllers to be able to select any object:

	// var startTime = new Date();
	// endTime = new Date();
	// var timeDiff = endTime - startTime; //in ms
	// // strip the ms
	// timeDiff /= 1000;  
	// // get seconds 
	// var seconds = timeDiff;
	// console.log(seconds + " seconds");


	cleanIntersected();
	intersectObjects( vrController1 );
	intersectObjects( vrController2 );
	
	handleVRDrawController( vrController1 );// VR SCULPT DRAWMODE
	handleVRDrawController( vrController2 );// VR SCULPT DRAWMODE

	if (ascii.modes["currentMode"].name == "drawMode") {
		updateVrDrawBlob();// VR SCULPT DRAWMODE;
	}


		// Testing Video texture of html element:
	// var video = document.querySelector('video');

	// if (renderIteration == 200) {
	// 	html2canvas(document.getElementById('capture')).then(function(screenshot) {
	// 		// context.drawImage(screenshot, 0, 0, canvas.width, canvas.height);
	// 		texture.needsUpdate = true;
	// 		// requestAnimationFrame(render);
	// 	});
	// 	console.log("REACHED" + renderIteration)
	// 	renderIteration = 0;
	// }
	// renderIteration += 1;

	//Video texture end////////////////////////////////////

	// requestAnimationFrame(animate);// now using the alternative way of animation frames that work with VR.
	
	// cuber.rotation.z += 0.01;
	// renderer.setClearColor(null);//ensure transparent background to try and accommodate for css3d.
	// renderer.background = null;
	// scene.background = null;//new THREE.Color(0x000000).setAlpha(0.5)
	// scene.background = new THREE.Color(0xADD8E8)//.setAlpha(0.5)
	// scene.background = new THREE.Color(0xFFFFFF)//.setAlpha(0.5)
	scene.background = new THREE.Color(0x000000)//.setAlpha(0.5)
	// lumoScene.background = new THREE.Color(0x0000FF)//.setAlpha(0.5)

	// if (vrControllers[0]) {
		// navigator.xr.requestSession();
		// else { // if our session was started already
		// 	// xrSession.end(); // request our session to end
		// }
		// const gamepad = navigator.getGamepads()[0];
		// if (gamepad) {
		// 	const analogStickPosition = {
		// 	x: gamepad.axes[0],
		// 	y: gamepad.axes[1]
		// 	};
		// 	console.log(analogStickPosition);
		// }
		// if (gamepad !== null)
		// document.getElementById("stderr").innerHTML = "gamepad: " + gamepad + "<br><br><br><br>";
		// let vrGamepad = vrControllers[0].getGamepad();
		// let vrInputSource = renderer.xr.getControllerGrip(0);//.inputSource;
		// console.log(vrInputSource);
		// if (vrInputSource.gamepad !== null) {
		// 	let vrGamepad = vrInputSource.gamepad;
		// 	let vrAxes = vrGamepad.axes;
		// 	let vrX = axes[0];
		// 	let vrY = axes[1];
		// 	console.log("vrX: " + vrX + ", vrY: " + vrY);
		// }
	// }

	mesh.object.rotation.x += 0.01;
	mesh.object.rotation.y += 0.02;
	crate2.object.rotation.y += 0.01, crate.object.rotation.y += 0.01;// rotate front and back simultaneously
	
	// let xrCamera;
	// if (renderer.xr.isPresenting === true) {
	// 	let cameraDirectionVector = new THREE.Vector3();
	// 	xrCamera = renderer.xr.getCamera(camera);
	// 	// xrCamera.getWorldDirection(cameraDirectionVector);
	// 	console.log(xrCamera);
	// }

	// const xrFrame = renderer.xr.getSession().renderState.baseLayer.session.frameOfReference;
	// const pose = xrFrame.getViewerPose(renderer.xr.getReferenceSpace());
	// if (pose) {
	// 	const head = pose.transform.position;
	// 	const orientation = pose.transform.orientation;
	// 	console.log(head);
	// }

// 	const vrposition = new THREE.Vector3();
// 	const vrrotation = new THREE.Quaternion();
// 	const vrscale = new THREE.Vector3();

// // the following line extracts the position, rotation and scale in world space
// 	camera.matrixWorld.decompose(vrposition, vrrotation, vrscale);
// 	console.log("vr pos: ", vrposition);
// 	// console.log("vr rot: ", vrrotation);
// 	// console.log("vr sca: ", vrscale);

	if (VRButton.xrSession) { // if our session is null - if it wasn't created
		if (!xrRefSpace) {
			VRButton.xrSession.requestReferenceSpace("local-floor").then((refSpace) => { // we request our referance space - an object that defines where the center of our space lies. Here we request a local-floor referance space - that one defines the center of the world to be where the center of the ground is
				xrRefSpace = refSpace; // we set our referance space to be the one returned by this function

				VRButton.xrSession.requestAnimationFrame(onSessionFrame); // at this point everything has been set up, so we can finally request an animation frame, on a function with the name of onSessionFrame
			});
		}
		else {
			VRButton.xrSession.requestReferenceSpace("local-floor").then((refSpace) => { // we request our referance space - an object that defines where the center of our space lies. Here we request a local-floor referance space - that one defines the center of the world to be where the center of the ground is
				xrRefSpace = refSpace; // we set our referance space to be the one returned by this function
				
				VRButton.xrSession.requestAnimationFrame(onSessionFrame); // at this point everything has been set up, so we can finally request an animation frame, on a function with the name of onSessionFrame
			});
		}
	}

	cameraMoveLogic();//moving the camera

	//CAMERA ROTATION//////////////////////////////////////////////////////////////////////////////
	vrXDiff = 0.0;
	vrYDiff = 0.0;
	if(keyboard[37]){ // left arrow key
		camera.rotation.y -= player.turnSpeed;
	}
	if(keyboard[39]){ // right arrow key
		camera.rotation.y += player.turnSpeed;
	}
	if(keyboard[38]){ // up arrow key
		camera.rotation.x -= player.turnSpeed;
	}
	if(keyboard[40]){ // down arrow key
		camera.rotation.x += player.turnSpeed;
	}

	if(keyboard[100]){ // 4 arrow key
		camera.rotation.z += 1 * Math.PI / 180;
	}
	if(keyboard[102]){ // 6 arrow key
		camera.rotation.z -= 1 * Math.PI / 180;
	}
	if(keyboard[67]){ // C clicked
		//change camera variable:
		camera_switch_iter += 0.1;
		if (camera_switch_iter > 1.0) {
			camera_switch_iter = 0.0;
			camera_state += 1;
			if (camera_state > 2)
				camera_state = 0;
		}
	}

	let l_M;
	
	//ANIMATE AN OBJECT:
	animate_timer_end = new Date();
	if (animate_timer_end - animate_timer_start > animate_speed)					//if time reached
	{
		animate_timer_start = animate_timer_end;						//inherit new time
		animation_index += 1;
		if (animation_index >= objects.droid[animation_id].length)
			animation_index = 0;
		scene.remove( cuber );
		cuber = objects.droid[animation_id][animation_index];
		cuber.position.set(-5, 0, 4);
		scene.add( cuber );
	}

	//TERRAIN COLLISION DETECTION:
    //# Find quadrant player_pos lies in:
    height_offset = [0, 0, 0]

    // h_player_pos = [ camera_position[0], camera_position[1], camera_position[2]]
    // xy = terrain_bounds(h_player_pos, height_map, heightmap_vertices)
    // if (xy) {
    //     if (xy[0] != -1 && xy[1] != -1) {
    //         height_offset = point_terrain(h_player_pos, xy, height_map, heightmap_vertices, [[0, 0, 0], [0, 0, 0], [0, 0, 0]])
	// 	}
	// }
	// objects.floor[0][0].position.y = -height_offset[1];

	// new TERRAIN COLLISION DETECTION FORMULA: // TODO: move this to its correct terrain function and store in a good place:

	//l_collision = l_collision || 0

	//OBJECT COLLISION DETECTION:
	let	l_collision = 0;

	// l_radius = Math.sqrt(Math.pow(cube.rhs[2].center[0] - cube.rhs[0].center[0], 2) + Math.pow(cube.rhs[2].center[1] - cube.rhs[0].center[1], 2)); 
	// if (l_radius < 0.1) {

	// CUBE COLLISION:
	let	l_collided;
	//TODO: Removed collision on object as conflicts with vr controller selection - it copies over to its new position without deleting the old collision: 
	// l_collided = collision_object(camera_position, [crate.object.position.x, crate.object.position.y, crate.object.position.z], 2.0);

	// if (l_collided)
	// 	crate.object.material.map = textures[0][3];
	// else
	// 	crate.object.material.map = textures[0][2];

	// l_collision = l_collision || l_collided;// || l_radius2 <= 0.5;


		//TREE COLLISION:
	// l_collision = l_collision || collision_object(camera_position, [objects.tree[0][0].rhs[0].position.x, objects.tree[0][0].rhs[0].position.y, objects.tree[0][0].rhs[0].position.z], 0.5);

	//WALL COLLISION DETECTION:
	// l_collision = l_collision || collision_wall(camera_position, [crate.object.position.x, crate.object.position.y, crate.object.position.z], [objects.tree[0][0].position.x, objects.tree[0][0].position.y, objects.tree[0][0].position.z], 0.1);

	// SET CAMERA MOVEMENT POSITIONS OFFICIALLY:
	// if (l_radius > 2.0 && l_radius2 > 0.5 && l_wall_collided === 0)//if will not collide with object then move player camera object:
	// animation_id = 1;
	// if (!l_collision)
	// 	camera.position.set(camera_position[0], camera_position[1], camera_position[2]);
	// else {
	// 	animation_id = 4;
	// 	// animation_index = 0;
	// }


	// ascii.render[0]();
	// RENDER SCREEN_TEXT:			ascii.js
	ascii.render[0]();

	// 3D TRIANGLE RAY COLLISION (REQUIRES ascii.render and camera.position):
	//TODO: this below commented code is no longer being hit due to no longer using screen_text[0][0] as aimer - but this is how collisions are done successfully:
	// let l_ctt = collision_triangles_translation;//collision_triangles;
	// let l_dp = [ screen_text[0][0].object.position.x, screen_text[0][0].object.position.y, screen_text[0][0].object.position.z ];
	// let	l_p = [ camera.position.x, camera.position.y, camera.position.z ];

	// let hit = rayIntersectsTriangle([l_p[0], l_p[1], l_p[2]], [l_dp[0] - l_p[0], l_dp[1] - l_p[1], l_dp[2] - l_p[2]], l_ctt[0][0], l_ctt[0][1], l_ctt[0][2]);
	// let hit2 = rayIntersectsTriangle([l_p[0], l_p[1], l_p[2]], [l_dp[0] - l_p[0], l_dp[1] - l_p[1], l_dp[2] - l_p[2]], l_ctt[1][0], l_ctt[1][1], l_ctt[1][2]);

	// // console.log("p: ", [l_p[0], l_p[1], l_p[2]])
	// // console.log("d: ", [l_dp[0] - l_p[0], l_dp[0] - l_p[1], l_dp[0] - l_p[2]])
	// //console.log("triangle", l_ctt);//collision_triangles);

	// // objects.platform[0].rhs[0].matrix[4] += 1.0;
	// // objects.platform[0].rhs[0].matrix.set([0,0,0,0]);

	// // objects.platform[0].rhs[0].matrix.decompose(
	// // 	objects.platform[0].rhs[0].position,
	// // 	objects.platform[0].rhs[0].quaternion,
	// // 	objects.platform[0].rhs[0].scale,
	// // )
	// // objects.platform[0].rhs[0].updateMatrix();
	// // scene.updateMatrixWorld();

	// //TODO: I believe this code isn't being hit anymore given we no longer use screentext
	// if (hit || hit2) {//if hit one of the 2 triangles that make up a square:
	// 	scene.remove( objects.platform[0] );
	// 	scene.remove( objects.platform[0].rhs[0] );
	// 	scene.add( objects.platform[0].rhs[0] );
	// 	// console.log(objects.platform[0].rhs[0]);
	// 	// console.log(objects.platform[0].rhs[0].children[0].geometry);
	// 	//TODO kinematics: get buffer geometry of the object and update it:
	// 	// let tempPlatformRhs = objects.platform[0].rhs[0].children[0].geometry.attributes.position.array;
	// 	// tempPlatformRhs[0] += 0.1;
	// 	// objects.platform[0].rhs[0].children[0].geometry.attributes.position.needsUpdate = true;
	// }
	// else {
	// 	scene.remove( objects.platform[0] );
	// 	scene.remove( objects.platform[0].rhs[0] );
	// 	scene.add( objects.platform[0] );
	// }

	//ANIMATION OBJECT MOVE TO INFRONT OF CAMERA:
	l_M = [0, -1.75, -0.1]
	rotatexyz(l_M, [0, 0, 0], 0, camera.rotation.y, 0 )

	cuber.position.x = camera.position.x + l_M[0];
	cuber.position.y = camera.position.y + l_M[1];
	cuber.position.z = camera.position.z + l_M[2];

	// cuber.rotation.x =  (90 * Math.PI / 180) + camera.rotation.x ;
	// cuber.rotation.y = camera.rotation.y;
	// if(keyboard[87] || keyboard[87] || keyboard[65] || keyboard[68]) // W key
		cuber.rotation.z = -camera.rotation.y + 0;//+ 180 * Math.PI / 180;

	//CAMERA ALLOW VERTICAL LOOK UP AND DOWNWARDS:
	camera_pointer.position.set(camera.position.x, camera.position.y, camera.position.z);
	//camera_pointer.rotation.x += 0.1;

	l_M = [0, 0, 1]
	rotatexyz(l_M, [0, 0, 0], camera.rotation.x, camera.rotation.y, 0 )

	camera_pointer.position.x += l_M[0];
	camera_pointer.position.y += l_M[1];
	camera_pointer.position.z += l_M[2];

	let temp = [camera.rotation.x, camera.rotation.y, camera.rotation.z]

	//THIRD PERSON CAMERA AND BEV CAMERA:
	if (camera_state === 1)
		l_M = [0, 0, -2]
	if (camera_state === 2) 
		l_M = [0, 4, -1]
	if (camera_state === 1 || camera_state === 2) {
		rotatexyz(l_M, [0, 0, 0], camera.rotation.x, camera.rotation.y, 0 )
		camera.position.x += l_M[0];
		camera.position.y += l_M[1];
		camera.position.z += l_M[2];
	}
	//TO LOOKAT LOCATION:
	if (camera_state === 0)	
		camera.lookAt(camera_pointer.position);
	else
		camera.lookAt(cuber.position);

	//TO RENDER THE SCENE THREEJS:
	// renderer.render(scene, camera);

	// TO RENDER LUMINESCENT SCENE:
	// Clear the renderer before rendering the scene
	renderer.clear();

	// Render the scene to the render target
	// renderer.setRenderTarget(renderTarget);
	// let randCubeMeshShadow = ((Math.random() * 2) / 10.0 + 0.8) * 0.1; 
	// // cubeMeshShadow2.scale.set(1.3 + randCubeMeshShadow, 1. + randCubeMeshShadow, 1.4 + randCubeMeshShadow);
	// // cubeMeshShadow.scale.set(1.0 + randCubeMeshShadow * 0.2, 1.0 + randCubeMeshShadow * 0.2, 1.0 + randCubeMeshShadow * 0.2);
	// cubeMesh.material.opacity = 0.1 + randCubeMeshShadow;
	// cubeMeshShadow.material.opacity = randCubeMeshShadow;
	// cubeMeshShadow2.material.opacity = randCubeMeshShadow - 0.05;
	// cubeMesh.scale.set(0.8 + randCubeMeshShadow * 0.2, 0.8 + randCubeMeshShadow * 0.2, 0.8 + randCubeMeshShadow * 0.2);

	renderer.render(scene, camera);
	// renderer.setRenderTarget(null);

	// Use the composer to apply the glow effect
	// composer.render();

	// Render the glowing cube
	// renderer.render(lumoScene, camera);//cubeScene	cubeCamera


	//CSS3D RENDER:
	let cssCamera = camera.clone();

		
	// cssGroup.position.set(cssGroup.position.x, cssGroup.position.y, cssGroup.position.z);

	// if camera turn left object must be located more right:
		// its position as a radius away from camera, rotate by invert of camera angle.x

			// it is currently 0 distance away from the camera it needs to translate the distance amount to its position from camera position:
			// it needs to translate to the right by the offset difference amount distance between the camera angle and the angle
		// console.log("camera.fov+angle:");
		// console.log(camera.fov);//90
		// console.log(camera.rotation.y * 180 / Math.PI);// 0 forward | 90 right | -90 left | 0 backward 

		// cssScene.remove( cssGroup.object );
		// let cssObject = new THREE.Object3D();
		// cssObject.position.set(cssGroup.position[0], cssGroup.position[1], cssGroup.position[2]);
		// cssObject.rotation.set(cssGroup.angle[0], cssGroup.angle[1], cssGroup.angle[2]);
		// cssObject.updateMatrixWorld(true);

		// var projector = new THREE.Projector();
		// var vector = new THREE.Vector3(cssGroup.position[0], cssGroup.position[1], cssGroup.position[2]);
		// vector.project(camera);

		// var x = (vector.x + 1) / 2 * window.innerWidth;
		// var y = -(vector.y - 1) / 2 * window.innerHeight;
		// console.log("LOCATION: " + x + " " + y);
		// console.log( window.innerwidth)
		// console.log(window.innerWidth / 2);

		// console.log(window.innerWidth / 2);
		// cssGroup.object.position.x = 0;//(window.innerWidth / 2);// - x;

		// cssGroup.object.position.x = (window.innerWidth / 2) - x;
		// console.log((x / (window.innerWidth)) * 100);

		// cssGroup.object.position.x = 
		// x1 = (window.innerWidth / 2) - x;
		// cssGroup.object.position.x = x1;
		// cssGroup.object.position.x = Math.pow(-0.010666666666666666 * x1,2) + 0.6666666666666667 * x1 + 24.999999999999996;
		// cssGroup.object.position.x = -Math.pow(0.010666666666666666 * x1, 2) + 0.6666666666666667 * x1 + 24.999999999999996;
			// cssGroup.object.position.x = -0.010666666666666666 * Math.pow(x1,2) + 0.6666666666666667 * x1 + 24.999999999999996;
			// cssGroup.object.position.x = Math.pow(-0.1 * x1,2) + 8.5 * x1 + -150;
		// cssGroup.object.position.x = -Math.pow(0.1 * x1, 2) + 8.5 * x1 + -150;
			// cssGroup.object.position.x = -0.1 * Math.pow(x1,2) + 8.5 * x1 + -150;

		// cssGroup.object.position.x = Math.pow(0.05 * x1, 2) + 0.3 * x1;// 25 r
		// cssGroup.object.position.x = Math.pow(0.07 * x1, 2);// 25 r
		// cssGroup.object.position.x = Math.pow(0.09 * x1, 2);// 50 r
		// cssGroup.object.position.x = Math.pow(0.05 * x1, 2);// closer slow backward
		// cssGroup.object.position.x = Math.pow(0.1 * x1, 2);//60 r
		// cssGroup.object.position.x = Math.pow(0.01 * x1, 2);// too slow/little
		// cssGroup.object.position.x = Math.pow(0.2 * x1, 2);//80

		// cssGroup.object.position.x = -0.0026666666666666666 * Math.pow(x1,2) + -0.6 * x1 + -48.333333333333336;
		// cssGroup.object.position.x = Math.pow(-0.0026666666666666666 * x1, 2) + -0.6 * x1 + -48.333333333333336;
		// cssGroup.object.position.x = -Math.pow(0.0026666666666666666 * x1, 2) + -0.6 * x1 + -48.333333333333336;
		// cssGroup.object.position.x = 0.016666666666666666 * Math.pow(x1, 2) + -1.5 * x1 + -97.91666666666667;
		// cssGroup.object.position.x = Math.pow(0.016666666666666666 * x1, 2) + -1.5 * x1 + -97.91666666666667;

		// Could you write me a function that takes in my value of the actualPosition and returns
		// the difference required to align it to the requiredPosition?
		// Here is some information on how some of their values on a scale of 0% - 100% compare to
		// each other so that you can find a pattern/ratio if possible:

		// smaller than 40% or greater then 60% of the requiredPositiion, the difference between actualPosition and requiredPosition starts becoming noticeably different,
		// at 0% of requiredPosition, the actualPosition is at about 25%
		// at 25% of requiredPosition, the actualPosition is at around 35%
		// anywhere between 40% and 60%, actualPosition and requiredPosition are almost identical values.
		// at 75% of requiredPosition, the actualPosition is at around 65%
		// at 100% of requiredPosition, the actualPosition is at about 75%

		// Given that by default the actualPosition is always initially at 50%, below is the current formula I am using that gives me the above actualPosition results:
		// actualPosition = (50) - requiredPosition; // 50 is 50%

		// IsObjectMoreToLeftOrRightOfCameraWithinFieldOfViewRange():
		// var frustum = new THREE.Frustum();
		// var cameraViewProjectionMatrix = new THREE.Matrix4();
		// cameraViewProjectionMatrix.multiplyMatrices( cssCamera.projectionMatrix, cssCamera.matrixWorldInverse );
		// frustum.setFromMatrix( cameraViewProjectionMatrix );
		// if ( frustum.containsPoint( cssObject.position ) ) {
		// 	// Object is within FieldOfView range of the camera
		// 	var vector = new THREE.Vector3();
		// 	vector.setFromMatrixPosition( cssObject.matrixWorld );
		// 	vector.project( cssCamera );
		// 	var direction = new THREE.Vector3( 0, 0, -1 );// direction changes depending whether on positive or negative z side of the plane
		// 	direction.applyQuaternion( cssCamera.quaternion );
		// 	let angle = Math.atan2( cssObject.position.z - cssCamera.position.x, cssObject.position.z - cssCamera.position.z );
		// 	let halfFov = THREE.Math.degToRad( cssCamera.fov + 40.0 ) / 2;
		// 	let angleTo = direction.angleTo( cssObject.position.clone().sub( cssCamera.position ).normalize() );
		// 	if ( angleTo <= halfFov ) {
		// 		// Object is within angle range
		// 		let angleDifference = Math.abs(angle - Math.atan2(vector.y,vector.x));
		// 		let leftRightValue;
		// 		if (angleDifference > Math.PI/2) {
		// 			leftRightValue = (vector.x < 0) ? "left" : "right";
		// 			angleTo *= (vector.x < 0) ? -1 : 1;
		// 		} else {
		// 			leftRightValue = (vector.x > 0) ? "right" : "left";
		// 			angleTo *= (vector.x > 0) ? 1 : -1;
		// 		}

		// 		let cssAngleMapper = [
		// 			0.0,
		// 			0.0, 1.0, 1.0, 1.0, 30.0, 36.0, 42.0, 48.0, 54.0, 60.0,
		// 			12.0, 16.0, 18.0, 22.0, 24.0, 26.0, 27.0, 30.0, 30.0, 120.0,
		// 			35.0, 37.0, 40.0, 40.0, 40.0, 40.0, 0.0, 0.0, 0.0, 0.0,
		// 			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
		// 			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
		// 			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
		// 		]
		// 		cssScene.add( cssGroup.object );
		// 		// cssGroup.object.position.x = Math.pow(12,(Math.abs((angleTo + angleTo * 2 * (1.0 - ((angleTo * 180 / Math.PI)/ 60) )) * 180 / Math.PI) / (60))) * (angleTo > 0 ? -1 : 1);
		// 		cssGroup.object.position.x = cssAngleMapper[Math.floor(Math.abs(angleTo * 180 / Math.PI))] * (angleTo > 0 ? -1 : 1);
		// 		console.log(Math.floor(Math.abs(angleTo * 180 / Math.PI)));
		// 	}
		// }

		// IsObjectMoreToLeftOrRightOfCameraOutsideOfFieldOfViewRange():
		// var vector = new THREE.Vector3();
		// vector.setFromMatrixPosition( cssObject.matrixWorld );
		// vector.project( camera );
		
		// if ( vector.x < -1 ) {
		// 	// Object is to the left of the camera
		// 	console.log("left");
		// } else if ( vector.x > 1 ) {
		// 	// Object is to the right of the camera
		// 	console.log("right");
		// } else {
		// 	// Object is in front of the camera
		// 	console.log("in front");
		// }

		// IsObjectOnScreenHandler:
		// var direction = new THREE.Vector3( 0, 0, -1 );// direction changes depending whether on positive or negative z side of the plane
		// direction.applyQuaternion( camera.quaternion );
		// let angle = Math.atan2( cssGroup.position[0] - camera.position.x, cssGroup.position[2] - camera.position.z );
		// let halfFov = THREE.Math.degToRad( camera.fov + 40.0 ) / 2;
		// let angleTo = direction.angleTo( new THREE.Vector3(cssGroup.position[0], cssGroup.position[1], cssGroup.position[2]).clone().sub( camera.position ).normalize() );
		// // if ( angle > -halfFov && angle < halfFov && Math.abs(angleTo) < halfFov ) {
		// if ( angleTo <= halfFov ) {
		// 	// Object is within angle range
		// 	// console.log(angleTo);
		// }
		// console.log("ANGLE TO: " + (angleTo * 180/Math.PI));
		// console.log("angle" + (angle * 180/Math.PI));

			// (cssCamera.position.x - cssGroup.position[0])
			// group.position.set( cssGroup.position[0], cssGroup.position[1], cssGroup.position[2] );// need to fix to work with css.

	// if camera turn right object must be located more left:
		// its position as a radius away from camera, rotate by invert of camera angle.x
	// if camera turn up object must be located more down:
		// its position as a radius away from camera, rotate by invert of camera angle.y
			// but note: object.y is up and object.z is down
	// if camera turn down object must be located more up:
		// its position as a radius away from camera, rotate by invert of camera angle.y
			// but note: object.y is up and object.z is down (0. DONE: Tested whether negative values work for object.position.y else use position.z: they work inversely for up and down respectively)
	// 1. store meta location of the object and reuse to calculate:
			// 2. the distance from the camera.z: and scale up or down depending on how close
				// 0, 2.3, 10: from these coordinates, calculate the correct ratio for z:
			// group.scale.set( cssCamera.position.x - cssGroup.position[2], cssGroup.position[2], cssGroup.position[2] );// need to fix to work with css.
			let l_radius = Math.sqrt(Math.pow(cssCamera.position.x - cssGroup.position[0], 2) + Math.pow(cssCamera.position.z - cssGroup.position[2], 2)); //distance between cssCamera and cssObject
			let l_cssScaleRatio = 1.0;
			cssGroup.object.scale.set( 1 / (l_radius * l_cssScaleRatio), 1 / (l_radius * l_cssScaleRatio), 1 / (l_radius * l_cssScaleRatio) );// need to fix to work with css.
			// console.log(l_radius + "::radius")//15  15 / 10

			l_radius = Math.sqrt(Math.pow(cssCamera.position.x - cssDiv.position[0], 2) + Math.pow(cssCamera.position.z - cssDiv.position[2], 2)); //distance between cssCamera and cssObject
			l_cssScaleRatio = 1.0;
			cssDiv.object.scale.set( 1 / (l_radius * l_cssScaleRatio), 1 / (l_radius * l_cssScaleRatio), 1 / (l_radius * l_cssScaleRatio) );// need to fix to work with css.

			// 4. object must not show if it is out of sight radius of the screen:
				// meaning: angle.x + or - 45deg out of this range must display: none
					// How to calculate if object is out of range? ...

	// if (cssGroup.scale.x - 0.01 >= 0.0) // makes smaller looks further away:
	// 	cssGroup.scale.set(cssGroup.scale.x - 0.01, cssGroup.scale.y - 0.01, cssGroup.scale.z - 0.01);

	// cssGroup.position.set(cssGroup.position.x, cssGroup.position.y, cssGroup.position.z);

	// cssCamera.position.set(cssCamera.position.x, cssCamera.position.y, cssCamera.position.z);


	cssGroup.object.meta = cssGroup;
	// cssGroup.object.position.set(100.0, 0.0, 0.0);

	cssRenderer.render( cssScene, cssCamera );
	
	// scene.updateMatrixWorld();//needsUpdate() requirement???

	//THIRD PERSON CAMERA AND BEV CAMEERA UNDO:
	if (camera_state === 1 || camera_state === 2) {
		camera.position.x -= l_M[0];
		camera.position.y -= l_M[1];
		camera.position.z -= l_M[2];
	}

	// TO RESET LOOKAT POSITION:
	camera.rotation.x = temp[0]
	camera.rotation.y = temp[1]
	camera.rotation.z = temp[2]
	

}

function keyDown(event){
	keyboard[event.keyCode] = true;
}

function keyUp(event){
	keyboard[event.keyCode] = false;
}

var mousedown = false
var	mx = -1, my = -1
var	touchdown = false
var tx = -1, ty = -1

function initMove(evt) {
	mousedown = true;
	mx = evt.clientX;
	my = evt.clientY;
}

//object.mousehook:
function	move(evt) {	
	if (mousedown) {
		camera.rotation.y += (evt.clientX - mx) * 0.01  //player.turnSpeed;
		camera.rotation.x += (evt.clientY - my) * 0.01
		// for (let i = 0; i < 1; i++) {
			// // objects[i].mousehook[0]((evt.clientX - mx), (evt.clientY - my));
			// objects[i].angle[0] += (evt.clientX - mx) * Math.PI / 360;
			// objects[i].angle[1] += (evt.clientY - my) * Math.PI / 360;//180;

			// objects[i].angle[0] = cube.angle[0] % 360;
			// objects[i].angle[1] = cube.angle[1] % 360;
		// }
		mx = evt.clientX;
		my = evt.clientY;
	}

}

function stopMove() {
	mousedown = false;
}

function touchStart(event) {
	touchdown = true
	tx = event.touches[0].clientX
	ty = event.touches[0].clientY
}

function touchEnd(event) {
	touchdown = false
}

function touchMove(event) {
	if (touchdown) {
		let l_x = event.touches[0].clientX
		let l_y = event.touches[0].clientY

		camera.rotation.y += (l_x - tx) * 0.005  //player.turnSpeed;
		camera.rotation.x += (l_y - ty) * 0.005

		tx = l_x
		ty = l_y
	}
//  document.getElementById("demo").innerHTML = x + ", " + y;
}

function	reSize(event) {
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
	// renderer.setSize(window.innerWidth, window.innerHeight);
	// renderer.render(this.scene, this.camera); 
	renderer.setSize(window.innerWidth, window.innerHeight);
	cssRenderer.setSize(window.innerWidth, window.innerHeight);

	// console.log(g_objects)
	// var exporter = new OBJExporter();
	// console.log(exporter.parse(objects.floor[0][0]));

	// var exporter = new OBJExporter();
	// console.log(exporter.parse(vrDrawBlob));	
	// downloadVrDrawBlobObject("draw_blob_object.obj", exportVrDrawBlobFaces(vrDrawBlob));
	// console.log(result);
	// var newGeometry = new THREE.Geometry();
	// newGeometry.vertices = result.vertices;//faceVertices;
	// newGeometry.faces = result.indices;//faceIndices;
	// var newMesh = new THREE.Mesh(newGeometry);
	// var exporter = new THREE.OBJExporter();
	// console.log(exporter.parse(newMesh));
}

function downloadVrDrawBlobObject(objFileName, blenderObjFileText) {
	// Assuming you have a reference to the .obj file in text form
	const objFileText = blenderObjFileText;//"your .obj file text here";

	// Create a Blob object from the text
	const blob = new Blob([objFileText], { type: 'text/plain' });

	// Create a temporary link element to trigger the download
	const link = document.createElement('a');
	link.href = URL.createObjectURL(blob);

	// Set the filename for the download
	link.download = objFileName;//'your_file_name.obj';

	// Trigger the download
	link.click();

	// Cleanup the URL object
	URL.revokeObjectURL(link.href);
}

function exportVrDrawBlobFaces(marchingCube) {
	// Export the geometry to a Wavefront OBJ format
	const exporter = new OBJExporter();
	const objData = exporter.parse(marchingCube);// vrDrawBlob);

	// Process the OBJ data to remove duplicate vertices and faces
	const lines = objData.split('\n');
	const vertexMap = new Map();
	const uniqueVertices = [];
	const uniqueFaces = [];

	const originalVerticesMapping = [];

	for (let i = 0; i < lines.length; i++) {
	const line = lines[i];

	if (line.startsWith('v ')) {
		// Vertex line
		const vertexCoords = line.split(' ').slice(1).map(parseFloat);
		const vertexKey = vertexCoords.join(',');

		let vertexIndex;
		if (!vertexMap.has(vertexKey)) {
		// Add new unique vertex to the map and array
		vertexIndex = uniqueVertices.length / 3;
		vertexMap.set(vertexKey, vertexIndex);
		uniqueVertices.push(...vertexCoords);
		} else {
		vertexIndex = vertexMap.get(vertexKey);
		}
		originalVerticesMapping.push(vertexIndex);

	} else if (line.startsWith('f ')) {
		// Face line
		const faceIndices = line.split(' ').slice(1).map(index => {
		const subIndices = index.split('/').map(subIndex => {
			const vertexIndex = parseInt(subIndex) - 1; // Subtract 1 since OBJ indices start from 1
			let newVertexIndex = originalVerticesMapping[vertexIndex];
			vertexKey = uniqueVertices.slice(newVertexIndex * 3, newVertexIndex * 3 + 3).join(',');
			mappedIndex = vertexMap.get(vertexKey);// TODO: could even be this being +1 'd when returned??? but less likely
			return mappedIndex + 1; // Add 1 since OBJ indices start from 1
		});

		return subIndices.join('/');
		});

		// Check if the face is already in the unique faces array
		const faceLine = faceIndices.join(' ');
		if (!uniqueFaces.includes(faceLine) && !faceIndices.includes(NaN)) {
		// Add new unique face to the array
		uniqueFaces.push(faceLine);
		}
	}
	}

	// Reconstruct the OBJ data with the unique vertices and faces
	let reconstructedOBJ = '';

	for (let i = 0; i < uniqueVertices.length; i += 3) {
	const vertexLine = `v ${uniqueVertices[i]} ${uniqueVertices[i + 1]} ${uniqueVertices[i + 2]}`;
	reconstructedOBJ += vertexLine + '\n';
	}

	for (let i = 0; i < uniqueFaces.length; i++) {
		const faceLine = `f ${uniqueFaces[i]}`;
		const processedFaceLine = faceLine
		.split(' ')
		.map(face => {
		  const faceIndices = face.split('/').map(index => (isNaN(index) ? '' : index));
		  return faceIndices.join('/');
		})
		.join(' ');
		reconstructedOBJ += 'f ' + processedFaceLine + '\n'
		// reconstructedOBJ += faceLine + '\n';
		// reconstructedOBJ += faceLine.replace("NaN", "") + '\n';
	}

	// Save the reconstructed OBJ data to a file or further process it as needed
	return reconstructedOBJ;

}

window.addEventListener('keydown', keyDown);
window.addEventListener('keyup', keyUp);
// window.addEventListener('keyup', keyUp);

// window.addEventListener('mousedown', initMove);
// window.addEventListener('mousemove', move);
// window.addEventListener('mouseup', stopMove);

// window.addEventListener('touchstart', touchStart);
// window.addEventListener('touchmove', touchMove);
// window.addEventListener('touchend', touchEnd);

window.addEventListener("resize", reSize);
// ctx.canvas.width  = window.innerWidth;
// ctx.canvas.height = window.innerHeight;

window.onload = init;
