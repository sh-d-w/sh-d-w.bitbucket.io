// //pseudo:
// 	let dcm, quaternions, angles, angularVelocity, test;

// 	dcm = angle_to_dcm(75.00, 0.00, 0.00, "zyx");	// to direction cosines
// 	quaternions = dcm_to_quat(dcm);//to quaternions

// //	angularVelocity = angular_velocity([75.0 * Math.PI / 180, 0.0 * Math.PI / 180, 0.0 * Math.PI / 180], "zyx");
// // 	test = kinematic_differential_equation_for_quaternions(quaternions, angularVelocity, "zyx");//kinematics differential equation (on quaternions)
// // 	quaternions = kinematic_differential_equation_for_quaternions(quaternions, angularVelocity, "zyx");//kinematics differential equation (on quaternions)

// // 1. either only works when rotating all/multi of them
// // 2. else to use initial formulas shown without angular velocity

// 	dcm = quat_to_dcm(quaternions);//quarternions back to direction cosines
// 	angles = dcm_to_angle(dcm, "zyx");//back to eular angles in zyx format
// 	angles = [angles[2], angles[1], angles[0]];//zyx back-to xyz

// //[0, 0, 0, 0]
// //[[0,0,0],[0,0,0],[0,0,0]]

// //this works in place of the kinematics differential equation for what I am trying to do:
// let testAngles = dcm_to_angle(dcm_rotatex(75.0 * Math.PI / 180, dcm_rotatey(75.0 * Math.PI / 180, dcm)), "zyx");
// //dcm_to_angle(quat_to_dcm(test), "zyx")
// testAngles = [testAngles[2], testAngles[1], testAngles[0]]//zyx back-to xyz

// document.getElementById("demo").innerHTML = "Hello World <br>" + 
// angles + "<br>" +
// "test: " + testAngles + "<br>" +
// JSON.stringify(quaternions);// +
//EulerAngles([ Math.atan2(+dcm[0][1], +dcm[0][0]), 0, 0] );
//JSON.stringify(dcm);

//the multiplication of dcm's is a dcm

function dcm_rotatez(a, dcm) {
	let mdcm = [//rotate x and y axis around z:
    	[ Math.cos(a), Math.sin(a), 0.0 ],
        [-Math.sin(a), Math.cos(a), 0.0],
		[0.0, 0.0, 1.0]
    ];//matrix DCM

	let ldcm = multiply2_3x3Matrices(mdcm, dcm);
	return (ldcm);
}

function dcm_rotatey(a, dcm) {
	let mdcm = [//rotate x and z axis around y:
		[ Math.cos(a), 0.0, -Math.sin(a) ],
		[ 0.0, 1.0, 0.0 ],
		[ Math.sin(a), 0.0, Math.cos(a) ]
    ];//matrix DCM

	let ldcm = multiply2_3x3Matrices(mdcm, dcm);
	return (ldcm);
}

function	dcm_rotatex(a, dcm) {
	let mdcm = [//rotate y and z axis around x:
		[ 1.0, 0.0, 0.0 ],
		[ 0.0, Math.cos(a), Math.sin(a) ],
		[ 0.0, -Math.sin(a), Math.cos(a) ]
	];//matrix DCM

	let ldcm = multiply2_3x3Matrices(mdcm, dcm);
	return (ldcm);
}

function	multiply2_3x3Matrices(mdcm, dcm) {
	return [
	[	mdcm[0][0] * dcm[0][0] + mdcm[0][1] * dcm[1][0] + mdcm[0][2] * dcm[2][0], 
		mdcm[0][0] * dcm[0][1] + mdcm[0][1] * dcm[1][1] + mdcm[0][2] * dcm[2][1],
		mdcm[0][0] * dcm[0][2] + mdcm[0][1] * dcm[1][2] + mdcm[0][2] * dcm[2][2]
    ],
    [	mdcm[1][0] * dcm[0][0] + mdcm[1][1] * dcm[1][0] + mdcm[1][2] * dcm[2][0], 
		mdcm[1][0] * dcm[0][1] + mdcm[1][1] * dcm[1][1] + mdcm[1][2] * dcm[2][1],
		mdcm[1][0] * dcm[0][2] + mdcm[1][1] * dcm[1][2] + mdcm[1][2] * dcm[2][2]
    ],
  	[	mdcm[2][0] * dcm[0][0] + mdcm[2][1] * dcm[1][0] + mdcm[2][2] * dcm[2][0], 
		mdcm[2][0] * dcm[0][1] + mdcm[2][1] * dcm[1][1] + mdcm[2][2] * dcm[2][1],
		mdcm[2][0] * dcm[0][2] + mdcm[2][1] * dcm[1][2] + mdcm[2][2] * dcm[2][2]
    ]
	];
}

function angular_velocity(angles, rot_seq/*e.g zyx*/) {
	//zyx orientation:
	if (rot_seq === "zyx") {
      let a = angles[2], t = angles[1], p = angles[0];//x, y, z
      let dt = [a, t, p];//because: [ alpha / 1, theta / 1, phi / 1];//ch
      let w = [
          (-Math.sin(t) * dt[0] + dt[2]),
          (Math.sin(p) * Math.cos(t) * dt[0] + Math.cos(p) * dt[1]), 
          (Math.cos(p) * Math.cos(t) * dt[0] -Math.sin(p) * dt[1])
      ];
      return (w);
    }
	return null;//[0, 0, 0];
}

function kinematic_differential_equation_for_quaternions(q/*quaternion*/, w/*angular velocity (vector)*/, rot_seq/*e.g zyx*/) {
	//zyx orientation:
	let result = [0, 0, 0, 0];//0.5 * [C] * w

//https://www.vectornav.com/resources/inertial-navigation-primer/math-fundamentals/math-attitudetran
//	[ q[3] -q[2] q[1] ]
//	[ q[2] q[3] -q[0] ]
//	[ -q[1] q[0] q[3] ]
//	[ -q[0] -q[1] -q[3] ]
	result[0] = 0.5 * (q[3] * w[0] + -q[2] * w[1] + q[1] * w[2]);
	result[1] = 0.5 * (q[2] * w[0] + q[3] * w[1] + -q[0] * w[2]);
	result[2] = 0.5 * (-q[1] * w[0] + q[0] * w[1] + q[3] * w[2]);
	result[3] = 0.5 * (-q[0] * w[0] + -q[1] * w[1] + -q[3] * w[2]);


	//youtube version:
	//[-q1 -q2 -q3]
    //[q0 -q3 q2]
    //[q3 q0 -q1]
    //[-q2 q1 q0]
//    result[0] = 0.5 * (-q[1] * w[0] -q[2] * w[1] -q[3] * w[2]);
//    result[1] = 0.5 * (q[0] * w[0] -q[3] * w[1] + q[2] * w[2]);
//    result[2] = 0.5 * (q[3] * w[0] + q[0] * w[1] - q[1] * w[2]);
//    result[3] = 0.5 * (-q[2] * w[0] + q[1] * w[1] + q[0] * w[2]);

	return (result);
}


function angle_to_dcm(a1, a2, a3, rot_seq/*e.g zyx*/) {
    //https://github.com/JuliaSpace/ReferenceFrameRotations.jl/blob/master/src/conversions/angle_to_dcm.jl
    //# Compute the sines and cosines.
    let s1 = Math.sin(a1 * Math.PI / 180), c1 = Math.cos(a1 * Math.PI / 180);
    let s2 = Math.sin(a2 * Math.PI / 180), c2 = Math.cos(a2 * Math.PI / 180);
    let s3 = Math.sin(a3 * Math.PI / 180), c3 = Math.cos(a3 * Math.PI / 180);

    if (rot_seq === "zyx") {
        return [
        	[c2 * c1,                c2 * s1,             -s2],
            [s3 * s2 * c1 - c3 * s1, s3 * s2 * s1 + c3 * c1, s3 * c2],
            [c3 * s2 * c1 + s3 * s1, c3 * s2 * s1 - s3 * c1, c3 * c2]
        ];
    }
}

function dcm_to_angle(dcm, rot_seq /*e.g zyx*/) {
    if (rot_seq === "zyx") {
        //# Check for singularities.
        if ( !(Math.abs(dcm[0][2]) >= 1 - Number.EPSILON) ) {
            return EulerAngles([
                _mod_atan(+dcm[0][1], +dcm[0][0]),
                Math.asin(-dcm[0][2]),
                _mod_atan(+dcm[1][2], +dcm[2][2])
            ]);
        } else {
            return EulerAngles([
                _mod_atan(-dcm[1][0], +dcm[1][1]),
                _mod_asin(-dcm[0][2]),
                0.0
			]);
        }
    }
}


//- **[1]**: http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/
function dcm_to_quat(dcm) {
    if  (dcm[0][0] + dcm[1][1] + dcm[2][2] > 0) {//tr(dcm) # in julia
        //# f = 4 * q0
        f = 2 * Math.sqrt( (dcm[0][0] + dcm[1][1] + dcm[2][2]) + 1)

        return [
            f / 4,
            (dcm[1][2] - dcm[2][1]) / f,
            (dcm[2][0] - dcm[0][2]) / f,
            (dcm[0][1] - dcm[1][0]) / f
        ];
    } else if ( (dcm[0][0] > dcm[1][1]) && (dcm[0][0] > dcm[2][2]) ) {
        //# f = 4 * q1
        f = 2 * Math.sqrt(1 + dcm[0][0] - dcm[1][1] - dcm[2][2])

        //# Real part.
        q0 = (dcm[1][2] - dcm[2][1]) / f;

        //# Make sure that the real part is always positive.
        s = (q0 > 0) ? +1 : -1;

        return [
            s * q0,
            s * f / 4,
            s * (dcm[0][1] + dcm[1][0]) / f,
            s * (dcm[2][0] + dcm[0][2]) / f
        ];
    } else if ( dcm[1][1] > dcm[2, 2] ) {
        //# f = 4 * q2
        f = 2 * Math.sqrt(1 + dcm[1][1] - dcm[0][0] - dcm[2][2]);

        //# Real part.
        q0 = (dcm[2][0] - dcm[0][2]) / f;

        //# Make sure that the real part is always positive.
        s = (q0 > 0) ? +1 : -1;

        return [
            s * q0,
            s * (dcm[0][1] + dcm[1][0]) / f,
            s * f / 4,
            s * (dcm[2][1] + dcm[1][2]) / f
        ];
    } else {
        //# f = 4 * q3
        f = 2 * Math.sqrt(1 + dcm[2][2] - dcm[0][0] - dcm[1][1]);

        //# Real part.
        q0 = (dcm[0][1] - dcm[1][0]) / f;

        //# Make sure that the real part is always positive.
        s = (q0 > 0) ? +1 : -1;

        return [
            s * q0,
            s * (dcm[0][2] + dcm[2][0]) / f,
            s * (dcm[1][2] + dcm[2][1]) / f,
            s * f / 4
        ];
    }
}

function quat_to_dcm(q/*Quaternion*/) {
    //# Auxiliary variables.
    let q0 = q[0];
    let q1 = q[1];
    let q2 = q[2];
    let q3 = q[3];

    return [
          [Math.pow(q0, 2) + Math.pow(q1, 2) - Math.pow(q2, 2) - Math.pow(q3, 2),   2 * (q1 * q2 + q0 * q3)   ,   2 * (q1 * q3 - q0 * q2)],
          [2 * (q1 * q2 - q0 * q3)   , Math.pow(q0, 2) - Math.pow(q1, 2) + Math.pow(q2, 2) - Math.pow(q3, 2),   2 * (q2 * q3 + q0 * q1)],
          [2 * (q1 * q3 + q0 * q2)   ,   2 * (q2 * q3 - q0 * q1)   , Math.pow(q0, 2) - Math.pow(q1, 2) - Math.pow(q2, 2) + Math.pow(q3, 2)]
    ];
}

function EulerAngles(xyz) {
	let radToDeg = 180.0 / Math.PI

	xyz[0] *= radToDeg;
	xyz[1] *= radToDeg;
	xyz[2] *= radToDeg;
    return xyz;
}

//################################################################################
//#                              Private functions
//################################################################################

//# This modified function computes exactly what `atan(y,x)` computes except that
//# it will neglect signed zeros. Hence:
//#
//#   _mod_atan(0.0, -0.0) = _mod_atan(-0.0, 0.0) = 0.0
//#
//# The signed zero can lead to problems when converting from DCM to Euler angles.
function _mod_atan(y, x) {
	if (y === 0.0 || x === 0.0) {
    	return 0.0;
    }
	return Math.atan2(y, x);
}

//# This modified function computes the `acos(x)` if `|x| <= 1` and computes
//# `acos( sign(x) )`  if `|x| > 1` to avoid numerical errors when converting DCM
//# to  Euler Angles.
function _mod_acos(x) {
    if (x > 1) {
        return 0.0;
    } else if (x < -1) {
        return Math.PI;
    } else {
        return Math.acos(x);
    }
}

//# This modified function computes the `asin(x)` if `|x| <= 1` and computes
//# `asin( sign(x) )`  if `|x| > 1` to avoid numerical errors when converting DCM
//# to  Euler Angles.
function _mod_asin(x) {
    if (x > 1) {
        return Math.PI / 2;
    } else if (x < -1) {
        return -(Math.PI / 2);
    } else {
        return Math.asin(x);
    }
}
