var ascii = new object_("", 0);

ascii.screen_text;

ascii.mousehook = [];
ascii.keyhook = [];

ascii.construct = [];
ascii.render = [];
ascii.generate = [];

//TODO: find suitable location for this raycasting of camera and vrheadset object:
ascii.sphere;//raycaster collision object
// ascii.sphere2;//raycaster collision object testing if fixes bug

ascii.nodes = [];// will add vr nodes from the json object
ascii.renderNodesDiagram = [];

ascii.setModes = [];
ascii.modes = {};

ascii.construct[0] = function() {
	//RENDER SCREEN_TEXT OBJECT:
	screen_text[0] = [];
	screen_text[1] = [];//sh d w

	selectableObjects.push(new object_("screen_text_00", 0));
	screen_text[0][0] = selectableObjects[selectableObjects.length - 1];
	screen_text[0][0].object = objects.ascii[14].clone(true);
	screen_text[0][0].object.position.x += 2;
	scene.add( screen_text[0][0].object );
	// screen_text[0][18] = objects.ascii[18].clone(true);
	// scene.add( screen_text[0][18] );// S

	let l_material = new THREE.MeshBasicMaterial({ color: '#FFAAAA' })
	let l_word = "SH D W";
	screen_text[1][0] = ascii.generate[0](l_word, l_material);
	screen_text[1][0].position.set(0, 7, 10);
	scene.add(screen_text[1][0]);

	// l_material = new THREE.MeshBasicMaterial({ color: '#AAFFAA' })
	// l_word = "FEATURING";
	// screen_text[1][1] = ascii.generate[0](l_word, l_material);
	// screen_text[1][1].scale.set(0.5, 0.5, 0.5);
	// screen_text[1][1].position.set(0, 5.7, 10);

	// scene.add(screen_text[1][1]);

	// l_material = new THREE.MeshBasicMaterial({ color: '#00AAFF' })
	// l_word = "FINGERPRINT";
	// screen_text[1][2] = ascii.generate[0](l_word, l_material);
	// screen_text[1][2].position.set(0, 4, 10);
	// scene.add(screen_text[1][2]);

	// l_word = "SOFTWARES";
	// // l_material.color = '#FF0000'
	// screen_text[1][3] = ascii.generate[0](l_word, l_material);
	// screen_text[1][3].position.set(0, 3.2, 10);
	// scene.add(screen_text[1][3]);


	ascii.setModes[0]();//set modes list
	ascii.renderNodesDiagram[0]();

	//TODO: move this logic from ascii.js: raycaster sphere object:
	ascii.sphere = new THREE.Mesh( 
		new THREE.SphereGeometry( 0.2, 32, 16 ),//SphereGeometry(radius : Float, widthSegments : Integer, heightSegments : Integer, phiStart : Float, phiLength : Float, thetaStart : Float, thetaLength : Float)
		new THREE.MeshBasicMaterial( {
			color: 0x00ff00,
			transparent: true,
			opacity: 0.5
		})
	);
	// ascii.sphere.scale.set(0.5, 0.5, 0.5);
	// scene.add( ascii.sphere );

	// ascii.sphere2 = new THREE.Mesh( 
	// 	new THREE.SphereGeometry( 0.2, 32, 16 ),//SphereGeometry(radius : Float, widthSegments : Integer, heightSegments : Integer, phiStart : Float, phiLength : Float, thetaStart : Float, thetaLength : Float)
	// 	new THREE.MeshBasicMaterial( {
	// 		color: 0xff0000,
	// 		transparent: true,
	// 		opacity: 0.5
	// 	})
	// );


}

ascii.construct[1] = function(p_object) {
}

ascii.render[0] = function() {
	// RENDER the collision object with the center of the screen ascii.js

	let offsetReferenceSpace = renderer.xr.getReferenceSpace();//TODO: cater for mouse and keyboard again too.

	if (!offsetReferenceSpace) { // if our session is null - if it wasn't created
		let raycaster = new THREE.Raycaster();
		// let raycaster2 = new THREE.Raycaster();
		// let direction = new THREE.Vector3();
		var mouse = new THREE.Vector2();
		mouse.x = 0;//fixes ray at center of screen:
		mouse.y = 0;
		raycaster.setFromCamera(mouse, camera, offsetReferenceSpace);
		// raycaster2.setFromCamera(mouse, camera, offsetReferenceSpace);
		// let direction = new THREE.Vector3();
		// let axis = new THREE.Vector3(0, 1, 0);
		// let angle = Math.PI;
		// direction.copy(raycaster.ray.direction);
		// direction.applyAxisAngle(axis, angle);
		// raycaster2.ray.direction.copy(direction);
		// raycaster2.ray.direction.negate();


		scene.remove( ascii.sphere );	
		scene.remove( cuber );
		// scene.remove( ascii.sphere2 );	
		scene.remove(vrController1);
		scene.remove(vrController2);
		scene.remove(vrControllerGrip1);
		scene.remove(vrControllerGrip2);


		let intersects = raycaster.intersectObjects(scene.children);
		// let intersects2 = raycaster2.intersectObjects(scene.children);

		scene.add( cuber );
		scene.add(vrController1);
		scene.add(vrController2);
		scene.add(vrControllerGrip1);
		scene.add(vrControllerGrip2);

		if (intersects.length > 0) {
			// render sphere:
			// console.log(intersects[0].point)
			ascii.sphere.position.copy(intersects[0].point);
			ascii.sphere.scale.set(1, 1, 1);
			ascii.sphere.lookAt(camera.position);
			ascii.sphere.visible = true;
			scene.add( ascii.sphere );

		} else {
			ascii.sphere.visible = false;
		}

		// if (intersects2.length > 0) {
		// 	// render sphere:
		// 	// console.log(intersects2[0].point)
		// 	ascii.sphere2.position.copy(intersects2[0].point);
		// 	ascii.sphere2.scale.set(1, 1, 1);
		// 	ascii.sphere2.lookAt(camera.position);
		// 	ascii.sphere2.visible = true;
		// 	scene.add( ascii.sphere2 );

		// } else {
		// 	ascii.sphere2.visible = false;
		// }
	}

	if (!offsetReferenceSpace) {
		let pLocal = new THREE.Vector3(0, 0, 1);
		let pWorld = pLocal.applyMatrix4(camera.matrixWorld);
		let dir = pWorld.sub(camera.position).normalize();
		let distance = -2;
		let position = new THREE.Vector3();
		position.addVectors(camera.position, dir.multiplyScalar(distance));
		ascii.modes["currentMode"].object.position.x = position.x;
		ascii.modes["currentMode"].object.position.y = position.y - 0.2;
		ascii.modes["currentMode"].object.position.z = position.z;

		// BELOW Makes an object face the camera:
		let pos = new THREE.Vector3();
		let rotation = new THREE.Quaternion();
		let scale = new THREE.Vector3();
		// The following line extracts the position, rotation and scale in world space
		camera.matrixWorld.decompose(pos, rotation, scale);
		ascii.modes["currentMode"].object.lookAt(pos);
		ascii.modes["currentMode"].object.scale.set(0.5, 0.5, 0.5)

	} else {
		let pLocal = new THREE.Vector3(0, 0, 1);
		let pWorld = pLocal.applyMatrix4(vrController2.matrixWorld);
		let dir = pWorld.sub(vrController2.position).normalize();
		let distance = 0.2;
		let pos = new THREE.Vector3();
		pos.addVectors(vrController2.position, dir.multiplyScalar(distance));
		ascii.modes["currentMode"].object.position.x = pos.x;
		ascii.modes["currentMode"].object.position.y = pos.y;
		ascii.modes["currentMode"].object.position.z = pos.z;
		ascii.modes["currentMode"].object.scale.set(0.1, 0.1, 0.1)

		ascii.modes["currentMode"].object.rotation.x = vrController2.rotation.x;
		ascii.modes["currentMode"].object.rotation.y = vrController2.rotation.y;
		ascii.modes["currentMode"].object.rotation.z = vrController2.rotation.z;

		// console.log("REACHEDVRRIGHT")
	}

	// ascii.modes.textureMode.object.setFromNormalAndCoplanarPoint (camera.position.clone ().normalize (), scene.position)

	// ascii.modes.textureMode.object.lookAt(camera.position);
	// ascii.modes.textureMode.object.lookAt(camera.position.clone().negate());


	// ascii.modes.textureMode.object.position.set(0, 0, 1);
	// camera.add(ascii.modes.textureMode.object);
	// ascii.modes.textureMode.object.quaternion.copy(camera.quaternion);
	// ascii.modes.textureMode.object.position.copy(camera.position);
	// ascii.modes.textureMode.object.quaternion.copy(camera.quaternion);
	// ascii.modes.textureMode.object.position.x = camera.position.x;
	// ascii.modes.textureMode.object.position.y = camera.position.y;
	// ascii.modes.textureMode.object.position.z = camera.position.z;

	// ascii.nodes[0].rotation.y = ascii.nodes[0].rotation.y + 0.01;

}

ascii.keyhook[0] = function() {

}

ascii.mousehook[0] = function(p_object, evt) {
}

ascii.setModes[0] = function() {
	ascii.setModes[1]("objectMode");// A. DONE: object move mode
	ascii.setModes[1]("saveMode");// B. save current objects states - ON THE SERVER
	ascii.setModes[1]("keyboardMode");// C. Keyboard mode - First plan out the the key combo mapping
	ascii.setModes[1]("drawMode");// D. Draw mode
	ascii.setModes[1]("textureMode");	// E. object texture switching mode
	ascii.setModes[1]("scaleMode");// F. object scaling mode
	ascii.setModes[1]("characterMode");// G. charecter control mode
	ascii.setModes[1]("kinematicsMode");// H. kinematics (edit) mode
	ascii.setModes[1]("npcMode");// I. NPC instructions recorder mode
	ascii.setModes[1]("decalMode");// J. Decal mode
	ascii.setModes[1]("internalLayerMode");// K. object internal layer select (makes outer object layer semi-transparent and the inner layers clickable - and go more and more in depth wise)
	ascii.setModes[1]("terrainMode");// L. terrain mode //12
	ascii.setModes[1]("diagramMode");// M. Diagram mode
	ascii.setModes[1]("objectSwitchMode");// N. switch objects - 2d array sorting of object options -
	//render the mode we're currently in:
	ascii.modes["currentMode"] = ascii.modes["textureMode"];//ascii.modes["textureMode"];
	scene.add(ascii.modes["currentMode"].object)

}

ascii.setModes[1] = function(mode) {
	let l_word = mode;// let l_word = "SH D W";
	let	l_alpha_id;
	let	l_char_id = 0;
	let	rootWordLength = l_word.length;
	let spacing = 0.0;//spacing ratio between each layer/list of nodes

	ascii.modes[mode] = new object_(mode, 0)
	ascii.modes[mode].name = mode;
	ascii.modes[mode].object = new THREE.Group();
	for (let i = 0; i < l_word.length; i++) {
			if (l_word[i] == ' ')
				continue;
			let ascii_mode;
			l_alpha_id = l_word.toUpperCase().charCodeAt(i) - 65;
			ascii_mode = objects.ascii[l_alpha_id].clone(true);
			ascii_mode.rotation.x = 270 * Math.PI / 180;
			// ascii_mode.rotation.y = 90 * Math.PI / 180;
			ascii_mode.rotation.z = -0 * Math.PI / 180;
			ascii_mode.scale.set(0.1, 0.1, 0.1);
			ascii_mode.position.set((i - (l_word.length / 2)) * 0.1, 0.0, 0.0);
			ascii_mode.name = "ascii.modes[0]." + mode + "=" + l_char_id;
			scene.add( ascii_mode );
			l_char_id += 1;
			ascii.modes[mode].object.add(ascii_mode);
	}
	ascii.modes[mode].object.name = ascii.modes[mode].name;

}
ascii.renderNodesDiagram[0] = function() {
	let lsRecursiveData = {
		name : "Options",
		rhs : [
		  {
			name : "Degree",
			rhs : [
				{
				  name : "BCOM",
				  rhs : [
					{
					  name : "MBA",
					  rhs : []
					},
				  ]
				},
				{
				  name : "Computer Science",
				  rhs : [
					{
					  name : "MBA",
					  rhs : []
					},
				  ]
				},
			]
		  },
		  {
			name : "Science Matric",
			rhs : [
			{
				  name : "Mathematics",
				  rhs : [
					{
					  name : "MBA",
					  rhs : []
					},
				  ]
				},
				{
				  name : "Algorithms",
				  rhs : [
					{
					  name : "MBA",
					  rhs : []
					},
				  ]
				},
				{
				  name : "AI",
				  rhs : [
					{
					  name : "MBA",
					  rhs : []
					},
				  ]
				},
			]
		  },
		  {
			name : "CV",
			rhs : [
			{
				  name : "Canada Dubai",
				  rhs : [
					{
					name : "Degree",
					rhs : [
						{
						  name : "BCOM",
						  rhs : [
							{
							  name : "MBA",
							  rhs : []
							},
						  ]
						},
						{
						  name : "Computer Science",
						  rhs : [
							{
							  name : "MBA",
							  rhs : []
							},
						  ]
						},
					]
				  }
				  ]
				},
				// {
				//   name : "{inboundMessage6}",
				//   rhs : []
				// },
			]
		  },
		  {
			name : "Projects",
			rhs : [
				{
				  name : "Kinematics",
				  rhs : [
					{
					  name : "CV",
					  rhs : [
						{
						  name : "Robtics",
						  rhs : []
						},
						{
						  name : "AR VR",
						  rhs : []
						},
	
					  ]
					},
				  ]
				},
				{
				  name : "Get Launched",
				  rhs : [
					{
					  name : "CV",
					  rhs : []
					},
				  ]
				},
				{
				  name : "Iliad",
				  rhs : [
					{
					  name : "CV",
					  rhs : []
					},
				  ]
				},
	
			]
		  },
		  {
			name : "AWS cloud",
			rhs : [
			  {
				name : "CV",
				rhs : []
			  },
			  {
				name : "Expires",
				rhs : []
			  },
			]
		  },
	
		]
	};

	//render the above diagram:
	let l_padding = 5;//number of chars distance
	let l_parentLength = 0;// no parent on root node
	let l_word = lsRecursiveData.name;// let l_word = "SH D W";
	let	l_alpha_id;
	let	l_char_id = 0;
	let	rootWordLength = l_word.length;
	let spacing = 0.0;//spacing ratio between each layer/list of nodes
	ascii.nodes.push([]);

	ascii.nodes[0] = new object_("ascii.nodes[0]." + lsRecursiveData.name, 0)
	let l_material = new THREE.MeshBasicMaterial({ color: '#FFFF00' })
	l_word = lsRecursiveData.name;
	ascii.nodes[0].object = ascii.generate[0](l_word.toUpperCase(), l_material);// single object more memory use but selectable
	ascii.nodes[0].object.position.set(-10, 1, 5);
	ascii.nodes[0].object.rotation.set(0, -Math.PI / 2, 0);
	ascii.nodes[0].object.scale.set(0.1, 0.1, 0.1);
	scene.add(ascii.nodes[0].object);
	l_parentLength += l_word.length + l_padding;

	//render rhs but first calc its layers max width
	// const material = new THREE.LineBasicMaterial({ color: 0x00ffff });
	
	// const points = [];
	// points.push( new THREE.Vector3( -10, 2 + 0.05, (-(l_word.length - 1) + (l_word.length / 2)) * 0.1 ) );
	// points.push( new THREE.Vector3(-10, 2 + 0.05 + 0 * 0.1/* its position on list */, -rootWordLength * 0.2 + (-0 + (lsRecursiveData.rhs[0].name.length / 2)) * 0.1) );
	
	// const geometry = new THREE.BufferGeometry().setFromPoints( points );
	
	// const line = new THREE.Line( geometry, material );
	// scene.add( line );

	let maxWordLenRhs = 0;
	for (let j = 0; j < lsRecursiveData.rhs.length; j++) {
		l_word = lsRecursiveData.rhs[j].name;// let l_word = "SH D W";
		l_alpha_id;
		l_char_id = 0;
		if (l_word.length > maxWordLenRhs)
			maxWordLenRhs = l_word.length;
		ascii.nodes.push([]);
		// for (let i = 0; i < l_word.length; i++) {//foreach word print word on screen at position
		// 		if (l_word[i] == ' ')
		// 			continue;
		// 		l_alpha_id = l_word.toUpperCase().charCodeAt(i) - 65;
		// 		ascii.nodes[j + 1][l_char_id] = objects.ascii[l_alpha_id].clone(true);
		// 		ascii.nodes[j + 1][l_char_id].rotation.x = 270 * Math.PI / 180;
		// 		ascii.nodes[j + 1][l_char_id].rotation.z = -270 * Math.PI / 180;
		// 		ascii.nodes[j + 1][l_char_id].scale.set(0.1, 0.1, 0.1);
		// 		ascii.nodes[j + 1][l_char_id].position.set(-10, 2 + j * 0.1/* its position on list */, -rootWordLength * 0.2 + (-i + (l_word.length / 2)) * 0.1);
		// 		scene.add( ascii.nodes[j + 1][l_char_id] );
		// 		l_char_id += 1;
		// }
		ascii.nodes[j + 1] = new object_("ascii.nodes[1]." + l_word, 0);
		l_material = new THREE.MeshBasicMaterial({ color: getRandomHexColor() });
		ascii.nodes[j + 1].object = ascii.generate[0](l_word.toUpperCase(), l_material);// single object more memory use but selectable
		ascii.nodes[j + 1].object.position.set(-10, 1 + j * 0.1 /* its position on list */, 5 - l_parentLength * 0.1);
		ascii.nodes[j + 1].object.rotation.set(0, -Math.PI / 2, 0);
		ascii.nodes[j + 1].object.scale.set(0.1, 0.1, 0.1);
		scene.add(ascii.nodes[j + 1].object);
	
	}
	l_parentLength += maxWordLenRhs + l_padding;//add on for next iteration of rhs nodes
	//and its max height shifts root node up to the center of this heirarchy.
}

function getRandomHexColor() {// TODO: find appropriate storage folder for this function i.e lib
	// Define an array of hexadecimal digits
	const hexChars = [
	  '0',
	  '1',
	  '2',
	  '3',
	  '4',
	  '5',
	  '6',
	  '7',
	  '8',
	  '9',
	  'A',
	  'B',
	  'C',
	  'D',
	  'E',
	  'F',
	];
	// Generate an array of six random indices from 0 to 15
	const hexIndices = Array.from({ length: 6 }, () =>
	  Math.floor(Math.random() * 16)
	);
	// Map each index to its corresponding hex digit and join them into a string
	const hexCode = hexIndices.map((i) => hexChars[i]).join('');
	// Return the string with a "#" prefix
	return `#${hexCode}`;
};

ascii.generate[0] = function(p_word, p_material) {// Generates a single word as 1 object file instance
	// this function: takes more memory and can't change individual charecter colours but offer's moving the single instance object around in 3d space when selected
	let l_word = p_word;
	// let l_word = "SH D W";
	let	l_alpha_id;
	// let	l_char_id = 0;
	let	l_word_len = l_word.length;
	let obj3d = [];
	let wordGroup = new THREE.Group();
	for (let i = 0; i < l_word.length; i++) {
			if (l_word[i] == ' ')
				continue;
			l_alpha_id = l_word.charCodeAt(i) - 65;
			obj3d = objects.ascii[l_alpha_id].clone(true);
			obj3d.rotation.x = -90 * Math.PI / 180;
			obj3d.rotation.z = 180 * Math.PI / 180;
			obj3d.scale.set(1.0, 1.0, 1.0);
			obj3d.position.set((-i + (l_word.length / 2)) * 0.8, 0, 0);
			// screen_text[1][l_char_id].traverse( function ( child ) {
			// 	child.material = new THREE.MeshBasicMaterial({ color: '#00FFFF' });
			// });
			// screen_text[1][l_char_id].position.x = -i + (l_word.length / 2);
			// screen_text[1][l_char_id].position.y = 4;
			// screen_text[1][l_char_id].position.z = 10;
			// scene.add( screen_text[1][l_char_id] );
			// l_char_id += 1;
		obj3d.updateMatrixWorld();
		wordGroup.add(obj3d);
	}
	var exporter = new OBJExporter();
	return load_obj(exporter.parse(wordGroup), [], true, p_material, false, true);	//converts all group objects into a single object instance
}
// var ascii = new object_("", 0);
// ascii.origin;

// ascii.mousehook = [];
// ascii.keyhook = [];

// ascii.construct = [];
// ascii.render = [];
// ascii.generate = [];


// ascii.construct[0] = function() {
// }

// ascii.construct[1] = function(p_object) {
// }

// ascii.render[0] = function() {
// }

// ascii.keyhook[0] = function() {

// }

// ascii.mousehook[0] = function(p_object, evt) {

// }