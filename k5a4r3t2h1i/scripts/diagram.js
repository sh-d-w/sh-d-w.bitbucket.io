// TODO: 
// 1. fix padding top

// node info (for updating):
var nodes = [];// we can search this later against box items that share the mouses coordinates
var prevSelectedNode = null;
// TODO: must sort these nodes by x coordinates for optimisation purposes
// 1. firstly onmouseover of the canvas should call a function that takes the mouse position and iterates through the nodes until it finds one with the same node.dimensions.topLeft and node.dimensions.topRight if so check same node.dimentions.bottomLeft and node.dimensions.bottomRight

// meta data:
var xOffsetSpacing = 20;
var yOffsetSpacing = 2 + 9;
var boxPadding = 2 + 2;
var startPaddingYOffset = 20;
var startPaddingXOffset = 20;
var colourSpectrumStart = 10;

var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");

// let messageBox = {
// 	"x" : 10,
// 	"y" : 25,
// 	"size" : 18,
// 	"message" : "Hello world",
//     "color" : "orange"
// };

// //console.log(messageBox);
// let box1 = nodeAlgorithm["linear"].drawWordBox(ctx, messageBox, true);
// //messageBox.y = 3000;
// box1.x = 100;
// box1.color = "red";
// box1.message = "new Object()"
// nodeAlgorithm["linear"].drawWordBox(ctx, box1, true);
// messageBox.y = 100;
// nodeAlgorithm["linear"].drawWordBox(ctx, messageBox, true);
let windowWidth = ctx.canvas.width;//  = window.innerWidth;
let windowHeight = ctx.canvas.height;// = window.innerHeight;
// render all the recursive directories:


// CIRCULAR DIAGRAM VARIABLES - stored here because javascript cries for no good reason when including multiple scripts:
// var xOffsetSpacing = 0;//10//20;
// var yOffsetSpacing = 2 + 9;
// var boxPadding = 2 + 2;
// var startPaddingYOffset = 20;
// var colourSpectrumStart = 10;
// var nodes = [];

let centerX = canvas.width / 2, centerY = canvas.height / 2;// todo set to whereever it calculates is needed to be dependant on larger diagrams

let rootParentNode;

var parentBoxHeight;
var totalMaxYLength = 0;

var prevAngle;// to know what was the angle of the previous node?
var prevAngleLayer;// to know if it came from a parent or a child to know what to do in that case


let strokeStyles = [// TODO store these for future
  '#ff7571',// 0 peach
  '#ff3b9e',// 1 pink
  '#ff3cc7',// 2 more pink
  '#f440c6',// 3 even more pink
  '#f53cf3',// 4 purplish
  '#d03bf7',// 5 purple
  '#d03bff',// 6 more purple
  '#ac3aff',// 7 even more purple
  '#a478ff',// 8 lilack
  '#8079ff',// 9 indigo
  '#419eff',// 10 blueish
  '#39c1ff',// 11 light blue
  '#2ee3e4',// 12 teal
  '#1effb5',// 13 turquise
  '#1eff8d',// 14 light green
  '#66ff66',// 15 bright green
  '#8dff65',// 16 yellow green
  '#b4ff14',// 17 light yellowish
  '#e1e01a',// 18 yellow
  '#ffbb1e',// 19 light orange
  '#ff9820',// 20 orange
]

var greyedOutColor = "#3e4952";//"#495a60";//"#889ca4";//"#555555";
var collapsedColor = "#FFFFFF"//"#FF00FF";
var redOutColor = "#b36eda";//scifi pink now //"rgb(255,80,80)";
var greenedOutColor = "#00FFFF";//"#adfbf8";//scifi neon blue //"#39FF14";
var intermediateOutColor = "#a9c6e5";// scifi mid indigoish blue
var specialColors = [
  // greyedOutColor, greyed out isn't special per say.
  redOutColor,
  greenedOutColor,
  intermediateOutColor,
  // "rgb(255,80,80)",
]

var nodeAlgorithm = { // will store the functions so they can be recalled accordingly
  circular: {},
  linear: {}
}
var nodeAlgorithmCurrentType = "linear";

  // render current item
  var rootNodeBox;
  
  // rootNodeBox.y = (windowHeight / 2) - (rootNodeBox.topLeft.y / 2);
    // rootNodeBox = nodeAlgorithm["linear"].getWordBox(ctx, rootNodeBox);
    // console.log(rootNodeBox);
    // nodeAlgorithm["linear"].drawWordBox(ctx, rootNodeBox, true);
  // foreach (item in current.rhs) {
    // recursiveNodeRender(lsRecursiveData.rhs, rootNodeBox.topRight.x, rootNodeBox);
    var layerMax;//an extending array of the tracking of the MaxXLength of each Layer
    // layerMax.push({nodeXmin : 0, nodeXmax : 0, nodeCount : 0});
    // layerMax = 


nodeAlgorithm["linear"].reRenderDiagram = function() {
  // This is how to reload the tree: TODO put into its on reloadUI() method:
  layerMax = []// always reset this because we build this information up when calculating the positions below:

  if (window.lsRecursiveData) {
    lsRecursiveData = window.lsRecursiveData;
  }
  rootNodeBox = nodeAlgorithm["linear"].getWordBox(ctx, {
    "x" : 0,
    "y" : 0,
    "size" : 10,
    "message" : lsRecursiveData.name.trim(),
    "color" : "#00FFFF",
    "offset" : {//offset will be the spacing you want between the nodes
      "x" : 10, 
      "y" : 10
    }
  });

  // reset all temp variables:
  nodes = [];
  prevSelectedNode = null;
  currentHighlightedNode = null;
  emphasizedStateNode = null;
  foundANode = false;

  canvas = document.getElementById("myCanvas")
  canvas.getContext("2d").clearRect(0, 0, canvas.width, canvas.height);

  nodeAlgorithm["linear"].recursiveRightMostFIrstLayeredNodeTrack([lsRecursiveData], 0, layerMax, rootNodeBox, true);//do not skip the first element
  nodeAlgorithm["linear"].recursiveRightMostFIrstNodeRender([lsRecursiveData], 0, 0, layerMax, rootNodeBox, null/* lhs */, true); 
}

nodeAlgorithm["linear"].recursiveRightMostFIrstLayeredNodeTrack = function(rhs, layer, layerMax, rootNodeBox, firstRender) {
  //gets the maxXLength stored per layerCount in layerMax[layerNbr]:
  // let maxXLength = layerMax[layer].nodeXmin;
  // let rhsLength = rhs.length;
  let count = 1;
  let numberOfNodes = 0;

  // if (!rhs) {
  //   rhs = [];// just add an empty rhs if there isn't one to make the algo work smooth else we could even null check it which would be better but this is easier now..
  // }

  //1. this loop prioritises getting the maxXLength for the Layer
  if (layer >= layerMax.length)
    layerMax.push({nodeXmin : 0, nodeXmax : 0, nodeCount : 0, currentPosY : 0});
  let childNodeBox;
  if (rhs)
    rhs.forEach(rhsNode => {

        //render each rhs node:
        childNodeBox = nodeAlgorithm["linear"].getWordBox(ctx, {
          "x" : 0,
          "y" : 0,
          "size" : rootNodeBox.size,
          "message" : rhsNode.name.trim(),
          "color" : "orange",// unused currently as using only roots color value...
          // TODO: these items loose their root properties like collapsed etc, thus need to ensure those properties are kept so maybe make the initial instance off the item itself.
          "collapsed": rhsNode.collapsed,
        });
        //calculate the max of this layer:
        // console.log({"LLAYERR" : layer, "BRX" : childNodeBox.bottomRight.x, "MAX" : layerMax[layer].nodeXmax })
        if (childNodeBox.bottomRight.x >= layerMax[layer].nodeXmax) {
          layerMax[layer].nodeXmin = childNodeBox.bottomLeft.x;
          layerMax[layer].nodeXmax = childNodeBox.bottomRight.x;
        }
    
      if (!rhsNode.collapsed) {
        nodeAlgorithm["linear"].recursiveRightMostFIrstLayeredNodeTrack(rhsNode.rhs, layer + 1, layerMax, rootNodeBox);//do not skip the first element
      }
    });
  return layerMax;
}
    
nodeAlgorithm["linear"].recursiveRightMostFIrstNodeRender = function(rhs, totalMaxXLength, layer, layerMax, rootNodeBox, lhs, firstRender) {

  // let maxXLength = 0;
  let rhsLength = rhs.length;
  // let count = 1;

  //2. this loop subtracts the maxXLength of current layer to get starting x position of current layer
  //   and adds this to the sub total for the previous layer to know where it should begin itself on the x axis:
  let boxSize = Math.abs(rootNodeBox.bottomLeft.y - rootNodeBox.topLeft.y);// can optimise this by not keeping it here in the recursive loop as just needs single calculation initially
  let childNodeBox;
  let rhsIndex = -1;
  rhs.forEach(rhsNode => {
      ++rhsIndex;
      // DONE: switch to loop recursive 1 time to get the meta data
      // DONE: and then loop a second time to render the meta data
      //render each rhs node
      childNodeBox = JSON.parse(JSON.stringify(rootNodeBox));
      childNodeBox.message = rhsNode.name;
      childNodeBox.collapsed = rhsNode.collapsed;
      childNodeBox.specialColor = rhsNode.specialColor;
      childNodeBox.x = totalMaxXLength;//totalMaxXLength;
      if (rhsNode.rhs && rhsNode.rhs.length > 0 && !rhsNode.collapsed) {
        // get the yposition for this LHS node:
        nodeAlgorithm["linear"].recursiveRightMostFIrstNodeRender(rhsNode.rhs, totalMaxXLength + layerMax[layer].nodeXmax + xOffsetSpacing, layer + 1, layerMax, rootNodeBox, rhsNode);// totalMaxXLength + maxXLength, layer
        // childNodeBox.y = rhsMetaData.lhsYPos;
        //      childNodeBox.y = ((layerMax[layer + 1].nodeCount + 1) - (rhsNode.rhs.length / 2)) * boxSize - (boxSize / 2);
      
        // totalMaxXLength = rhsMetaData.totalMaxXLength;
        //DONE: SWITCH to use this formula instead:
        childNodeBox.y = rhsNode.rhs[0].dimensions.topRight.y + 1 + (Math.abs(rhsNode.rhs[0].dimensions.topRight.y - rhsNode.rhs[rhsNode.rhs.length - 1].dimensions.bottomRight.y) / 2) + (boxSize / 4);// div 4 but works
      }
      else {
        // if no rhs :
        // childNodeBox.y = (windowHeight / (rhsLength + 1)) * (count) - (boxSize / 2);//rhsMetaData.lhsYPos;
        if (layer == layerMax.length - 1) {
          childNodeBox.y = (layerMax[layer].nodeCount + 1) * boxSize;//rhsMetaData.lhsYPos;
        } else {
        //DONE: missing scenario:
        //use current layerMax[layerMax.length - 1].bottomRight.y if there is no rhs
          if (layerMax[layerMax.length - 1].currentPosY > 0) {
            childNodeBox.y = layerMax[layerMax.length - 1].currentPosY + boxSize;
            layerMax[layerMax.length - 1].currentPosY += boxSize;
          } else {
            // childNodeBox.y = (layerMax[layer].nodeCount + 1) * boxSize;//rhsMetaData.lhsYPos;
            childNodeBox.y = (layerMax[layer].nodeCount + 1) * boxSize;//rhsMetaData.lhsYPos;
            // childNodeBox.y = 0 + boxSize;
          }
        }

      }
      childNodeBox = nodeAlgorithm["linear"].getWordBox(ctx, childNodeBox);
      // console.log(childNodeBox);
      // console.log(totalMaxXLength.toString() + JSON.stringify(rhsMetaData));
      // console.log({"boxsize" : boxSize, "layerMax+1" : layerMax, "LYR" : layer});

      //store the nodeSize in the object itself:
      childNodeBox.dimensions = rhsNode.dimensions = {
        topRight : childNodeBox.topRight,
        bottomRight : childNodeBox.bottomRight,
        topLeft : childNodeBox.topLeft,//unused?? No longer unused since can select nodes
        bottomLeft : childNodeBox.bottomLeft,//unused?? No longer unused "  "   "
        message: rhsNode.name
        //...
      }

      if (childNodeBox.bottomRight.y > layerMax[layerMax.length - 1].currentPosY) {
        layerMax[layerMax.length - 1].currentPosY = childNodeBox.bottomRight.y + yOffsetSpacing;
      }

      layerMax[layer].nodeCount += 1;
      let colour = strokeStyles[(rhsIndex + colourSpectrumStart) % 20];
      let rhsNodes = rhsNode.collapsed ? [] : (rhsNode.rhs ? rhsNode.rhs : []);

      // if this is emphasizedNode or a child of emphasized node, then be in the below color:
      let emphasizedParent = nodeAlgorithm["linear"].getEmphasizedParentNode(rhsNode);

      let currentColor =  specialColors.includes(rhsNode.specialColor) ? rhsNode.specialColor : colour;
      if (!emphasizedStateNode || emphasizedParent) {
        nodeAlgorithm["linear"].drawWordBox(ctx, childNodeBox, true, [rhsNode, ...rhsNodes], rhsIndex, currentColor);
      } else {
        nodeAlgorithm["linear"].drawWordBox(ctx, childNodeBox, true, [rhsNode, ...rhsNodes], rhsIndex, greyedOutColor);
      }
      // add the node to the array for future rearrangement here:
      rhsNode.colour = colour;
      rhsNode.message = rhsNode.name;
      rhsNode.x = childNodeBox.x;
      rhsNode.y = childNodeBox.y;
      rhsNode.lhs = lhs;
      nodes.push({ node: rhsNode });
  });
}


    
// function  recursiveNodeRender(rhs, totalMaxXLength, rootNodeBox) {
// this was no rhs first attempt which currently has its flaws:

//   let maxXLength = 0;
//   let rhsLength = rhs.length;
//   let count = 1;
//   rhs.forEach(rhsNode => {
//     //render each rhs node
//     let childNodeBox = JSON.parse(JSON.stringify(rootNodeBox));
//     childNodeBox.x = totalMaxXLength;
//     childNodeBox.y = Math.abs(rootNodeBox.topLeft.y - rootNodeBox.bottomLeft.y/*padding gap*/) * count;
//     childNodeBox.message = rhsNode.name;
//     childNodeBox = nodeAlgorithm["linear"].getWordBox(ctx, childNodeBox);
//     console.log(childNodeBox);
//     nodeAlgorithm["linear"].drawWordBox(ctx, childNodeBox, true);
//     //calculate the max of this layer:
//     if (childNodeBox.topRight.x - totalMaxXLength > maxXLength)
//       maxXLength = childNodeBox.topRight.x - totalMaxXLength;

//     count += 1;
//   });

//   rhs.forEach(rhsNode => {

//     //render each rhs->rhs node (rhs most):
//     recursiveNodeRender(rhsNode.rhs, totalMaxXLength + maxXLength, rootNodeBox);
//   });
// }

nodeAlgorithm["linear"].getWordBox = function(ctx, msgBox) {
  if (msgBox.message.includes('\n')) {
    return nodeAlgorithm["linear"].getMultiLineWordBox(ctx, msgBox);// allows for a single node to span over multiple lines
  }
  ctx.font = msgBox.size + "px Arial";
  let measureTextCorrection = msgBox.message.substring(0, msgBox.message.length - 1) + "q";// q gives node maximum font height when measuring so we get consistency
  let internalMetrics = ctx.measureText(measureTextCorrection);

  msgBox.actualBoundingBoxLeft = internalMetrics.actualBoundingBoxLeft;
  msgBox.actualBoundingBoxAscent = internalMetrics.actualBoundingBoxAscent;
  msgBox.actualBoundingBoxRight = internalMetrics.actualBoundingBoxRight;
  msgBox.actualBoundingBoxDescent = internalMetrics.actualBoundingBoxDescent;

  nodeAlgorithm["linear"].updateWordBox(msgBox);

  let outBox = JSON.parse(JSON.stringify(msgBox));//duplicate json Object  
  return outBox;
}

nodeAlgorithm["linear"].getMultiLineWordBox = function(ctx, msgBox) {
    ctx.font = msgBox.size + "px Arial";
  
    // then should split and iterate through the lines and increase the bottom descents  
    let internalMetrics = JSON.parse(JSON.stringify(ctx.measureText("")));// empty just to initialise the structure, but copy it so is not immutable

    // console.log("INT METRICS 1: ", internalMetrics, ctx.measureText(msgBox.message));

    let splitstr = msgBox.message.trim().split('\n');
    let maxLen = 0;
    let maxLenStr;
    for (let i=0; i < splitstr.length; i++) {
      let len = splitstr[i].length;
      if (len > maxLen) {
        maxLen = len;
        maxLenStr = splitstr[i];
      }
    }

    let im;
    let abbs = 0;
    for (let i=0; i < splitstr.length; i++) {
      im = ctx.measureText(splitstr[i]);
      abbs += im.fontBoundingBoxAscent + im.fontBoundingBoxDescent;
      // console.log("INT METRICS loop: ", im, splitstr[i])
      if (i === 0) {
        internalMetrics.actualBoundingBoxAscent = im.actualBoundingBoxAscent;
        internalMetrics.actualBoundingBoxLeft = im.actualBoundingBoxLeft;
        // internalMetrics.actualBoundingBoxDescent = im.actualBoundingBoxDescent;// + im.fontBoundingBoxAscent + im.fontBoundingBoxDescent;
      }
      // else {
        // internalMetrics.actualBoundingBoxDescent += i * (im.fontBoundingBoxAscent /*+ im.fontBoundingBoxDescent*/);// + im.actualBoundingBoxDescent;// increments the descent
      // }
      if (splitstr[i].length === maxLen) {
        internalMetrics.actualBoundingBoxRight = im.actualBoundingBoxRight;
      }
    }
    console.log(im)

    internalMetrics.actualBoundingBoxDescent = (abbs) - ((im.fontBoundingBoxAscent + im.fontBoundingBoxDescent) / 1);


    // console.log("INT METRICS 2: ", internalMetrics, maxLen);

    msgBox.actualBoundingBoxLeft = internalMetrics.actualBoundingBoxLeft;
    msgBox.actualBoundingBoxAscent = internalMetrics.actualBoundingBoxAscent;
    msgBox.actualBoundingBoxRight = internalMetrics.actualBoundingBoxRight;
    msgBox.actualBoundingBoxDescent = internalMetrics.actualBoundingBoxDescent;
  
    nodeAlgorithm["linear"].updateWordBox(msgBox);
  
    let outBox = JSON.parse(JSON.stringify(msgBox));//duplicate json Object  
    return outBox;
}

nodeAlgorithm["linear"].updateWordBox = function(msgBox) {
  msgBox.topLeft = {
  	"x" : msgBox.x - msgBox.actualBoundingBoxLeft,
    "y" : msgBox.y - msgBox.actualBoundingBoxAscent
  };

  msgBox.topRight = {//msgBox.bottomLeft = {
  	"x" : msgBox.x + msgBox.actualBoundingBoxRight,
  	"y" : msgBox.y - msgBox.actualBoundingBoxAscent
  };

  msgBox.bottomRight = {
  	"x" : msgBox.x + msgBox.actualBoundingBoxRight,
    "y" : msgBox.y + msgBox.actualBoundingBoxDescent
  };

  msgBox.bottomLeft = {//msgBox.topRight = {
  	"x" : msgBox.x - msgBox.actualBoundingBoxLeft,
  	"y" : msgBox.y + msgBox.actualBoundingBoxDescent
  }

  return msgBox;
}

nodeAlgorithm["linear"].drawWordBox = function(ctx, msgBox, fill, rhs, rhsNodeIndex, colour) {
  let fillColor = colour//msgBox.color;
  // let strokeColor = "green"//fillColor//msgBox.color;

  nodeAlgorithm["linear"].renderNodeBoxWithText(msgBox, fillColor);
  //if drawlines:
  if (rhs && rhs.length > 0) {
    // ctx.strokeStyle = strokeColor//"black";
    // console.log(rhs)
    let boxSize = Math.abs(rhs[0].dimensions.topRight.y - rhs[0].dimensions.bottomRight.y);
    let halfBoxSize = boxSize / 2;
    for (let i = 1; i < rhs.length; i++) {
      ctx.strokeStyle = strokeStyles[(i + colourSpectrumStart) % 20];

      // below draws the lines between the parent and child node:
      ctx.beginPath();
      ctx.moveTo(
          rhs[0].dimensions.topRight.x + startPaddingXOffset,
          rhs[0].dimensions.topRight.y + halfBoxSize + startPaddingYOffset
      );
      ctx.lineTo(
        rhs[i].dimensions.topLeft.x + startPaddingXOffset,
        rhs[i].dimensions.topLeft.y + halfBoxSize + startPaddingYOffset
      );
      ctx.closePath();
      ctx.stroke();
    }
  }
  
  return msgBox;
}

nodeAlgorithm["linear"].basicColorNodeBoxWithText = function(msgBox, color) {
  ctx.fillStyle = color;//msgBox.color;
  ctx.strokeStyle = color;//msgBox.color;
  ctx.font = msgBox.size + "px Arial";
}

nodeAlgorithm["linear"].renderNodeBoxWithText = function(msgBox, color) {
  // if (msgBox.name === "parent") {
  //   console.log("<<<<>>>>> found ", msgBox);
  //   console.log((new Error()).stack);

  // }
  msgBox.prevColor = msgBox.currentColor;

  // if (msgBox.specialColor && color === greyedOutColor) {
  // if (msgBox.specialColor && color !== greyedOutColor && color !== "orange") {
  if (msgBox.specialColor && color !== greyedOutColor && color !== "orange") {
    color = msgBox.specialColor;// override greys to specialColor in the even they have been specially coloured.
  }
  msgBox.currentColor = color;
  // if (color != null && specialColors.includes(color)) {
  //   msgBox.specialColor = color;// if user specified a special color it should stay a special color until that is disabled on the node.
  // }
  nodeAlgorithm["linear"].basicColorNodeBoxWithText(msgBox, color);
  // Historical: MANUAL DRAWING OF THE BOX:
  // ctx.beginPath();
  // ctx.moveTo(
  //     msgBox.topLeft.x - boxPadding,
  //     msgBox.topLeft.y + - boxPadding
  // );
  // ctx.lineTo(
  //   msgBox.bottomLeft.x - boxPadding,
  //   msgBox.bottomLeft.y + boxPadding
  // );
  // ctx.lineTo(
  //   msgBox.bottomRight.x + boxPadding, 
  //   msgBox.bottomRight.y + boxPadding
  // );
  // ctx.lineTo(
  // 	msgBox.topRight.x + boxPadding,
  //   msgBox.topRight.y - boxPadding
  // );
  // ctx.closePath();
  // ctx.fill();
  // ctx.strokeStyle = "blue";

  // Latest: Using existing box drawing capabilities with radius edges:
  let boxWidth = (msgBox.dimensions.topRight.x + boxPadding) - (msgBox.dimensions.topLeft.x - boxPadding)
  let boxHeight = (msgBox.dimensions.bottomLeft.y + boxPadding) - (msgBox.dimensions.topLeft.y + - boxPadding)

  // ctx.lineWidth = "20px";
  // ctx.strokeStyle = 'white';

  ctx.beginPath();

  ctx.roundRect(msgBox.dimensions.topLeft.x - boxPadding + startPaddingXOffset, msgBox.dimensions.topLeft.y + - boxPadding + startPaddingYOffset, boxWidth, boxHeight, [10]);
  // ctx.strokeStyle = "#000000"//msgBox.color;
  // ctx.stroke();
  ctx.fill();
  // ctx.roundRect(10, 20, 150, 100, [40]);
  // ctx.closePath();

  // ctx.strokeStyle = "black";

  // ctx.beginPath();
  // console.log("MMSSSSG: " + msgBox.message)
  ctx.fillStyle = "#000000";

  // TODO check for '\n' then split and of the font dimensions, we need to use that to know how much to increment by
  if (msgBox.message.includes('\n')) {
    // text spanning multiple lines:
    let im = ctx.measureText("");
    let yLineIncrement = im.fontBoundingBoxAscent + im.fontBoundingBoxDescent;
    let msgs = msgBox.message.trim().split("\n");
    for (let i=0; i < msgs.length; i++) {
      let lineHeight = i * yLineIncrement;
      ctx.fillText(msgs[i] + " \n", msgBox.x + startPaddingXOffset, msgBox.y + startPaddingYOffset + lineHeight);
    }

  } else {
    // basic text not spanning multiple lines:
    let fixLineSpacing = " \n";
    ctx.fillText(msgBox.message + fixLineSpacing, msgBox.x + startPaddingXOffset, msgBox.y + startPaddingYOffset);
  }

  // ctx.lineWidth = "200px";
  if (msgBox.collapsed) {
    ctx.strokeStyle = collapsedColor;//nodeAlgorithm["linear"].invertColor(color);
    ctx.beginPath();
    ctx.roundRect(msgBox.dimensions.topLeft.x - boxPadding + startPaddingXOffset -1, msgBox.dimensions.topLeft.y + - boxPadding + startPaddingYOffset - 1, boxWidth + 2, boxHeight + 2, [10]);
    // ctx.strokeStyle = "#000000"//msgBox.color;
    ctx.stroke();
    ctx.roundRect(msgBox.dimensions.topLeft.x - boxPadding + startPaddingXOffset -2, msgBox.dimensions.topLeft.y + - boxPadding + startPaddingYOffset - 2, boxWidth + 4, boxHeight + 4, [10]);
    // ctx.strokeStyle = "#000000"//msgBox.color;
    ctx.stroke();
  }
  // ctx.strokeStyle = "white";//msgBox.color;
  // ctx.lineWidth = "5px";
  // ctx.stroke();
  // ctx.closePath();

//   let randRot = Math.floor((Math.random() * 10)) * (-1 + 2 * ((Math.random() * 2) === 1));
//   ctx.rotate((randRot * Math.PI) / 180);
// ctx.rotate((-randRot * Math.PI) / 180);

}

// https://stackoverflow.com/questions/35969656/how-can-i-generate-the-opposite-color-according-to-current-color:
nodeAlgorithm["linear"].invertColor = function(hex) {
  if (hex.indexOf('#') === 0) {
      hex = hex.slice(1);
  }
  // convert 3-digit hex to 6-digits.
  if (hex.length === 3) {
      hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
  }
  if (hex.length !== 6) {
      throw new Error('Invalid HEX color.');
  }
  // invert color components
  var r = (255 - parseInt(hex.slice(0, 2), 16)).toString(16),
      g = (255 - parseInt(hex.slice(2, 4), 16)).toString(16),
      b = (255 - parseInt(hex.slice(4, 6), 16)).toString(16);
  // pad each with zeros and return
  return '#' + nodeAlgorithm["linear"].padZero(r) + nodeAlgorithm["linear"].padZero(g) + nodeAlgorithm["linear"].padZero(b);
}

nodeAlgorithm["linear"].padZero = function(str, len) {
  len = len || 2;
  var zeros = new Array(len).join('0');
  return (zeros + str).slice(-len);
}

nodeAlgorithm["linear"].getMousePos = function(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };
}

var foundANode = false;// var to expose it to be used elsewhere.
var currentHighlightedNode;// exposes the current/last highlighted node
var emphasizedStateNode;

nodeAlgorithm["linear"].foundEmphasizedNodeIn = function(node) {
  if (!emphasizedStateNode)
    return false;

  let found = false;
  let tmpNode = node;
  while (tmpNode) {// lhs traversal
    if (tmpNode === emphasizedStateNode) {
      // nodeAlgorithm["linear"].renderLhsNodes(tmpNode.lhs, "#FFFFFF")
      // then default colors
      found = true;
      break;
    }
    tmpNode = tmpNode.lhs;
  }
  tmpNode = node;
  while (tmpNode) {// rhs traversal
    if (tmpNode === emphasizedStateNode) {
      found = true;
      break;
    }
    tmpNode = tmpNode.rhs;
  }
  if (!found) {// then greyscale it and its children as isn't emphasized/focused:
    nodeAlgorithm["linear"].renderLhsNodes(node.lhs, greyedOutColor);//LHS nodes should never take on special colors, only node itself and RHS nodes of it can
    // nodeAlgorithm["linear"].renderLhsNodes(emphasizedStateNode, null)

    let revertColor = greyedOutColor;
    nodeAlgorithm["linear"].renderNodeBoxWithText(node, revertColor);
    if (!node.collapsed) {
      nodeAlgorithm["linear"].renderRhsNodes(node.rhs, revertColor);
    }

    let foundCollapsedParent = nodeAlgorithm["linear"].getCollapsedParent(emphasizedStateNode);
    if (foundCollapsedParent) {
      nodeAlgorithm["linear"].renderLhsNodes(foundCollapsedParent, null)
    } else {
      nodeAlgorithm["linear"].renderLhsNodes(emphasizedStateNode, null)
    }
    if (!emphasizedStateNode.collapsed && !foundCollapsedParent) {
      nodeAlgorithm["linear"].renderRhsNodes(emphasizedStateNode.rhs, null)
    }
  }
  // else {// FOR DEBUG PURPOSES ONLY:
  //   nodeAlgorithm["linear"].renderLhsNodes(node, "#FFFFFF")
  //   nodeAlgorithm["linear"].renderRhsNodes(node.lhs.rhs, "#FFFFFF")
  // }

  return false;// else not found any instance of the emphasized node thus you know what color to choose.
}

nodeAlgorithm["linear"].mouseMove = function(evt) {
  var pos = nodeAlgorithm["linear"].getMousePos(canvas, evt);
  foundANode = false;

  // loop through nodes and check x coords
  for (let i=0; i < nodes.length; i++) {
    if (nodes[i].node.dimensions.topLeft.x + startPaddingXOffset < pos.x && nodes[i].node.dimensions.topRight.x + startPaddingXOffset > pos.x) {
      if (nodes[i].node.dimensions.topLeft.y + startPaddingYOffset < pos.y && nodes[i].node.dimensions.bottomLeft.y + startPaddingYOffset > pos.y) {
        // then default prevSelectNode based on
        if (!nodeAlgorithm["linear"].isParentCollapsed(nodes[i].node)) {

          let currentNode = nodes[i].node;
          let isNotSameNode = prevSelectedNode !== currentNode;
          foundANode = true;
          if (prevSelectedNode && isNotSameNode) {
            nodeAlgorithm["linear"].renderNodeBoxWithText(prevSelectedNode, prevSelectedNode.colour);

            nodeAlgorithm["linear"].renderLhsNodes(prevSelectedNode, null);
          }
          if (isNotSameNode || (emphasizedStateNode)) {
            nodeAlgorithm["linear"].renderNodeBoxWithText(nodes[i].node, "orange")
            nodeAlgorithm["linear"].renderLhsNodes(currentNode.lhs, "orange");
          }
          if (isNotSameNode) {
            currentHighlightedNode = currentNode;
          }
          prevSelectedNode = nodes[i].node;
          // console.log("found node: " + nodes[i].node.name)
          break;
        }
      }
    }
  }
  if (!foundANode && prevSelectedNode) {
    let foundCollapsedParent = nodeAlgorithm["linear"].getCollapsedParent(prevSelectedNode);
    if (!foundCollapsedParent) {
      let newColor = prevSelectedNode.colour//prevSelectedNode.specialColor ? prevSelectedNode.specialColor : prevSelectedNode.colour;
      nodeAlgorithm["linear"].renderNodeBoxWithText(prevSelectedNode, newColor);
      nodeAlgorithm["linear"].renderLhsNodes(prevSelectedNode.lhs, null);
    } else {
      nodeAlgorithm["linear"].renderLhsNodes(foundCollapsedParent, null);
    }

    // TODO: fix issue of items in children of collapsed: getting rendered: whether collapsed parent or not and not found a node then:
    nodeAlgorithm["linear"].foundEmphasizedNodeIn(prevSelectedNode);
    prevSelectedNode = null;
  }
  // context.fillStyle = "#000000";
  // context.fillRect (pos.x, pos.y, 4, 4);
}

function mouseMove(evt) {
  // nodeAlgorithmCurrentType = "circular"
  // nodeAlgorithm[nodeAlgorithmCurrentType].reRenderDiagram();

  // console.log(nodeAlgorithm)
  if (nodeAlgorithmCurrentType === "linear") {
    nodeAlgorithm["linear"].mouseMove(evt);
  } else if (nodeAlgorithmCurrentType === "circular") {
    nodeAlgorithm["circular"].mouseMove(evt);
  }
}


nodeAlgorithm["linear"].isParentCollapsed = function(node) {
  let parent = node.lhs;
  while (parent) {
    if (parent.collapsed === true) {
      // console.log(node.name);
      return true;
    }
    parent = parent.lhs;
  }
  return false;
}

nodeAlgorithm["linear"].getCollapsedParent = function(node) {
  let deepestCollapse = null;

  let parent = node.lhs;
  while (parent) {
    if (parent.collapsed === true) {
      deepestCollapse = parent;
    }
    parent = parent.lhs;
  }
  return deepestCollapse;
}

nodeAlgorithm["linear"].getEmphasizedParentNode = function(node) {
  if (!emphasizedStateNode)
    return null;

  let parent = node.lhs;
  while (parent) {
    if (parent === emphasizedStateNode) {
      return parent;
    }
    parent = parent.lhs;
  }
  return null;
}

nodeAlgorithm["linear"].renderLhsNodes = function(lhs, colour) {
  while (lhs) {
    nodeAlgorithm["linear"].renderNodeBoxWithText(lhs, colour ? colour : lhs.colour);
    lhs = lhs.lhs;
  }
}

nodeAlgorithm["linear"].renderRhsNodes = function(rhs, colour) {
  if (rhs) {
    for (let i = 0; i< rhs.length; i++) {
      nodeAlgorithm["linear"].renderNodeBoxWithText(rhs[i], colour ? colour : rhs[i].colour);
      if (!rhs[i].collapsed) {
        nodeAlgorithm["linear"].renderRhsNodes(rhs[i].rhs, colour)
      }
    }
  }
}

nodeAlgorithm["linear"].renderRhsNodesGreyExcept = function(parent, exceptNode) {
  if (parent && parent.rhs) {
    for(let i=0; i< parent.rhs.length; i++) {
      if (parent.rhs[i] !== exceptNode) {
        //DONE: color is already in color and we don't mess with that, just the temp rendering. // backup originalNodeColor : todo rather back it up on first render in originalColor on itself : later can add a userColor
        // set the color to grey (in temp rendering):
        nodeAlgorithm["linear"].renderNodeBoxWithText(parent.rhs[i], greyedOutColor/* grey or special color supposedly */);
        if (!parent.rhs[i].collapsed) {
          nodeAlgorithm["linear"].renderRhsNodesGreyExcept(parent.rhs[i], exceptNode);// recur for all sub children except exceptNodes children.
        }
      } else {
        // nodeAlgorithm["linear"].getCollapsedParent(exceptNode)
        nodeAlgorithm["linear"].renderLhsNodes(exceptNode, null/* back to its default color */); // all of exceptNodes parents mustn't be grey so undo it now that we past those nodes.
        if (!exceptNode.collapsed) {
          nodeAlgorithm["linear"].renderRhsNodes(exceptNode.rhs, null);// revert all its children to default colors
        }
      }
    }
  }
}

nodeAlgorithm["linear"].renderSubsectionInColor = function(node, color, isSpecialColor) {
  if (node) {
    node.specialColor = (isSpecialColor ? color : node.colour);// save the specialColor
    console.log("value: ", node);
    nodeAlgorithm["linear"].renderNodeBoxWithText(node, color);

    if (node.rhs && !node.collapsed) {
      for(let i=0; i< node.rhs.length; i++) {
        nodeAlgorithm["linear"].renderSubsectionInColor(node.rhs[i], color, isSpecialColor);
      }
    }
  }
}




//RENDERS NODES FROM LEFT TO RIGHT:
nodeAlgorithm[nodeAlgorithmCurrentType].reRenderDiagram();// first time render here actually
// }
// recur its rhs but with max.x length of the previous set
