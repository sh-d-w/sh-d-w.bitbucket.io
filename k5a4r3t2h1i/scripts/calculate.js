#!/opt/homebrew/bin/node

// [story points] PLANNING / next steps:
// 1. [3] Interval Value generation of any function of x? such that it renders a table of all the values within an interval that makes sense to try.
// 2. [5] Interval interpreter to know included and excluded value or not.
    // supposed syntax: x E (a, b), x E [a, b], x E [a, b) or x E (a, b]          and potentially:     x E R, x E N, X E I etc.

    // take into account Real Numbers, Natural numbers, Integers (and later Q' irrational numbers, non-real numbers and Q rational numbers)
// 3. [13] (FLAGGED)Critical points generator potentially using the derivative functions equated at 0 and second derivative rules as well.
    // A. (To do this we first need a calculus factoriser basic algorithm going)
    //      A.1 choose which syntax we will interpret the formula in (thinking usual computer text fashion i.e e^(x-1))
            // where ^ is pow                       and brackets is an entire item/hyperthetical number to the factoriser
            // where / is division                  and brackets is an entire item/hyperthetical number to the factoriser
            // where * is multiplication            and brackets is an entire item/hyperthetical number to the factoriser
            // where ln is natural log              and brackets is an entire item/hyperthetical number to the factoriser
            // where y' or x' is derivative of      and brackets is an entire item/hyperthetical number to the factoriser   (we could in future potentially even do implicit differentiation).
            // then basic principles of the outcomes of outer brackets multiplied by inner brackets etc can come into play to tell us the breakdown of all the potential factorising options.
                // Further to this it can brute force all the factorising combinations (up to a max limit so it doesn't bomb out) and reveal all the factorisation alternatives of the final solutions.
    //      A.2 handle exponents
    // B. (To do this we first need a calculus factoriser basic algorithm going)
// 4. Calculating the domains and ranges of functions of x by: (Generally knowing the critical points to test would help in establishing this (but we're not there yet))
    // A. (can tell us if the funtion is continuous at that point and or on which intervals as well).
        // A.1 plugging in x as infinity or y as infinity to determine vertical and horizontal assymptotes.
        // A.2 plugging in the actual value first seeing if it equates to anything instantly, else we resort to using the left and right limit test of that x value in the next step.
        // A.3 plugging in approximate values from the left and the right of x to establish whether the limit exits or not or is left or right continuous or infinity(Does Not Exist).

// #!/Users/root1/.nvm/versions/node/v18.3.0/bin/node

// #!/opt/homebrew/bin/node

// 	$myfile = fopen("/Users/root1/.bruw/bin/calculator/calculate.php", "w") or die("Unable 
// to open 
// file!");
// 	$txt = '<?php ' .
// 	'function factorial($n) ' .
// 	'{ ' .
// 	' if($n <= 1) { ' .
// 	'    return 1; ' .  
// 	'  } ' .
// 	'  else { ' .
// 	'    return $n * factorial($n - 1); ' .
// 	'  } ' .
// 	'} ' .
// 	' echo ' . $argv[1] . '; ?>';
// 	fwrite($myfile, $txt);
// 	fclose($myfile);

// 	system("php ~/.bruw/bin/calculator/calculate.php")

// var fs    = require('fs');
// var util  = require('util');
// // const system = require('system-commands')


// fs.open('/Users/root1/.bruw/bin/calculator/calculate.js', 'w', function(err, fd) {
    // for (var i = 0; i < 100; i++) {
        let mathVariables = `
        let pow = Math.pow
        let sqrt = Math.sqrt
        let log = Math.log
        let ln = Math.log
        let pi = Math.PI
        let abs = Math.abs
        
        let cos = Math.cos
        let tan = Math.tan
        let sin = Math.sin

        let cosh = function(x) {
            return (Math.pow(Math.E, x) + Math.pow(Math.E, -x)) / 2;
        }
        let sinh = function(x) {
            return (Math.pow(Math.E, x) - Math.pow(Math.E, -x)) / 2;
        }
        let tanh = function(x) { return (sinh(x) / cosh(x)); }
        let csch = function(x) { return (1 / sinh(x)); }
        let cosech = csch;        
        let sech = function(x) { return (1 / cosh(x)); }
        let coth = function(x) { return (cosh(x) / sinh(x)); }

    
        let cosec = function (x) { return 1 / Math.sin(x); }
        let csc = cosec;
        let sec = function (x) { return 1 / Math.cos(x); }
        let cot = function (x) { return 1 / Math.tan(x); }
    
        // let arccosec = Math.acosec
        // let arcsec = Math.asec
        // function arccot(x) { return Math.PI / 2 - Math.atan(x); } // might be wrong formula
    
        let arcsin = Math.asin
        let arctan = Math.atan
        let arccos = Math.acos
    
        
        let asin = Math.asin
        let atan = Math.atan
        let acos = Math.acos
        let e = Math.E;
        // let epsilon = Number.EPSILON  this is strangely some other number in math and not e
    
        let logn = function(x, y) {
            return Math.log(y) / Math.log(x);
        }
    
        `
    
    let generalLib =   `
        function sort2dByFirstElementFunction(a, b) {
            if (a[0] === b[0]) {
                return 0;
            }
            else {
                return (a[0] < b[0]) ? -1 : 1;
            }
        }
    `
    
    let leastSquareLib = `
        // now get gradient using least-squares-method:
        function leastSquareMethodGradient(array2d) {
            let sumX = (function(arr){
                var sum = 0;
                for(var i=0; i<arr.length; i++)
                    sum += arr[i][0];
                return sum;
            }(array2d))
    
            let sumY = (function(arr){
                var sum = 0;
                for(var i=0; i<arr.length; i++)
                    sum += arr[i][1];
                return sum;
            }(array2d))
    
            let sumXY = (function(arr){
                var sum = 0;
                for(var i=0; i<arr.length; i++)
                    sum += arr[i][0] * arr[i][1];
                return sum;
            }(array2d))
    
            let sumXSquare = (function(arr){
                var sum = 0;
                for(var i=0; i<arr.length; i++)
                    sum += Math.pow(arr[i][0], 2);
                return sum;
            }(array2d))
            //debug: console.log([sumX, sumY, sumXY, sumXSquare, Math.pow(sumX, 2)])
            return (array2d.length * sumXY - sumX * sumY) / (array2d.length * sumXSquare - Math.pow(sumX, 2));
        }
    
        function leastSquareMethodB(gradient, array2d) {
            let sumX = (function(arr){
                var sum = 0;
                for(var i=0; i<arr.length; i++)
                    sum += arr[i][0];
                return sum;
            }(array2d))
    
            let sumY = (function(arr){
                var sum = 0;
                for(var i=0; i<arr.length; i++)
                    sum += arr[i][1];
                return sum;
            }(array2d))
            return (sumY - gradient * (sumX)) / array2d.length;
        }
    
        function leastSquareFunction(array2dDataset) {
            // sort 2d array by first element of the second layer:
            let sorted2dDataset = array2dDataset.sort(sort2dByFirstElementFunction);
        
            let gradient = leastSquareMethodGradient(sorted2dDataset);
            let b = leastSquareMethodB(gradient, sorted2dDataset);
            return "y = " + gradient + "x + " + b;
        }
    
    `
    
    let correlationCoefficientLib = `
        function correlationCoefficient(array2dDataset) {
            // 1d array: [x,y,z, a, b, c ...]
        
            let sorted2dDataset = array2dDataset.sort(sort2dByFirstElementFunction);
        
            let sumX = (function(arr){
                var sum = 0;
                for(var i=0; i<arr.length; i++)
                    sum += arr[i][0];
                return sum;
            }(sorted2dDataset))
        
            let sumY = (function(arr){
                var sum = 0;
                for(var i=0; i<arr.length; i++)
                    sum += arr[i][1];
                return sum;
            }(sorted2dDataset))
        
            let averageX = sumX / sorted2dDataset.length;
            let averageY = sumY / sorted2dDataset.length;
        
            // The actual calculations:
        
            let sumXMinusXAverageTimesYMinusYAverage = (function(arr, aveX, aveY){
                var sum = 0;
                for(var i=0; i<arr.length; i++)
                    sum += (arr[i][0] - aveX) * (arr[i][1] - aveY);
                return sum;
            }(sorted2dDataset, averageX, averageY))
        
            let sumXMinusXAverageSquare = (function(arr, ave){
                var sum = 0;
                for(var i=0; i<arr.length; i++)
                    sum += Math.pow(arr[i][0] - ave, 2);
                return sum;
            }(sorted2dDataset, averageX))
        
            let sumYMinusYAverageSquare = (function(arr, ave){
                var sum = 0;
                for(var i=0; i<arr.length; i++)
                    sum += Math.pow(arr[i][1] - ave, 2);
                return sum;
            }(sorted2dDataset, averageY))
        
            let result = sumXMinusXAverageTimesYMinusYAverage / Math.sqrt(sumXMinusXAverageSquare * sumYMinusYAverageSquare);
        
            let absRes = Math.abs(result);
            let stringInfo = absRes < 0.2 ? "Very weak" : absRes < 0.4 ? "Weak" : absRes < 0.6 ? "Moderate" : absRes < 0.8 ? "Strong" : absRes <= 1.0 ? "Very strong" : "No correlation";
            //+ " [r < 0.2 VERY WEAK, r < 0.4 WEAK, r < 0.6 MODERATE, r < 0.8 STRONG, r <= 1 VERY STRONG]"
        
            return (result) + " " + stringInfo + " correlation";
        }
    
    `
    
    let standardDeviationLib = `
        function standardDeviation(array2dDataset) {
    
            let sorted2dDataset = array2dDataset.sort(sort2dByFirstElementFunction);
        
            let sumX = (function(arr){
                var sum = 0;
                for(var i=0; i<arr.length; i++)
                    sum += arr[i][0];
                return sum;
            }(sorted2dDataset))
        
            let sumY = (function(arr){
                var sum = 0;
                for(var i=0; i<arr.length; i++)
                    sum += arr[i][1];
                return sum;
            }(sorted2dDataset))
        
            let averageX = sumX / sorted2dDataset.length;
            let averageY = sumY / sorted2dDataset.length;
            let n = sorted2dDataset.length;
        
            let sumXMinusAverageSquared = (function(arr, ave){
                var sum = 0;
                for(var i=0; i<arr.length; i++)
                    sum += Math.pow(arr[i][0] - ave, 2);
                return sum;
            }(sorted2dDataset, averageX))
        
            let sumYMinusAverageSquared = (function(arr, ave){
                var sum = 0;
                for(var i=0; i<arr.length; i++)
                    sum += Math.pow(arr[i][1] - ave, 2);
                return sum;
            }(sorted2dDataset, averageY))
        
            return "SD for X: " + Math.sqrt(sumXMinusAverageSquared / n) + " || SD for Y: " + Math.sqrt(sumYMinusAverageSquared / n);
        }
    
    `
    let quadraticEquationLib = `
        function quadraticEquation(a, b, c) {
            if (!a && !b && !c) {
                return "ERROR: NO ARGS PARSED: run like: quadraticFunction(a, b, c)";
            }
        
            let ansPlus = (-b + Math.sqrt(Math.pow(b, 2) - 4 * a * c)) / (2 * a);
            let ansMinus = (-b - Math.sqrt(Math.pow(b, 2) - 4 * a * c)) / (2 * a);
        
            return "-: " + ansMinus + " || +: " + ansPlus;
        }
    
    `
    
    let factorisationLib = `
        function rationalise(floatNum) {
            // # if is array:
            if (floatNum.length > 0) {
                let results = [];
                for (let i=0; i < floatNum.length; i++) {
                    results.push(rationalise(floatNum[i]));
                } 
                return results;
            }
    
            // approximate rounded to 3 decimal places:
    
            let absoluteNum = Math.abs(floatNum.toFixed(9));// still some discrepency needed so toFixed 9
            let approxNum = parseFloat(absoluteNum.toFixed(3)) // rounded to 3 decimals here
            let result = "";
            let approximate = "";
            for (let i = 1; i <= 100; i++) {
                for (let j = 0; j <= 100 ; j++) {
                        // console.log(j / i, j % i)
                    let ij = j / i;
                    if (ij == absoluteNum) {
                        approximate = (ij != absoluteNum) ? "≈" : "";
                        result = (approximate) + ((floatNum < 0 ? "-": "") + j + "/" + i);
                        return result;
                    } else if ( ij <= approxNum + 0.005 && ij >= approxNum - 0.005 ) {
                        approximate = (ij != approxNum) ? "≈" : "";
                        result = (approximate) + ((floatNum < 0 ? "-": "") + j + "/" + i);
                    }
                }
            }
    
            // Precise:
            // for (let i = 1; i <= 100; i++) {
            //     for (let j = 0; j <= 100 ; j++) {
            //             // console.log(j / i, j % i)
            //         if (j / i == floatNum) {
            //             return (j + "/" + i);
            //         }
            //     }
            // }
            if (result && approximate === "") {
                return result;
            } else {
                // Square Roots: try again but consider putting a sqrt() in either denominator or numerator for numbers up to 100 on both numerator and/or denominator:
                for (let i = 1; i <= 100; i++) {
                    for (let j = 0; j <= 100 ; j++) {
                        let ij = j / sqrt(i);
                        if (ij.toFixed(9) == absoluteNum.toFixed(9)) {
                            approximate = "";
                            // approximate = (ij != absoluteNum) ? "≈" : "";
                            result = (approximate) + ((floatNum < 0 ? "-": "") + j + "/√" + i+"");
                            return result;
                        }
                        ij = sqrt(j) / i;
                        if (ij.toFixed(9) == absoluteNum.toFixed(9)) {
                            approximate = "";
                            // approximate = (ij != absoluteNum) ? "≈" : "";
                            result = (approximate) + ((floatNum < 0 ? "-": "") + "√" + j + "/" + i);
                            return result;
                        }
                    }
                }    
            }
            return "NOT FOUND"//floatNum;// else not found the rational for it
        }
        let r = rationalise;
    
    `
    let vectorsLib = `
        // 2D vectors:
        function magnitude(vecArray) {
            return Math.sqrt(pow(vecArray[0], 2) + pow(vecArray[1], 2))
        }
    
        function unitVector(vecArray) {
            if (vecArray.length < 2)
                return "ERROR: incorrect vector size";
    
            let mag = magnitude(vecArray);
            return ([vecArray[0] / mag, vecArray[1] / mag]);
        }
    
        function gradient(orderedPairA, orderedPairB) {
            return (orderedPairB[1] - orderedPairA[1]) / (orderedPairB[0] - orderedPairA[0])
        }
    
        function vectorAngle(vecA, vecB) {
            let dot = dotProduct(vecA, vecB);
            let magA = magnitude(vecA);
            let magB = magnitude(vecB);
    
            // formula: cos(theta) = dot / (magA * magB);
    
            let rhs = dot / (magA * magB);
            return arccos(rhs);
        }
    
        function dotProduct(vec1, vec2) {
            return vec1[0] * vec2[0] + vec1[1] * vec2[1];
        }
    
        // 3D+ vectors:
    `
    // var to make globally accessible the mathFunctionsLib:
    var mathFunctions = "";
    mathFunctions += generalLib + leastSquareLib + correlationCoefficientLib + standardDeviationLib + quadraticEquationLib + factorisationLib + vectorsLib;
    // concatenate more math libs here...
    
    mathFunctions = mathVariables + mathFunctions + '\n'; // concatenated to prevent issues with the order of writing to the file
    
        // fs.write(fd, '#!/opt/homebrew/bin/node\n\n\n',function(){});
        // // fs.write(fd, mathVariables,function(){});
        // fs.write(fd, mathFunctions,function(){});
        // // fs.write(fd, 'console.log(' + process.argv[2] + ')',function(){});
        // //    fs.write(fd, util.format('line %d\n', i),function(){});
        // // }
        // fs.close(fd);
    // });
    
    function evaluate(equation) {
        return eval(mathFunctions + '\n' + equation);
    }
    
    // const { exec } = require("child_process");
    
    // exec("/opt/homebrew/bin/node /Users/root1/.bruw/bin/calculator/calculate.js", (error, stdout, stderr) => {
    //     if (error) {
    //         console.log(`error: ${error.message}`);
    //         return;
    //     }
    //     if (stderr) {
    //         console.log(`stderr: ${stderr}`);
    //         return;
    //     }
    //     // else worked thus print the returned result::
    //     console.log(`${stdout}`);
    // });
    
    // console.log("true")
    