
nodeAlgorithm["circular"].init = function() {
  // console.log(nodeAlgorithm, nodeAlgorithmCurrentType)
  // lsRecursiveData = {
  //     name : "Out of FPS",
  //     rhs : [
  //       { name : "Rocket", rhs : [] },
  //       { name : "diagram render algorithm", rhs : [      { name : "iliad-test-visualiser", rhs : [] }, { name : "AAAA", rhs : [] }, { name : "BBBB", rhs : [] }, { name : "CCCC", rhs : [] }, { name : "this is a test", rhs : [] },    ] },
  //       { name : "longitude & latitude", rhs : [] },
  //       { name : "Mini map algorithm", rhs : [] },
  //       { name : "Space travel", rhs : [      { name : "commit collisions", rhs : [{ name : "FIRST OOOUTER", rhs : [] },{ name : "FIRST OOOUTER", rhs : [] },{ name : "FIRST OOOUTER", rhs : [] },{ name : "FIRST OOOUTER", rhs : [] },{ name : "OOOUTER", rhs : [] },{ name : "OOOUTER-2", rhs : [{ name : "What have we here...", rhs : [] },] },{ name : "Well, well well", rhs : [] },] }, { name : "inner", rhs : [] }, ] },
  //       { name : "Mouth animations", rhs : [] },
  //       { name : "Space station", rhs : [] },
  //       { name : "exploratory missions", rhs : [] },
  //       { name : "Scifi city", rhs : [] },
  //       { name : "Digital portfolio room", rhs : [] },
  //       { name : "Lizzard premise calculus", rhs : [] },
  //       { name : "Earth skybox", rhs : [] },
  //       { name : "Rocket game", rhs : [      { name : "Conditional decision making", rhs : [] },{ name : "this is a test", rhs : [] },   ] },
  //       { name : "Interactions mini games", rhs : [] },
  //       { name : "Fingerprints Digital Media", rhs : [] },
  //     ]
  // };

  canvas = document.getElementById("myCanvas");
  ctx = canvas.getContext("2d");

  windowWidth = ctx.canvas.width;//  = window.innerWidth;
  windowHeight = ctx.canvas.height;// = window.innerHeight;
  // render all the recursive directories:

  strokeStyles = [// TODO store these for future
    '#ff7571',// 0 peach
    '#ff3b9e',// 1 pink
    '#ff3cc7',// 2 more pink
    '#f440c6',// 3 even more pink
    '#f53cf3',// 4 purplish
    '#d03bf7',// 5 purple
    '#d03bff',// 6 more purple
    '#ac3aff',// 7 even more purple
    '#a478ff',// 8 lilack
    '#8079ff',// 9 indigo
    '#419eff',// 10 blueish
    '#39c1ff',// 11 light blue
    '#2ee3e4',// 12 teal
    '#1effb5',// 13 turquise
    '#1eff8d',// 14 light green
    '#66ff66',// 15 bright green
    '#8dff65',// 16 yellow green
    '#b4ff14',// 17 light yellowish
    '#e1e01a',// 18 yellow
    '#ffbb1e',// 19 light orange
    '#ff9820',// 20 orange
  ]


  // greyedOutColor = greyedOutColor ? greyedOutColor : "#3e4952";//"#495a60";//"#889ca4";//"#555555";
  // collapsedColor = collapsedColor ? collapsedColor : "#FF00FF";
  // redOutColor = redOutColor ? redOutColor : "yellow";//"rgb(255,80,80)";
  // greenedOutColor = greenedOutColor ? greenedOutColor : "#39FF14";
  // intermediateOutColor = intermediateOutColor ? intermediateOutColor : "#fff"
  // var specialColors = specialColors ? specialColors : [
  //   // greyedOutColor, greyed out isn't special per say.
  //   redOutColor,
  //   greenedOutColor,
  //   intermediateOutColor
  // ];


  // layerMax = [];//an extending array of the tracking of the MaxXLength of each Layer

  // var x=150 + 30;
  // var y=150;
  // var labelRotation=-45;

  xOffsetSpacing = 0;//10//20;
  yOffsetSpacing = 2 + 9;
  boxPadding = 2 + 2;
  startPaddingYOffset = 20;
  colourSpectrumStart = 10;
  // nodes = []; // for some retarded reason this breaks everything so dont set it here.

  centerX = canvas.width / 2, centerY = canvas.height / 2;// todo set to whereever it calculates is needed to be dependant on larger diagrams

  rootParentNode;

  parentBoxHeight;
  totalMaxYLength = 0;

  prevAngle;// to know what was the angle of the previous node?
  prevAngleLayer;// to know if it came from a parent or a child to know what to do in that case

}

nodeAlgorithm["circular"].reRenderDiagram = function() {
  nodeAlgorithm["circular"].init();// circular initialisations (TODO: consider might only need to be done once not all the time at this location)

  // This is how to reload the tree: TODO put into its on reloadUI() method:
  layerMax = []// always reset this because we build this information up when calculating the positions below:

  // if (window.lsRecursiveData) {
  //   lsRecursiveData = window.lsRecursiveData;
  // }
  rootNodeBox = nodeAlgorithm["circular"].getWordBox(ctx, {
    "x" : 0,
    "y" : 0,
    "size" : 10,//18,
    "message" : lsRecursiveData.name,
    "color" : "#00FFFF",
    "offset" : {//offset will be the spacing you want between the nodes
      "x" : 10, 
      "y" : 10
    }
  });

  ctx.clearRect(0,0,canvas.width,canvas.height);

  totalMaxYLength = 0;
  // centerX, centerY // TODO: will need to set it to the new position based off size calculated etc
  
  foundANode = false;// var to expose it to be used elsewhere.

  // // reset all temp variables:
  nodes = [];
  prevSelectedNode = null;
  currentHighlightedNode = null;
  emphasizedStateNode = null;
  foundANode = false;

  // canvas = document.getElementById("myCanvas")
  // canvas.getContext("2d").clearRect(0, 0, canvas.width, canvas.height);

  // nodeAlgorithm["linear"].recursiveRightMostFIrstLayeredNodeTrack([lsRecursiveData], 0, layerMax, rootNodeBox, true);//do not skip the first element
  // nodeAlgorithm["linear"].recursiveRightMostFIrstNodeRender([lsRecursiveData], 0, 0, layerMax, rootNodeBox, null/* lhs */, true); 

  //RENDERS NODES FROM LEFT TO RIGHT:
  nodeAlgorithm["circular"].recursiveRightMostFIrstLayeredNodeTrack([lsRecursiveData], 0, layerMax, rootNodeBox, true);//do not skip the first element
  // totalMaxYLength += 2;
  nodeAlgorithm["circular"].recursiveRightMostFIrstNodeRender([lsRecursiveData], 0, 0, layerMax, rootNodeBox, null/* lhs */, true);


}


nodeAlgorithm["circular"].clearTempMetaData = function(rhs) {
    for (let i = 0; i < rhs.length; i++) {
        rhs[i].angle = undefined;
        rhs[i].center = undefined;
        rhs[i].lhs = undefined;

        rhs[i].circularOrientation = undefined;
        // rhs[i].dimensions = undefined;
        rhs[i].layer = undefined;
        rhs[i].nodeEndPos = undefined;
        rhs[i].nodeStartPos = undefined;
        rhs[i].rotTranslate = undefined;
        rhs[i].rotTranslateCurr = undefined;
        rhs[i].topLeft = undefined;
        rhs[i].topRight = undefined;
        rhs[i].totalMaxXLength = undefined;

        /*
        rhs[i].offset = undefined;
        rhs[i].x = undefined;
        rhs[i].y = undefined;
        rhs[i].actualBoundingBoxAscent = undefined;
        rhs[i].actualBoundingBoxDescent = undefined;
        rhs[i].actualBoundingBoxLeft = undefined;
        rhs[i].actualBoundingBoxRight = undefined;
        */
        // rhs[i]. = undefined;

        if (rhs[i].rhs)
          nodeAlgorithm["circular"].clearTempMetaData(rhs[i].rhs);
    }
}
nodeAlgorithm["circular"].recursiveRightMostFIrstLayeredNodeTrack = function(rhs, layer, layerMax, rootNodeBox, firstRender) {
    if (firstRender) {
      nodeAlgorithm["circular"].clearTempMetaData([ lsRecursiveData ]);
        rootNodeBox = nodeAlgorithm["circular"].getWordBox(ctx, {
            "x" : 0,
            "y" : 0,
            "size" : 10,//18,
            "message" : lsRecursiveData.name,
            "color" : "#00FFFF",
            "offset" : {//offset will be the spacing you want between the nodes
              "x" : 10, 
              "y" : 10
            }
          });
      
        // reset certain variables:
        totalMaxYLength = 0;
        prevAngle = undefined;// to know what was the angle of the previous node?
        prevAngleLayer = undefined;// to know if it came from a parent or a child to know what to do in that case

        foundANode = false;// var to expose it to be used elsewhere.
        currentHighlightedNode = null;// exposes the current/last highlighted node
        emphasizedStateNode = undefined;
        prevSelectedNode = undefined;

        parentBoxHeight = undefined;

        xOffsetSpacing = 0//10//20;
        yOffsetSpacing = 2 + 9;
        boxPadding = 2 + 2;
        startPaddingYOffset = 20;
        colourSpectrumStart = 10;
        nodes = [];
    }

    //gets the maxXLength stored per layerCount in layerMax[layerNbr]:
    // let maxXLength = layerMax[layer].nodeXmin;
    // let rhsLength = rhs.length;
    let count = 1;
    let numberOfNodes = 0;
    //1. this loop prioritises getting the maxXLength for the Layer
    if (layer >= layerMax.length)
        layerMax.push({nodeXmin : 0, nodeXmax : 0, nodeCount : 0, totalNodeCount : 0, currentPosY : 0});
    let childNodeBox;
    if (rhs && rhs.length > totalMaxYLength)
        totalMaxYLength = rhs.length;
    if (rhs) {
        rhs.forEach(rhsNode => {
            //render each rhs node:
            childNodeBox = nodeAlgorithm["circular"].getWordBox(ctx, {
                "x" : 0,
                "y" : 0,
                "size" : rootNodeBox.size,
                "message" : rhsNode.name,
                "color" : "orange",// unused currently as using only roots color value...
                "collapsed": rhsNode.collapsed,
            });
            //calculate the max of this layer:
            if (childNodeBox.bottomRight.x >= layerMax[layer].nodeXmax) {
                layerMax[layer].nodeXmin = childNodeBox.bottomLeft.x;
                layerMax[layer].nodeXmax = childNodeBox.bottomRight.x;
            }
            layerMax[layer].totalNodeCount += 1;
            if (!rhsNode.collapsed) {
              nodeAlgorithm["circular"].recursiveRightMostFIrstLayeredNodeTrack(rhsNode.rhs, layer + 1, layerMax, rootNodeBox, false);//do not skip the first element
            }
        });
    }
  return layerMax;
}

nodeAlgorithm["circular"].recursiveRightMostFIrstNodeRender = function(rhs, totalMaxXLength, layer, layerMax, rootNodeBox, lhs, firstRender) {
    // let rhsLength = rhs.length;

    if (firstRender) {
        totalMaxYLength += 2//2//02;
    }
//2. this loop subtracts the maxXLength of current layer to get starting x position of current layer
  //   and adds this to the sub total for the previous layer to know where it should begin itself on the x axis:
  let boxSize = Math.abs(rootNodeBox.bottomLeft.y - rootNodeBox.topLeft.y);// can optimise this by not keeping it here in the recursive loop as just needs single calculation initially
  let childNodeBox;
  let rhsIndex = -1;
  rhs.forEach(rhsNode => {
    ++rhsIndex;

    childNodeBox = JSON.parse(JSON.stringify(rootNodeBox, function(key, value) {// stringifies but bypasses lhs circular dependency issues:
      if (key === 'lhs') {
        return undefined; // Skip the 'lhs' property
      }
      return value;
    }));

    childNodeBox.collapsed = rhsNode.collapsed;
    childNodeBox.message = rhsNode.name;
    childNodeBox.x = totalMaxXLength;//totalMaxXLength;
    if (rhsNode.rhs && rhsNode.rhs.length > 0 && !rhsNode.collapsed) {
      // get the yposition for this LHS node:
      nodeAlgorithm["circular"].recursiveRightMostFIrstNodeRender(rhsNode.rhs, totalMaxXLength + layerMax[layer].nodeXmax + xOffsetSpacing, layer + 1, layerMax, rootNodeBox, rhsNode);// totalMaxXLength + maxXLength, layer

      childNodeBox.y = rhsNode.rhs[0].dimensions.topRight.y + 1 + (Math.abs(rhsNode.rhs[0].dimensions.topRight.y - rhsNode.rhs[rhsNode.rhs.length - 1].dimensions.bottomRight.y) / 2) + (boxSize / 4);// div 4 but works
    }
    else {
      // if no rhs :
      // childNodeBox.y = (windowHeight / (rhsLength + 1)) * (count) - (boxSize / 2);//rhsMetaData.lhsYPos;
      if (layer == layerMax.length - 1) {
        childNodeBox.y = (layerMax[layer].nodeCount + 1) * boxSize;//rhsMetaData.lhsYPos;
      } else {
      //DONE: missing scenario:
      //use current layerMax[layerMax.length - 1].bottomRight.y if there is no rhs
        if (layerMax[layerMax.length - 1].currentPosY > 0) {
          childNodeBox.y = layerMax[layerMax.length - 1].currentPosY + boxSize;
          layerMax[layerMax.length - 1].currentPosY += boxSize;
        } else {
          // childNodeBox.y = (layerMax[layer].nodeCount + 1) * boxSize;//rhsMetaData.lhsYPos;
          childNodeBox.y = (layerMax[layer].nodeCount + 1) * boxSize;//rhsMetaData.lhsYPos;
          // childNodeBox.y = 0 + boxSize;
        }
      }

    }
    childNodeBox = nodeAlgorithm["circular"].getWordBox(ctx, childNodeBox);

    //store the nodeSize in the object itself:
    childNodeBox.dimensions = rhsNode.dimensions = {
      topRight : childNodeBox.topRight,
      bottomRight : childNodeBox.bottomRight,
      topLeft : childNodeBox.topLeft,//unused?? No longer unused since can select nodes
      bottomLeft : childNodeBox.bottomLeft,//unused?? No longer unused "  "   "
      message: rhsNode.name
      //...
    }

    // brainstorm:
    // 1 INITIALLY: the radius length should increase per layer thus defaultRadius * (layer + 1)
    // 2. INITIALLY: render the nodes at an equi-distant: (360 / rhsLength) radius distance away from each outher
    // render the nodes here

    if (childNodeBox.bottomRight.y > layerMax[layerMax.length - 1].currentPosY) {
      layerMax[layerMax.length - 1].currentPosY = childNodeBox.bottomRight.y + yOffsetSpacing;
    }

    layerMax[layer].nodeCount += 1;

    let msgBox = childNodeBox;
    let boxWidth = (msgBox.dimensions.topRight.x + boxPadding) - (msgBox.dimensions.topLeft.x - boxPadding)
    let boxHeight = (msgBox.dimensions.bottomLeft.y + boxPadding) - (msgBox.dimensions.topLeft.y + - boxPadding)

    let colour = strokeStyles[(rhsIndex + colourSpectrumStart) % 20];
    let angle;
    if (rhsNode.lhs && rhsNode.lhs.angle) {
        angle = rhsNode.lhs.angle + (360 / totalMaxYLength);
    } else if (prevAngle) {
      let offsetSpacer = 1 * ((layer / layerMax.length));
      let offsetRatio = (layer > 1 ? ((layer / layerMax.length + offsetSpacer) / (layer * 2/*4*/)) : 1);
      if (prevAngleLayer == layer)
        angle = prevAngle + (360 / totalMaxYLength) * offsetRatio;
      else if (prevAngleLayer < layer) {
        angle = prevAngle + (360 / totalMaxYLength) * offsetRatio;        // for now then just keep this latest angle of the last child so within that range and not incremented
      } else if (prevAngleLayer > layer) {
        angle = prevAngle;// + (360 / totalMaxYLength) * (offsetRatio * (layer / layerMax.length)); 
        // TODO: then if the prevParent is the same node as this rhsNode then don't increment else increment by (360 / totalMaxYLength)
        // TODO: then if the prevParent is the same node as the lhs then only increment by half the total length
      }
    } else {
      angle = (360 / totalMaxXLength);
    }
    prevAngle = angle;
    prevAngleLayer = layer;
    // first validate that the rhsNode.lhs doesn't already have an angle:
    // if (rhsNode.lhs && rhsNode.lhs.angle) {
    //   // then 
    //   angle = rhsNode.lhs.angle + (360 / totalMaxXLength)//(totalMaxYLength)// should + the totalMaxYLength angle increment
    // }
    // else if (layer == layerMax.length - 1) {
    //   // then ratio them an equidistant apart
    //   angle = (360 / layerMax[layer].totalNodeCount) * layerMax[layer].nodeCount// 0//(360 / rhsLength) * rhsIndex;//0;//45  // for now just to get length distance away correct, we set to 0
    // }
    // else if (rhsNode.rhs && rhsNode.rhs.length > 0) {
    //   // then current angle for the node must be the average or center angle of its children nodes
    //   angle = (rhsNode.rhs[0] + rhsNode.rhs[rhsNode.rhs.length - 1]) / 2;
    // } else if (prevAngle) {
    //   // then current angle must take off the prev angle value
    //   angle = prevAngle;
    // }
    // if (!angle) {
    //   angle = (360 / totalMaxXLength)//(totalMaxYLength)// should + the totalMaxYLength angle increment

    // }
    // prevAngle = angle;

    // drawWordBox(centerX, centerY, childNodeBox, angle, colour);
    let rotTranslate = [msgBox.dimensions.topLeft.x - boxPadding + boxWidth / 2, msgBox.dimensions.topLeft.y + - boxPadding + startPaddingYOffset + boxHeight / 2, 0];
    // let rotTranslateCurr = [1 * 300 * layer - 150 * (0.1 * (layerMax.length - layer)) + layerMax[layer].nodeXmin * (((layer - 1) / layerMax.length)), 0, 0];
    // let rotTranslateCurr = [Math.abs(layerMax[layer].nodeXmax) + boxWidth / 2, 0, 0];
    // let rotTranslateCurr = [Math.abs(layerMax[layer].nodeXmax) + boxWidth / 2 + totalMaxXLength, 0, 0];
    let rotTranslateCurr = [totalMaxXLength + Math.abs(layerMax[layer].nodeXmax) - boxWidth / 2, 0, 0];
    let lineBeginning = [totalMaxXLength + Math.abs(layerMax[layer].nodeXmax), 0, 0];// correct end point
    let lineEnding = [totalMaxXLength + Math.abs(layerMax[layer].nodeXmax) - boxWidth, 0, 0];//correct Front point
    let center = [0, 0, 0];



    nodeAlgorithm["circular"].rotatezyx(rotTranslate, center, 0, 0, (angle +  Math.atan(rotTranslate[1] / rotTranslate[0])) * Math.PI / 180)
    nodeAlgorithm["circular"].rotatezyx(rotTranslateCurr, center, 0, 0, (angle +  Math.atan(rotTranslate[1] / rotTranslate[0])) * Math.PI / 180)
    nodeAlgorithm["circular"].rotatezyx(lineEnding, center, 0, 0, (angle +  Math.atan(rotTranslate[1] / rotTranslate[0])) * Math.PI / 180)
    if (!lhs)
    {
      lineEnding = [centerX, centerY, 0];
      lineBeginning = [centerX, centerY, 0];
    }
    else {
      lineEnding = [lineEnding[0] + centerX,lineEnding[1] + centerY,lineEnding[2]]
    }

    // let currPosX = rotTranslate[0] + centerX, currPosY = rotTranslate[1] + centerY

    // add the node to the array for future rearrangement here:

    // rhsNode
    rhsNode.colour = colour;
    rhsNode.message = rhsNode.name;
    rhsNode.x = childNodeBox.x;
    rhsNode.y = childNodeBox.y;
    rhsNode.lhs = lhs;
    // NEW circular node information:
    rhsNode.center = { x: rotTranslateCurr[0] + centerX, y: rotTranslateCurr[1] + centerY }// TODO this looks wrong, probably needs to be multiplied not added because length is fixed to 1 in trig and not actual box size representation
    rhsNode.angle = angle;
    rhsNode.nodeEndPos = lineBeginning;
    rhsNode.nodeStartPos = lineEnding;
    rhsNode.rotTranslate = rotTranslate;
    rhsNode.rotTranslateCurr = rotTranslateCurr;
    rhsNode.layer = layer;
    rhsNode.totalMaxXLength = totalMaxXLength;

    childNodeBox.angle = angle;
    childNodeBox.node = rhsNode;
    childNodeBox.name = rhsNode.name;
    childNodeBox.center = { x: rotTranslateCurr[0] + centerX, y: rotTranslateCurr[1] + centerY }
    childNodeBox.rotTranslate = rotTranslate;
    childNodeBox.rotTranslateCurr = rotTranslateCurr;
    childNodeBox.nodeEndPos = lineBeginning;
    childNodeBox.nodeStartPos = lineEnding;
    childNodeBox.layer = layer;
    childNodeBox.totalMaxXLength = totalMaxXLength;
    childNodeBox.lhs = lhs;
    childNodeBox.colour = colour;

    // childNodeBox.lhs = lhs;

    nodes.push({ node: childNodeBox });// NEW CODE: added this back now to add new features


    if (!lhs)
    {
      rootParentNode = childNodeBox;// currentNode then has to be rootParent of all children so store this information here for drawWordBox use.
        // nodeAlgorithm["circular"].drawWordBox(centerX, centerY, childNodeBox, 0/* remove it's angle*/, colour, lineEnding, rhsNode);//center parent node
    }

    nodeAlgorithm["circular"].drawWordBox(rotTranslateCurr[0] + centerX, rotTranslateCurr[1] + centerY, childNodeBox, angle, colour, lineEnding, rhsNode);

  });
}

nodeAlgorithm["circular"].getWordBox = function(ctx, msgBox) {
  ctx.font = msgBox.size + "px Arial";
  let internalMetrics = ctx.measureText(msgBox.message);

  msgBox.actualBoundingBoxLeft = internalMetrics.actualBoundingBoxLeft;
  msgBox.actualBoundingBoxAscent = internalMetrics.actualBoundingBoxAscent;
  msgBox.actualBoundingBoxRight = internalMetrics.actualBoundingBoxRight;
  msgBox.actualBoundingBoxDescent = internalMetrics.actualBoundingBoxDescent;

  nodeAlgorithm["circular"].updateWordBox(msgBox);

  // let outBox = JSON.parse(JSON.stringify(msgBox));//duplicate json Object  
  let outBox = JSON.parse(JSON.stringify(msgBox, function(key, value) {// stringifies but bypasses lhs circular dependency issues:
    if (key === 'lhs') {
      return undefined; // Skip the 'lhs' property
    }
    return value;
  }));

  return outBox;
}

nodeAlgorithm["circular"].updateWordBox = function(msgBox) {
  msgBox.topLeft = {
  	"x" : msgBox.x - msgBox.actualBoundingBoxLeft,
    "y" : msgBox.y - msgBox.actualBoundingBoxAscent
  };

  msgBox.topRight = {//msgBox.bottomLeft = {
  	"x" : msgBox.x + msgBox.actualBoundingBoxRight,
  	"y" : msgBox.y - msgBox.actualBoundingBoxAscent
  };

  msgBox.bottomRight = {
  	"x" : msgBox.x + msgBox.actualBoundingBoxRight,
    "y" : msgBox.y + msgBox.actualBoundingBoxDescent
  };

  msgBox.bottomLeft = {//msgBox.topRight = {
  	"x" : msgBox.x - msgBox.actualBoundingBoxLeft,
  	"y" : msgBox.y + msgBox.actualBoundingBoxDescent
  }

  return msgBox;
}

// window.scrollTo(window.width, 0);
// nodeAlgorithm["circular"].(x, y, labelRotation);
// nodeAlgorithm["circular"].drawWordBox(x + 100, y, labelRotation + 45);
nodeAlgorithm["circular"].drawWordBox = function(x, y, msgBox, angle, colour, rotTranslateLineBeginning, rhsNode) {
    // meta color details:
    let color = colour;
    msgBox.prevColor = msgBox.currentColor;
    msgBox.currentColor = color;
  
    if (msgBox.specialColor && color === greyedOutColor) {
      color = msgBox.specialColor;// override greys to specialColor in the even they have been specially coloured.
    }
    if (color != null && specialColors.includes(color)) {
      msgBox.specialColor = color;// if user specified a special color it should stay a special color until that is disabled on the node.
    }
    ///////////////////////////////////////////////////////

    var label=msgBox.message;

    // if (msgBox.message === "diagram render algorithm") {
    //     console.log("FOUND:: ", msgBox);
    //     // console.log((new Error()).stack);        
    // }

    ctx.font = rootNodeBox.size + "px Arial";
    let internalMetrics = ctx.measureText(label);

    // msgBox.angle = angle;
    // msgBox.

    let isRootParentNode = !msgBox.lhs;

    // draw lines:
    if (rhsNode.rhs && !rhsNode.collapsed) {
      for (let i = 0; i < rhsNode.rhs.length; i++) {
        ctx.strokeStyle = strokeStyles[(i + colourSpectrumStart) % 20];

        ctx.beginPath();
        // if (isRootParentNode) {
        if (isRootParentNode) {
            ctx.moveTo(centerX, centerY);
        } else {
            ctx.moveTo(x, y);
        }
        // } else {
        //   ctx.moveTo(centerX, centerY);
        // }

        ctx.lineTo(
          rhsNode.rhs[i].nodeStartPos[0],
          rhsNode.rhs[i].nodeStartPos[1],
        );
        ctx.closePath();
        ctx.stroke();

      }
    }
    // draw the nodes at their angle and positions:
    ctx.save();
    ctx.textAlign = "center";
    ctx.textBaseline = "middle";

    if (isRootParentNode) {
      ctx.translate(centerX, centerY);
    } else {
      ctx.translate(x, y);      
      if (angle > 90 && angle < 270)
        ctx.rotate((angle + 180) * Math.PI / 180);
      else
        ctx.rotate(angle * Math.PI / 180);
    }
    ctx.beginPath();


// document.getElementById("stdout").innerHTML = internalMetrics.actualBoundingBoxLeft + "<br> " + internalMetrics.actualBoundingBoxAscent + "<br> " + internalMetrics.actualBoundingBoxRight + "<br>" + internalMetrics.actualBoundingBoxDescent;

    ctx.fillStyle = colour//"#00FFFF"//msgBox.color;

    let length = (internalMetrics.actualBoundingBoxRight - internalMetrics.actualBoundingBoxLeft) + boxPadding;
    let height = (internalMetrics.actualBoundingBoxDescent - internalMetrics.actualBoundingBoxAscent) * boxPadding / 2;

    if (rhsNode.rhs && rhsNode.rhs.length > 0 && msgBox.collapsed) {
        ctx.strokeStyle = collapsedColor;//invertColor(color);
        ctx.lineWidth = 4;

        ctx.beginPath();

        ctx.roundRect(-0.5 * length, -0.5 * height, length, height, [10]);

        // ctx.roundRect(-0.5 * length - 1, -0.5 * height - 1, length + 2, height + 2, [10]);
        ctx.stroke();

        // ctx.roundRect(-0.5 * length - 2, -0.5 * height - 2, length + 4, height + 4, [10]);
        // ctx.stroke();
    }

    ctx.roundRect(-0.5 * length, -0.5 * height, length, height, [10]);
    ctx.fill();

    // let boxWidth = (msgBox.dimensions.topRight.x + boxPadding) - (msgBox.dimensions.topLeft.x - boxPadding)
    // let boxHeight = (msgBox.dimensions.bottomLeft.y + boxPadding) - (msgBox.dimensions.topLeft.y + - boxPadding)
    // ctx.beginPath();
    // // ctx.roundRect(msgBox.dimensions.topLeft.x - boxPadding, msgBox.dimensions.topLeft.y + - boxPadding + startPaddingYOffset, boxWidth, boxHeight, [10]);
    // ctx.roundRect(msgBox.dimensions.topLeft.x - boxPadding, msgBox.dimensions.topLeft.y + - boxPadding + startPaddingYOffset, boxWidth, boxHeight, [10]);
    // ctx.fill();

    ctx.fillStyle = "#000000"//"#000000"//msgBox.color;
    ctx.fillText(label, 0 + 1, 0);

    let internalAngles = ctx.measureText(label);


    // NEW for circular node orientation capturing:
    // console.log("INTERNAL ANGLES:", internalAngles, x, y)
    let circularOrientation = nodeAlgorithm["circular"].getWordBox(ctx, msgBox);// get it now whilst rotated:
    msgBox.circularOrientation = {
      topRight : circularOrientation.topRight,
      bottomRight : circularOrientation.bottomRight,
      topLeft : circularOrientation.topLeft,//unused?? No longer unused since can select nodes
      bottomLeft : circularOrientation.bottomLeft,//unused?? No longer unused "  "   "
      message: msgBox.name,
      internalAngles: internalAngles,
      x: x,
      y: y
    }

    // console.log("COMPARE", nodeAlgorithm["circular"].getWordBox(ctx, msgBox), tempValDelete);

    // console.log(internalAngles);


    ctx.restore();
    //
    // ctx.beginPath();
    // ctx.arc(internalAngles.actualBoundingBoxLeft,internalAngles.actualBoundingBoxAscent,3,0,Math.PI*2);
    // ctx.closePath();
    // ctx.fill();
    // ctx.arc(x, y,3,0,Math.PI*2);

    // draw the startPos of the node:
    // ctx.beginPath();
    // ctx.arc(rotTranslateLineBeginning[0], rotTranslateLineBeginning[1],3,0,Math.PI*2);
    // ctx.closePath();
    // ctx.fill();


}

nodeAlgorithm["circular"].rotatezyx = function(M, center, beta, theta, phi) {
	// Rotation matrix coefficients
	let		ct, st;
	let		x;
	let		y;
	let		z;

	//z: (moved to top so it can update first which is now correctly)
	x = M[0];
	y = M[1];
	z = M[2];
	ct = Math.cos(phi);
	st = Math.sin(phi);
	M[0] = ct * x - st * y;
	M[1] = st * x + ct * y;

	//y
	x = M[0];
	y = M[1];
	z = M[2];
	ct = Math.cos(theta);
	st = Math.sin(theta);
	M[0] = ct * x - st * z;
	M[2] = st * x + ct * z;

	//x
	x = M[0];
	y = M[1];
	z = M[2];
	ct = Math.cos(beta);
	st = Math.sin(beta);
	M[1] = ct * y - st * z;
	M[2] = st * y + ct * z;

}

  // TODO:
  // 1. DONE: render nodes around the center circle node
  // 2. DONE: render these nodes a consistant distance from each other
  // 3. DONE: render the second layer of nodes
  // 4. DONE: calculate deepest level nodes count and perimeter of that outside space to know how many max nodes can fit there and spread nodes within that radius region evenly 

  // 5. store the processed position and angle results on the node itself.
  // 6. align the current level to the same angle as its latest rhs child node.
  // 7. pad the nodes a bit cleaner so that when we draw the lines we can see them nicely
  // 8. draw the lines

  // 9. store the nodes in a nodes = [], to make selectable like our diagram.js code.

  // CLEANING UP THE CODE:
  // all var variables should be moved more to the top, together.

  // NEW CODE:  ///////////////////////////////////////////////////////////
  nodeAlgorithm["circular"].getMousePos = function(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };
}

// currentHighlightedNode;// exposes the current/last highlighted node
// emphasizedStateNode;
// prevSelectedNode;

nodeAlgorithm["circular"].foundEmphasizedNodeIn = function(node) {
  if (!emphasizedStateNode)
    return false;

  let found = false;
  let tmpNode = node;
  while (tmpNode) {// lhs traversal
    if (tmpNode === emphasizedStateNode) {
      // nodeAlgorithm["circular"].renderLhsNodes(tmpNode.lhs, "#FFFFFF")
      // then default colors
      found = true;
      break;
    }
    tmpNode = tmpNode.lhs;
  }
  tmpNode = node;
  while (tmpNode) {// rhs traversal
    if (tmpNode === emphasizedStateNode) {
      found = true;
      break;
    }
    tmpNode = tmpNode.rhs;
  }
  if (!found) {// then greyscale it and its children as isn't emphasized/focused:
    nodeAlgorithm["circular"].renderLhsNodes(node.lhs, greyedOutColor);//LHS nodes should never take on special colors, only node itself and RHS nodes of it can
    // nodeAlgorithm["circular"].renderLhsNodes(emphasizedStateNode, null)

    let revertColor = greyedOutColor;
    nodeAlgorithm["circular"].drawWordBox(node.rotTranslateCurr[0] + centerX, node.rotTranslateCurr[1] + centerY, node, node.angle/*0 remove it's angle*/, revertColor, node.nodeStartPos, node);//center parent node
    // renderNodeBoxWithText(node, revertColor);
    if (!node.collapsed) {
      nodeAlgorithm["circular"].renderRhsNodes(node.rhs, revertColor);
    }

    let foundCollapsedParent = nodeAlgorithm["circular"].getCollapsedParent(emphasizedStateNode);
    if (foundCollapsedParent) {
      nodeAlgorithm["circular"].renderLhsNodes(foundCollapsedParent, null)
    } else {
      nodeAlgorithm["circular"].renderLhsNodes(emphasizedStateNode, null)
    }
    if (!emphasizedStateNode.collapsed && !foundCollapsedParent) {
      nodeAlgorithm["circular"].renderRhsNodes(emphasizedStateNode.rhs, null)
    }
  }
  // else {// FOR DEBUG PURPOSES ONLY:
  //   nodeAlgorithm["circular"].renderLhsNodes(node, "#FFFFFF")
  //   nodeAlgorithm["circular"].renderRhsNodes(node.lhs.rhs, "#FFFFFF")
  // }

  return false;// else not found any instance of the emphasized node thus you know what color to choose.
}


// https://stackoverflow.com/questions/849211/shortest-distance-between-a-point-and-a-line-segment
nodeAlgorithm["circular"].sqr = function(x) { return x * x }
nodeAlgorithm["circular"].dist2 = function(v, w) { return nodeAlgorithm["circular"].sqr(v.x - w.x) + nodeAlgorithm["circular"].sqr(v.y - w.y) }
nodeAlgorithm["circular"].distToSegmentSquared = function(p, v, w) {
  let l2 = nodeAlgorithm["circular"].dist2(v, w);
  if (l2 == 0) return nodeAlgorithm["circular"].dist2(p, v);
  let t = ((p.x - v.x) * (w.x - v.x) + (p.y - v.y) * (w.y - v.y)) / l2;
  t = Math.max(0, Math.min(1, t));
  return nodeAlgorithm["circular"].dist2(p, { x: v.x + t * (w.x - v.x),
                    y: v.y + t * (w.y - v.y) });
}
nodeAlgorithm["circular"].distToSegment = function(p, v, w) { return Math.sqrt(nodeAlgorithm["circular"].distToSegmentSquared(p, v, w)); }
///////////////////////////////////////////////////////////////////////////////////////////////

nodeAlgorithm["circular"].isWithinBox = function(nodeIndex, mousePos) {// NEW for nodeAlgorithm["circular"].mouseMove() with circular renderer
  let i = nodeIndex;
  let pos = mousePos;

    if (!nodes[i].node.circularOrientation)
        return false;
    let msgBox = nodes[i].node;
    let boxWidth = (msgBox.dimensions.topRight.x + boxPadding) - (msgBox.dimensions.topLeft.x - boxPadding)
    let boxHeight = (msgBox.dimensions.bottomLeft.y + boxPadding) - (msgBox.dimensions.topLeft.y + - boxPadding)

    let charSize = nodes[i].node.circularOrientation.internalAngles.width / (nodes[i].node.message.length);



    let angle = nodes[i].node.angle;
    let totalMaxXLength = nodes[i].node.totalMaxXLength;
    let layer = nodes[i].node.layer;
    let rotTranslate = [msgBox.dimensions.topLeft.x - boxPadding + boxWidth / 2, msgBox.dimensions.topLeft.y + - boxPadding + startPaddingYOffset + boxHeight / 2, 0];

    // let rotTranslateCurr = [totalMaxXLength + Math.abs(layerMax[layer].nodeXmax) - boxWidth / 2, 0, 0];
    let lineBeginning = [totalMaxXLength + Math.abs(layerMax[layer].nodeXmax), 0, 0];// correct end point
    let lineEnding = [totalMaxXLength + Math.abs(layerMax[layer].nodeXmax) - boxWidth, 0, 0];//correct Front point
    let center = [0, 0, 0];

    // console.log("BW :" + boxWidth + " - " + (layerMax[layer].nodeXmax))
    // console.log("BW :" + layerMax[layer].nodeXmin + " - " + (layerMax[layer].nodeXmax))
    // console.log("RR :" + rotTranslateCurr[0])


    nodeAlgorithm["circular"].rotatezyx(rotTranslate, center, 0, 0, (angle +  Math.atan(rotTranslate[1] / rotTranslate[0])) * Math.PI / 180)
    // nodeAlgorithm["circular"].rotatezyx(rotTranslateCurr, center, 0, 0, (angle +  Math.atan(rotTranslate[1] / rotTranslate[0])) * Math.PI / 180)
    nodeAlgorithm["circular"].rotatezyx(lineEnding, center, 0, 0, (angle +  Math.atan(rotTranslate[1] / rotTranslate[0])) * Math.PI / 180)
    nodeAlgorithm["circular"].rotatezyx(lineBeginning, center, 0, 0, (angle +  Math.atan(rotTranslate[1] / rotTranslate[0])) * Math.PI / 180)


    // backup:
    // let lineBeginning = [nodes[i].node.totalMaxXLength + Math.abs(layerMax[nodes[i].node.layer].nodeXmax), 0/*-charSize / 2*/, 0];// correct end point
    // let lineEnding = [nodes[i].node.totalMaxXLength + Math.abs(layerMax[nodes[i].node.layer].nodeXmax) - boxWidth, 0/*-charSize / 2*/, 0];//correct Front point

    // let center = [0, 0, 0];

    // nodeAlgorithm["circular"].rotatezyx(rotTranslate, center, 0, 0, (angle +  Math.atan(rotTranslate[1] / rotTranslate[0])) * Math.PI / 180)
    // nodeAlgorithm["circular"].rotatezyx(rotTranslateCurr, center, 0, 0, (angle +  Math.atan(rotTranslate[1] / rotTranslate[0])) * Math.PI / 180)
    // nodeAlgorithm["circular"].rotatezyx(lineEnding, center, 0, 0, (angle +  Math.atan(rotTranslate[1] / rotTranslate[0])) * Math.PI / 180)

    // console.log("X Y", nodes[i].node.x, nodes[i].node.y)
    // let angle = nodes[i].node.angle;
    let boxEnd = lineBeginning;//[rotTranslate[0] + nodes[i].node.circularOrientation.internalAngles.width / 2, rotTranslate[1], rotTranslate[2]]//[nodes[i].node.circularOrientation.internalAngles.width / 2 + nodes[i].node.x, 0, 0];
    // boxEnd[0] += boxWidth;
    let boxBegin = lineEnding;//[rotTranslate[0] -nodes[i].node.circularOrientation.internalAngles.width / 2, rotTranslate[1], rotTranslate[2]]//[-nodes[i].node.circularOrientation.internalAngles.width / 2 + nodes[i].node.x, 0, 0];

    // backup:
    // nodeAlgorithm["circular"].rotatezyx(boxBegin, [0, 0, 0], 0, 0, (angle +  Math.atan(boxBegin[1] / boxBegin[0])) * Math.PI / 180)
    // nodeAlgorithm["circular"].rotatezyx(boxEnd, [0, 0, 0], 0, 0, (angle +  Math.atan(boxEnd[1] / boxEnd[0])) * Math.PI / 180)

    // console.log("arr", [ boxBegin[0] + centerX, boxBegin[1] + centerY, boxBegin[2] ], nodes[i].node.center)

      // let correctedStartPos = [(nodes[i].node.rotTranslateCurr[0] + centerX) + nodes[i].node.rotTranslate[0] + 0.1,
      //   (nodes[i].node.rotTranslateCurr[1] + centerY) + nodes[i].node.rotTranslate[1] + 0.1, 0];
      // let correctedEndPos = [(nodes[i].node.rotTranslateCurr[0] + centerX) + nodes[i].node.rotTranslate[0],
      //   (nodes[i].node.rotTranslateCurr[1] + centerY) + nodes[i].node.rotTranslate[1], 0];
      // console.log("found Node: ", correctedStartPos, correctedEndPos, nodes[i].node);

      
      // if (nodes[i].node.message === "5. Compound Angles:") {
      //   console.log(
      //     nodeAlgorithm["circular"].distToSegment(
      //       { x: pos.x, y: pos.y },
      //       { x: centerX + boxBegin[0], y: centerY + boxBegin[1] },
      //       { x: centerX + boxEnd[0], y: centerY + boxEnd[1] },

      //     ) <= charSize,
      //     pos,
      //     { x: centerX + boxBegin[0], y: centerY + boxBegin[1] },
      //     { x: centerX + boxEnd[0], y: centerY + boxEnd[1] },

      //   );//, charSize
      // }
      // console.log("charSize" + charSize);

      // )
      
      // }

      // console.log(
      if (nodeAlgorithm["circular"].distToSegment(
          { x: pos.x, y: pos.y },
          // { x: correctedStartPos[0], y: correctedStartPos[1] },
          // { x: correctedEndPos[0], y: correctedEndPos[1] }
          { x: centerX + boxBegin[0], y: centerY + boxBegin[1] },
          { x: centerX + boxEnd[0], y: centerY + boxEnd[1] },
      //     [centerX + boxBegin[0], 0, centerY + boxBegin[1]],
      //     [centerY + boxEnd[0], 0, centerY + boxEnd[1]],

        ) <= charSize) {
          // rather return items we'd wan't as well:
          return true;//, charSize
      }
      return false;
      // )
      // console.log(
      // collision_wall([pos.x, 0, pos.y],
      //     // [correctedStartPos[0], 0, correctedStartPos[1]],
      //     // [correctedEndPos[0], 0, correctedEndPos[1]],
      //     [centerX + boxBegin[0], 0, centerY + boxBegin[1]],
      //     [centerY + boxEnd[0], 0, centerY + boxEnd[1]],
      //     boxHeight//(nodes[i].node.nodeStartPos[1] - correctedEndPos[1])//4.64599609375 / 2/*thickness?*/
      //   )
      // )

}

nodeAlgorithm["circular"].mouseMove = function(evt) {
  parentBoxHeight = lsRecursiveData.dimensions.bottomLeft.y - lsRecursiveData.dimensions.topLeft.y;
  // console.log(lsRecursiveData.dimensions);

  var pos = nodeAlgorithm["circular"].getMousePos(canvas, evt);
  foundANode = false;

  // loop through nodes and check x coords
  for (let i=0; i < nodes.length; i++) {
    // console.log(nodes[i].node.circularOrientation.topLeft.x +" ||<|| "+ (pos.x - centerX));
    // console.log(findPoint(
    //   nodes[i].node.circularOrientation.topLeft.x, nodes[i].node.circularOrientation.topLeft.y,
    //   nodes[i].node.circularOrientation.bottomLeft.x, nodes[i].node.circularOrientation.bottomLeft.y,
    //   pos.x, pos.y
    // ) ? true : "");
    // if (nodes[i].node.message == "3. Trigonometry") {
    // }


    // if (nodes[i].node.dimensions.topLeft.x < pos.x && nodes[i].node.dimensions.topRight.x > pos.x) {
    //   if (nodes[i].node.dimensions.topLeft.y + startPaddingYOffset < pos.y && nodes[i].node.dimensions.bottomLeft.y + startPaddingYOffset > pos.y) {
    if (nodeAlgorithm["circular"].isWithinBox(i, pos)) {
        // console.log(nodes[i].node.message);// DONE finds the correct node to highlight at the correct positions.
        // then default prevSelectNode based on
        if (!nodeAlgorithm["circular"].isParentCollapsed(nodes[i].node)) {

          let currentNode = nodes[i].node;
          let isNotSameNode = prevSelectedNode !== currentNode;
          foundANode = true;

          if (prevSelectedNode && isNotSameNode) {
            nodeAlgorithm["circular"].drawWordBox(nodes[i].node.rotTranslateCurr[0] + centerX, nodes[i].node.rotTranslateCurr[1] + centerY, prevSelectedNode, prevSelectedNode.angle/*0 remove it's angle*/, prevSelectedNode.colour, prevSelectedNode.nodeStartPos, prevSelectedNode);//center parent node
            // renderNodeBoxWithText(prevSelectedNode, prevSelectedNode.colour);

            nodeAlgorithm["circular"].renderLhsNodes(prevSelectedNode, null);
          }
          if (isNotSameNode || (emphasizedStateNode)) {

            // if (!nodes[i].node.lhs) {
            //   nodeAlgorithm["circular"].drawWordBox(centerX, centerY, nodes[i].node, nodes[i].node.angle/*0 remove it's angle*/, "orange", nodes[i].node.nodeStartPos, nodes[i].node);//center parent node
            // } else {
              
              nodeAlgorithm["circular"].drawWordBox(nodes[i].node.rotTranslateCurr[0] + centerX, nodes[i].node.rotTranslateCurr[1] + centerY, nodes[i].node, nodes[i].node.angle/*0 remove it's angle*/, "orange", nodes[i].node.nodeStartPos, nodes[i].node);//center parent node
            // }
            // renderNodeBoxWithText(nodes[i].node, "orange")

            nodeAlgorithm["circular"].renderLhsNodes(currentNode.lhs, "orange");
          }
          if (isNotSameNode) {
            currentHighlightedNode = currentNode.node;
          }
          prevSelectedNode = nodes[i].node;
          // console.log("found node: " + nodes[i].node.name)
          break;
        }
      // }
    }
  }
  if (!foundANode && prevSelectedNode) {
    let foundCollapsedParent = nodeAlgorithm["circular"].getCollapsedParent(prevSelectedNode);
    if (!foundCollapsedParent) {
      let newColor = prevSelectedNode.specialColor ? prevSelectedNode.specialColor : prevSelectedNode.colour;
      // if (!newColor)
        // console.log(prevSelectedNode)

      nodeAlgorithm["circular"].drawWordBox(prevSelectedNode.rotTranslateCurr[0] + centerX, prevSelectedNode.rotTranslateCurr[1] + centerY, prevSelectedNode, prevSelectedNode.angle/*0 remove it's angle*/, newColor, prevSelectedNode.nodeStartPos, prevSelectedNode);//center parent node
      // renderNodeBoxWithText(prevSelectedNode, newColor);

      nodeAlgorithm["circular"].renderLhsNodes(prevSelectedNode.lhs, null);
    } else {
      nodeAlgorithm["circular"].renderLhsNodes(foundCollapsedParent, null);
    }

      // TODO: fix issue of items in children of collapsed: getting rendered: whether collapsed parent or not and not found a node then:
      nodeAlgorithm["circular"].foundEmphasizedNodeIn(prevSelectedNode);
    prevSelectedNode = null;
  }
  // context.fillStyle = "#000000";
  // context.fillRect (pos.x, pos.y, 4, 4);
}


nodeAlgorithm["circular"].isParentCollapsed = function(node) {
  let parent = node.lhs;
  while (parent) {
    if (parent.collapsed === true) {
      // console.log(node.name);
      return true;
    }
    parent = parent.lhs;
  }
  return false;
}

nodeAlgorithm["circular"].getCollapsedParent = function(node) {
  let deepestCollapse = null;

  let parent = node.lhs;
  while (parent) {
    if (parent.collapsed === true) {
      deepestCollapse = parent;
    }
    parent = parent.lhs;
  }
  return deepestCollapse;
}

nodeAlgorithm["circular"].getEmphasizedParentNode = function(node) {
  if (!emphasizedStateNode)
    return null;

  let parent = node.lhs;
  while (parent) {
    if (parent === emphasizedStateNode) {
      return parent;
    }
    parent = parent.lhs;
  }
  return null;
}

nodeAlgorithm["circular"].renderLhsNodes = function(lhs, colour) {
  while (lhs) {
    nodeAlgorithm["circular"].drawWordBox(lhs.rotTranslateCurr[0] + centerX, lhs.rotTranslateCurr[1] + centerY, lhs, lhs.angle/*0 remove it's angle*/, colour ? colour : lhs.colour, lhs.nodeStartPos, lhs);//center parent node
    // renderNodeBoxWithText(lhs, colour ? colour : lhs.colour);

    lhs = lhs.lhs;
  }
}

nodeAlgorithm["circular"].renderRhsNodes = function(rhs, colour) {
  if (rhs) {
    for (let i = 0; i< rhs.length; i++) {
      nodeAlgorithm["circular"].drawWordBox(rhs[i].rotTranslateCurr[0] + centerX, rhs[i].rotTranslateCurr[1] + centerY, rhs[i], rhs[i].angle/*0 remove it's angle*/, colour ? colour : rhs[i].colour, rhs[i].nodeStartPos, rhs[i]);//center parent node
      // renderNodeBoxWithText(rhs[i], colour ? colour : rhs[i].colour);

      if (!rhs[i].collapsed) {
        nodeAlgorithm["circular"].renderRhsNodes(rhs[i].rhs, colour)
      }
    }
  }
}

nodeAlgorithm["circular"].renderRhsNodesGreyExcept = function(parent, exceptNode) {
  if (parent) {
    if (exceptNode.node) {// override to make more rootlevel
        exceptNode = exceptNode.node;
    }
    for(let i=0; i< parent.rhs.length; i++) {
      if (parent.rhs[i] !== exceptNode) {
        //DONE: color is already in color and we don't mess with that, just the temp rendering. // backup originalNodeColor : todo rather back it up on first render in originalColor on itself : later can add a userColor
        // set the color to grey (in temp rendering):
        let specialColor = parent.rhs[i].specialColor ? parent.rhs[i].specialColor : greyedOutColor;

        nodeAlgorithm["circular"].drawWordBox(parent.rhs[i].rotTranslateCurr[0] + centerX, parent.rhs[i].rotTranslateCurr[1] + centerY, parent.rhs[i], parent.rhs[i].angle/*0 remove it's angle*/, specialColor, parent.rhs[i].nodeStartPos, parent.rhs[i]);//center parent node
        // renderNodeBoxWithText(parent.rhs[i], specialColor/* grey or special color supposedly */);

        if (!parent.rhs[i].collapsed) {
            // console.log("RRRREEECHED", parent.rhs[i]);
            nodeAlgorithm["circular"].renderRhsNodesGreyExcept(parent.rhs[i], exceptNode);// recur for all sub children except exceptNodes children.
        }
      } else {
        // nodeAlgorithm["circular"].getCollapsedParent(exceptNode)
        // console.log("REEECHED")
        nodeAlgorithm["circular"].renderLhsNodes(exceptNode, null/* back to its default color */); // all of exceptNodes parents mustn't be grey so undo it now that we past those nodes.
        if (!exceptNode.collapsed) {
          nodeAlgorithm["circular"].renderRhsNodes(exceptNode.rhs, null);// revert all its children to default colors
        }
      }
    }
  }
}

nodeAlgorithm["circular"].renderSubsectionInColor = function(node, color) {
  if (node) {
    nodeAlgorithm["circular"].drawWordBox(node.rotTranslateCurr[0] + centerX, node.rotTranslateCurr[1] + centerY, node, node.angle/*0 remove it's angle*/, color, node.nodeStartPos, node);//center parent node
    // renderNodeBoxWithText(node, color);
    if (node.rhs) {
        for(let i=0; i< node.rhs.length; i++) {
          nodeAlgorithm["circular"].renderSubsectionInColor(node.rhs[i], color);
        }
    }
  }
}


// function mouseMove(evt) {
//   if (nodeAlgorithmCurrentType === "linear") {
//     nodeAlgorithm["linear"].mouseMove(evt);
//   } else if (nodeAlgorithmCurrentType === "circular") {
//     nodeAlgorithm["circular"].mouseMove(evt);
//   }
// }
