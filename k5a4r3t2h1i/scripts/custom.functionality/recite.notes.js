
function recite() {//spreadSpeechReciteNotes()
    // function-spreads-speech-capabilities-to-children:
    // overwrite all children.customCode functions with the new one:

    let nodeViral = `async function spreadReciteNoteLogic() {
            async function callNodeServer(node) {
                const paramValue = unSymbolise(node.name);
                const url = \`http://localhost:3000/read?param=\${encodeURIComponent(paramValue)}\`;

                try {
                    const response = await fetch(url);
                    const data = await response.text();

                    // console.log(data)// nothing to do with the data for now so dont print it
                    // document.getElementById('response').innerText = data;
                } catch (error) {
                     console.log('Error: ' + error)
                    // document.getElementById('response').innerText = 'Error: ' + error;
                }
            }
            let currSpeechRootParent = currentHighlightedNode;

            async function callRecursiveSpeech(rhs, rootParent) {
                if (rhs) {
                    for (let i = 0; i < rhs.length; i++) {
                        if (rootParent !== currentHighlightedNode) {
                            break;
                        }
                        // color the node in pink currently
                        renderNodeBoxWithText(rhs[i], "pink")

                        // recite the node name on the server // potentially add a delay here (but on server side)
                        // console.log("recursiveeeee: " + rhs[i].name)
                        await callNodeServer(rhs[i]);

                        // reset the color of node back to what it was
                        renderNodeBoxWithText(rhs[i], rhs[i].prevColor ? rhs[i].prevColor : (rhs[i].colour ? rhs[i].colour : "blue"))// default back to its original color

                        // recurUntil all inner nodes are reached:
                        if (!rhs[i].collapsed) {// only read the ones being displayed currently
                            await callRecursiveSpeech(rhs[i].rhs, rootParent);
                        }
                    }
                }
                return true;
            }
            while (currSpeechRootParent === currentHighlightedNode) {
                await callRecursiveSpeech([currSpeechRootParent], currSpeechRootParent);
                // consider sleep server timing.
                // TODO: consider making these completely asynchronous if needed
            }
            return true;

        }`

    function recursiveCustomOverride(rhs) {
        if (rhs) {
            for (let i = 0; i < rhs.length; i++) {
                // currentHighlightedNode.customCode = nodeViral;
                if (!rhs[i].customCode) {
                    rhs[i].customCode = nodeViral;
                }
                recursiveCustomOverride(rhs[i].rhs);
            }
        }

    }
    currentHighlightedNode.customCode = nodeViral;
    recursiveCustomOverride([currentHighlightedNode])
    // TODO: could even evaluate currentHighlightedNode.customCode here if want to get it to trigger autonomously
    return true;
}