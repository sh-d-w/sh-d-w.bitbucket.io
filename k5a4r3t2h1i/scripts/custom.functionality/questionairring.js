function cueCards() {
    // 2. onClick again should deactivate the cueCarding and reset the rhs nodes
    let cueCardResetFunction = ` function cueCardResetFunction() {
        global.questionairre.node.rhs = global.questionairre.rhs;
        global.questionairre.node.customCode = "cueCards;"
        global.questionairre = undefined;
        reRenderDiagram();
    }`


    // 2. secθ = 1/cosθ
    // cosecθ = 1/sinθ

// 1. onclick should activate and start the cueCarding
    global.questionairre = {
        node: currentHighlightedNode,
        rhs: currentHighlightedNode.rhs,
        unAsked: [],
        originalList: [],
        correct: [],
        incorrect: [],// considering when/if to use this but correctly used should help remember effectively down the line
        correctAnswer: "",
        firstTimeCorrect: undefined, //  will add this to know if user got first time correct and then if so can remove from unAsked for now (as well as if with voice can move on to next immediately)
        functionDependencies: [
            loadNextQuestion.toString(),
            shuffle.toString()
        ]
    }
    global.questionairre.node.rhs = [];// remove rhs to use for questionairring
    voiceServerSocket();

    // fetch and store all unAsked questions:
    let category = {name: global.questionairre.node.name, formulas: []};
    global.questionairre.unAsked.push(category);
    recurForCategories(category, global.questionairre.rhs);
    global.questionairre.originalList = JSON.parse(JSON.stringify(global.questionairre.unAsked));

    loadNextQuestion();
    reRenderDiagram();
    // TODO now overwrite the customCode for this node for the deactivate behaviour:
    global.questionairre.node.customCode = cueCardResetFunction;


    // Internal functions we'll need for queCarding:
    function loadNextQuestion() {
        global.questionairre.firstTimeCorrect = true;// always true until proven false
        if (global.questionairre.unAsked.length > 0) {
            while (global.questionairre.unAsked.length > 0) {
                let sectionIndex = Math.floor((Math.random() * 50)) % global.questionairre.unAsked.length;
    
                console.log("QUESTIONAIRRING DETAILS: ", sectionIndex,
                    global.questionairre.unAsked[sectionIndex], global.questionairre)
                if (global.questionairre.unAsked[sectionIndex].formulas.length > 0) {
                    let questionIndex = Math.floor((Math.random() * 50)) % global.questionairre.unAsked[sectionIndex].formulas.length;
                    let question = global.questionairre.unAsked[sectionIndex].formulas[questionIndex];
                    // now reconstruct a new rhs for question.question:
                        // from up to 4 random question results within a subsection:
 
                    global.questionairre.unAskedSectionIndex = sectionIndex;
                    global.questionairre.unAskedQuestionIndex = questionIndex;
                    
                    let rhs = []

                    rhs.push({
                        name: question.question,
                        rhs: [],
                        customCode: `function f() {
                            loadNextQuestion();
                            reRenderDiagram();
                            ` + global.questionairre.functionDependencies[0] + global.questionairre.functionDependencies[1] + `
                        }`// onClick will load a new question
                    })
                    // question.question
    
                    question.answer
    
                    let originalQuestions;
                    for (let i=0; i < global.questionairre.originalList.length; i++) {
                        if (global.questionairre.unAsked[sectionIndex].name === global.questionairre.originalList[i].name) {
                            originalQuestions = global.questionairre.originalList[i].formulas;
                            break;
                        }
                    }
    
                    let includedAnswers = [question.answer];
                    while (includedAnswers.length < 4 && includedAnswers.length < originalQuestions.length) {
                        let randomIndex = Math.floor(Math.random() * 50) % originalQuestions.length;
                        if (!includedAnswers.includes(originalQuestions[randomIndex].answer)) {
                            includedAnswers.push(originalQuestions[randomIndex].answer);
                        }
    
                    }
    
                    shuffle(includedAnswers);// jumble up answers
                    // push the 4 random ordered answers
                    for (let i=0; i < includedAnswers.length; i++) {
                        rhs[0].rhs.push({
                            name: includedAnswers[i],
                            customCode: `function f(voiceNode) {
                                let node = voiceNode ? voiceNode : currentHighlightedNode;
                                if (node.name === global.questionairre.correctAnswer) {
                                    node.specialColor = greenedOutColor;
                                    if (global.questionairre.firstTimeCorrect) {
                                        // TODO after n seconds must then load next question
                                        loadNextQuestion();
                                        reRenderDiagram();

                                        // need to remove it from unAsked questions:
                                        let si = global.questionairre.unAskedSectionIndex;
                                        let qi = global.questionairre.unAskedQuestionIndex;
                                        // removes the current question:
                                        global.questionairre.unAsked[si].formulas
                                            = [ ...global.questionairre.unAsked[si].formulas.slice(0, qi), ...global.questionairre.unAsked[si].formulas.slice(qi + 1) ];
                                    }
                                } else {
                                    node.specialColor = redOutColor;
                                    global.questionairre.firstTimeCorrect = false;
                                }
                                emphasizedStateNode = node.lhs;
                                reRenderDiagram();
                                ` + global.questionairre.functionDependencies[0] + global.questionairre.functionDependencies[1] + `
                            }`,
                            lhs: rhs[0]
                        });
                        
                    }
                    global.questionairre.correctAnswer = question.answer;
                    global.questionairre.node.rhs = rhs;// load in these new questions for the user
                    global.questionairre.socketRhs = rhs[0].rhs;
                    break;// exit the while loop as we found a legitamate scenario.
                } else {
                    // remove the empty node:
                    global.questionairre.unAsked = [ ...global.questionairre.unAsked.slice(0, sectionIndex), ...global.questionairre.unAsked.slice(sectionIndex + 1) ];// removes the currentNode
                }
            }
        } else {
            // must then reload unanswered from
            global.questionairre.unAsked = JSON.parse(JSON.stringify(global.questionairre.originalList));
            loadNextQuestion();
        }
    }
    
    function shuffle(array) {
        let currentIndex = array.length;
      
        // While there remain elements to shuffle...
        while (currentIndex != 0) {
      
          // Pick a remaining element...
          let randomIndex = Math.floor(Math.random() * currentIndex);
          currentIndex--;
      
          // And swap it with the current element.
          [array[currentIndex], array[randomIndex]] = [
            array[randomIndex], array[currentIndex]];
        }
      }
    
    function recurForCategories(category, rhs) {
        if (rhs && rhs.length > 0) {
            for (let i=0; i < rhs.length; i++) {
                if (rhs[i].name.includes("=")) {
                    let result = rhs[i].name.split("=");
                    if (result.length === 2) {
                        category.formulas.push({ question: result[0], answer: result[1] });
                    } else {
                        console.log("QUESTIONAIRRE ERROR on: ", rhs[i].name);
                    }
                    if (rhs[i].rhs && rhs[i].rhs.length > 0) {
                        recurForCategories(category, rhs[i].rhs);// recur in but dont change category
                    }
                } else {
                    // new category so recur into it:
                    let newCategory = {name: rhs[i].name, formulas: []};
                    global.questionairre.unAsked.push(newCategory);
                    recurForCategories(newCategory, rhs[i].rhs)// recur and add to new category
                }
            }
        }
    }

    function voiceServerSocket() {
        global.questionairre.socket = new WebSocket('ws://localhost:3000');
        
        // let accelerometerData = { x: 0, y: 0, z: 0 };
        // let gyroscopeData = { x: 0, y: 0, z: 0 };

        global.questionairre.socket.onmessage = function (event) {
            const data = event.data.trim();

            console.log("VOICE: DATA: ", data);
            let index = parseInt(data);
            // if (data === "1") {

            // } else if (data === "2") {
            // } else if (data === "3") {
            // } else if (data === "4") {
            // } else if (data === "5") {
            //     // means go next
            // }
            let customCode;
            let selectNode;
            if (index - 1 < global.questionairre.socketRhs.length) {
                selectNode = global.questionairre.socketRhs[index - 1];
            } else if (index === 5) {
                // then user said next:
                // thus click the currentNodes parent
                selectNode = global.questionairre.socketRhs[0].lhs;
            }

            let result = new Function("return " + selectNode.customCode)();
            // currently do nothing with result as internal logics what matters
            console.log("QUESTIONAIRE VOICE CLICKED: ", index, result(selectNode))// we are parsing in the node selected by voice

            eval()

        };
    }

}
