// TODO: 
// 1. fix padding top

// node info (for updating):
var nodes = [];// we can search this later against box items that share the mouses coordinates
var prevSelectedNode = null;
// TODO: must sort these nodes by x coordinates for optimisation purposes
// 1. firstly onmouseover of the canvas should call a function that takes the mouse position and iterates through the nodes until it finds one with the same node.dimensions.topLeft and node.dimensions.topRight if so check same node.dimentions.bottomLeft and node.dimensions.bottomRight

// meta data:
var xOffsetSpacing = 20;
var yOffsetSpacing = 2 + 9;
var boxPadding = 2 + 2;
var startPaddingYOffset = 20;
var colourSpectrumStart = 10;

var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");

// let messageBox = {
// 	"x" : 10,
// 	"y" : 25,
// 	"size" : 18,
// 	"message" : "Hello world",
//     "color" : "orange"
// };

// //console.log(messageBox);
// let box1 = drawWordBox(ctx, messageBox, true);
// //messageBox.y = 3000;
// box1.x = 100;
// box1.color = "red";
// box1.message = "new Object()"
// drawWordBox(ctx, box1, true);
// messageBox.y = 100;
// drawWordBox(ctx, messageBox, true);
let windowWidth = ctx.canvas.width;//  = window.innerWidth;
let windowHeight = ctx.canvas.height;// = window.innerHeight;
// render all the recursive directories:

let strokeStyles = [// TODO store these for future
  '#ff7571',// 0 peach
  '#ff3b9e',// 1 pink
  '#ff3cc7',// 2 more pink
  '#f440c6',// 3 even more pink
  '#f53cf3',// 4 purplish
  '#d03bf7',// 5 purple
  '#d03bff',// 6 more purple
  '#ac3aff',// 7 even more purple
  '#a478ff',// 8 lilack
  '#8079ff',// 9 indigo
  '#419eff',// 10 blueish
  '#39c1ff',// 11 light blue
  '#2ee3e4',// 12 teal
  '#1effb5',// 13 turquise
  '#1eff8d',// 14 light green
  '#66ff66',// 15 bright green
  '#8dff65',// 16 yellow green
  '#b4ff14',// 17 light yellowish
  '#e1e01a',// 18 yellow
  '#ffbb1e',// 19 light orange
  '#ff9820',// 20 orange
]


  // render current item
  let rootNodeBox = getWordBox(ctx, {
      "x" : 0,
      "y" : 0,
      "size" : 18,
      "message" : lsRecursiveData.name,
      "color" : "#00FFFF",
      "offset" : {//offset will be the spacing you want between the nodes
        "x" : 10, 
        "y" : 10
      }
    });
    // rootNodeBox.y = (windowHeight / 2) - (rootNodeBox.topLeft.y / 2);
    // rootNodeBox = getWordBox(ctx, rootNodeBox);
    // console.log(rootNodeBox);
    // drawWordBox(ctx, rootNodeBox, true);
  // foreach (item in current.rhs) {
    // recursiveNodeRender(lsRecursiveData.rhs, rootNodeBox.topRight.x, rootNodeBox);
    let layerMax = [];//an extending array of the tracking of the MaxXLength of each Layer
    // layerMax.push({nodeXmin : 0, nodeXmax : 0, nodeCount : 0});
    // layerMax = 

    //RENDERS NODES FROM LEFT TO RIGHT:
    recursiveRightMostFIrstLayeredNodeTrack([lsRecursiveData], 0, layerMax, rootNodeBox);//do not skip the first element
    recursiveRightMostFIrstNodeRender([lsRecursiveData], 0, 0, layerMax, rootNodeBox, null/* lhs */);
    // }
    // recur its rhs but with max.x length of the previous set

function  recursiveRightMostFIrstLayeredNodeTrack(rhs, layer, layerMax, rootNodeBox) {
  //gets the maxXLength stored per layerCount in layerMax[layerNbr]:
  // let maxXLength = layerMax[layer].nodeXmin;
  let rhsLength = rhs.length;
  let count = 1;
  let numberOfNodes = 0;
  //1. this loop prioritises getting the maxXLength for the Layer
  if (layer >= layerMax.length)
    layerMax.push({nodeXmin : 0, nodeXmax : 0, nodeCount : 0, currentPosY : 0});
  let childNodeBox;
  rhs.forEach(rhsNode => {
    //render each rhs node:
    childNodeBox = getWordBox(ctx, {
      "x" : 0,
      "y" : 0,
      "size" : rootNodeBox.size,
      "message" : rhsNode.name,
      "color" : "orange"// unused currently as using only roots color value...
    });
    //calculate the max of this layer:
    // console.log({"LLAYERR" : layer, "BRX" : childNodeBox.bottomRight.x, "MAX" : layerMax[layer].nodeXmax })
    if (childNodeBox.bottomRight.x >= layerMax[layer].nodeXmax) {
      layerMax[layer].nodeXmin = childNodeBox.bottomLeft.x;
      layerMax[layer].nodeXmax = childNodeBox.bottomRight.x;
    }
    recursiveRightMostFIrstLayeredNodeTrack(rhsNode.rhs, layer + 1, layerMax, rootNodeBox);//do not skip the first element
  });
  return layerMax;
}
    
function  recursiveRightMostFIrstNodeRender(rhs, totalMaxXLength, layer, layerMax, rootNodeBox, lhs) {

  // let maxXLength = 0;
  let rhsLength = rhs.length;
  // let count = 1;

  //2. this loop subtracts the maxXLength of current layer to get starting x position of current layer
  //   and adds this to the sub total for the previous layer to know where it should begin itself on the x axis:
  let boxSize = Math.abs(rootNodeBox.bottomLeft.y - rootNodeBox.topLeft.y);// can optimise this by not keeping it here in the recursive loop as just needs single calculation initially
  let childNodeBox;
  let rhsIndex = -1;
  rhs.forEach(rhsNode => {
    ++rhsIndex;
    // DONE: switch to loop recursive 1 time to get the meta data
    // DONE: and then loop a second time to render the meta data
    //render each rhs node
    childNodeBox = JSON.parse(JSON.stringify(rootNodeBox));
    childNodeBox.message = rhsNode.name;
    childNodeBox.x = totalMaxXLength;//totalMaxXLength;
    if (rhsNode.rhs.length > 0) {
      // get the yposition for this LHS node:
      recursiveRightMostFIrstNodeRender(rhsNode.rhs, totalMaxXLength + layerMax[layer].nodeXmax + xOffsetSpacing, layer + 1, layerMax, rootNodeBox, rhsNode);// totalMaxXLength + maxXLength, layer
      // childNodeBox.y = rhsMetaData.lhsYPos;
//      childNodeBox.y = ((layerMax[layer + 1].nodeCount + 1) - (rhsNode.rhs.length / 2)) * boxSize - (boxSize / 2);

      // totalMaxXLength = rhsMetaData.totalMaxXLength;
      //DONE: SWITCH to use this formula instead:
      childNodeBox.y = rhsNode.rhs[0].dimensions.topRight.y + 1 + (Math.abs(rhsNode.rhs[0].dimensions.topRight.y - rhsNode.rhs[rhsNode.rhs.length - 1].dimensions.bottomRight.y) / 2) + (boxSize / 4);// div 4 but works
    }
    else {
      // if no rhs :
      // childNodeBox.y = (windowHeight / (rhsLength + 1)) * (count) - (boxSize / 2);//rhsMetaData.lhsYPos;
      if (layer == layerMax.length - 1) {
        childNodeBox.y = (layerMax[layer].nodeCount + 1) * boxSize;//rhsMetaData.lhsYPos;
      } else {
      //DONE: missing scenario:
      //use current layerMax[layerMax.length - 1].bottomRight.y if there is no rhs
        if (layerMax[layerMax.length - 1].currentPosY > 0) {
          childNodeBox.y = layerMax[layerMax.length - 1].currentPosY + boxSize;
          layerMax[layerMax.length - 1].currentPosY += boxSize;
        } else {
          // childNodeBox.y = (layerMax[layer].nodeCount + 1) * boxSize;//rhsMetaData.lhsYPos;
          childNodeBox.y = (layerMax[layer].nodeCount + 1) * boxSize;//rhsMetaData.lhsYPos;
          // childNodeBox.y = 0 + boxSize;
        }
      }

    }
    childNodeBox = getWordBox(ctx, childNodeBox);
    // console.log(childNodeBox);
    // console.log(totalMaxXLength.toString() + JSON.stringify(rhsMetaData));
    // console.log({"boxsize" : boxSize, "layerMax+1" : layerMax, "LYR" : layer});

    //store the nodeSize in the object itself:
    childNodeBox.dimensions = rhsNode.dimensions = {
      topRight : childNodeBox.topRight,
      bottomRight : childNodeBox.bottomRight,
      topLeft : childNodeBox.topLeft,//unused?? No longer unused since can select nodes
      bottomLeft : childNodeBox.bottomLeft,//unused?? No longer unused "  "   "
      message: rhsNode.name
      //...
    }

    if (childNodeBox.bottomRight.y > layerMax[layerMax.length - 1].currentPosY) {
      layerMax[layerMax.length - 1].currentPosY = childNodeBox.bottomRight.y + yOffsetSpacing;
    }

    layerMax[layer].nodeCount += 1;
    let colour = strokeStyles[(rhsIndex + colourSpectrumStart) % 20];
    drawWordBox(ctx, childNodeBox, true, [rhsNode, ...rhsNode.rhs], rhsIndex, colour);

    // add the node to the array for future rearrangement here:
    rhsNode.colour = colour;
    rhsNode.message = rhsNode.name;
    rhsNode.x = childNodeBox.x;
    rhsNode.y = childNodeBox.y;
    rhsNode.lhs = lhs;
    nodes.push({ node: rhsNode });
  });
}


    
// function  recursiveNodeRender(rhs, totalMaxXLength, rootNodeBox) {
// this was no rhs first attempt which currently has its flaws:

//   let maxXLength = 0;
//   let rhsLength = rhs.length;
//   let count = 1;
//   rhs.forEach(rhsNode => {
//     //render each rhs node
//     let childNodeBox = JSON.parse(JSON.stringify(rootNodeBox));
//     childNodeBox.x = totalMaxXLength;
//     childNodeBox.y = Math.abs(rootNodeBox.topLeft.y - rootNodeBox.bottomLeft.y/*padding gap*/) * count;
//     childNodeBox.message = rhsNode.name;
//     childNodeBox = getWordBox(ctx, childNodeBox);
//     console.log(childNodeBox);
//     drawWordBox(ctx, childNodeBox, true);
//     //calculate the max of this layer:
//     if (childNodeBox.topRight.x - totalMaxXLength > maxXLength)
//       maxXLength = childNodeBox.topRight.x - totalMaxXLength;

//     count += 1;
//   });

//   rhs.forEach(rhsNode => {

//     //render each rhs->rhs node (rhs most):
//     recursiveNodeRender(rhsNode.rhs, totalMaxXLength + maxXLength, rootNodeBox);
//   });
// }

function	getWordBox(ctx, msgBox) {
  ctx.font = msgBox.size + "px Arial";
  let internalMetrics = ctx.measureText(msgBox.message);

  msgBox.actualBoundingBoxLeft = internalMetrics.actualBoundingBoxLeft;
  msgBox.actualBoundingBoxAscent = internalMetrics.actualBoundingBoxAscent;
  msgBox.actualBoundingBoxRight = internalMetrics.actualBoundingBoxRight;
  msgBox.actualBoundingBoxDescent = internalMetrics.actualBoundingBoxDescent;

  updateWordBox(msgBox);

  let outBox = JSON.parse(JSON.stringify(msgBox));//duplicate json Object  
  return outBox;
}

function  updateWordBox(msgBox) {
  msgBox.topLeft = {
  	"x" : msgBox.x - msgBox.actualBoundingBoxLeft,
    "y" : msgBox.y - msgBox.actualBoundingBoxAscent
  };

  msgBox.topRight = {//msgBox.bottomLeft = {
  	"x" : msgBox.x + msgBox.actualBoundingBoxRight,
  	"y" : msgBox.y - msgBox.actualBoundingBoxAscent
  };

  msgBox.bottomRight = {
  	"x" : msgBox.x + msgBox.actualBoundingBoxRight,
    "y" : msgBox.y + msgBox.actualBoundingBoxDescent
  };

  msgBox.bottomLeft = {//msgBox.topRight = {
  	"x" : msgBox.x - msgBox.actualBoundingBoxLeft,
  	"y" : msgBox.y + msgBox.actualBoundingBoxDescent
  }

  return msgBox;
}

function	drawWordBox(ctx, msgBox, fill, rhs, rhsNodeIndex, colour) {
  let fillColor = colour//msgBox.color;
  let strokeColor = fillColor//msgBox.color;

  renderNodeBoxWithText(msgBox, fillColor);
  //if drawlines:
  if (rhs && rhs.length > 0) {
    ctx.strokeStyle = "black";
    // console.log(rhs)
    let boxSize = Math.abs(rhs[0].dimensions.topRight.y - rhs[0].dimensions.bottomRight.y);
    let halfBoxSize = boxSize / 2;
    for (let i = 1; i < rhs.length; i++) {
      ctx.strokeStyle = strokeStyles[(i + colourSpectrumStart) % 20];

      ctx.beginPath();
      ctx.moveTo(
          rhs[0].dimensions.topRight.x,
          rhs[0].dimensions.topRight.y + halfBoxSize + startPaddingYOffset
      );
      ctx.lineTo(
        rhs[i].dimensions.topLeft.x,
        rhs[i].dimensions.topLeft.y + halfBoxSize + startPaddingYOffset
      );
      ctx.closePath();
      ctx.stroke();
    }
  }
  
  return msgBox;
}

function renderNodeBoxWithText(msgBox, color) {
  ctx.fillStyle = color;//msgBox.color;
  ctx.strokeStyle = color;//msgBox.color;
  ctx.font = msgBox.size + "px Arial";

  // Historical: MANUAL DRAWING OF THE BOX:
  // ctx.beginPath();
  // ctx.moveTo(
  //     msgBox.topLeft.x - boxPadding,
  //     msgBox.topLeft.y + - boxPadding
  // );
  // ctx.lineTo(
  //   msgBox.bottomLeft.x - boxPadding,
  //   msgBox.bottomLeft.y + boxPadding
  // );
  // ctx.lineTo(
  //   msgBox.bottomRight.x + boxPadding, 
  //   msgBox.bottomRight.y + boxPadding
  // );
  // ctx.lineTo(
  // 	msgBox.topRight.x + boxPadding,
  //   msgBox.topRight.y - boxPadding
  // );
  // ctx.closePath();
  // ctx.fill();
  // ctx.strokeStyle = "blue";

  // Latest: Using existing box drawing capabilities with radius edges:
  let boxWidth = (msgBox.dimensions.topRight.x + boxPadding) - (msgBox.dimensions.topLeft.x - boxPadding)
  let boxHeight = (msgBox.dimensions.bottomLeft.y + boxPadding) - (msgBox.dimensions.topLeft.y + - boxPadding)

  ctx.beginPath();

  ctx.roundRect(msgBox.dimensions.topLeft.x - boxPadding, msgBox.dimensions.topLeft.y + - boxPadding + startPaddingYOffset, boxWidth, boxHeight, [10]);
  // ctx.strokeStyle = "#000000"//msgBox.color;
  // ctx.stroke();
  ctx.fill();
  // ctx.roundRect(10, 20, 150, 100, [40]);
  // ctx.closePath();

  // ctx.strokeStyle = "black";

  // ctx.beginPath();
  // console.log("MMSSSSG: " + msgBox.message)
  ctx.fillStyle = "#000000";
  ctx.fillText(msgBox.message, msgBox.x, msgBox.y + startPaddingYOffset);
  // ctx.stroke();
  // ctx.closePath();

//   let randRot = Math.floor((Math.random() * 10)) * (-1 + 2 * ((Math.random() * 2) === 1));
//   ctx.rotate((randRot * Math.PI) / 180);
// ctx.rotate((-randRot * Math.PI) / 180);

}

function getMousePos(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };
}

function mouseMove(evt) {
  var pos = getMousePos(canvas, evt);

  // loop through nodes and check x coords
  let foundANode = false;
  for (let i=0; i < nodes.length; i++) {
    if (nodes[i].node.dimensions.topLeft.x < pos.x && nodes[i].node.dimensions.topRight.x > pos.x) {
      if (nodes[i].node.dimensions.topLeft.y + startPaddingYOffset < pos.y && nodes[i].node.dimensions.bottomLeft.y + startPaddingYOffset > pos.y) {
        let currentNode = nodes[i].node;
        let isNotSameNode = prevSelectedNode !== currentNode;
        foundANode = true;
        if (prevSelectedNode && isNotSameNode) {
          renderNodeBoxWithText(prevSelectedNode, prevSelectedNode.colour)
          renderLhsNodes(prevSelectedNode, null);
        }
        if (isNotSameNode) {
          renderNodeBoxWithText(nodes[i].node, "orange")
          renderLhsNodes(currentNode.lhs, "orange");
        }
        prevSelectedNode = nodes[i].node;
        // console.log("found node: " + nodes[i].node.name)
        break;
      }
    }
  }
  if (!foundANode && prevSelectedNode) {
    renderNodeBoxWithText(prevSelectedNode, prevSelectedNode.colour)
    renderLhsNodes(prevSelectedNode, null);
    prevSelectedNode = null;
  }

  // context.fillStyle = "#000000";
  // context.fillRect (pos.x, pos.y, 4, 4);
}
function renderLhsNodes(lhs, colour) {
  while (lhs) {
    renderNodeBoxWithText(lhs, colour ? colour : lhs.colour);
    lhs = lhs.lhs;
  }
}
