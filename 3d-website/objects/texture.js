var fieldOfViewRadians;

var program;
var positionLocation;
var texcoordLocation;
var matrixLocation;
var textureLocation;
var positionBuffer;
var texcoordBuffer;

var texture;
var image;

var modelXRotationRadians;
var modelYRotationRadians;
var then;

function isPowerOf2(value) {
    return (value & (value - 1)) === 0;
}

function radToDeg(r) {
    return r * 180 / Math.PI;
}

function degToRad(d) {
    return d * Math.PI / 180;
}

async function load_texture() {
  // Get A WebGL context
  /** @type {HTMLCanvasElement} */
  // canvas_gpu = document.querySelector("#canvas-gpu");
  // gl = canvas_gpu.getContext("webgl");
  // if (!gl) {
    // return;
  // }

	// main_loader();//WEBGL LOADER;

	// <!-- document.getElementById("scripts").value = "<script id='fragment-shader-3d'>precision mediump float;varying vec2 v_texcoord;uniform sampler2D u_texture;void main() {gl_FragColor = texture2D(u_texture, v_texcoord);}</script>"; -->

	// <!-- var my_awesome_script = document.createElement('fragment-shader-3d'); -->
	// <!-- my_awesome_script.setAttribute('src', 'fragment-shader-3d.js'); -->
	// <!-- document.head.appendChild(my_awesome_script); -->

	// <!-- var s = document.createElement( 'script' ); -->
	// <!-- s.setAttribute( 'src', "fragment-shader-3d.js" ); -->
	// <!-- s.setAttribute("id", "fragment-shader-3d"); -->
	// <!-- document.body.appendChild( s ); -->

  // setup GLSL program
  program = webglUtils.createProgramFromScripts(gl, ["vertex-shader-3d", "fragment-shader-3d"]);

  // look up where the vertex data needs to go.
  positionLocation = gl.getAttribLocation(program, "a_position");
  texcoordLocation = gl.getAttribLocation(program, "a_texcoord");

  // lookup uniforms
  matrixLocation = gl.getUniformLocation(program, "u_matrix");
  textureLocation = gl.getUniformLocation(program, "u_texture");

  // Create a buffer for positions
  positionBuffer = gl.createBuffer();
  // Bind it to ARRAY_BUFFER (think of it as ARRAY_BUFFER = positionBuffer)
  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

  // Put the positions in the buffer
  setGeometry(gl);

  // provide texture coordinates for the rectangle.
  texcoordBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, texcoordBuffer);
  // Set Texcoords.
  setTexcoords(gl);

  // Create a texture.
  texture = gl.createTexture();
  gl.bindTexture(gl.TEXTURE_2D, texture);
  // Fill the texture with a 1x1 blue pixel.
  gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE,
                new Uint8Array([0, 0, 255, 255]));
  // Asynchronously load an image
  image = new Image();
  image.src = "data:image/png;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/4QAiRXhpZgAATU0AKgAAAAgAAQESAAMAAAABAAEAAAAAAAD/7AARRHVja3kAAQAEAAAAUAAA/+EDPmh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8APD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4NCjx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4NCgk8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPg0KCQk8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M2IChNYWNpbnRvc2gpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkQ3NDhEREE3N0YzQzExRTNCNUVERTBENjZFMDI5NzRGIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkQ3NDhEREE4N0YzQzExRTNCNUVERTBENjZFMDI5NzRGIj4NCgkJCTx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkQ3NDhEREE1N0YzQzExRTNCNUVERTBENjZFMDI5NzRGIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkQ3NDhEREE2N0YzQzExRTNCNUVERTBENjZFMDI5NzRGIi8+DQoJCTwvcmRmOkRlc2NyaXB0aW9uPg0KCTwvcmRmOlJERj4NCjwveDp4bXBtZXRhPg0KPD94cGFja2V0IGVuZD0ndyc/Pv/bAEMAAgEBAgEBAgICAgICAgIDBQMDAwMDBgQEAwUHBgcHBwYHBwgJCwkICAoIBwcKDQoKCwwMDAwHCQ4PDQwOCwwMDP/bAEMBAgICAwMDBgMDBgwIBwgMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDP/AABEIAQACAAMBIgACEQEDEQH/xAAfAAABBQEBAQEBAQAAAAAAAAAAAQIDBAUGBwgJCgv/xAC1EAACAQMDAgQDBQUEBAAAAX0BAgMABBEFEiExQQYTUWEHInEUMoGRoQgjQrHBFVLR8CQzYnKCCQoWFxgZGiUmJygpKjQ1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4eLj5OXm5+jp6vHy8/T19vf4+fr/xAAfAQADAQEBAQEBAQEBAAAAAAAAAQIDBAUGBwgJCgv/xAC1EQACAQIEBAMEBwUEBAABAncAAQIDEQQFITEGEkFRB2FxEyIygQgUQpGhscEJIzNS8BVictEKFiQ04SXxFxgZGiYnKCkqNTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqCg4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2dri4+Tl5ufo6ery8/T19vf4+fr/2gAMAwEAAhEDEQA/AP38ozRQKADNFFFABRRRQAA5rxb9p/8Ab++Df7F95pNr8TPH2j+F77XZAllZSCW4vJlO/wDfGCFHkWAFGBmZREGwpYEgH8yP+Cq3/Bwp4ij8e+MvhT8EWh0GHw7qEukar4vMqvf3MkMnk3EVjEyFYU879z9qbezDzGiVMRTN+XOuavqHjbxFqGueMrrVNWk1i6e51C7lv5Li8vGHyrJNPIXeZsbCC2CQGyRjbXHPFRT5VuehRwE5Lmloj+oD4U/8FBPgb8cdfj0nwn8WPAOuapPNHBbWlvrcHnXzOgdfs6FgZ1+8haIMokimiJEkMqJ7KrbhX8YA1fWNQudUuPLvIPDsly8EP2q2VAE3jaXTJVmOCnLHJ3cnk1+nH/BH/wD4K96h+wtqS+H/AIgTaprHwl1jMcdxbSXV6vhSSN1VBbwb2SO32vM00cSby5jYbuUrSNbWzJqYNqPNF3P6CM5oqhpeow65ptveWs0N1a3UazQzQuJI5kYZVlYcMpBBBHBBq/mug4QJwKTdzS0UAFFN/wAKdQAUZzSMcCk/5aUAOzmiiigAooooAM5rxn9qH/goL8Hf2MbzSbX4mePdF8L32uSBLKylEtxeTKd/74wQo8iwAowMzKIg2FLAkA/mL/wVV/4OFPEUfjzxl8Kfgi0Ogw+HdQl0jVfF5lV7+5khk8m4isYmQrCnnfuftTb2YeY0SpiKZvy51zV9Q8a+ItQ1zxldapq0msXT3OoXct/JcXl4w+VZJp5C7zNjYQWwSA2SMba46mKinyrc7qOBlJc0tEf0+fCf9v34G/G7Xo9J8J/FrwDrmpXE0cFtaW+twedfs6B1+zoWBnX7yFogyiSKaIkSQyIntAPmCv4wRq+sahc6pceXeQeHZLl4IftVsqAJvG0umSrMcFOWOTu5PJr9OP8AgkB/wV71D9hfU18P/ECbVNY+EusZjjuLaS6vV8KSRuqoLeDeyR2+15mmjiTeXMbDdylaRr62YVMG1Hmi7n9BFN35qjpWow65ptveWs0N1a3UazQzQuJI5kYZVlYcMpBBBHBBrQzXQcIm4UbuaWigBC2DTfOG/bzzQxyaYB+8X6+ntUyutQJqKKKoAooooAKKKKACiiigAooooAKKKKACiiigAooooAK+f/8AgrF/yiy/aW/7JV4o/wDTRdV9AV8//wDBWL/lFl+0t/2SrxR/6aLqgD+IOiiigAooooAKKKKACiiigAooooAKKKKACiiigD+/ymnr+FOpp6/hQA6iiigBrDLVk+JdTm0Pwzf3lrZzahcWdvJPHaxyJG9yyqWEas5CKWIwCxCjPJA5rW/5aV45+31cW/8AwxD8YI7ia8gjuvB2r2ytZzSQ3TPJZyxokLRgyeczsqoEBcuyhQSQDMnZXZUYtySR/KV4Y+Ip8a+KNY1zVHtYNQ1gyapcyJb7oy08zyNFhfuwmSU4HOcY2jcAsXxF8WrZ30V5ayST7SIdoQL5xA3bPLJJKDBAbGT/ABAdK6OL4R3fwT1jWfDHjTwlrul+K0KRiARKLhS4V1h2kFWEnyFSGKsVG1irV+k2n/8ABG3w7rH7O7XnizS/Ddj4wbTzLJcWdoGj0t2PmNtkO0lkPy7iegxnFfM47HUsNyubvzbW1+Z95l+V1sVFxircu99NeiPzgS6tZPC9vo9vH5dqsgu5gjqfNVCMA5y524KhBuG4kk7juGBoVxd2/hma+tmvmtdQkKzxwXZtkt5d2EWQAFmXcCvBBU85G4E+9eH/ANkLx5pCapLb6PoevHSLH7RPqNhqUUjNCtzLAhjjVWdpLiRCqHgEKWOApI0o/gVd/B7x7pvg/wATaPc+EL7XNiLJfuViuSqp+6ZmCr5ilycnr8zc7VU6f2hRsmnczeT1r+9G2p+pX/BsX+0RN42/Zc8XfDm8vrNm+HOshtM0+G2nD2Njdp5x3St+7Km7+2Kka4ZfKfgoUY/p2f3kdfkJ/wAG9/7Ow+HX7aHxk1qx8y60mx0KzsGu4pP9HFxcTeb5bdnk225YBT+7R1yMTIW/XxDsj+le9RqKcIyXVHyOMoulWlB7pjhwKKKK2OMQ/eFLRRQAU0HH5U4nApv+FADqKKKAGsMtWT4l1ObQ/DN/eWtnNqFxZ28k8drHIkb3LKpYRqzkIpYjALEKM8kDmtb/AJaV43+31cW//DEPxgjuJryCO68HavbK1nNJDdM8lnLGiQtGDJ5zOyqgQFy7KFBJAMydldlRi3JJH8pfhj4inxr4o1jXNUe1g1DWDJqlzIlvujLTzPI0WF+7CZJTgc5xjaNwCxfEXxatnfRXlrJJPtIh2hAvnEDds8skkoMEBsZP8QHSuji+Ed38E9Y1nwx408Ja7pfitCkYgESi4UuFdYdpBVhJ8hUhirFRtYq1fpNp/wDwRt8O6x+zu154s0vw3Y+MG08yyXFnaBo9Ldj5jbZDtJZD8u4noMZxXzOOx1LDOLm7821tfmfeZfldXFQcYq3LvfTXoj84EurWTwvb6Pbx+XarILuYI6nzVQjAOcuduCoQbhuJJO47hgaFcXdv4ZmvrZr5rXUJCs8cF2bZLeXdhFkABZl3ArwQVPORuBPvXh/9kLx5pCapLb6PoevHSLH7RPqNhqUUjNCtzLAhjjVWdpLiRCqHgEKWOApI0o/gVd/B7x7pvg/xNo9z4Qvtc2Isl+5WK5Kqn7pmYKvmKXJyevzNztVTp/aFKyadzOWT1r+9G2p+pX/BsX+0RN42/Zc8XfDm8vrNm+HOshtM0+G2nD2Njdp5x3St+7Km7+2Kka4ZfKfgoUY/p2f3sVfkH/wb4/s7j4dftn/GTWrHzLrSbHQrOwa7ik/0cXFxN5vlt2eTbblgFP7tHXIxMhb9fE+SOveo1FOMZLqj5HGUXSrSg90x44FFFFbHGNUZH6U0R5fd6E1JTY6nd6gOoooqgCiiigAooooAKKKKACiiigAooooAKKKKACiiigAr5/8A+CsX/KLL9pb/ALJV4o/9NF1X0BXz/wD8FYv+UWX7S3/ZKvFH/pouqAP4g6KKKACiiigAooooAKKKKACiiigAooooAKKKKAP7/KKKKACiiigCuy7TXC/tFeEL/wAc/BzWrLSbeO81a3WLULC1kcRpd3FtMlxFCzE4RZJIlQsfuhicHGK9CK7hUax7Wb/aOazlFSi4vZlU5OElOO6dz8bf2qP2bPDvxM/by+BfxE0v+z7zSde+ySyAwmGaR7SJruCRlOC25YF3FgMGJVIJ3CvsX4gfCS48X+DI/skelzbW3YurBLwr8wyURyELBckbuM8cdR8yft7/ABy8A+Av+Cgnw70nwz448O6xp9nrF/eazp1jeRXTeGr68NzHcLM6Mdnm3BLGKTLo7zchDGifZ3wp8aW+oaLb27eW0boCrfwyfQ9+PSvzrMMPKOJVKq9Emov53P1vL8Zz4L29FatrmXysfH97/wAE0NO1DxloviqyvLzwz4jsYi2p3ukadZ6b58huI2UILeONJFEaPuE0Tr87D5wcjsP20PgJN8cfFHh2xOrTaL4atZVu7ueAf6TuKYWUkYlZVUOAsLo5fZk7SXj+pfiRerp+kQw2aqsckmbt4owzxoEY8DIySwUZ7A546jkrDx94R8V20dra3kV5fafHsvIWAElsCzCJzycBysoUjhsEgkDJyfMp3uvdtvtfbTW5rSxTlBXi7SutN7b3elullcf/AMEjPgi3wf8A2c9cuJLcWN14m8UahqFxai7e8+zPCU0/Z5z/ADyYWxXDNk4wOgAH1fIfm+leZfsl2K2nwN0uaNt0OoXN7qMEgbcssNxeTzxOD3VkkVhjjBGOK9PkG41+iYS/sY33svyR+U5jJSxE2tuZ/mOByKKBxRXUcYAYFFN/5aU6gAooooAKKKKAK5Xaa4X9ovwhqHjn4Oa1ZaTbx3mrW6xahYWsjiNLu4tpkuIoWYnCLJJEqFj90MTg4xXoLLuFMWPa7f7RzWcoqUXF7MqnJwkpx3Tufjb+1R+zX4d+Jn7eXwL+Iml/2feaTr32SWQGEwzSPaRNdwSMpwW3LAu4sBgxKpBO4V9ifED4SXHi/wAGR/ZI9Lm2tuxdWCXhX5hkojkIWC5I3cZ446j5l/b2+OfgHwF/wUE+Hek+GfHHh3WNPs9Yv7zWdOsbyK6bw1fXhuY7hZnRjs824JYxSZdHebkIY0T7O+FXjO31DRbe3by2jkQFW/hk+h78elfneYYeUcQqVV6JNRfzufrWX4znwXt6S1bXMvlY+P73/gmjp+oeM9F8VWd5eeGfEVjEW1K90jTrPTfPkNxGyhBbxxpIojR9wmidfnYfODkdh+2h8BZvjl4n8O2P9rTaL4btZVu7ueAf6TuKYWUkYlZVUOAsLo5fZk7SXj+pPiTfLp2kQw2aqsckmbt4owzxoEY8DIySwUZ7A546jk9P8feEfFdtHa2t5FeX2nx7LyFgBJbAswic8nAcrKFI4bBIJAycfeU73Xu232vtprc2p4pygrxdndab23u9LdLK47/gkb8EW+D/AOzlrlxJbixuvE3inUNQuLUXb3n2Z4Smn7POf55MLYrhmycYHQAD6wkPzfSvMf2S7FbT4G6XNG26HULm91GCQNuWWG4vJ54nB7qySKwxxgjHFeoON1fomEv7GN97L8kflOYSUsRNrbmf5jgciigcUV1HGNRs0R/c5ojGBTqmIBRRRVAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABXhv/BTPwlq3j7/gm/8AtBaHoWm6hrWta18NfEdhp+n2Fu9zdX9xLpdykUMUSAvJI7sqqigliQACTXuVFAH8AdFanizwpqngPxTqeha7peoaLrmi3cthqGn39u9vdWFxE5SWGWJwHjkR1ZWRgCpBBAIrLoAKKKKACiiigAooooAKKKKACiiigAooooA/v8pp/u06mng7qAHUUUUAAORXg3/BQL9uLw3+wV+zrq3jDXLiNtUmje00DTc5k1bUGjYxRKP7o2l5G6JGjtzgA6v7Xv7afgH9iP4ZXHiXxtrUNgjFo7GxjIkvtWnAz5FtDndLJyCQMKoO52RAzj+en9tr9sLxZ+3X8Zb7xp40umSGTzItF0vfutPD9mzZS2i7M/yjzZRtMzrk7VEcaFm9joo0eZ3exwvwL/ZO17xoPHGueG5l1TUL6WFrGLzN0sogtk3xyZx+9djON3RmjU5Gcj7w/wCCRn/BQHU/FGvf8Kt8Zb4de0cm3tFug8d0UXJ2uGAOQv5ADj1+F/2Yfj1qX7Kvxb0/xDDDJqmjSMI7+0WQZlhbphlyFYE5Gc89eCQfvz4sfsi/CX9uTw/a+PvCuoahovjDUIY59K8S6TeyWmoWUsYO0PGGCEqeHR1JIAwQMGvjs+ouFS1ZNwlrGSWqdtn5H6dw7yVaSVJpSjdSi3ZNXvdeep9deMPgBD4yOpa1qWqa5q0l4p8zT5X+1WsUQbICWrERtxjK4529c815r4W/Zd8L694v8JyaLo3/AAjura1qi6JpF3ZaJFoNxbQmOSW6kAiRH2pDBLKsc/yPJAg2sxQn5d+Kf7bn7Tn7Knwxks/GGk6L4q0XRVigl8UaewtZ73dIkMQmgZsiSR5UTCZBY9Bxu89/Zg/4LVSeL/2svCvirX5c6RpcE32iyswZktpOEMyg4LSKruhCjOHfAODXFleX1K7i1Zxvq090uljozbNPq1CdB6Sa91NK6v1T/wCCf0J6PplvoWmWtnZwx2trbRLDDFGoWOJFAVVUDgAAAADsK0QcivOP2c/2nPBP7V3w6g8U+A/EWn+I9Jkf7PLJbMVktZgoZoZonCyQyhWUmOVVcBlO3BBPo9ffbaH5FK99QooooEFFNHA/GnUAIxwKT/lpSv8AdpP+WlADqKKKAAHIrwX/AIKBftxeG/2Cv2ddW8Ya5cRtqk0b2mgabnMmrag0bGKJR/dG0vI3RI0ducAHW/a9/bT8A/sR/DK48S+NtahsEYtHY2MZEl9q04GfItoc7pZOQSBhVB3OyIGcfz0/ttftheLP26/jLfeNPGl0yQyeZFoul791p4fs2bKW0XZn+UebKNpmdcnaojjQs3sdFGjzO72OF+Bf7J2veNB441zw3MuqahfSwtYxeZullEFsm+OTOP3rsZxu6M0anIzkfeH/AASM/wCCgOp+KNd/4Vb4y8yHXtHJt7RboPHdFFydrhgDkL+QA49fhf8AZh+PWpfsq/FvT/EMMMmqaNIwjv7RZBmWFumGXIVgTkZzz14JB+/Piv8Asi/CX9uPQLXx94V1DUNF8YahDHPpXiXSb2S01CyljB2h4wwQlTw6OpJAGCBg18fn1FwqWrJuMtYyS1Tts/I/TeHeSrSSpNKUbqUW7Jq97rzPrrxj8AYfGR1LWtS1TXNWkvFPmafK/wBqtYog2QEtWIjbjGVxzt655rzXwt+y74X17xf4Tk0XRv8AhHdW1rVF0TSLuy0SLQbi2hMckt1IBEiPtSGCWVY5/keSBBtZihPy78U/23P2nP2VPhjJZ+MNJ0XxVouirFBL4o09haz3u6RIYhNAzZEkjyomEyCx6Djd57+zB/wWqk8X/tZeFfFWvy50jS4JvtFlZgzJbScIZlBwWkVXdCFGcO+AcGuHK8vqVnFqzjfVp7pdLHTm2ZfVqM6L0k17qaV1fqmf0JaNpdvoWmWtnZwx2trbRLDDFGoWOJFAVVUDgAAAADsK0gcivN/2c/2nPBP7V3w7g8U+A/EWn+I9Jkf7PLJbMVktZgoZoZonCyQyhWUmOVVcBlO3BBPpFffbaH5HK99QooooJG/406mq2Wp1LqAUUUUwCiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA/jk/4OP8A4W6D8H/+C2Xx70nw7Y/2dp93qtlrcsXnSTb7zUNMtL+8lzIzEeZdXM8m0Hau/aoVQqj4er9AP+Do3/lOv8dP+4B/6j+mV+f9ABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAH9/lNVqdTSmBQAY3LXyr/wVZ/4KQaT/AME6/gVBfpb2+s+OvFkkll4Y0WSUotzIgUzXU235xbW6uhcrgs0kUYZDKrr9TTSrbxMzMFVRkknAAr+Z3/goz+1RJ+3J+3H40+IFrfpqHhWGYaD4WYBTH/ZVszpDJH0JS4lM90Cw3YusHAUBc6krLQ6MPR9pKz2OH+LHxb8VfHvx5deLPGmt3XiTxNqQ/fXl0eEjzlYIU+7DCpJ2xxgKCSeWZmPJzRCSLbhlBzwRwPXGQQOBjGOen01rOx8yw+ZW7k4H6j2qGSw3xyQttZuvuR9PX9entTpXTuepKKSsjhr6xezjuIYbmSKORfLWMcxuwIx+7Iwp5z3B9+o634UftM/ET9m97hfC+tNZRSAFLddjxSP/AHxHKjqW+TBcjIHoM45/Unaw1aOOQ8XQZY2PAm45RgDye+eufwqaKyS6j81WH2i3BiSIr86FiBuDcZwBLj/f9QK3qRhOPLUSa89TOnKpCSlSk0+6dj9Wv2MviV4Z/bw/Zwm8N/ELVvDus+LNShmsNY0yNBa3U9oVKGXy+BvOfMEkACxkxfddDX44yeHYfCf7S3h66WR4bHxJH50txFFwWaIF2xg4Eiy46f3xwWyPYNGg/sme3nWSRdjK6yq2GiYdHUjocj65+tP8X+D4fE9xDtZbefcktvMIw3kSo25Wwe2cqQMZVmGea8XA4OOFrSlF+7Jp2tovT7z3Mwx08VRiqi9+Kte+rWm50f7Nv7Tvjb9iv4y2Pj3wPqUlnqVq0a6hZmZls/EFqjZNpdLyHjKs21iC0RcvGVcZr+jH9jn9rjwv+2j8DtH8beF53a11KLdJby4E9nKCVkhkAJAdHVkbBK5U4JGCf5hNY1iTwxCLfWrf7CjIES4UmS3fAAG08srAdVcdz8xHX6u/4I0ft7an+x3+1Zp+gX11u8F+PpFguImk/dw3YG1ZkPT5kVVYdfkXgHOPanZrmR8ziKd1fqj+iyobm58kcfyqPTNSh1XT4bqFleGdA6sO4IzVojIrM4SOGTeBUlR24xu+tSUAI/3aT/CiSj/CgAxvFfKv/BVj/gpBpP8AwTr+BcF+lvb6z468WSSWXhjRZJSi3MiBTNdTbfnFtbq6FyuCzSRRhkMquv1NNMtvEzMwVVGSScACv5nf+CjH7VEn7cf7cXjT4gWt8moeFYZhoPhZgFMf9lWzOkMkfQlLiUz3QLDdi6wcBQFzqSstDow9H2krPY4f4sfFvxV8e/Hl14s8aa3deJPE2pD99eXR4SPOVghT7sMKknbHGAoJJ5ZmY8nNEJItuGUHPBHA9cZBA4GMY56fTWs7HzLD5lbuTgfqPaoZLDfHJC21m6+5H09f16e1OldO56kopKyOGvrF7OO4hhuZIo5F8tYxzG7AjH7sjCnnPcH36jrfhR+0z8RP2b3uF8L601lFIAUt12PFI/8AfEcqOpb5MFyMgegzjn9SdrDVo45DxdBljY8CbjlGAPJ7565/CporJLqPzVYfaLcGJIivzoWIG4NxnAEuP9/1ArepGE48tRJrz1M6cqkJKVKTT7p2P1a/Yy+JXhn9vD9nCbw38QtW8O6z4s1KGaw1jTI0FrdT2hUoZfL4G858wSQALGTF910NfjjJ4dh8J/tLeHrpZHhsfEkfnS3EUXBZogXbGDgSLLjp/fHBbI9g0aD+yZ7edZJF2MrrKrYaJh0dSOhyPrn60/xf4Ph8T3EO1lt59yS28wjDeRKjblbB7ZypAxlWYZ5rxcDg44WtKUX7smna2i9PvPczDHTxNGKqL34q176tabnR/s2/tO+Nv2LPjLY+PfA+pSWepWrRrqFmZmWz8QWqNk2l0vIeMqzbWILRFy8ZVxmv6MP2Of2uPC/7aPwO0fxt4XmdrXUot0lvLgT2coJWSGQAkB0dWRsErlTgkYJ/mF1jWJPDEIt9at/sKMgRLhSZLd8AAbTyysB1Vx3PzEdfq7/gjR+3tqf7Hf7Vmn6BfXW7wX4+kWC4iaT93DdgbVmQ9PmRVVh1+ReAc49qdmuZHzOIp3V+qP6LKhubnyRx/Ko9M1KHVdPhuoWV4Z0Dqw7gjNWiMiszhI0k3JTkPFMgGQw96VD8/wDn3qZboCSiiiqAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD+YH/g9W/5Sm+Af+yVad/6d9Yr8ga/X7/g9W/5Sm+Af+yVad/6d9Yr8gaACiiigAooooAKKKKACiiigAooooAKKKKAP7/KavK06mrwtAHxF/wcBftAXnwL/wCCbHiix02aa31L4jXtv4OimTH7uG5DyXinkN89lBdRApllaVW6KSPwQ8PRKJreMbf9aG+YcACEH+bV+o3/AAdV+K9Qi1f4D6GtxcLo9xF4g1KW3z+5nuYTpcUUmP78cdxcKD2Fww7mvyzs22yrt+8xZVUc/wDLLHH4xmsZfEetg42hfudFpU0iQw/x+dG0jbgxxnnJxz3xx71m63rR0gpcOqwmF1Mozx5ZYK7AnGdu7d+A+lWLfVpIYWdNrL9hTYcnkYb1PsOPasPxebfUbOe1umdofLUDafmTJYEjnA4OTkHOMdDXVTKncm8b6RFqdoYJt0UVxjbIv3raXnDr7g/571n+E5LjXtQFvI0VvfWrCC4iJ4Moy0ZX/YkDEg9iVBBIOLmiS/214SjjmYTSIuyST/no6/KzYHTJUnrxn8K5vxXBcWlnHq9mxS90vCXAUbvNh3ZBx6owDDvx6E1o43VjNSs7o9Ohj4UN8i5aKVGHzQsOGH4H/PpchjkDxq3+sjzGQD6f05zXPeD/ABtJ4zvWubxi32xEXf8A3SihNvvtK9f7rLyTknor8Na2y7v9Zbttb/aUjg5x3H9PpXnyjyuzPQjLmjdFH4saQuufD+a4kVd1jPHMf7uGbynJ/wCASMe/I+leC3Ut38PdDmvNOV1utDvUu7KMHCrKGkYY7DIMXoM19C+JNTW78B6/APvXGk3Uag938psHPr3/AArxLX41v/Dl/Iq7vtCRz7l9GMWc+wWFj+J9K6qEbwaOPE2un5H9Pv8AwSe/aNh/am/YQ8A+MI5Gk/tDToyxb7wO0cH3r6RVdtflB/wak/Hqz1j9jzVPhrcXitrHg3Up0ETt87RFyVbHoQQfxr9YKk8fbQiUYuG/CpaQ/eFLSjsAUUUUwPh//g4C/aAvPgb/AME2PFFjps0tvqXxGvbfwdFMmP3cNyHkvFPIb57KC6iBTLK0qt0Ukfgj4eiUTW8Y2/60N8w4AEIP82r9RP8Ag6r8WahHq3wH0NbiZdHuIvEGpS2+79zPcwnS4opMf3447i4UHsLhh3NflrZttlXb95iyqo5/5ZY4/GM1jL4j1sHG0L9zotKmkSGH+Pzo2kbcGOM85OOe+OPes3W9aOkFLh1WEwuplGePLLBXYE4zt3bvwH0qxb6tJDCzptZfsKbDk8jDep9hx7Vh+Lzb6jZz2t0ztD5agbT8yZLAkc4HBycg5xjoa6qZU7k3jfSItTtDBNuiiuMbZF+9bS84dfcH/Pes/wAJyXGvagLeRore+tWEFxETwZRloyv+xIGJB7EqCCQcXNEl/trwlHHMwmkRdkkn/PR1+VmwOmSpPXjP4VzfiuC4tLOPV7Nil7peEuAo3ebDuyDj1RgGHfj0JrRxurGalZ3R6dDHwob5Fy0UqMPmhYcMPwP+fS5DHIHjVv8AWR5jIB9P6c5rnvB/jaTxnetc3jFvtiIu/wDulFCbffaV6/3WXknJPRX4a1tl3f6y3ba3+0pHBzjuP6fSvPlHldmehGXNG6KPxY0hdc+H81xIq7rGeOY/3cM3lOT/AMAkY9+R9K8Fupbv4e6HNeacrrdaHepd2UYOFWUNIwx2GQYvQZr6F8Samt34D1+AfeuNJuo1B7v5TYOfXv8AhXiWvxrf+HL+RV3faEjn3L6MYs59gsLH8T6V1UI3g0ceJtdPyP6ff+CT/wC0bD+1N+wh4B8YRyNJ/aGnRli33gdo4PvX0iq7a/KD/g1J+PVnrH7HmqfDW4vFbWPBupToInb52iLkq2PQgg/jX6wVJ4+2hGpxI/1FEYy34U4HI/Gmw/fal1AkooopgFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQB/MD/AMHq3/KU3wD/ANkq07/076xX5A1+v3/B6t/ylN8A/wDZKtO/9O+sV+QNABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAH9/lNI+enU1RjOaAPzL/wCDk/4DXnxQ+Fvw18QWdv5zeG7zUbXcFyQbiOCQLn3+yHjvt9q/Ft9Ju9FmUXEckbRMDnH3drHJ/KRvyr+oL9sX4QW/xq+CtxptxGJms7mO7iBGcMA0ZP8A3zI1fjF+1N+xdJ4Y1u8SO1bySzAqF7HIP/oX6VNtbnZh63KuVnxNo7SQKY3XzUKva9MFcfMhz/unH4iqurXdjcxorTLH9ot/LIb5cHjk/juFdp4s+FGo6LfTKscyNImQSPlWVO/44H4LXA6zp8iboZ7JlZT50OPugHkqe+OSPxHpXRE6JSuin4B1H7H4h1LT3bK3RW8iQHJIdVEg/wC/ik/Vj7VY1ScabqUizL5kcuY5OPlKnjkdO/X3ryrWdYm+Hvxd0fc032W5f7MGcHDJIpKn0++nPv6V6f4ylXVdFW5j+8vB9jnr/nmtvQx3QfDSFtF12401mykYE0Q/vBOpz/ubP89fSLPUEvtLtmkHygGJwT1GcjHr7fQV5L4X1r7Vf2dyzqkkYa0uCfl+V1YIf++to/Ku48Pap52n4DfeBYg/wkd/5/55rlxEdeY7cPLSzNi9tlh8y1ulikjKlGRvuzIwIIOOcEHHXoa8zfQovC899ot3ukVLTyoGfrInlTeWx7Z+ePPbO7616hNKusWcc3HnRjYxxwfQjFc74w8L3XiC6028t4mM1g628oIOHhkmjYZ9lIYf9tCe1Z4erZ2Y69O6ubn/AASh/ae1z9lP/gpH4G1ixuLiHw/4ilbSdaQghJAxyjsPZuMnuxHpX9UemarDrek295bsskNxGJFIPUEZr+XD9hT9j/XtW8b2Or6r50sy7SGYktkY4CgYRVOcDk+pOK/oR/Ya+JGpL4FsdB1lmaW1jCRO3VgKo8mtbmuj6LByKKb/AIU6gxAnAoooJwKAPzJ/4OTvgNefFD4XfDXxBZ2/nN4bvNRtdwXJBuI4JAuff7IeO+32r8W30m70WZRcRyRtEwOcfd2scn8pG/Kv6gv2xfhBb/Gv4K3Gm3EazNZ3Md3ECM4YBoyf++ZGr8Yv2pv2LpPDGt3iR2reSWYFQvY5B/8AQv0qba3OzD1uVcrPibR2kgUxuvmoVe16YK4+ZDn/AHTj8RVXVruxuY0Vplj+0W/lkN8uDxyfx3Cu08WfCjUdFvplWOZGkTIJHyrKnf8AHA/Ba4HWdPkTdDPZMrKfOhx90A8lT3xyR+I9K6InRKV0U/AOo/Y/EOpae7ZW6K3kSA5JDqokH/fxSfqx9qsapONN1KRZl8yOXMcnHylTxyOnfr715VrOsTfD34u6Puab7Lcv9mDODhkkUlT6ffTn39K9P8ZSrquircx/eXg+xz1/zzW3oY7oPhpC2i67caazZSMCaIf3gnU5/wBzZ/nr6RZ6gl9pds0g+UAxOCeozkY9fb6CvJfC+tfar+zuWdUkjDWlwT8vyurBD/31tH5V3Hh7VPO0/Ab7wLEH+Ejv/P8AzzXLiI68x24eWlmbF7bLD5lrdLFJGVKMjfdmRgQQcc4IOOvQ15m+hReF577RbvdIqWnlQM/WRPKm8tj2z88ee2d31r1CaVdYs45uPOjGxjjg+hGK53xh4XuvEF1pt5bxMZrB1t5QQcPDJNGwz7KQw/7aE9qzw9Wzsx16d1c3P+CUP7T2ufsp/wDBSPwNrFjcXEPh/wARStpOtIQQkgY5R2Hs3GT3Yj0r+qTS9Vh1vSLe7t2WSG4jEikHqCM1/Lf+wp+x/r2reN7HV9V86WZdpDMSWyMcBQMIqnOByfUnFf0I/sNfEjUl8C2Og6yzNLaxhInbqwFUeTWtzXR9D7dxFOUZWnE4FRwDI3etBj5klFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAfzA/8Hq3/KU3wD/2SrTv/TvrFfkDX6/f8Hq3/KU3wD/2SrTv/TvrFfkDQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQB/f5RRRQBHNEtxC0bLuVhgg9xXi3x0/ZA0f4n200kcaR3DA9q9tooA/LX49/wDBPO6026mZtLaQKdyyxJnPY5H5/nXy38QP2DZ4L2TbbyKNxIymMA9eK/ea70u31CPbNEkg9xXK+IPgr4f11GWfS7Vg3+yP8KOaxpGT2P58fiL/AME65vFNjHG1jG01q4lhbaMqR/n9Ko2//BP7UrrTWha1YLIuP93/APVX75X/AOx34RvmJ/s+KPPp+nasm6/YX8Lyn92qr1ONveq52mV7RrqfgjpH/BNfUtPv5N0cgjm+9tOCp65HuCAQfUCu203/AIJ/XkEm7yW/eAMwAxhiOePrk/jX7YN+wp4dZf4f++f0x+lOX9hvQYhtVo/++Tx/n+lTKpfRo0jWa2Z+RPw9/YNa2uB51uzBuDx1r2LwT+wJYvt82zTawKHK5JBr9JtL/Y30WybLOq7egVM/4V1nh/4C6JoTq3k+cy46gKOOnFZ8vYcsRJqzZ8nfs3/sLQ6EiyQaesfzZLuuB7kCvrP4dfBqz8FokhVWnXv6V2Frp0dnGqwqI1UYAHarFaHINTrTqKKAGyfcNA/u0rDIoOc0ARTQrPEyOu5WGCD3FeL/ABy/ZA0f4n2s0kcaR3DA9q9m+ysTncv/AHyf8aDbyf31/BP/AK9TfyKjprc/L349/wDBPO6026mZtLaQKdyyxJnPY5H5/nXy38QP2DZ4L2TbbyKNxIymMA9eK/eC702O/TbPHHKuMYZf8/WuX1/4MaH4gLCbSbORW7kYP5Y/z+lHtHFFxk9rn8+vxF/4J1zeKbGONrGNprVxLC20ZUj/AD+lUbf/AIJ/aldaa0LWrBZFx/u//qr97r79j3wrfMW/s23Rj6Mf8PSsa4/Yh8NyP8tqqrnP3j/h2/z3qfrVtbP7maxv3X3n4MaR/wAE19S0+/k3RyCOb7204Knrke4IBB9QK7bTf+Cf15BJu8lv3gDMAMYYjnj65P41+1Z/YY8P4+6u71x/9bv6VLH+xBokP3fL/Ff88+9HtuZbP7iuZrZr7z8i/h7+wa1tcDzrdmDcHjrXsXgn9gSxfb5tmm1gUOVySDX6Qab+x1pNg6/6sKDztBzj8q7DQvgHouhFWWPzGX+8nB/DNZx16B9YdrN/qfKH7N/7C0OhIskGnrH82S7rge5Ar6y+HXwcs/BaRyFVa4Xv6V1lvp62sarFsRV6DZ/9ep2hZR8rD8VzW1/I5ZD92FpY12IBUYjYfxH8jUg4/wD1U9SR1FFFMAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP5gf+D1b/lKb4B/7JVp3/p31ivyBr9fv+D1b/lKb4B/7JVp3/p31ivyBoAKKKKACiiigAooooAKKKKACiiigAooooA/v8ooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA/mB/4PVv8AlKb4B/7JVp3/AKd9Yr8ga/X7/g9W/wCUpvgH/slWnf8Ap31ivyBoAKKKKACiiigAooooAKKKKACiiigAooooA/v8ooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA/mB/wCD1b/lKb4B/wCyVad/6d9Yr8ga/X7/AIPVv+UpvgH/ALJVp3/p31ivyBoAKKKKACiiigAooooAKKKKACiiigAooooA/v8AKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP5gf+D1b/lKb4B/7JVp3/p31ivyBr7S/wCDgD9rvxl+15/wVe+LU3jCbT2j+G/iDUfAOg29naiCOz0vTdQuooVJyXkkd2lmd3YkyTuF2RiONPi2gAooooAKKKKACiiigAooooAKKKKACiiigD+/yiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD+IP/grF/ylN/aW/wCyq+KP/TvdV8/1qeLfFeqePPFOpa7rup6hrWua1dy3+oahf3D3F1f3Erl5ZpZXJeSR3ZmZ2JLEkkkmsugAooooAKKKKACiiigAooooAKKKKACiiigD+/yiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACvh7/g4/8Ailr3wf8A+CJvx71bw7ff2dqF3pVloksvkxzb7PUNTtLC8ixIrAeZa3M8e4Dcu/cpVgrD7hr8/wD/AIOjf+UFHx0/7gH/AKkGmUAfyB0UUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAf3+UUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAV+R/8AwebeLdW8Pf8ABKPwvaafqWoWNnr3xK02w1OC3uHij1G3XT9TuFhmVSBJGJ4IJQjZAkhjbG5FI/XCvyB/4PVPm/4JZeAf+yq6d/6aNYoA/mBooooAKKKKACiiigAooooAKKKKACiiigAooooA/v8AKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAK/IH/g9W/wCUWXgH/squnf8Apo1iv1+r8gf+D1b/AJRZeAf+yq6d/wCmjWKAP5gaKKKACiiigAooooAKKKKACiiigAooooAKKKKAP7/KKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAK/IH/AIPVv+UWXgH/ALKrp3/po1iv1+r8gf8Ag9W/5RZeAf8Asqunf+mjWKAP5gaKKKACiiigAooooAKKKKACiiigAooooAKKKKAP/9k=";

  image.addEventListener('load', function() {
    // Now that the image has loaded make copy it to the texture.
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA,gl.UNSIGNED_BYTE, image);

    // Check if the image is a power of 2 in both dimensions.
    if (isPowerOf2(image.width) && isPowerOf2(image.height)) {
       // Yes, it's a power of 2. Generate mips.
       gl.generateMipmap(gl.TEXTURE_2D);
    } else {
       // No, it's not a power of 2. Turn of mips and set wrapping to clamp to edge
       gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
       gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
       gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    }
  });

  fieldOfViewRadians = degToRad(60);
  modelXRotationRadians = degToRad(0);
  modelYRotationRadians = degToRad(0);

  // Get the starting time.
  then = 0;

//  requestAnimationFrame(drawTexture);

}


var		g_angx = 0;
// Draw the scene.
function drawTexture(time, p_object) {
    // convert to seconds
    time *= 0.001;

	// render(time);
    // Subtract the previous time from the current time
    var deltaTime = time - then;
    // Remember the current time for the next frame.
    then = time;

    webglUtils.resizeCanvasToDisplaySize(gl.canvas);

    // Tell WebGL how to convert from clip space to pixels
    gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

    // gl.enable(gl.CULL_FACE);
    gl.enable(gl.DEPTH_TEST);

    // Clear the canvas AND the depth buffer.
    // <!-- gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT); -->

    // Tell it to use our program (pair of shaders)
    gl.useProgram(program);

    // Turn on the position attribute
    gl.enableVertexAttribArray(positionLocation);

    // Bind the position buffer.
    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

    // Tell the position attribute how to get data out of positionBuffer (ARRAY_BUFFER)
    var size = 3;          // 3 components per iteration
    var type = gl.FLOAT;   // the data is 32bit floats
    var normalize = false; // don't normalize the data
    var stride = 0;        // 0 = move forward size * sizeof(type) each iteration to get the next position
    var offset = 0;        // start at the beginning of the buffer
    gl.vertexAttribPointer(
        positionLocation, size, type, normalize, stride, offset);

    // Turn on the texcoord attribute
    gl.enableVertexAttribArray(texcoordLocation);

    // bind the texcoord buffer.
    gl.bindBuffer(gl.ARRAY_BUFFER, texcoordBuffer);

    // Tell the texcoord attribute how to get data out of texcoordBuffer (ARRAY_BUFFER)
    var size = 2;          // 2 components per iteration
    var type = gl.FLOAT;   // the data is 32bit floats
    var normalize = false; // don't normalize the data
    var stride = 0;        // 0 = move forward size * sizeof(type) each iteration to get the next position
    var offset = 0;        // start at the beginning of the buffer
    gl.vertexAttribPointer(
        texcoordLocation, size, type, normalize, stride, offset);

    // Compute the projection matrix
    var aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
    var projectionMatrix = m4.perspective(fieldOfViewRadians, aspect, 0.1, 500);

    var cameraPosition = [0, 0, 4];
    var up = [0, 1, 0];
    var target = [0, 0, 0];

    // Compute the camera's matrix using look at.
    var cameraMatrix = m4.lookAt(cameraPosition, target, up);

    // Make a view matrix from the camera matrix.
    var viewMatrix = m4.inverse(cameraMatrix);

    var viewProjectionMatrix = m4.multiply(projectionMatrix, viewMatrix);


    // Animate the rotation
    modelYRotationRadians += -0.7 * deltaTime;
    modelXRotationRadians = 180 * Math.PI / 180;//-0.4 * deltaTime;

	var matrix;
	// matrix = m4.translation(100.0, 200.0, 1000.0);
	let	l_center = [ 0.0, 0.0, 0.0 ];
	// g_angx += 0.001;
	// if (g_angx > 360)
		// g_angx = 0.0;
	// rotatexyz(l_center, object.origin, g_angx, 0.0, 0.0);

	matrix = m4.translate(viewProjectionMatrix, cube.translate[0] + l_center[0], cube.translate[2] + 0.3 + l_center[2], -cube.translate[1] + l_center[1]);
	matrix = m4.xRotate(matrix, modelXRotationRadians + cube.angle[1]);
	matrix = m4.yRotate(matrix, /*modelYRotationRadians +*/ -cube.angle[0]);
	matrix = m4.zRotate(matrix, cube.angle[2]);
	matrix = m4.translate(matrix, -0.3 * 0.0, 0.0, 0.0);
	matrix = m4.scale(matrix, 0.5, 0.5, 0.5);

	// Set the matrix.
	gl.uniformMatrix4fv(matrixLocation, false, matrix);

    // Tell the shader to use texture unit 0 for u_texture
    gl.uniform1i(textureLocation, 0);

    // Draw the geometry.
    gl.drawArrays(gl.TRIANGLES, 0, 6 * 6);

   // requestAnimationFrame(drawTexture);
}

// Fill the buffer with the values that define a cube.
function setGeometry(gl) {
  var positions = new Float32Array(
    [
    -0.5, -0.5,  -0.0,
    -0.5,  0.5,  -0.0,
     0.5, -0.5,  -0.0,
    -0.5,  0.5,  -0.0,
     0.5,  0.5,  -0.0,
     0.5, -0.5,  -0.0,

    -0.5, -0.5,   0.0,
     0.5, -0.5,   0.0,
    -0.5,  0.5,   0.0,
    -0.5,  0.5,   0.0,
     0.5, -0.5,   0.0,
     0.5,  0.5,   0.0,

	//tested:
    -0.0,   0.0, -0.0,
    -0.0,   0.0,  0.0,
     0.0,   0.0, -0.0,
    -0.0,   0.0,  0.0,
     0.0,   0.0,  0.0,
     0.0,   0.0, -0.0,

	//tested y
    -0.0,  -0.0, -0.0,
     0.0,  -0.0, -0.0,
    -0.0,  -0.0,  0.0,
    -0.0,  -0.0,  0.0,
     0.0,  -0.0, -0.0,
     0.0,  -0.0,  0.0,
	//tested:
    -0.0,  -0.0, -0.0,
    -0.0,  -0.0,  0.0,
    -0.0,   0.0, -0.0,
    -0.0,  -0.0,  0.0,
    -0.0,   0.0,  0.0,
    -0.0,   0.0, -0.0,

	//tested
     0.0,  -0.0, -0.0,
     0.0,   0.0, -0.0,
     0.0,  -0.0,  0.0,
     0.0,  -0.0,  0.0,
     0.0,   0.0, -0.0,
     0.0,   0.0,  0.0,
    ]);
  gl.bufferData(gl.ARRAY_BUFFER, positions, gl.STATIC_DRAW);
}

// Fill the buffer with texture coordinates the cube.
function setTexcoords(gl) {
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(
        [
        // select the top left image
        0   , 0  ,
        0   , 0.5,
        0.25, 0  ,
        0   , 0.5,
        0.25, 0.5,
        0.25, 0  ,
        // select the top middle image
        0.25, 0  ,
        0.5 , 0  ,
        0.25, 0.5,
        0.25, 0.5,
        0.5 , 0  ,
        0.5 , 0.5,
        // select to top right image
        0.5 , 0  ,
        0.5 , 0.5,
        0.75, 0  ,
        0.5 , 0.5,
        0.75, 0.5,
        0.75, 0  ,
        // select the bottom left image
        0   , 0.5,
        0.25, 0.5,
        0   , 1  ,
        0   , 1  ,
        0.25, 0.5,
        0.25, 1  ,
        // select the bottom middle image
        0.25, 0.5,
        0.25, 1  ,
        0.5 , 0.5,
        0.25, 1  ,
        0.5 , 1  ,
        0.5 , 0.5,
        // select the bottom right image
        0.5 , 0.5,
        0.75, 0.5,
        0.5 , 1  ,
        0.5 , 1  ,
        0.75, 0.5,
        0.75, 1  ,
      ]),
      gl.STATIC_DRAW);
}
