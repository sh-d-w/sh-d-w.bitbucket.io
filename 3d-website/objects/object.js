function	object_(p_name, p_data) {
	var	name;
	var	angle;

	var	delta;

	var	vertices;
	var	faces;
}

var object = new object_("", 0);
object.origin;

object.mousehook = [];
object.keyhook = [];

object.construct = [];
object.generate = [];

object.construct[0] = function(p_object) {
	object.origin = [0.0, 0.0, 0.0];
}

object.construct[1] = function(p_object) {
	p_object.vertices = [];
	p_object.faces = [];

	p_object.color = [0, 150, 250, 0.1];

	p_object.center = [0.0, 0.0, 0.0];
	p_object.scale = [1.0, 1.0, 1.0];
	p_object.translate = [0.0, 0.0, 0.0];
	p_object.angle = [0.0, 0.0, 0.0];
}


object.keyhook[0] = function() {

}

object.mousehook[0] = function(p_object, evt) {
	let		theta = (evt.clientX - mx) * Math.PI / 360;
	let		phi = (evt.clientY - my) * Math.PI / 180;

	//do on meta data instead:
	for (let i = 0; i < 8; ++i) {
		rotatexyz(p_object.vertices[i], object.origin, theta, phi, 0);
	}
	// mx = evt.clientX;
	// my = evt.clientY;
}

object.generate[0] = function(p_file, p_object, p_center, p_state) {
	//Cube generate: // Generate the vertices / faces:
	//let		l_file;

	// l_file = "# Blender v2.79 (sub 0) OBJ File: ''\\n# www.blender.org\\nmtllib untitled.mtl\\no Untitled\\nv 1.000000 -1.000000 -1.749569\\nv 1.000000 -1.000000 1.492121\\nv 1.000000 1.000000 -1.960221\\nv 0.999999 1.000000 2.410499\\nv 2.493852 -2.077528 0.000000\\nv 2.245833 -0.505093 0.000001\\nv 1.746925 -0.538764 0.814219\\nv 1.746926 -0.538764 -0.848202\\nvt 0.000000 0.000000\\nvt 1.000000 1.000000\\nvt 0.000000 1.000000\\nvt 1.000000 0.000000\\nvt 0.000000 1.000000\\nvt 0.000000 0.000000\\nvt 0.000000 0.000000\\nvt 1.000000 1.000000\\nvt 0.000000 1.000000\\nvt 0.000000 0.000000\\nvt 1.000000 0.000000\\nvt 0.000000 0.000000\\nvt 1.000000 1.000000\\nvt 1.000000 0.000000\\nvn -0.7844 0.0650 0.6168\\nvn -0.8518 -0.1344 0.5064\\nvn -0.6878 0.0494 -0.7242\\nvn -0.8426 -0.1329 -0.5218\\nvn -0.8485 0.1222 -0.5149\\nvn -0.7656 -0.0171 0.6431\\nvn -0.8600 -0.0504 0.5078\\nvn -0.7345 0.2832 -0.6167\\nusemtl Material\\ns off\\nf 1/1/1 8/2/1 3/3/1\\nf 6/4/2 8/5/2 5/6/2\\nf 2/7/3 7/8/3 5/9/3\\nf 6/10/4 5/11/4 7/8/4\\nf 4/12/5 6/4/5 7/8/5\\nf 1/1/6 5/11/6 8/2/6\\nf 6/4/7 3/13/7 8/5/7\\nf 2/7/8 4/14/8 7/8/8";
	//l_file = p_files;//files.cube;//"# Blender v2.80 (sub 75) OBJ File: ''\\n# www.blender.org\\nmtllib cube.mtl\\no Cube\\nv 1.000000 1.000000 -1.000000\\nv 1.000000 -1.000000 -1.000000\\nv 1.000000 1.000000 1.000000\\nv 1.000000 -1.000000 1.000000\\nv -1.000000 1.000000 -1.000000\\nv -1.000000 -1.000000 -1.000000\\nv -1.000000 1.000000 1.000000\\nv -1.000000 -1.000000 1.000000\\nvt 0.375000 0.000000\\nvt 0.625000 0.000000\\nvt 0.625000 0.250000\\nvt 0.375000 0.250000\\nvt 0.375000 0.250000\\nvt 0.625000 0.250000\\nvt 0.625000 0.500000\\nvt 0.375000 0.500000\\nvt 0.625000 0.750000\\nvt 0.375000 0.750000\\nvt 0.625000 0.750000\\nvt 0.625000 1.000000\\nvt 0.375000 1.000000\\nvt 0.125000 0.500000\\nvt 0.375000 0.500000\\nvt 0.375000 0.750000\\nvt 0.125000 0.750000\\nvt 0.625000 0.500000\\nvt 0.875000 0.500000\\nvt 0.875000 0.750000\\nvn 0.0000 1.0000 0.0000\\nvn 0.0000 0.0000 1.0000\\nvn -1.0000 0.0000 0.0000\\nvn 0.0000 -1.0000 0.0000\\nvn 1.0000 0.0000 0.0000\\nvn 0.0000 0.0000 -1.0000\\nusemtl Material\\ns off\\nf 1/1/1 5/2/1 7/3/1 3/4/1\\nf 4/5/2 3/6/2 7/7/2 8/8/2\\nf 8/8/3 7/7/3 5/9/3 6/10/3\\nf 6/10/4 2/11/4 4/12/4 8/13/4\\nf 2/14/5 1/15/5 3/16/5 4/17/5\\nf 6/18/6 5/19/6 1/20/6 2/11/6";

	// l_file = "# Blender v2.79 (sub 0) OBJ File: ''\\n# www.blender.org\\nmtllib line.mtl\\no line\\nv -0.000000 1.000000 0.000000\\nv 0.000000 -1.000000 -0.000000\\nf 1 2";
	// l_file = "# Blender v2.79 (sub 0) OBJ File: ''\\n# www.blender.org\\nmtllib pentagonal.mtl\\no Cube.001\\nv 1.000000 -1.000000 -0.698430\\nv 1.000000 0.364235 -1.118387\\nv 1.000000 1.221010 0.000001\\nv 0.999999 0.355595 1.207578\\nv 1.000000 -1.000000 0.800823\\nvt 0.500000 1.000000\\nvt 0.975528 0.654508\\nvt 0.793893 0.095491\\nvt 0.206107 0.095492\\nvt 0.024472 0.654509\\nvn 1.0000 0.0000 0.0000\\nusemtl Material.001\\ns 1\\nf 1/1/1 2/2/1 3/3/1 4/4/1 5/5/1";

	//CPU:
	if (p_state === 0/*CPU*/ || p_state === 2/*CPU+GPU*/)
		object.generate[1](p_object, p_file);

	//GPU:
	if (p_state === 1/*GPU*/ || p_state === 2/*CPU+GPU*/) {
		p_object.gpu = [];

		p_object.gpu[0/* faces  */] = object.generate[2](p_file);
		p_object.gpu[1/* buffer */] = webglUtils.createBufferInfoFromArrays(gl, p_object.gpu[0]);
	}
	// p_object.gpu[0/* faces  */] = object.generate[2](l_file);
	// p_object.gpu[1/* buffer */] = webglUtils.createBufferInfoFromArrays(gl, cube.gpu[0]);

	// p_object.vertices[0] = [ -1, -1, 1 ];
	// p_object.vertices[1] = [ -1, -1, -1 ];
	// p_object.vertices[2] = [ 1, -1, -1 ];
	// p_object.vertices[3] = [ 1, -1, 1 ];
	// p_object.vertices[4] = [ 1, 1, 1 ];
	// p_object.vertices[5] = [ 1, 1, -1 ];
	// p_object.vertices[6] = [ -1, 1, -1 ];
	// p_object.vertices[7] = [ -1, 1, 1 ];

	// Generate the faces
	// p_object.faces[0] = [ p_object.vertices[0], p_object.vertices[1], p_object.vertices[2], p_object.vertices[3] ];
	// p_object.faces[1] = [ p_object.vertices[3], p_object.vertices[2], p_object.vertices[5], p_object.vertices[4] ];
	// p_object.faces[2] = [ p_object.vertices[4], p_object.vertices[5], p_object.vertices[6], p_object.vertices[7] ];
	// p_object.faces[3] = [ p_object.vertices[7], p_object.vertices[6], p_object.vertices[1], p_object.vertices[0] ];
	// p_object.faces[4] = [ p_object.vertices[7], p_object.vertices[0], p_object.vertices[3], p_object.vertices[4] ];
	// p_object.faces[5] = [ p_object.vertices[1], p_object.vertices[6], p_object.vertices[5], p_object.vertices[2] ];
}

object.generate[1] = function(p_object, p_file) {
	//write blender object file for CPU parser here
	let		l_object;
	let		l_line;
	let		j, k, l_faces;

	j = -1;
	k = -1;

	//file:
	l_object = p_file.split("\\n");
	for (let i = 0; i < l_object.length; ++i) {
		//vertex:
		if (l_object[i][0] == 'v' && l_object[i][1] == ' ')
		{
			l_line = l_object[i].split(" ");
			p_object.vertices[++j] = [ parseInt(l_line[1]), parseInt(l_line[2]), parseInt(l_line[3]) ];
		}
		//faces:
		if (l_object[i][0] == 'f')
		{
			l_line = l_object[i].split(" ");
			
			p_object.faces[++k] = [];
			l_faces = l_line.length - 1;
			for (let l = 0; l < l_faces; l++) {
				p_object.faces[k][l] = p_object.vertices[parseInt(l_line[l + 1][0]) - 1];
			}
		}
	}

	// // p_object.vertices[0] = [ -1, -1, 1 ];
	// p_object.vertices[1] = [ -1, -1, -1 ];
	// p_object.vertices[2] = [ 1, -1, -1 ];
	// p_object.vertices[3] = [ 1, -1, 1 ];
	// p_object.vertices[4] = [ 1, 1, 1 ];
	// p_object.vertices[5] = [ 1, 1, -1 ];
	// p_object.vertices[6] = [ -1, 1, -1 ];
	// p_object.vertices[7] = [ -1, 1, 1 ];

}

//webgl GPU:
object.generate[2] = function(text) {
// This is not a full .obj parser.
// see http://paulbourke.net/dataformats/obj/

	// because indices are base 1 let's just fill in the 0th data
	const objPositions = [[0, 0, 0]];
	const objTexcoords = [[0, 0]];
	const objNormals = [[0, 0, 0]];

	// same order as `f` indices
	const objVertexData = [
		objPositions,
		objTexcoords,
		objNormals,
	];

	// same order as `f` indices
	let webglVertexData = [
		[],   // positions
		[],   // texcoords
		[],   // normals
	];

	function newGeometry() {
		// If there is an existing geometry and it's
		// not empty then start a new one.
		if (geometry && geometry.data.position.length) {
			geometry = undefined;
		}
		setGeometry();
	}

	function addVertex(vert) {
		const ptn = vert.split('/');
		ptn.forEach((objIndexStr, i) => {
			if (!objIndexStr) {
				return;
			}
			const objIndex = parseInt(objIndexStr);
			const index = objIndex + (objIndex >= 0 ? 0 : objVertexData[i].length);
			webglVertexData[i].push(...objVertexData[i][index]);
		});
	}

	const keywords = {
	v(parts) {
		objPositions.push(parts.map(parseFloat));
	},
	vn(parts) {
		objNormals.push(parts.map(parseFloat));
	},
	vt(parts) {
		// should check for missing v and extra w?
		objTexcoords.push(parts.map(parseFloat));
	},
	f(parts) {
		const numTriangles = parts.length - 2;
		for (let tri = 0; tri < numTriangles; ++tri) {
				addVertex(parts[0]);
				addVertex(parts[tri + 1]);
				addVertex(parts[tri + 2]);
			}
		},
	};

	const keywordRE = /(\w*)(?: )*(.*)/;
	const lines = text.split('\\n');
	for (let lineNo = 0; lineNo < lines.length; ++lineNo) {
		const line = lines[lineNo].trim();
		if (line === '' || line.startsWith('#')) {
			continue;
		}
		const m = keywordRE.exec(line);
		if (!m) {
			continue;
		}
		const [, keyword, unparsedArgs] = m;
		const parts = line.split(/\s+/).slice(1);
		const handler = keywords[keyword];
		if (!handler) {
			console.warn('unhandled keyword:', keyword);  // eslint-disable-line no-console
			continue;
		}
		handler(parts, unparsedArgs);
	}

	return {
		position: webglVertexData[0],
		texcoord: webglVertexData[1],
		normal: webglVertexData[2],
	};
}

