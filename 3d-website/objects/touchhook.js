var	touchhook = new object_("", 0);

var	tx = 0;
var	ty = 0;

// let		mx = 0, my = 0;
// var		g_analog = document.getElementById("analog.stick");

touchhook.touchstart = function(event) {
	tx = event.touches[0].clientX;
	ty = event.touches[0].clientY;

	//DISPLAY TOUCHSCREEN JOYSTICK:
	if (g_joystick == false) {
		document.getElementById("analogue").style.display = "inline-block";
		document.getElementById("analog.stick").style.display = "inline";
		g_joystick = true;
	}
	document.getElementById("stdout").innerHTML = "touchSTART* reached";
}

touchhook.analogangle = function() {
	if (g_analogdown == false)
		return (0);
	let		l_analog = document.getElementById("analog.stick");
	let		l_x = 0, l_y = 0;
	let		l_q = 1, l_quad = [0, 180, 180, 360];

	l_x = parseInt(l_analog.style.left) + 25 - 50;//+quad - cent
	l_y = parseInt(l_analog.style.bottom) + 25 - 50;

	l_q = (l_x > 0 && l_y < 0) + 2 * (l_x < 0) + (l_x < 0 && l_y > 0);

	return ( Math.atan( (l_x / l_y)) * (180 / Math.PI) + l_quad[l_q] );
}

touchhook.touchmove = function (event) {
	//OPERATE ANALOG STICK:
	if (g_analogdown == true) {//if analog stick was touched
	    //(to move analog stick until radius-max ontouchmove)
			let		l_analog = document.getElementById("analog.stick");

			if (tx < 100)
				l_analog.style.left = (tx - 25) + "px";
			if (window.innerHeight - ty < 100)
				l_analog.style.bottom = (window.innerHeight - ty - 25) + "px";

		// let		l_x = 0, l_y = 0;
        // let		l_q = 1, l_quad = [0, 180, 180, 360];
        
        // l_x = parseInt(l_analog.style.left) + 25 - 50;//+quad - cent
        // l_y = parseInt(l_analog.style.bottom) + 25 - 50;

		// l_q = (l_x > 0 && l_y < 0) + 2 * (l_x < 0) + (l_x < 0 && l_y > 0);

	   // // document.getElementById("stdout").innerHTML = "q " + l_q + "; " +
       // //     (Math.atan( ((l_x) / (l_y))) * (180 / Math.PI) + l_quad[l_q]);
	   // cube.touchhook( touchhook.analogangle() );
		tx = event.touches[0].clientX;
		ty = event.touches[0].clientY;
		document.getElementById("stdout").innerHTML = "touchMOVE* BEGINNING OF THE END reached";
		return ;
	}
	//ROTATE CUBE SCENE:
		for (let i = 0; i < 1; i++) {
			// objects[i].mousehook[0]((evt.clientX - mx), (evt.clientY - my));
			objects[i].angle[0] += (event.touches[0].clientX - tx) * Math.PI / 360;
			objects[i].angle[1] += (event.touches[0].clientY - ty) * Math.PI / 360;//180;

			objects[i].angle[0] = cube.angle[0] % 360;
			objects[i].angle[1] = cube.angle[1] % 360;
		}
		tx = event.touches[0].clientX;
		ty = event.touches[0].clientY;

	// document.getElementById("stdout").innerHTML = "touchMOVE reached";
	return ;
}

touchhook.touchend = function(evt) {

	let		l_analog = document.getElementById("analog.stick");

	l_analog.style.left = 25 + "px";
	l_analog.style.bottom = 25 + "px";
	g_analogdown = false;
	document.getElementById("stdout").innerHTML = "touchEND** reached";

}

touchhook.analogstickstart = function(evt) {
	//true that analog was touched.
	g_analogdown = true;
	document.getElementById("stdout").innerHTML += "touchSTART***** reached";

}
