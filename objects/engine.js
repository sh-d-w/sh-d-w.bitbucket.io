
//COLLISION DETECTION ALGORITHMS:
function	collision_object(p1, o1, p_buffer) {
// collision_object(camera_position, [objects.tree[0][0].position.x, objects.tree[0][0].position.y, objects.tree[0][0].position.z], 2.0);
	let l_radius = Math.sqrt(Math.pow(o1[0] - p1[0], 2) + Math.pow(o1[2] - p1[2], 2)); 

	return ( 1 * (l_radius < p_buffer) + 0);
}

function	collision_wall(p1, l1, l2, p_buffer) {
// collision_wall(camera_position, [crate.position.x, crate.position.y, crate.position.z], [objects.tree[0][0].position.x, objects.tree[0][0].position.y, objects.tree[0][0].position.z], 0.1)
	let lineLen = Math.sqrt(Math.pow(l1[0] - l2[0], 2) + Math.pow(l1[2] - l2[2], 2)); //dist(x1,y1, x2,y2);
	let d1	    = Math.sqrt(Math.pow(l1[0] - p1[0], 2) + Math.pow(l1[2] - p1[2], 2)); //dist(px,py, x1,y1);
	let d2      = Math.sqrt(Math.pow(l2[0] - p1[0], 2) + Math.pow(l2[2] - p1[2], 2)); //dist(px,py, x2,y2);

	return (1 * (d1+d2 >= lineLen-p_buffer && d1+d2 <= lineLen+p_buffer) + 0 );
	// if (d1+d2 >= lineLen-0.1 && d1+d2 <= lineLen+0.1) {
		// return 1;
	// }
	// else
		// return 0;
	// return (d1 + d2 >= lineLen - p_buffer && d1 + d2 <= lineLen + p_buffer);
}


//ROTATION ALGORITHMS:
// Rotate a vertice
function rotatexyz(M, center, beta, theta, phi) {
	// Rotation matrix coefficients
	let		ct, st;
	let		x;
	let		y;
	let		z;

	//x
	x = M[0];
	y = M[1];
	z = M[2];
	ct = Math.cos(beta);
	st = Math.sin(beta);
	M[0] = ct * x - st * y;
	M[1] = st * x + ct * y;

	//y
	x = M[0];
	y = M[1];
	z = M[2];
	ct = Math.cos(theta);
	st = Math.sin(theta);
	M[1] = ct * y - st * z;
	M[2] = st * y + ct * z;

	//z
	x = M[0];
	y = M[1];
	z = M[2];
	ct = Math.cos(phi);
	st = Math.sin(phi);
	M[0] = ct * x - st * z;
	M[2] = st * x + ct * z;
}
