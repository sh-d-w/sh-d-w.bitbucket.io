var scene, camera, renderer, mesh;
var meshFloor, ambientLight, light;

var crate, crateTexture, crateNormalMap, crateBumpMap;

//3D OBJECT VARIABLES
var cuber;
var obj_loader;

var camera_pointer;
var	camera_state = 0;	/* 0 = FPS		1 = TPS		2 = BEV */
var	camera_switch_iter = 0;
var	camera_position;	// temp variable to be able to calculate object collisions and movement prevention

var keyboard = {};
var player = { height:1.8, speed:0.2, turnSpeed:Math.PI*0.02 };
var USE_WIREFRAME = false;


//SHAD TEST VARIABLES
// 	var g_angle = 0		
	// var texturer;
	var textures = [];
	var objects = new object_("", 0);
	var	images = [];
	
	var images_loaded = 0;
	var no_of_images = 0;

	var	animation_id = 1;
	var	animation_index = 0;
	var animate_timer_start = new Date();
	var animate_timer_end;
	var animate_speed = 75;
	
	var	screen_text = [];

	//GLOBALS:
	var g_vertices;				//storage of vertices from traverse function
	var collision_triangles		// the triangles got from g_vertices for platform object

	var collision_triangles_translation		// a duplicate of collision_triangles but with translated values


//heightmap variables:
var	heightmap_vertices = []
var	height_map = []
	
var	height_offset = [0, 0, 0]       //# The output vector representing the height to drop map by due to terrain differences


// debug tool
function	ft_write(p_fd, p_line, p_size) {
	let	i;
	
	i = 0;
	while (i < p_line.length && i < p_size) {
		document.getElementById(p_fd).innerHTML += p_line[i]
		i++;
	}
}

function	len(p_vector) {
	return (p_vector.length);
}

function	abs(p_number) {
	return Math.abs(p_number);
}

function	straight_line(p_p1, p_p2, p_time) {
    let l_direction_vector = [p_p1[0] - p_p2[0], p_p1[1] - p_p2[1], p_p1[2] - p_p2[2]];

    return [p_p1[0] - l_direction_vector[0] * p_time, p_p1[1] - l_direction_vector[1] * p_time, p_p1[2] - l_direction_vector[2] * p_time];
}

//# SETTING HEIGHTMAP ARRAY:
function	heightmap_load(p_file, p_heightmap, p_heightmap_vertices) {
    // <!-- l_fd = open(p_file, "r") -->

    // <!-- l_file = l_fd.read() -->
	let	l_file = p_file.replaceAll("\\n", '\n')//temp until fix the object file_data
    let i = 0
    let l_f = 0 // # for faces section:
    let l_fc = 0

    l_file = l_file.split("\n")


	for (const [l_key, line] of Object.entries(l_file)) {
		<!-- document.getElementById("demo").innerHTML += l_key + " " + line + "<br>" -->
        if (line && line[0] == 'v' && line[1] == ' ') {
            let vertice = line.split(" ");
            p_heightmap_vertices.push([parseFloat(vertice[1]), parseFloat(vertice[2]), parseFloat(vertice[3])])
            i += 1;
		}
        // # sort_faces()
        if (line && line[0] == 'f' && line[1] == ' ') {
            l_fc += 1
            //# GET NUMERIC VERTEX NUMBERS FOR EACH FACE:
            l_face = line.split(" ");
			l_face.splice(0, 1);

            l_face_len = len(l_face)
			for (let i = 0; i < l_face_len; i++)
                l_face[i] = parseInt(l_face[i].split("/")[0]);
            //# print(l_face)

            //# STORE IN APPROPRIATE ORDER:
            //# [4, 1, 3]     [ [ , ], [ ] ]
            let l_arr = [];
            //# SORTS ORDER OF FACE, MAKE READY TO STORE IN HEIGHTMAP VECTOR:
            if (abs(p_heightmap_vertices[l_face[0] - 1][2] - p_heightmap_vertices[l_face[1] - 1][2]) < 0.02) {        // # if same y's
                if (p_heightmap_vertices[l_face[0] - 1][0] < p_heightmap_vertices[l_face[1] - 1][0])     // # if p1 x < p2 x    then 
                    l_arr.push([ l_face[0], l_face[1] ])
                else    //# else store in any horizontal order
                    l_arr.push([ l_face[1], l_face[0] ])
                if (p_heightmap_vertices[l_face[0] - 1][2] > p_heightmap_vertices[l_face[2] - 1][2])     // # then append at top
                    l_arr.splice(0, 0, [ l_face[2] ])
                else       // # else append at bottom
                    l_arr.push([ l_face[2] ])
            } else if (abs(p_heightmap_vertices[l_face[0] - 1][2] - p_heightmap_vertices[l_face[2] - 1][2]) < 0.02) {
                if (p_heightmap_vertices[l_face[0] - 1][0] < p_heightmap_vertices[l_face[2] - 1][0])     // # if p1 x < p2 x    then 
                    l_arr.push([ l_face[0], l_face[2] ])
                else    // # else store in any horizontal order
                    l_arr.push([ l_face[2], l_face[0] ])
                if (p_heightmap_vertices[l_face[0] - 1][2] > p_heightmap_vertices[l_face[1] - 1][2])     // # then append at top
                    l_arr.splice(0, 0, [ l_face[1] ])
                else      // # else append at bottom
                    l_arr.push([ l_face[1] ])
            } else if (abs(p_heightmap_vertices[l_face[1] - 1][2] - p_heightmap_vertices[l_face[2] - 1][2]) < 0.02) {
                if (p_heightmap_vertices[l_face[1] - 1][0] < p_heightmap_vertices[l_face[2] - 1][0])     // # if p1 x < p2 x    then 
                    l_arr.push([ l_face[1], l_face[2] ])
                else    // # else store in any horizontal order
                    l_arr.push([ l_face[2], l_face[1] ])
                if (p_heightmap_vertices[l_face[1] - 1][2] > p_heightmap_vertices[l_face[0] - 1][2])     // # then append at top
                    l_arr.splice(0, 0, [ l_face[0] ])
                else       // # else append at bottom
                    l_arr.push([ l_face[0] ])
            } else
                console.log("HEIGHTMAP ERROR: NOT TRIANGULATED OBJECT OR OBJECT FACES NOT SYMETRICAL. ", p_heightmap_vertices[l_face[0] - 1][2], " : ", p_heightmap_vertices[l_face[1] - 1][2], " : ", p_heightmap_vertices[l_face[2] - 1][2])

            // # STORE AT APPROPRIATE LOCATION IN MEMEORY (&& merge in some instances):
            if (l_f == 0) {                                                      // # first element in heightmap array
                p_heightmap.push( [ l_arr ])    // # store in height_map[0]
            }
			else {   // # else if > or smaller than, store appropriately
                // # # thereafter
                if (p_heightmap_vertices[l_arr[0][0] - 1][2] < p_heightmap_vertices[p_heightmap[0][0][0][0] - 1][2] && abs(p_heightmap_vertices[l_arr[0][0] - 1][2] - p_heightmap_vertices[p_heightmap[0][0][0][0] - 1][2]) > 0.02)
                    p_heightmap.splice(0, 0, [ l_arr ]) //# then append at beginning of height_map array
                else if (p_heightmap_vertices[l_arr[0][0] - 1][2] > p_heightmap_vertices[p_heightmap[len(p_heightmap) - 1][0][0][0] - 1][2] && abs(p_heightmap_vertices[l_arr[0][0] - 1][2] - p_heightmap_vertices[p_heightmap[len(p_heightmap) - 1][0][0][0] - 1][2]) > 0.02)
                    p_heightmap.push([ l_arr ]) //# then append at end of height_map array
                else {
                    let j = 0
                    let found = 1
                    while (j < len(p_heightmap) && found) {                    // # search through heights in height_maps
                        if (abs(p_heightmap_vertices[l_arr[0][0] - 1][2] - p_heightmap_vertices[p_heightmap[j][0][0][0] - 1][2]) < 0.02) {   // # if same y value
                            // # p_heightmap[j].push(p_p_heightmap_vertices[len(p_p_heightmap_vertices) - 1])                   # then add vertice to y layer
                            // # print("SAME LINE REACHED: FOUND ", l_arr)
                            
                            // # loop through width wise
                            let l_i = 0
                            let l_found = 1
                            let p_heightmap_len = len(p_heightmap[j])
                            while (l_i < p_heightmap_len && l_found) {    // # loop through width
                                // # if 2 points match then check if diagnals cause square then merge vertices
                                // # else if no face fits && found correct storage position (then insert)

                                // # IF 2 VERTICES MATCH:
                                let l_count = 0
								let l_matrix;
                                if (abs(p_heightmap_vertices[p_heightmap[j][l_i][0][0] - 1][0] - p_heightmap_vertices[p_heightmap[j][l_i][1][0] - 1][0]) < 0.02) {
                                    l_matrix = [0, 0]
                                    if (p_heightmap[j][l_i][0][len(p_heightmap[j][l_i][0]) - 1] == l_face[0]) {
                                        l_matrix[0] = l_face[0]
                                        l_count += 1
									}
                                    else if (p_heightmap[j][l_i][0][len(p_heightmap[j][l_i][0]) - 1] == l_face[1]) {
                                        l_matrix[0] = l_face[1]
                                        l_count += 1
									}
                                    else if (p_heightmap[j][l_i][0][len(p_heightmap[j][l_i][0]) - 1] == l_face[2]) {
                                        l_matrix[0] = l_face[2]
                                        l_count += 1
									}

                                    if (p_heightmap[j][l_i][1][len(p_heightmap[j][l_i][1]) - 1] == l_face[0]) {
                                        l_matrix[1] = l_face[0]
                                        l_count += 1
									}
                                    else if (p_heightmap[j][l_i][1][len(p_heightmap[j][l_i][1]) - 1] == l_face[1]) {
                                        l_matrix[1] = l_face[1]
                                        l_count += 1
									}
                                    else if (p_heightmap[j][l_i][1][len(p_heightmap[j][l_i][1]) - 1] == l_face[2]) {
                                        l_matrix[1] = l_face[2]
                                        l_count += 1
									}
                                }
								if (l_count == 2) {    // # if 2 vertices match (&& on edge of 1 triangle) then select id of face && merge into this face
                                    l_face_len = len(l_face)
                                    l_id = -1
									for (let t_i = 0; t_i < l_face_len; t_i++)
                                        if (l_face[t_i] != l_matrix[0] && l_face[t_i] != l_matrix[1])
                                            l_id = l_face[t_i]
                                    // # use l_id && store at
                                    if (len(p_heightmap[j][l_i][0]) < 2) {         // # if not max len
                                        p_heightmap[j][l_i][0].push(l_id)
                                        l_found = 0
									}
                                    else if (len(p_heightmap[j][l_i][1]) < 2) {
                                        p_heightmap[j][l_i][1].push(l_id)
                                        l_found = 0
									}
                                }
								else { // # else check front end of triangle for connection
                                    l_count = 0
                                    if (abs(p_heightmap_vertices[p_heightmap[j][l_i][0][len(p_heightmap[j][l_i][0]) - 1] - 1][0] - p_heightmap_vertices[p_heightmap[j][l_i][1][len(p_heightmap[j][l_i][1]) - 1] - 1][0]) < 0.02) {
                                        l_matrix = [0, 0]
                                        if (p_heightmap[j][l_i][0][0] == l_face[0]) {
                                            l_matrix[0] = l_face[0]
                                            l_count += 1
										}
                                        else if (p_heightmap[j][l_i][0][0] == l_face[1]) {
                                            l_matrix[0] = l_face[1]
                                            l_count += 1
										}
                                        else if (p_heightmap[j][l_i][0][0] == l_face[2]) {
                                            l_matrix[0] = l_face[2]
                                            l_count += 1
										}
										
                                        if (p_heightmap[j][l_i][1][0] == l_face[0]) {
                                            l_matrix[1] = l_face[0]
                                            l_count += 1
										}
                                        else if (p_heightmap[j][l_i][1][0] == l_face[1]) {
                                            l_matrix[1] = l_face[1]
                                            l_count += 1
										}
                                        else if (p_heightmap[j][l_i][1][0] == l_face[2]) {
                                            l_matrix[1] = l_face[2]
                                            l_count += 1
										}
                                    }
									if (l_count == 2) {    // # if 2 vertices match (&& on edge of 1 triangle) then select id of face && merge into this face
                                        l_face_len = len(l_face)
                                        l_id = -1
										for (let t_i = 0; t_i < l_face_len; t_i++)
                                            if (l_face[t_i] != l_matrix[0] && l_face[t_i] != l_matrix[1])
                                                l_id = l_face[t_i]
                                        // # use l_id && store at
                                        if (len(p_heightmap[j][l_i][0]) < 2) {
                                            p_heightmap[j][l_i][0].push(l_id)
                                            l_found = 0
										}
                                        else if (len(p_heightmap[j][l_i][1]) < 2) {
                                            p_heightmap[j][l_i][1].push(l_id)
                                            l_found = 0
										}
									}
                                }
								l_i += 1
							}
                            if (l_found == 1) // # if not found: then add at end of p_heightmap[j].push()
                                p_heightmap[j].push( l_arr )
                            found = 0
                        }
						else if (p_heightmap_vertices[l_arr[0][0] - 1][2] - p_heightmap_vertices[p_heightmap[j][0][0][0] - 1][2] < 0) {          //# if y value now smaller then search through value
                            p_heightmap.splice(j, 0, [ l_arr ])                 // # insert new vector at location x in height_maps
                            found = 0
						}
                        j += 1
					}
				}
			}
            l_f += 1
		}
	}
    // <!-- print("HEIGHTMAP LOAD END REACHED!!") -->
}

function	terrain_bounds(p_xy_position, p_heightmap, p_heightmap_vertices) {
    // # IF WITHIN BASIC BOUNDS:
    // # print("YESS: ",  heightmap_vertices[p_heightmap[0][0][0][0] - 1][2], " : ", heightmap_vertices[p_heightmap[len(p_heightmap) - 1][0][1][0] - 1][2])

    if (p_xy_position[2] >= p_heightmap_vertices[p_heightmap[0][0][0][0] - 1][2] && p_xy_position[2] <= p_heightmap_vertices[p_heightmap[len(p_heightmap) - 1][0][1][0] - 1][2]) {
        //# THEN FIND WHICH BOUNDS ITS IN
        let l_x = -1
        let l_y = 0
        let l_h_leny = len(p_heightmap)
        //# LOOP THROUGH UNTIL FOUND ROW FOR DECENT Y VALUE RANGE
        while (l_y + 1 < l_h_leny && p_xy_position[2] > p_heightmap_vertices[p_heightmap[l_y + 1][0][1][0] - 1][2])       // # until Y value is within range of a quadrant
            l_y += 1
        //# LOOP THROUGH X AXIS AND COMPARE IF IS A SQUARE AND IF X POSITION WITHIN X RANGES:
        let l_h_lenx = len(p_heightmap[l_y])
        let l_found = 1
        let l_i = 0
        while (l_i < l_h_lenx && l_found) {
            //# CHECK IF SQUARE
            if (len(p_heightmap[l_y][l_i][0]) == 2 && len(p_heightmap[l_y][l_i][1]) == 2) {
                //# CHECK IF xposition WITHIN RANGE  THEN SET l_x to l_i && l_found = 0 
                if (p_xy_position[0] >= p_heightmap_vertices[p_heightmap[l_y][l_i][0][0] - 1][0] - 0.02 && p_xy_position[0] <= p_heightmap_vertices[p_heightmap[l_y][l_i][0][1] - 1][0] + 0.02) {
                    l_x = l_i
                    l_found = 0
				}
			}
            else {
                //# CHECK IF IS A TRIANGLE:
                if (len(p_heightmap[l_y][l_i][0]) == 2 && len(p_heightmap[l_y][l_i][1]) == 1) {
                    if (p_xy_position[0] >= p_heightmap_vertices[p_heightmap[l_y][l_i][0][0] - 1][0] - 0.02 && p_xy_position[0] <= p_heightmap_vertices[p_heightmap[l_y][l_i][0][1] - 1][0] + 0.02)
                        l_found = 0
                    //pass
				}
                else if (len(p_heightmap[l_y][l_i][0]) == 1 && len(p_heightmap[l_y][l_i][1]) == 2) {
                    if (p_xy_position[0] >= p_heightmap_vertices[p_heightmap[l_y][l_i][1][0] - 1][0] - 0.02 && p_xy_position[0] <= p_heightmap_vertices[p_heightmap[l_y][l_i][1][1] - 1][0] + 0.02)
                        l_found = 0
				}
                    //pass
                //pass
			}
            l_i += 1
		}
        if (l_found == 0)    // # if found could be triangle if either l_x == -1 or l_y == -1
            return [l_x, l_y]
        else if (l_x == -1 || l_y == -1)
            return (0)
        else
            return [l_x, l_y]
	}
    return (0)
}

function	point_terrain(p_xy_position, p_xy, p_heightmap, p_heightmap_vertices, p_line_xxy) {
    //# x plane 1:
    time_x = p_xy_position[0] / abs(p_heightmap_vertices[p_heightmap[p_xy[1]][p_xy[0]][0][0] - 1][0] + p_heightmap_vertices[p_heightmap[p_xy[1]][p_xy[0]][0][1] - 1][0])
    time = time_x

    p_line_xxy[0] = straight_line(p_heightmap_vertices[p_heightmap[p_xy[1]][p_xy[0]][0][0] - 1], p_heightmap_vertices[p_heightmap[p_xy[1]][p_xy[0]][0][1] - 1], time)

    //# # x plane 2:
    time_x = p_xy_position[0] / abs(p_heightmap_vertices[p_heightmap[p_xy[1]][p_xy[0]][1][0] - 1][0] + p_heightmap_vertices[p_heightmap[p_xy[1]][p_xy[0]][1][1] - 1][0])
    time = time_x

    p_line_xxy[1] = straight_line(p_heightmap_vertices[p_heightmap[p_xy[1]][p_xy[0]][1][0] - 1], p_heightmap_vertices[p_heightmap[p_xy[1]][p_xy[0]][1][1] - 1], time)

    //# # x plane 2:
    time_y = p_xy_position[2] / abs(p_line_xxy[0][2] + p_line_xxy[1][2])
    time = time_y

    p_line_xxy[2] = straight_line(p_line_xxy[0], p_line_xxy[1], time)

    //# DEBUG: PRINT OUTPUTS:
	// document.getElementById("stdout").innerHTML += "<br>" + p_line_xxy[0] 
	// document.getElementById("stdout").innerHTML += "<br>" + p_line_xxy[1]
	// document.getElementById("stdout").innerHTML += "<br>" + p_line_xxy[2]

    return p_line_xxy[2]
}



function	load_obj(p_obj_file, p_texture, p_shadow) {
	let	l_location
	let	l_objstring = p_obj_file.replaceAll("\\n", '\n')//temp until fix the object file_data

	l_location = obj_loader.parse(String(l_objstring));

	l_location.traverse( function ( child ) {
		if ( child instanceof THREE.Mesh ) {
			child.castShadow = p_shadow;
			child.receiveShadow = p_shadow;
			// // child.material = new THREE.MeshBasicMaterial();
			child.material.map = p_texture;
			// child.material.map = new THREE.MeshBasicMaterial();

			child.material.wireframe = false;
			// child.material.transparent = true;
			// // set opacity to 50%
			// child.material.opacity = 0.5; 
			
			//get vertices data:
			g_vertices = child.geometry.attributes.position.array;
			// console.log(child);
			// console.log("geo ", child.geometry.attributes.position.array);
			// console.log( (new THREE.Geometry().fromBufferGeometry( child.geometry )).geometry);
			// geometry = new THREE.Geometry().fromBufferGeometry( object.children["0"].geometry );
		}
	} );
	l_location.vertices = [1, 2, 3];

	// p_location.position.set(-5, 0, 4);
	// cuber.rotation.x = 90 * Math.PI / 180;
	// p_location.rotation.x = -90 * Math.PI / 180;
	// cuber.rotation.z = 90 * Math.PI / 180;

	// cuber.material.map = textures[0];
	// scene.add( p_location );
	return l_location;
}

function init(){
	// var dataurl = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2ZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYxIDY0LjE0MDk0OSwgMjAxMC8xMi8wNy0xMDo1NzowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpERUY4NEY4NjY2ODhFNDExODgxNkNGMzk4MzBFNzE1MCIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpFMzc0MTg1MTg4NjYxMUU0QUQ3REM0MEJFQThCMjdCOSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpFMzc0MTg1MDg4NjYxMUU0QUQ3REM0MEJFQThCMjdCOSIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M1LjEgV2luZG93cyI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkRGRjg0Rjg2NjY4OEU0MTE4ODE2Q0YzOTgzMEU3MTUwIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkRFRjg0Rjg2NjY4OEU0MTE4ODE2Q0YzOTgzMEU3MTUwIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+87tNGwAAByJJREFUeNpcl1tIl3cYx3/+/Vt5yDxUeGjaIJWyiAWtBVZSMYeIbEYMhK5GsbZBdbHbbbDLwViDLgbrorbb2C4kpEiYQ6OahVmzYWJaauWxg2nmYc/n6f/98+oLL+/7/n7P8fucfm/KgwcPQkpKSujt7Q2vX78Oqamp4fHjx2Fqaio8f/48rFixInR0dISGhoZgV1lfX9+Htl++atWq+MaNG/tzc3PbX7169c/CwsLMypUrw/T0dOjp6QmbN28ODx8+dFkFBQWhsLAwlJWVhbGxsdDf3x9GR0d9L47yxcVFV8wVj8f9nWcsFvOnXWlm4Lf37t07YUZmpaWluWGzs7OhpKQkbNiw4a7R/TIxMfGr0U4neJw/es/Pz7tsjOHKyckJTokRMPEUMYQJAzLT09PPGwoNKLX3oOvly5fBjArj4+OVFRUVP2dkZDROTk4eM76uubm5EL0MoWD7Ye3atf6+bdu28ObNm7cIsCAEUIqHbPL+6NGjHywcrhyIo8hYGISQw71u3boPsrKymox2r8nslzOSywUPiKMXPc6thSgx98zMzHsWr2NAh3I8kAG8c2MYTzzmtv2S4uLiH+39UFQeTvJED3SEz+nxHG8xQt4glDUjOm7ep65evTpkZ2e7IjwQjZQrLBjJZfQNJu+jFy9eNMsZoTo8POyJigHmYIgp8UgIWZoQmG2xrREzNNE76gU8KM/MzPRQwp+fn3/cFRg/hsqAJ0+eeAWYcZ6U7rKYgIfFBHGZZfU7KGMNYQjiQjHrFu+Ql5fnqMCDHGQkQrPXjCs28kElInvogV55F49mKgsIxxsjeNfeU1jjmxAoDBiDcgTRKyzzQxRqDDUDc0zk+3b/sTz+OKTcS5ahSlHfpjhVtQtcGAZ0GME6DYxv4olQVRHNzEoyNDY2UnIVFsYlpUgYvPwSZb8EARnANTQ0lHf//v1gXc7XYRYTXmIExrAuj3jHgMHBwTAwMIBh1WZEn/FdsO05hRhn8N6RkfcKgSeGKbLGU4eHEAta9vBYlQAy7JHAhIU9Ysw6xls11Fj7/c3kFSkEGA09MryfCHIZoNq1jJ6M9ghgZj7wbq3Xjbx165YrBFKMw/s1a9aEPXv2hOrqajfIsn4miizdExnkCXkUk4JoI+IqLy+/KkRgQphqHYG7d+/2dopCaBC2ZcuWUFNTE/bt2+ey4DNPRw3qMSm1JuXes/f06dMQR7AaiBKR2GzduvWCGfG9teIcDKHG8VZzAsj3798frFQ9TzCgtLTUyzKRxL5udMyFKTUvZgEIgii648RYDNEQmLLhHTt2tJqH9SjHapRSdsRw/fr1nhcgyB43/BjPGqiwZvLbNJi0JzTcIRFjjQaT+rcJ6In2cxTDpEGE9+qGoIhQGcGNtyb3L4149QrlHLpjOgsoDOpm3Bavy8Y8p1GKIFCgn1uZJrsi+xixvJc8e/bsksW5Qx0UGhI2WnkxWaIOFbXOxuslC8F/Gr+UJeugYLnhHZDYG11SgXv1FrVxa1afm4HJgwHyCaEaW7IRwSRPQEBNwt4XTUG7eVKZGLXOpO4HD4mmASbjE6GcMJqhaGUhl8yXQ6Aa4wX4lx9M9LSM/d3WF1CoNiyhCKCslJBKtATEscS1pNOS0FGdMZoHtypBDGo+Bu/fNlpbgA7h1D3MjFVqWXGFV6M5MROyjSc3mnQKsw4yfrgRNLJ++SnGCBetJ5wxgYvkgE4+XMBPPElMbt4JD+8mPN9y5IzJSFXvQCFIQUdXdT2Kt4RKcTJL7dtQaLG1EZKOG8Q4JZEDTMXu7m4/1qssMcyO62HTpk0f37lz5ycAVdxBD+TkcDIJ+dDA0RgW09mzZ7+z6ZbPcRrhGKDpqORUs5IcKgN++0f4yhI59fDhw19Ah5HqOy5fI1Iwqh8ghMxubm7+sr29/RRtliTk9Cv4iaViqvzBGX5A+Dbvfa2zs/O45UNpVVXVn9acrloe/Wt6FnyuSBnClSQkiMWpwDw/df369a+xnJhZMvrfDBCyBhIqJ7okPaGystK/iTN/VMovC1Ht7du3a82gWaucThta53ft2tWc0tXV5Z6qLKzmw8WLFz87d+7cNwZXiRDBsLq6OlfIrEcBhhYVFbkDNrhCfX29OwIPo/rkyZMeFng1A8gRhddou+MqPyNIMXj3nj59+kRbW9snMMgoGUCiHTlyJOzcudNjefPmTV/n96y2tnbJX1NTU5OXJzKQFR37aveGajrjuNBgPtTS0vLptWvXqoBa8dS/nBoTP5XATjJahvvN0Wv79u2ehJqAyLhx40ayN8gJNTr1DkNzPn706NHLIyMjlfox0YiNtk9ZTewpOfsrTp6eUXzlypVw8OBBRwIZIMVfME6gTL1FsiKzISNuyseNoHe5QnUwwac9YD9w4IDXP4iggFbMQZTDCIna2trq6+SITszqLzoTJPQM/C/AACIm/vZ9H964AAAAAElFTkSuQmCC";

	var imageElement = document.createElement('img');

	imageElement.src = g_images[0][2]//dataurl;

	// initialise images array:
	for (let i = 0; i < g_images.length; i++) {
		images[i] = [];
		for (let j = 0; j < g_images[i].length; j++) {
			images[i][j]= document.createElement('img');
			images[i][j].src = g_images[i][j]//dataurl;
			images[i][j].onload = function(e) {
				let	m = 0;
				images_loaded += 1;
				if (images_loaded === no_of_images) {	// IF ALL IMAGES ARE LOADED:
					for (let k = 0; k < g_images.length; k++) {
						let n = 0;
						textures[m] = [];
						for (let l = 0; l < g_images[k].length; l++) {
							textures[m][n] = new THREE.Texture( images[k][l] );
							textures[m][n].needsUpdate = true;
							n += 1;
						}
						m += 1;
					}
					initialize();
				}
			};
			no_of_images += 1;
		}
	}

	imageElement.onload = function(e) {
		let	m = 0;
		images_loaded += 1;
		if (images_loaded === no_of_images) {	// IF ALL IMAGES ARE LOADED:
			for (let k = 0; k < g_images.length; k++) {
				let n = 0;
				textures[m] = [];
				for (let l = 0; l < g_images[k].length; l++) {
					textures[m][n] = new THREE.Texture( images[k][l] );
					textures[m][n].needsUpdate = true;
					n += 1;
				}
				m += 1;
			}
			initialize();
		}
	};
}



function	initialize() {	
	scene = new THREE.Scene();
	camera = new THREE.PerspectiveCamera(90, 1280/720, 0.1, 1000);

	mesh = new THREE.Mesh(
		new THREE.BoxGeometry(1,1,1),
		new THREE.MeshPhongMaterial({color:0xff4444, wireframe:USE_WIREFRAME})
	);
	// mesh.position.y += 1;
	mesh.receiveShadow = true;
	mesh.castShadow = true;
	mesh.position.set(-10, 2.3, 10);
	scene.add(mesh);
	
	// meshFloor = new THREE.Mesh(
		// new THREE.PlaneGeometry(20,20, 10,10),
		// // new THREE.MeshPhongMaterial({color:0x00ff00, wireframe:USE_WIREFRAME})
		// new THREE.MeshPhongMaterial({
			// color:0xffffff,
			// // map: new THREE.MeshBasicMaterial(),
			// map:textures[1][7],//5
			// // map:crateTexture,
			// // bumpMap:crateBumpMap,
			// // normalMap:crateNormalMap
		// })

	// );
	// meshFloor.rotation.x -= Math.PI / 2;
	// meshFloor.receiveShadow = true;
	// scene.add(meshFloor);
	
	ambientLight = new THREE.AmbientLight(0xffffff, 0.2);
	scene.add(ambientLight);
	
	light = new THREE.PointLight(0xffffff, 0.8, 18);
	light.position.set(-3,6,-3);
	light.castShadow = true;
	light.shadow.camera.near = 0.1;
	light.shadow.camera.far = 25;
	scene.add(light);


	ambientLight = new THREE.AmbientLight(0xffffff, 0.2);
	scene.add(ambientLight);

	light = new THREE.PointLight(0xffFFff, 1.0, 18);
	light.position.set(40,6,-3);
	light.castShadow = true;
	light.shadow.camera.near = 0.1;
	light.shadow.camera.far = 25;
	scene.add(light);


	crate = new THREE.Mesh(
		new THREE.BoxGeometry(3,3,3),
		new THREE.MeshPhongMaterial({
			color:0xffffff,
			// map: new THREE.MeshBasicMaterial(),
			map:textures[0][2],
			// map:crateTexture,
			// bumpMap:crateBumpMap,
			// normalMap:crateNormalMap
		})
	);
	scene.add(crate);
	crate.position.set(10, 3/2, 10);
	crate.receiveShadow = true;
	crate.castShadow = true;

// obj string below represents a cube

	// var objString = "# Blender v2.79 (sub 0) OBJ File: ''\\n# www.blender.org\\nmtllib standing.mtl\\no Beta_Surface.001_Beta_Surface.003_Beta_Surface.001_Beta_Surface\\nv -0.055797 0.036602 1.375807\\nv -0.066129 0.024279 1.305225\\nv -0.059960 -0.056927 1.308568\\nv -0.032565 -0.098236 1.371638\\nv -0.072397 -0.019541 1.461976\\nv -0.086837 -0.012002 1.458889\\nv 0.005178 -0.130459 1.267946\\nv -0.003282 -0.124958 1.260204\\nv 0.173446 -0.035432 1.472839\\nv 0.220386 0.079900 1.476570\\nv -0.114849 0.077944 1.461272\\nv 0.138675 -0.102452 1.386265\\nv 0.056382 -0.131362 1.239833\\nv 0.108814 -0.134442 1.273625\\nv 0.001078 -0.039566 1.292019\\nv 0.003892 -0.075937 1.280316\\nv -0.005603 0.005163 1.289665\\nv 0.188980 -0.005596 1.322159\\nv 0.191063 -0.018225 1.428879\\nv 0.176270 -0.064543 1.323979\\nv 0.112129 -0.080246 1.285809\\nv 0.118060 -0.129702 1.266704\\nv 0.056358 -0.125076 1.226074\\nv 0.057707 -0.071405 1.263363\\nv -0.043261 0.001382 1.745833\\nv -0.046029 -0.044809 1.695270\\nv -0.033252 -0.069508 1.717711\\nv -0.023826 -0.046217 1.763843\\nv -0.029572 -0.003674 1.771911\\nv 0.116664 -0.043990 1.297567\\nv 0.126939 0.000120 1.296130\\nv 0.175517 -0.062158 1.314712\\nv 0.119187 0.104836 1.254964\\nv 0.014388 0.108951 1.249680\\nv 0.027724 0.117898 1.265675\\nv 0.105244 0.114997 1.269861\\nv 0.127059 -0.047027 1.695324\\nv 0.121742 -0.018236 1.613266\\nv 0.125918 0.058319 1.646131\\nv 0.124726 0.063571 1.725127\\nv 0.125443 -0.000780 1.745886\\nv 0.020988 -0.053359 1.787904\\nv 0.020656 0.042939 1.793500\\nv -0.026351 0.041828 1.767654\\nv 0.024099 0.105945 1.746304\\nv -0.022744 0.086432 1.734197\\nv 0.062583 0.042402 1.793512\\nv 0.060784 0.105475 1.746316\\nv 0.060083 0.108878 1.627931\\nv 0.059052 0.097860 1.609611\\nv 0.025151 0.098332 1.609342\\nv 0.024505 0.109354 1.627770\\nv 0.024701 0.122657 1.669926\\nv 0.060657 0.122196 1.669937\\nv 0.103888 0.002396 1.586175\\nv 0.107091 0.044041 1.599715\\nv 0.105775 0.084900 1.635643\\nv 0.111612 -0.005483 1.771955\\nv 0.107120 0.084768 1.734237\\nv 0.109562 0.040086 1.767696\\nv -0.021826 0.086585 1.635138\\nv 0.024795 0.053887 1.573683\\nv 0.008982 0.053124 1.577569\\nv -0.024534 0.045885 1.598214\\nv 0.016399 -0.118383 1.613644\\nv 0.022883 -0.121914 1.665285\\nv -0.040882 0.065693 1.725075\\nv -0.042628 0.060524 1.645601\\nv -0.040838 -0.016009 1.612050\\nv 0.024504 -0.092032 1.545746\\nv 0.055226 -0.092458 1.546052\\nv 0.062518 -0.119008 1.613685\\nv 0.056212 -0.122341 1.665295\\nv 0.058160 0.053339 1.574695\\nv 0.073925 0.052111 1.578985\\nv 0.095552 -0.005941 1.577931\\nv 0.052332 -0.062539 1.540777\\nv 0.028404 -0.062199 1.540438\\nv -0.014364 -0.004279 1.575859\\nv 0.059787 -0.053856 1.787917\\nv -0.022539 0.004248 1.584169\\nv 0.113638 -0.071391 1.717757\\nv 0.104783 -0.047866 1.763882\\nv 0.012566 0.095879 0.114945\\nv 0.055080 0.071003 0.128567\\nv 0.039300 0.013731 0.142489\\nv -0.019489 0.017048 0.115791\\nv -0.024659 0.071866 0.107042\\nv 0.246014 -0.112687 0.586780\\nv 0.182346 -0.076354 0.581802\\nv 0.163110 0.050130 0.860189\\nv 0.250675 0.020226 0.962899\\nv 0.242130 -0.059044 0.995467\\nv 0.221468 -0.171352 0.593523\\nv 0.252500 -0.075970 0.068912\\nv 0.246319 -0.065917 0.018054\\nv 0.193298 -0.086553 0.022482\\nv 0.198713 -0.096679 0.087370\\nv 0.279854 -0.127124 0.074917\\nv 0.277012 -0.124291 0.024739\\nv 0.273013 -0.125047 0.019964\\nv 0.219734 -0.138836 0.039245\\nv 0.238313 -0.202080 0.009133\\nv 0.231372 -0.216540 0.051143\\nv 0.204362 -0.149004 0.102841\\nv 0.314886 -0.190396 0.049699\\nv 0.313468 -0.188310 0.006932\\nv -0.087423 -0.037427 0.028023\\nv -0.092065 -0.083843 0.028334\\nv -0.034992 -0.136704 0.035121\\nv -0.006870 -0.079281 0.051816\\nv 0.239898 -0.235967 0.007750\\nv 0.270100 -0.297677 0.008700\\nv 0.269970 -0.292523 0.039107\\nv 0.243003 -0.238205 0.053160\\nv 0.270958 -0.295707 0.006591\\nv 0.330849 -0.246264 0.013362\\nv 0.327335 -0.236831 0.031306\\nv 0.242307 -0.059390 0.114852\\nv 0.193954 -0.074554 0.123199\\nv 0.133183 -0.093797 0.490842\\nv 0.195310 -0.059321 0.505841\\nv 0.016484 -0.058678 0.470155\\nv -0.074025 -0.056955 0.506910\\nv 0.257633 -0.117712 0.532838\\nv 0.274527 -0.090871 0.114411\\nv 0.257711 -0.142013 0.125418\\nv 0.236399 -0.185940 0.526476\\nv 0.162961 0.094158 1.400671\\nv 0.228772 0.084070 1.164644\\nv 0.221559 0.010438 1.159318\\nv 0.165791 0.003456 1.387844\\nv 0.229974 0.080741 1.408008\\nv 0.302036 0.072047 1.172424\\nv -0.056831 0.019431 1.366201\\nv -0.106649 0.024649 1.360510\\nv -0.150184 0.045652 1.163726\\nv -0.088740 0.051058 1.134566\\nv 0.279602 0.008558 1.195031\\nv 0.215155 0.008037 1.386278\\nv 0.237553 0.086763 1.140920\\nv 0.282803 0.081534 1.145255\\nv 0.327455 0.041712 0.892277\\nv 0.305832 0.051680 0.904286\\nv -0.172831 0.098308 0.871221\\nv -0.144970 0.118738 0.877015\\nv -0.121617 0.092188 0.878393\\nv -0.115639 0.057929 0.864399\\nv -0.138638 0.043843 0.867236\\nv -0.165388 0.078239 0.870644\\nv -0.085760 0.103628 1.106289\\nv -0.109290 0.132093 1.124182\\nv -0.166961 0.110666 1.111155\\nv -0.172141 0.068454 1.109573\\nv -0.152684 0.033294 1.119467\\nv -0.100224 0.068827 1.100654\\nv 0.324154 0.030928 0.814655\\nv 0.343716 0.037872 0.822742\\nv 0.323024 -0.028824 0.792243\\nv 0.317691 -0.029366 0.789013\\nv 0.309568 -0.028128 0.788467\\nv 0.302200 -0.026088 0.791871\\nv 0.296994 -0.024812 0.796275\\nv 0.293505 -0.023902 0.802491\\nv 0.310326 -0.043665 0.852739\\nv 0.323685 -0.035397 0.874136\\nv 0.298481 0.039595 0.870739\\nv 0.286242 -0.024665 0.859464\\nv 0.290449 -0.047611 0.821795\\nv 0.298886 -0.045395 0.862291\\nv 0.274389 -0.031572 0.862549\\nv 0.270334 -0.040504 0.833849\\nv 0.279279 -0.050872 0.816563\\nv 0.267017 -0.046751 0.824375\\nv 0.254480 -0.045914 0.804895\\nv 0.264609 -0.048962 0.797731\\nv 0.264470 -0.031602 0.793509\\nv 0.279314 -0.032372 0.813730\\nv 0.279629 -0.026191 0.834070\\nv 0.294978 -0.030796 0.823268\\nv 0.302851 -0.028133 0.855092\\nv -0.123173 0.094610 1.387379\\nv -0.170583 0.111312 1.145668\\nv -0.055173 0.107821 1.388745\\nv -0.096976 0.123757 1.147057\\nv 0.310214 -0.024034 0.917602\\nv 0.286670 -0.008299 1.152840\\nv 0.244180 0.010220 1.139407\\nv 0.281473 -0.023927 0.898228\\nv -0.139846 0.085433 0.761102\\nv -0.138509 0.019880 0.790987\\nv -0.173256 0.014679 0.780591\\nv -0.169380 0.082589 0.757458\\nv -0.175642 0.103790 0.843842\\nv -0.137538 0.113541 0.832434\\nv -0.156315 0.031751 0.830448\\nv -0.154605 0.023418 0.813732\\nv -0.152888 0.021572 0.812482\\nv -0.132397 0.025899 0.804244\\nv -0.127845 0.037498 0.824972\\nv -0.149074 0.014671 0.804206\\nv -0.148384 0.013170 0.802599\\nv -0.146647 0.012641 0.802832\\nv -0.147723 0.014717 0.804646\\nv -0.143649 0.013075 0.801246\\nv -0.144247 0.015178 0.803280\\nv -0.144905 0.010736 0.800386\\nv -0.142521 0.011319 0.798883\\nv -0.146508 0.011150 0.799980\\nv -0.140000 0.014040 0.799750\\nv -0.140328 0.016028 0.801549\\nv -0.139765 0.012219 0.797319\\nv -0.143611 0.008801 0.796233\\nv -0.141756 0.009299 0.795391\\nv -0.139772 0.010190 0.793720\\nv -0.141537 0.006864 0.791601\\nv -0.139775 0.007747 0.789970\\nv -0.143132 0.006332 0.792574\\nv -0.144985 0.008945 0.796010\\nv -0.144379 0.006328 0.792664\\nv -0.135324 0.017944 0.798628\\nv -0.136916 0.017278 0.799579\\nv -0.136997 0.015332 0.798022\\nv -0.135092 0.016821 0.796580\\nv -0.137533 0.013476 0.795113\\nv -0.135772 0.015102 0.793141\\nv -0.137939 0.011371 0.791604\\nv -0.136418 0.012890 0.789481\\nv -0.138070 0.008891 0.788030\\nv -0.136664 0.010262 0.786038\\nv -0.136181 0.006170 0.781074\\nv -0.136438 0.007780 0.783022\\nv -0.137746 0.006569 0.784512\\nv -0.137115 0.005130 0.781324\\nv -0.139477 0.005325 0.786455\\nv -0.138974 0.003531 0.783257\\nv -0.141370 0.004347 0.788465\\nv -0.141213 0.002277 0.785818\\nv -0.144206 0.002141 0.788884\\nv -0.143152 0.001670 0.787866\\nv -0.143068 0.003678 0.789978\\nv -0.144307 0.003726 0.790437\\nv -0.150406 0.016540 0.803713\\nv -0.150145 0.015017 0.801014\\nv -0.148128 0.012679 0.797837\\nv -0.150883 0.017506 0.799458\\nv -0.149138 0.014892 0.795411\\nv -0.150811 0.019811 0.803472\\nv -0.146384 0.010029 0.794249\\nv -0.145500 0.007093 0.791320\\nv -0.147280 0.011863 0.791841\\nv -0.146342 0.008598 0.788844\\nv -0.147378 0.014094 0.789321\\nv -0.123469 -0.005223 0.802566\\nv -0.148987 0.017438 0.792774\\nv -0.149805 0.019886 0.797032\\nv -0.149158 0.022103 0.801200\\nv -0.145475 0.004309 0.788969\\nv -0.144934 0.002272 0.787327\\nv -0.146276 0.005791 0.786236\\nv -0.145536 0.003735 0.784279\\nv -0.121845 -0.008642 0.800487\\nv -0.120009 -0.011058 0.799071\\nv -0.117891 -0.006235 0.795998\\nv -0.118771 -0.008189 0.796641\\nv -0.120599 -0.006104 0.797836\\nv -0.119008 -0.004063 0.796684\\nv -0.122418 -0.002927 0.799850\\nv -0.120741 -0.000669 0.798257\\nv -0.124582 0.000910 0.802076\\nv -0.122740 0.002997 0.800208\\nv -0.128644 0.012352 0.808857\\nv -0.127257 0.010434 0.807405\\nv -0.129688 0.009019 0.808950\\nv -0.144549 0.022322 0.801323\\nv -0.127246 0.005250 0.805843\\nv -0.125045 0.006982 0.804086\\nv -0.141512 0.024604 0.799023\\nv -0.124758 0.012012 0.807268\\nv -0.122517 0.008634 0.803477\\nv -0.140022 0.025940 0.792734\\nv -0.139776 0.024109 0.788776\\nv -0.140334 0.026842 0.796832\\nv -0.120361 0.004822 0.799829\\nv -0.118320 0.001016 0.798055\\nv -0.139599 0.021676 0.784898\\nv -0.115593 0.002219 0.799036\\nv -0.137399 0.024944 0.788357\\nv -0.136862 0.022032 0.785419\\nv -0.137841 0.026954 0.791613\\nv -0.138189 0.028091 0.794993\\nv -0.116684 -0.002911 0.796683\\nv -0.115934 -0.006072 0.796018\\nv -0.113685 -0.001956 0.797992\\nv -0.112303 -0.005590 0.797516\\nv -0.135300 0.015239 0.780808\\nv -0.135535 0.012401 0.777940\\nv -0.136185 0.018653 0.782922\\nv -0.132858 0.013874 0.778700\\nv -0.133542 0.013380 0.778230\\nv -0.133638 0.015716 0.780818\\nv -0.132773 0.015579 0.780680\\nv -0.133821 0.018488 0.783871\\nv -0.132919 0.018079 0.783634\\nv -0.134358 0.021737 0.786501\\nv -0.132954 0.021091 0.786978\\nv -0.133415 0.027209 0.794731\\nv -0.132975 0.026005 0.792817\\nv -0.134699 0.026722 0.792370\\nv -0.134518 0.027774 0.795390\\nv -0.134612 0.024659 0.789300\\nv -0.132826 0.023877 0.790090\\nv -0.133306 0.027046 0.795460\\nv -0.132461 0.025252 0.793081\\nv -0.132420 0.022880 0.790119\\nv -0.132468 0.023876 0.793497\\nv -0.132651 0.021409 0.790207\\nv -0.133375 0.025666 0.796408\\nv -0.132706 0.019992 0.786795\\nv -0.132785 0.016954 0.783223\\nv -0.133150 0.018458 0.786632\\nv -0.133365 0.015358 0.782796\\nv -0.134209 0.016682 0.786556\\nv -0.134288 0.013566 0.783186\\nv -0.133526 0.019502 0.790221\\nv -0.133140 0.021802 0.793648\\nv -0.134081 0.023092 0.796772\\nv -0.132688 0.014439 0.779997\\nv -0.132910 0.012644 0.777628\\nv -0.133345 0.012654 0.779175\\nv -0.133810 0.010513 0.776325\\nv -0.133860 0.010476 0.780113\\nv -0.134390 0.008081 0.777602\\nv -0.135409 0.006474 0.779845\\nv -0.135076 0.008885 0.781845\\nv -0.135209 0.011741 0.784748\\nv -0.134987 0.014647 0.788116\\nv -0.133800 0.019026 0.795295\\nv -0.134685 0.019672 0.798336\\nv -0.134348 0.017154 0.791701\\nv -0.136542 0.019399 0.800271\\nv -0.140499 0.018389 0.802680\\nv -0.136038 0.023542 0.798806\\nv -0.140072 0.023113 0.802743\\nv -0.144673 0.017741 0.804548\\nv -0.144321 0.022521 0.805039\\nv -0.138868 0.026372 0.802098\\nv -0.142998 0.025595 0.804082\\nv -0.135121 0.026445 0.798318\\nv -0.148542 0.017094 0.805610\\nv -0.149367 0.021452 0.804555\\nv -0.147561 0.024086 0.803133\\nv -0.143093 0.024431 0.802169\\nv -0.140771 0.026555 0.801228\\nv -0.138197 0.028053 0.798691\\nv -0.134785 0.027876 0.796849\\nv -0.130403 0.058671 0.817969\\nv -0.134387 0.041571 0.803285\\nv -0.155313 0.048942 0.826138\\nv -0.149381 0.034002 0.811629\\nv 0.037570 0.003659 0.101263\\nv 0.040543 0.002182 0.096366\\nv 0.047952 0.063313 0.071122\\nv 0.007654 0.085500 0.063866\\nv -0.033137 0.034925 0.070430\\nv 0.026132 0.015271 0.036955\\nv 0.005605 -0.059914 0.049685\\nv 0.000833 -0.045939 0.007340\\nv -0.074577 -0.025379 0.046423\\nv -0.067691 -0.030663 0.001640\\nv -0.024826 0.034756 0.016107\\nv 0.322352 -0.205286 0.037096\\nv -0.037364 -0.138583 0.010630\\nv -0.093258 -0.090911 0.014251\\nv 0.154029 -0.173921 0.474972\\nv 0.196304 -0.132150 0.143092\\nv 0.050016 0.013767 0.498506\\nv -0.081959 0.013724 0.519008\\nv 0.171601 -0.206839 0.567959\\nv 0.125788 -0.113396 0.586468\\nv 0.174945 -0.118451 0.958549\\nv -0.068861 -0.057006 0.571589\\nv -0.022651 -0.097228 0.546322\\nv -0.083511 -0.086971 0.932072\\nv -0.144038 -0.022105 0.971251\\nv -0.113877 0.090270 0.933920\\nv -0.038189 0.100795 0.886095\\nv -0.022205 0.033464 0.579966\\nv -0.074025 0.025030 0.572461\\nv 0.187346 0.109756 1.427752\\nv -0.082756 0.114828 1.439306\\nv 0.036468 -0.059891 0.575399\\nv 0.012103 -0.064858 0.866755\\nv 0.028932 0.030499 0.853905\\nv 0.029218 -0.000821 0.597701\\nv 0.093287 -0.037969 0.847727\\nv 0.179555 0.043185 1.403289\\nv -0.005586 0.059537 0.508549\\nv 0.271176 0.001620 0.906442\\nv 0.228862 0.044898 1.116606\\nv 0.334529 0.024413 0.912188\\nv 0.310554 0.044731 1.134419\\nv 0.333000 0.034359 0.886124\\nv 0.330864 0.030291 0.889623\\nv 0.282969 -0.016490 0.853893\\nv -0.175669 0.048800 0.843186\\nv -0.138017 0.049145 0.823270\\nv 0.016315 0.095144 0.016383\\nv 0.058990 0.067401 0.018742\\nv 0.020768 0.097333 0.018436\\nv -0.078323 -0.043567 0.007275\\nv -0.081370 -0.045799 0.007869\\nv -0.004057 -0.079041 0.006370\\nv 0.316433 -0.205911 0.009262\\nv 0.268549 -0.032501 0.821457\\nv 0.254929 -0.034517 0.804389\\nvt 0.269087 0.806751\\nvt 0.265120 0.740415\\nvt 0.345525 0.734496\\nvt 0.405837 0.789707\\nvt 0.354003 0.905914\\nvt 0.336307 0.911958\\nvt 0.443160 0.694538\\nvt 0.434623 0.687461\\nvt 0.631016 0.887444\\nvt 0.774957 0.894510\\nvt 0.197570 0.906832\\nvt 0.570235 0.789955\\nvt 0.488173 0.670648\\nvt 0.533136 0.694641\\nvt 0.226883 0.061011\\nvt 0.201695 0.062685\\nvt 0.256461 0.057193\\nvt 0.690677 0.740865\\nvt 0.674365 0.855330\\nvt 0.630750 0.734836\\nvt 0.201631 0.154569\\nvt 0.541690 0.687581\\nvt 0.488185 0.659216\\nvt 0.198416 0.108625\\nvt 0.669697 0.466362\\nvt 0.611544 0.477182\\nvt 0.602571 0.444413\\nvt 0.650608 0.421219\\nvt 0.682855 0.444651\\nvt 0.226817 0.156278\\nvt 0.256389 0.160136\\nvt 0.633550 0.725817\\nvt 0.831529 0.665681\\nvt 0.145295 0.665359\\nvt 0.125278 0.676926\\nvt 0.851633 0.677295\\nvt 0.363172 0.476946\\nvt 0.380324 0.549262\\nvt 0.313227 0.558994\\nvt 0.274677 0.510893\\nvt 0.304918 0.466023\\nvt 0.675548 0.374099\\nvt 0.749262 0.442548\\nvt 0.709978 0.471900\\nvt 0.769946 0.520335\\nvt 0.722418 0.515793\\nvt 0.225184 0.442086\\nvt 0.204412 0.520039\\nvt 0.270336 0.622768\\nvt 0.289960 0.633339\\nvt 0.684510 0.633390\\nvt 0.704106 0.622839\\nvt 0.738539 0.594092\\nvt 0.235849 0.593957\\nvt 0.380720 0.582360\\nvt 0.343166 0.589560\\nvt 0.297594 0.582612\\nvt 0.291753 0.444248\\nvt 0.252042 0.515513\\nvt 0.264539 0.471534\\nvt 0.676909 0.582739\\nvt 0.624565 0.641946\\nvt 0.626857 0.625446\\nvt 0.631401 0.589651\\nvt 0.505426 0.511744\\nvt 0.503998 0.465997\\nvt 0.699832 0.511166\\nvt 0.661317 0.559149\\nvt 0.594321 0.549374\\nvt 0.498887 0.566293\\nvt 0.475812 0.566286\\nvt 0.469335 0.511718\\nvt 0.470847 0.465962\\nvt 0.349987 0.641923\\nvt 0.347698 0.625404\\nvt 0.392583 0.587479\\nvt 0.467285 0.591644\\nvt 0.507389 0.591647\\nvt 0.582042 0.587541\\nvt 0.299145 0.373510\\nvt 0.593897 0.582434\\nvt 0.372221 0.444132\\nvt 0.324112 0.420793\\nvt 0.914665 0.684566\\nvt 0.777028 0.674562\\nvt 0.810517 0.699331\\nvt 0.849809 0.687467\\nvt 0.885442 0.681653\\nvt 0.186077 0.714320\\nvt 0.232176 0.703045\\nvt 0.308800 0.871717\\nvt 0.259164 0.943487\\nvt 0.207018 0.967641\\nvt 0.143431 0.710324\\nvt 0.732850 0.308763\\nvt 0.157306 0.214603\\nvt 0.165246 0.177921\\nvt 0.636000 0.300661\\nvt 0.709524 0.265747\\nvt 0.103292 0.213406\\nvt 0.105235 0.207910\\nvt 0.123772 0.155238\\nvt 0.060264 0.152133\\nvt 0.651181 0.221169\\nvt 0.647772 0.275817\\nvt 0.698450 0.207850\\nvt 0.712306 0.183971\\nvt 0.752505 0.243222\\nvt 0.335586 0.232934\\nvt 0.355004 0.213665\\nvt 0.329383 0.158982\\nvt 0.306666 0.185046\\nvt 0.083252 0.017216\\nvt 0.046131 0.014124\\nvt 0.328980 0.163540\\nvt 0.306666 0.185046\\nvt 0.046948 0.015571\\nvt 0.052200 0.070202\\nvt 0.349164 0.218483\\nvt 0.914665 0.684566\\nvt 0.949899 0.690928\\nvt 0.972839 0.935280\\nvt 0.923399 0.928118\\nvt 0.788764 0.903948\\nvt 0.828300 0.925160\\nvt 0.867007 0.936407\\nvt 0.885442 0.681653\\nvt 0.849809 0.687467\\nvt 0.828300 0.925160\\nvt 0.935927 0.389457\\nvt 0.911700 0.580740\\nvt 0.864263 0.572724\\nvt 0.869649 0.411605\\nvt 0.787690 0.410450\\nvt 0.785353 0.565140\\nvt 0.749845 0.576965\\nvt 0.755890 0.378523\\nvt 0.869649 0.411605\\nvt 0.836072 0.421955\\nvt 0.823626 0.559207\\nvt 0.864263 0.572724\\nvt 0.823626 0.559207\\nvt 0.836072 0.421955\\nvt 0.537263 0.369564\\nvt 0.568495 0.374342\\nvt 0.575509 0.557004\\nvt 0.559195 0.546644\\nvt 0.561940 0.578969\\nvt 0.559195 0.546644\\nvt 0.534948 0.545903\\nvt 0.511945 0.559783\\nvt 0.513556 0.583273\\nvt 0.545927 0.583488\\nvt 0.516423 0.378719\\nvt 0.542183 0.369643\\nvt 0.583229 0.382664\\nvt 0.613428 0.385487\\nvt 0.639643 0.387560\\nvt 0.674835 0.389215\\nvt 0.769087 0.170914\\nvt 0.190561 0.182792\\nvt 0.222753 0.226737\\nvt 0.222472 0.230553\\nvt 0.221581 0.235947\\nvt 0.716668 0.177986\\nvt 0.719248 0.174341\\nvt 0.721724 0.170854\\nvt 0.261368 0.207621\\nvt 0.220637 0.242493\\nvt 0.248948 0.197305\\nvt 0.262602 0.150640\\nvt 0.279761 0.198489\\nvt 0.412334 0.032055\\nvt 0.132093 0.017513\\nvt 0.110000 0.011949\\nvt 0.411404 0.015521\\nvt 0.170923 0.011336\\nvt 0.157474 0.007989\\nvt 0.582162 0.023428\\nvt 0.574952 0.029814\\nvt 0.157182 0.045724\\nvt 0.167110 0.026550\\nvt 0.113377 0.045923\\nvt 0.125658 0.046457\\nvt 0.125862 0.029067\\nvt 0.787690 0.410450\\nvt 0.785353 0.565140\\nvt 0.755890 0.378523\\nvt 0.749845 0.576965\\nvt 0.586240 0.550140\\nvt 0.630113 0.539985\\nvt 0.639643 0.387560\\nvt 0.662238 0.388020\\nvt 0.651235 0.550224\\nvt 0.769087 0.170914\\nvt 0.724382 0.168559\\nvt 0.222753 0.226737\\nvt 0.193289 0.183797\\nvt 0.241284 0.165555\\nvt 0.771928 0.120527\\nvt 0.136232 0.017503\\nvt 0.409440 0.033101\\nvt 0.409962 0.031219\\nvt 0.410030 0.016930\\nvt 0.112287 0.009502\\nvt 0.172407 0.012405\\nvt 0.174552 0.011152\\nvt 0.172422 0.008546\\nvt 0.170923 0.011336\\nvt 0.168104 0.006076\\nvt 0.167347 0.009215\\nvt 0.564408 0.019157\\nvt 0.566562 0.016615\\nvt 0.563494 0.013960\\nvt 0.560991 0.017175\\nvt 0.559527 0.019588\\nvt 0.563108 0.021755\\nvt 0.163312 0.005024\\nvt 0.163275 0.008138\\nvt 0.569004 0.014476\\nvt 0.566411 0.011303\\nvt 0.568419 0.022135\\nvt 0.570018 0.019928\\nvt 0.571985 0.018009\\nvt 0.573243 0.023152\\nvt 0.574978 0.021470\\nvt 0.571908 0.024968\\nvt 0.567275 0.024479\\nvt 0.570878 0.026863\\nvt 0.157474 0.007989\\nvt 0.159448 0.008028\\nvt 0.159176 0.005064\\nvt 0.156078 0.005724\\nvt 0.571418 0.008261\\nvt 0.569215 0.009486\\nvt 0.571452 0.012810\\nvt 0.573721 0.011387\\nvt 0.574114 0.016415\\nvt 0.576211 0.015013\\nvt 0.576857 0.019949\\nvt 0.578689 0.018658\\nvt 0.582162 0.023428\\nvt 0.580872 0.021638\\nvt 0.579452 0.022722\\nvt 0.581578 0.024401\\nvt 0.577734 0.024213\\nvt 0.579973 0.026084\\nvt 0.575935 0.025785\\nvt 0.577891 0.027759\\nvt 0.574952 0.029814\\nvt 0.576126 0.029090\\nvt 0.574479 0.027320\\nvt 0.573491 0.028773\\nvt 0.172311 0.014456\\nvt 0.175608 0.014599\\nvt 0.557713 0.022202\\nvt 0.561769 0.024693\\nvt 0.555751 0.025605\\nvt 0.560325 0.027856\\nvt 0.172026 0.017885\\nvt 0.175888 0.018868\\nvt 0.566164 0.027082\\nvt 0.570000 0.029171\\nvt 0.564981 0.030056\\nvt 0.569081 0.032009\\nvt 0.171407 0.047130\\nvt 0.168444 0.044188\\nvt 0.164270 0.047685\\nvt 0.166747 0.051021\\nvt 0.176408 0.042715\\nvt 0.172946 0.039712\\nvt 0.181389 0.038332\\nvt 0.177185 0.035483\\nvt 0.171499 0.021240\\nvt 0.175079 0.023236\\nvt 0.572878 0.030898\\nvt 0.575054 0.031564\\nvt 0.572419 0.033771\\nvt 0.575141 0.034643\\nvt 0.162167 0.053331\\nvt 0.160444 0.049522\\nvt 0.157564 0.050108\\nvt 0.158363 0.054224\\nvt 0.157182 0.045724\\nvt 0.157328 0.047206\\nvt 0.159400 0.046455\\nvt 0.158451 0.044552\\nvt 0.162356 0.044627\\nvt 0.160790 0.042323\\nvt 0.165735 0.041503\\nvt 0.163499 0.039307\\nvt 0.169233 0.026843\\nvt 0.169887 0.029685\\nvt 0.173019 0.027248\\nvt 0.170372 0.024604\\nvt 0.169532 0.037437\\nvt 0.172919 0.033462\\nvt 0.169445 0.031934\\nvt 0.166591 0.035666\\nvt 0.167110 0.026550\\nvt 0.166493 0.030205\\nvt 0.164014 0.034014\\nvt 0.163149 0.029105\\nvt 0.161483 0.032500\\nvt 0.164118 0.025852\\nvt 0.161525 0.037506\\nvt 0.159132 0.040578\\nvt 0.159440 0.035879\\nvt 0.157278 0.039010\\nvt 0.159060 0.031083\\nvt 0.157276 0.034383\\nvt 0.160319 0.027901\\nvt 0.161100 0.024947\\nvt 0.157147 0.043261\\nvt 0.156009 0.045215\\nvt 0.155205 0.041845\\nvt 0.153669 0.044095\\nvt 0.153172 0.040319\\nvt 0.151434 0.042684\\nvt 0.155224 0.037519\\nvt 0.148829 0.040027\\nvt 0.149688 0.041040\\nvt 0.151274 0.038992\\nvt 0.149562 0.038188\\nvt 0.153162 0.036272\\nvt 0.151030 0.035306\\nvt 0.155194 0.033102\\nvt 0.153036 0.031920\\nvt 0.156949 0.024086\\nvt 0.156010 0.025850\\nvt 0.157846 0.026794\\nvt 0.158269 0.024176\\nvt 0.156795 0.029833\\nvt 0.154693 0.028605\\nvt 0.156504 0.022712\\nvt 0.154493 0.024246\\nvt 0.152597 0.027106\\nvt 0.152762 0.021839\\nvt 0.150313 0.025289\\nvt 0.155317 0.019699\\nvt 0.150599 0.030735\\nvt 0.148642 0.034648\\nvt 0.147988 0.029538\\nvt 0.145942 0.034035\\nvt 0.145180 0.028299\\nvt 0.143170 0.033340\\nvt 0.147651 0.023338\\nvt 0.150641 0.018971\\nvt 0.153894 0.015918\\nvt 0.147441 0.038024\\nvt 0.147494 0.040631\\nvt 0.144904 0.038241\\nvt 0.145153 0.041579\\nvt 0.141959 0.038355\\nvt 0.141976 0.042522\\nvt 0.583364 0.023557\\nvt 0.582540 0.020912\\nvt 0.580594 0.017530\\nvt 0.584804 0.020117\\nvt 0.582662 0.016344\\nvt 0.585941 0.023435\\nvt 0.578319 0.013706\\nvt 0.580491 0.012435\\nvt 0.153278 0.007407\\nvt 0.156370 0.009494\\nvt 0.150227 0.009920\\nvt 0.153717 0.012740\\nvt 0.575964 0.009946\\nvt 0.578366 0.008509\\nvt 0.576081 0.005024\\nvt 0.573501 0.006718\\nvt 0.158939 0.010110\\nvt 0.163089 0.010519\\nvt 0.156789 0.014218\\nvt 0.162148 0.014682\\nvt 0.166904 0.011706\\nvt 0.166157 0.015774\\nvt 0.161771 0.018616\\nvt 0.165660 0.019331\\nvt 0.157579 0.018812\\nvt 0.170483 0.013578\\nvt 0.169939 0.017222\\nvt 0.169236 0.020348\\nvt 0.168066 0.023737\\nvt 0.164900 0.022975\\nvt 0.161592 0.022491\\nvt 0.158259 0.022265\\nvt 0.113073 0.029708\\nvt 0.113377 0.045923\\nvt 0.127553 0.029408\\nvt 0.127195 0.045702\\nvt 0.647772 0.275817\\nvt 0.644854 0.274388\\nvt 0.626152 0.309115\\nvt 0.735138 0.314172\\nvt 0.709524 0.265747\\nvt 0.123772 0.155238\\nvt 0.651181 0.221169\\nvt 0.060264 0.152133\\nvt 0.698450 0.207850\\nvt 0.039923 0.199466\\nvt 0.105235 0.207910\\nvt 0.334622 0.228930\\nvt 0.046131 0.014124\\nvt 0.048967 0.066358\\nvt 0.741680 0.918769\\nvt 0.788764 0.903948\\nvt 0.810517 0.699331\\nvt 0.741680 0.918769\\nvt 0.867007 0.936407\\nvt 0.101372 0.694944\\nvt 0.273530 0.673758\\nvt 0.145469 0.951492\\nvt 0.143431 0.710324\\nvt 0.101372 0.694944\\nvt 0.145469 0.951492\\nvt 0.207018 0.967641\\nvt 0.273726 0.922663\\nvt 0.301684 0.880327\\nvt 0.222920 0.703871\\nvt 0.195952 0.712353\\nvt 0.787368 0.844103\\nvt 0.178874 0.868318\\nvt 0.052757 0.709280\\nvt 0.069992 0.923156\\nvt 0.342543 0.841513\\nvt 0.264166 0.687636\\nvt 0.365998 0.835122\\nvt 0.911700 0.580740\\nvt 0.935927 0.389457\\nvt 0.727619 0.818428\\nvt 0.777028 0.674562\\nvt 0.923399 0.928118\\nvt 0.972839 0.935280\\nvt 0.949899 0.690928\\nvt 0.639772 0.551131\\nvt 0.661626 0.554438\\nvt 0.670969 0.546330\\nvt 0.691250 0.387132\\nvt 0.516317 0.547720\\nvt 0.506119 0.386278\\nvt 0.591184 0.544807\\nvt 0.599426 0.387714\\nvt 0.564360 0.566166\\nvt 0.559855 0.586100\\nvt 0.241703 0.162621\\nvt 0.243663 0.164294\\nvt 0.724183 0.132246\\nvt 0.706931 0.137518\\nvt 0.113073 0.029708\\nvt 0.248948 0.197305\\nvt 0.724183 0.132246\\nvt 0.279761 0.198489\\nvt 0.259209 0.146527\\nvt 0.163239 0.213075\\nvt 0.166073 0.180413\\nvt 0.166922 0.211982\\nvt 0.041568 0.205117\\nvt 0.077886 0.074076\\nvt 0.075898 0.075252\\nvt 0.083252 0.017216\\nvt 0.077886 0.074076\\nvt 0.156949 0.024086\\nvt 0.148829 0.040027\\nvn -0.9550 0.0574 -0.2910\\nvn -0.7876 0.0041 -0.6162\\nvn -0.8192 -0.1982 -0.5382\\nvn -0.5619 -0.7762 0.2860\\nvn -0.5035 -0.3058 0.8081\\nvn -0.8698 -0.1518 0.4695\\nvn -0.2900 -0.9562 -0.0405\\nvn -0.6424 -0.3855 -0.6624\\nvn 0.5278 -0.4836 0.6983\\nvn 0.8143 0.3738 0.4440\\nvn -0.8909 0.4092 0.1971\\nvn 0.4111 -0.8484 0.3334\\nvn -0.0269 -0.9973 -0.0683\\nvn 0.2011 -0.9795 -0.0093\\nvn -0.1347 0.1977 -0.9710\\nvn -0.2145 0.4593 -0.8620\\nvn -0.0529 -0.1845 -0.9814\\nvn 0.8196 -0.0726 -0.5684\\nvn 0.9668 -0.1878 -0.1732\\nvn 0.8124 -0.3920 -0.4316\\nvn 0.3538 0.3983 -0.8463\\nvn 0.6552 -0.4002 -0.6408\\nvn 0.0366 -0.2456 -0.9687\\nvn 0.0476 0.5174 -0.8544\\nvn -0.9204 -0.0202 0.3905\\nvn -0.8789 -0.4535 0.1481\\nvn -0.6211 -0.7184 0.3133\\nvn -0.5432 -0.5077 0.6687\\nvn -0.7217 -0.0528 0.6902\\nvn 0.2441 0.1675 -0.9552\\nvn 0.1159 -0.1849 -0.9759\\nvn 0.3875 0.0763 -0.9187\\nvn 0.5353 0.1708 -0.8272\\nvn -0.4087 0.2190 -0.8860\\nvn -0.2826 0.9275 -0.2449\\nvn 0.3651 0.9070 -0.2100\\nvn 0.9999 0.0025 -0.0168\\nvn 0.9998 -0.0043 -0.0174\\nvn 0.9998 0.0013 -0.0180\\nvn -0.1311 -0.5090 0.8507\\nvn -0.2114 0.2744 0.9381\\nvn -0.6468 0.2782 0.7101\\nvn -0.2409 0.8359 0.4932\\nvn -0.6741 0.6683 0.3147\\nvn 0.2275 0.2707 0.9354\\nvn 0.2620 0.8341 0.4854\\nvn 0.2951 0.8841 -0.3624\\nvn 0.2494 0.7341 -0.6316\\nvn -0.2186 0.7455 -0.6297\\nvn -0.2630 0.8950 -0.3602\\nvn -0.2509 0.9669 -0.0459\\nvn 0.2695 0.9621 -0.0416\\nvn 0.8751 0.4062 -0.2630\\nvn 0.8673 -0.2085 -0.4520\\nvn 0.7257 0.1179 -0.6778\\nvn 0.7103 0.3320 -0.6206\\nvn 0.6905 0.6530 -0.3111\\nvn 0.7212 -0.0500 0.6909\\nvn 0.9192 -0.0419 0.3915\\nvn 0.8746 0.4208 0.2408\\nvn 0.6921 0.6492 0.3156\\nvn 0.6522 0.2564 0.7133\\nvn -0.6767 0.6691 -0.3071\\nvn -0.0965 0.4745 -0.8749\\nvn -0.3281 0.4596 -0.8253\\nvn -0.6986 0.3712 -0.6117\\nvn -0.2188 -0.9562 -0.1943\\nvn -0.3055 -0.9466 0.1030\\nvn -0.9995 0.0279 -0.0114\\nvn -0.9995 0.0282 -0.0105\\nvn -0.9996 0.0261 -0.0147\\nvn -0.9996 0.0277 -0.0106\\nvn -0.4658 -0.5897 -0.6598\\nvn 0.4531 -0.5877 -0.6703\\nvn 0.1908 -0.9614 -0.1984\\nvn 0.2788 -0.9530 0.1185\\nvn 0.1434 0.4679 -0.8721\\nvn 0.3529 0.4399 -0.8258\\nvn 0.5312 0.0950 -0.8419\\nvn 0.3213 0.0156 -0.9468\\nvn -0.2760 0.0108 -0.9611\\nvn -0.4989 0.0658 -0.8641\\nvn 0.1248 -0.5002 0.8569\\nvn -0.8635 0.4373 0.2514\\nvn -0.8718 0.4262 -0.2416\\nvn -0.8739 -0.1796 -0.4518\\nvn -0.7326 0.1466 -0.6647\\nvn 0.8324 -0.4903 -0.2584\\nvn 0.8344 -0.4901 -0.2523\\nvn -0.8476 -0.4701 -0.2462\\nvn -0.8440 -0.4757 -0.2479\\nvn 0.6052 -0.7322 0.3125\\nvn 0.8643 -0.4795 0.1520\\nvn 0.5530 -0.4953 0.6700\\nvn 0.1155 0.7442 -0.6579\\nvn 0.8266 0.3088 -0.4704\\nvn 0.5878 -0.6576 -0.4712\\nvn -0.3958 -0.6072 -0.6889\\nvn -0.6031 0.3338 -0.7245\\nvn 0.7851 0.1936 -0.5883\\nvn -0.0203 0.6113 -0.7911\\nvn -0.2429 0.9700 0.0129\\nvn 0.4300 0.5703 0.6999\\nvn 0.2980 -0.1511 0.9425\\nvn 0.7035 -0.4929 -0.5120\\nvn 0.3857 0.7761 0.4989\\nvn 0.2226 0.7630 -0.6069\\nvn -0.7474 0.3391 -0.5714\\nvn -0.6878 0.3291 0.6470\\nvn 0.7292 0.2197 0.6480\\nvn 0.7641 0.5310 -0.3663\\nvn 0.2858 0.2599 -0.9224\\nvn -0.7281 -0.1755 -0.6627\\nvn -0.3442 -0.5435 -0.7656\\nvn -0.3689 -0.8816 0.2946\\nvn -0.5596 -0.2779 0.7808\\nvn 0.8386 -0.3994 0.3704\\nvn 0.6581 -0.3016 -0.6898\\nvn -0.5400 0.7449 0.3919\\nvn -0.8564 -0.1987 0.4765\\nvn 0.1661 -0.8546 0.4920\\nvn 0.6997 0.2691 0.6618\\nvn -0.7883 0.3669 -0.4940\\nvn 0.3289 -0.9344 -0.1367\\nvn -0.1073 -0.8442 0.5252\\nvn -0.6020 0.2147 0.7691\\nvn 0.4761 -0.6581 -0.5833\\nvn 0.7693 -0.2737 -0.5773\\nvn 0.8517 -0.1421 0.5044\\nvn 0.1085 0.6655 -0.7385\\nvn -0.7004 0.2954 -0.6498\\nvn -0.8651 0.2746 0.4196\\nvn -0.0029 0.8005 0.5993\\nvn 0.5740 -0.7789 0.2528\\nvn -0.4292 -0.6492 0.6280\\nvn 0.5332 0.1202 0.8374\\nvn 0.7191 0.0794 -0.6904\\nvn 0.4670 -0.6973 -0.5438\\nvn 0.1549 -0.7848 0.6000\\nvn -0.5695 0.6547 0.4970\\nvn -0.4692 0.6101 -0.6384\\nvn -0.3814 -0.5102 -0.7709\\nvn -0.6169 -0.6944 0.3704\\nvn 0.5114 0.3644 0.7782\\nvn 0.7852 0.3096 -0.5363\\nvn 0.6767 -0.5525 0.4867\\nvn -0.5268 -0.7260 0.4421\\nvn -0.6547 -0.6203 -0.4320\\nvn 0.5286 -0.4128 -0.7418\\nvn 0.6747 -0.6805 -0.2859\\nvn 0.4764 -0.6956 0.5377\\nvn -0.3718 0.8199 0.4353\\nvn 0.2073 0.6479 0.7329\\nvn 0.5300 0.4359 -0.7274\\nvn -0.1193 0.7062 -0.6979\\nvn -0.7108 0.4646 -0.5281\\nvn 0.0604 0.8720 -0.4858\\nvn 0.7374 0.3123 -0.5989\\nvn 0.6484 -0.3337 -0.6842\\nvn -0.1025 -0.7629 -0.6383\\nvn -0.7316 -0.1051 -0.6736\\nvn 0.8318 0.2132 0.5125\\nvn 0.1808 0.6400 0.7468\\nvn -0.5841 0.5013 0.6384\\nvn -0.6937 -0.3197 0.6454\\nvn -0.1674 -0.8336 0.5264\\nvn 0.7217 -0.4938 0.4851\\nvn -0.5367 0.6990 -0.4725\\nvn 0.7891 0.6104 0.0690\\nvn 0.6682 -0.4992 -0.5517\\nvn 0.2786 -0.4963 -0.8222\\nvn 0.2298 -0.3671 -0.9014\\nvn -0.2490 -0.3978 -0.8830\\nvn -0.6217 -0.2911 -0.7271\\nvn -0.8355 -0.0684 -0.5452\\nvn -0.2629 -0.8497 0.4571\\nvn 0.2695 -0.2911 0.9180\\nvn -0.6341 0.5422 0.5513\\nvn -0.9732 -0.1984 0.1164\\nvn -0.0174 -0.6360 -0.7715\\nvn 0.6699 -0.6493 0.3600\\nvn -0.5229 0.1210 0.8437\\nvn -0.8786 -0.4728 -0.0671\\nvn 0.7666 -0.6409 0.0401\\nvn -0.0709 -0.5571 0.8274\\nvn -0.9561 -0.2896 0.0446\\nvn 0.1716 -0.5424 -0.8224\\nvn -0.1282 0.6261 -0.7692\\nvn 0.6751 0.7365 0.0427\\nvn -0.3087 0.8994 -0.3096\\nvn 0.7336 0.3189 -0.6001\\nvn 0.9013 0.4329 0.0130\\nvn -0.5585 0.3542 0.7501\\nvn -0.7170 0.5251 -0.4584\\nvn 0.4933 0.5319 0.6883\\nvn 0.3880 0.7745 -0.4995\\nvn 0.3343 -0.8251 -0.4555\\nvn 0.2222 -0.7376 0.6377\\nvn -0.5467 -0.4922 0.6774\\nvn -0.4183 -0.5911 -0.6896\\nvn 0.3158 0.3890 -0.8654\\nvn 0.5874 -0.7167 -0.3758\\nvn -0.6091 -0.7421 -0.2798\\nvn -0.6193 0.4802 -0.6212\\nvn -0.3319 0.3197 0.8875\\nvn 0.4345 0.8067 0.4005\\nvn -0.7946 -0.1944 0.5751\\nvn -0.6078 -0.6568 -0.4463\\nvn -0.0298 -0.9069 -0.4202\\nvn 0.5160 -0.8048 -0.2934\\nvn 0.6669 -0.0464 0.7437\\nvn -0.7236 -0.5529 0.4132\\nvn -0.7069 -0.6642 0.2430\\nvn -0.0681 -0.7642 0.6414\\nvn -0.1185 -0.5040 0.8555\\nvn 0.3280 -0.6226 0.7105\\nvn 0.2586 -0.4669 0.8457\\nvn 0.0007 -0.8007 0.5991\\nvn 0.3713 -0.7461 0.5527\\nvn -0.6510 -0.7361 0.1853\\nvn 0.3888 -0.6755 0.6265\\nvn 0.3435 -0.4991 0.7956\\nvn 0.4688 -0.7428 0.4779\\nvn 0.0290 -0.8438 0.5358\\nvn 0.4145 -0.7800 0.4689\\nvn 0.5632 -0.7160 0.4125\\nvn 0.5005 -0.6663 0.5528\\nvn 0.6184 -0.6464 0.4468\\nvn 0.1614 -0.7465 0.6455\\nvn -0.6363 -0.7206 0.2754\\nvn -0.5976 -0.6213 0.5068\\nvn 0.6551 -0.4882 0.5766\\nvn 0.4446 -0.4984 0.7442\\nvn 0.5200 -0.7041 0.4836\\nvn 0.7175 -0.6166 0.3242\\nvn 0.6142 -0.6989 0.3666\\nvn 0.7324 -0.6303 0.2575\\nvn 0.6821 -0.6236 0.3820\\nvn 0.7435 -0.5822 0.3291\\nvn 0.7046 -0.5516 0.4463\\nvn 0.7326 -0.5295 0.4277\\nvn 0.4814 -0.1531 0.8630\\nvn 0.7000 -0.4989 0.5110\\nvn 0.6882 -0.5653 0.4548\\nvn 0.3229 0.0582 0.9446\\nvn 0.6451 -0.6028 0.4696\\nvn 0.3366 0.0419 0.9407\\nvn 0.5728 -0.6376 0.5151\\nvn 0.3060 -0.0062 0.9520\\nvn -0.5007 -0.3935 0.7710\\nvn 0.2108 -0.0756 0.9746\\nvn 0.3210 -0.6684 0.6709\\nvn -0.5337 -0.6207 0.5744\\nvn -0.9470 -0.1716 0.2717\\nvn -0.9241 -0.3618 -0.1232\\nvn -0.8572 -0.4915 -0.1539\\nvn -0.9652 0.0635 -0.2537\\nvn -0.9241 -0.2026 -0.3240\\nvn -0.9419 0.2946 0.1616\\nvn -0.8979 -0.4384 -0.0392\\nvn -0.9493 -0.2937 0.1124\\nvn -0.8223 -0.2234 -0.5234\\nvn -0.4075 0.3348 -0.8496\\nvn 0.2015 0.6365 -0.7445\\nvn 0.2244 0.2472 -0.9426\\nvn -0.1894 0.6677 -0.7200\\nvn -0.2393 0.6318 -0.7372\\nvn -0.4075 0.6871 -0.6015\\nvn -0.9616 -0.2496 0.1140\\nvn -0.8534 0.1595 0.4963\\nvn -0.3943 0.6479 -0.6517\\nvn -0.2486 0.9437 -0.2184\\nvn -0.0073 0.4602 -0.8878\\nvn -0.5227 0.8027 -0.2872\\nvn -0.4199 0.5747 -0.7024\\nvn -0.8828 0.4654 -0.0633\\nvn -0.6548 -0.0304 -0.7552\\nvn -0.2867 0.2255 -0.9311\\nvn -0.4217 0.0382 -0.9059\\nvn -0.3672 0.2343 -0.9001\\nvn -0.0600 0.2757 -0.9594\\nvn -0.4398 0.3139 -0.8415\\nvn 0.0348 -0.3958 -0.9177\\nvn -0.2879 0.0350 -0.9570\\nvn 0.2938 0.4638 -0.8358\\nvn 0.2715 0.2058 -0.9402\\nvn 0.0398 0.6328 -0.7733\\nvn -0.4663 0.4002 -0.7889\\nvn -0.6858 -0.2519 -0.6828\\nvn -0.7107 -0.3721 -0.5970\\nvn -0.8862 -0.0442 -0.4612\\nvn -0.9015 0.2717 -0.3369\\nvn -0.8684 0.3154 -0.3827\\nvn -0.8292 0.4944 -0.2607\\nvn -0.6237 -0.2111 -0.7526\\nvn -0.0728 0.1474 -0.9864\\nvn -0.8403 -0.2858 -0.4607\\nvn -0.7176 -0.6361 -0.2836\\nvn 0.1402 0.8227 -0.5510\\nvn 0.0917 0.5931 -0.7999\\nvn -0.0280 0.9213 -0.3878\\nvn -0.1321 0.9808 -0.1434\\nvn 0.2598 0.3617 -0.8954\\nvn 0.0845 0.9660 -0.2445\\nvn -0.7097 -0.1537 -0.6876\\nvn -0.5304 0.7456 -0.4034\\nvn -0.8629 0.3638 -0.3507\\nvn -0.8231 0.4834 0.2981\\nvn -0.4818 0.1041 -0.8701\\nvn 0.7436 0.6251 -0.2372\\nvn -0.2096 0.9774 0.0277\\nvn 0.1456 0.6945 -0.7046\\nvn 0.7963 0.4754 -0.3740\\nvn 0.3968 0.6008 -0.6939\\nvn 0.8032 0.4464 -0.3944\\nvn 0.5344 0.6050 -0.5903\\nvn 0.8279 0.4039 -0.3892\\nvn 0.7165 0.6940 -0.0711\\nvn 0.8004 0.5535 -0.2303\\nvn 0.3980 0.8483 -0.3493\\nvn 0.3691 0.9264 -0.0739\\nvn 0.5113 0.7107 -0.4832\\nvn 0.8225 0.4659 -0.3263\\nvn 0.8570 0.3704 0.3582\\nvn 0.9978 0.0552 0.0371\\nvn 0.9921 -0.0661 -0.1070\\nvn 0.9074 -0.2315 0.3507\\nvn 0.8879 -0.4359 0.1470\\nvn 0.8016 -0.0598 0.5949\\nvn 0.9817 -0.1843 -0.0482\\nvn 0.9528 -0.2942 0.0744\\nvn 0.8135 -0.5305 0.2383\\nvn 0.7856 -0.5312 0.3172\\nvn 0.8362 -0.4490 0.3149\\nvn 0.8506 -0.4076 0.3322\\nvn 0.8654 -0.4409 0.2382\\nvn 0.8883 -0.3224 0.3272\\nvn 0.7838 -0.1525 0.6020\\nvn 0.9366 -0.2544 0.2408\\nvn 0.5268 -0.0123 0.8499\\nvn 0.8374 -0.4305 0.3368\\nvn 0.6152 0.2139 0.7588\\nvn 0.8826 -0.3555 0.3076\\nvn 0.5602 0.2036 0.8029\\nvn 0.4542 0.1451 0.8790\\nvn 0.7769 -0.4548 0.4355\\nvn 0.7928 -0.4481 0.4132\\nvn 0.8290 -0.4831 0.2818\\nvn 0.8900 -0.3614 0.2780\\nvn 0.7746 -0.1272 0.6195\\nvn 0.8545 -0.4863 0.1825\\nvn 0.4741 -0.0482 0.8792\\nvn 0.3949 -0.1361 0.9086\\nvn 0.5949 -0.0544 0.8020\\nvn 0.4793 -0.0319 0.8771\\nvn 0.2019 -0.0866 0.9756\\nvn 0.0580 0.1919 0.9797\\nvn 0.4434 0.4632 0.7674\\nvn -0.0504 0.7821 0.6211\\nvn 0.6259 0.2041 0.7527\\nvn -0.2943 0.0126 0.9556\\nvn -0.5656 0.4826 0.6687\\nvn -0.3835 0.9205 0.0747\\nvn -0.4198 0.5542 -0.7188\\nvn -0.3823 0.9157 0.1242\\nvn 0.0347 0.9349 0.3533\\nvn 0.4691 0.7532 0.4611\\nvn 0.4352 0.8684 0.2375\\nvn 0.1217 0.0697 -0.9901\\nvn -0.3436 0.8018 0.4890\\nvn -0.6969 -0.0078 -0.7171\\nvn 0.1369 -0.0463 0.9895\\nvn 0.9256 -0.1693 0.3385\\nvn 0.7089 0.3869 0.5898\\nvn -0.2305 0.8520 0.4701\\nvn -0.7095 0.3586 0.6066\\nvn 0.7295 -0.2469 -0.6378\\nvn 0.2779 -0.8924 0.3556\\nvn 0.3408 -0.6310 -0.6969\\nvn -0.9166 -0.2539 0.3087\\nvn -0.6437 -0.2373 -0.7275\\nvn -0.4423 0.4490 -0.7764\\nvn 0.5540 0.6686 0.4961\\nvn 0.1910 -0.7836 -0.5912\\nvn -0.7170 -0.3123 -0.6232\\nvn -0.6608 -0.7182 0.2180\\nvn -0.6352 -0.6223 -0.4576\\nvn 0.7567 0.1422 0.6381\\nvn -0.6352 0.2493 0.7311\\nvn -0.1896 -0.8213 -0.5381\\nvn -0.8133 -0.0042 -0.5818\\nvn -0.3252 -0.5793 0.7474\\nvn -0.7101 -0.2125 -0.6713\\nvn 0.0072 -0.6528 -0.7575\\nvn 0.0623 -0.7474 0.6614\\nvn -0.5951 -0.1822 0.7827\\nvn -0.3829 0.5939 0.7076\\nvn 0.4703 0.7985 0.3758\\nvn 0.2529 0.6941 -0.6740\\nvn -0.5338 0.5439 -0.6475\\nvn 0.7002 0.7138 0.0158\\nvn -0.5895 0.7952 -0.1416\\nvn 0.6944 -0.3915 -0.6037\\nvn 0.7719 -0.4897 0.4054\\nvn 0.8607 0.3684 0.3513\\nvn 0.7804 0.3580 -0.5126\\nvn -0.9760 0.1602 0.1472\\nvn 0.9577 0.1444 -0.2489\\nvn 0.0651 0.7321 0.6780\\nvn -0.7432 -0.0625 -0.6662\\nvn -0.9369 0.1014 0.3346\\nvn 0.8742 0.0397 -0.4839\\nvn 0.8286 0.0725 0.5551\\nvn 0.2518 0.9369 0.2426\\nvn -0.4140 0.6553 0.6318\\nvn -0.3124 0.8617 0.3999\\nvn -0.2016 -0.5676 0.7982\\nvn 0.6376 -0.5268 0.5621\\nvn 0.0071 0.5360 -0.8442\\nvn 0.7925 0.2144 -0.5710\\nvn 0.5346 0.5574 -0.6353\\nvn -0.0941 0.6473 -0.7564\\nvn -0.6087 0.0301 -0.7928\\nvn 0.7538 0.1217 -0.6457\\nvn 0.2236 0.7719 -0.5952\\nvn 0.0546 0.5759 0.8157\\nvn -0.7730 0.5486 -0.3185\\nusemtl None\\ns 1\\nf 1/1/1 2/2/2 3/3/3 4/4/4 5/5/5 6/6/6\\nf 7/7/7 4/4/4 3/3/3 8/8/8\\nf 9/9/9 10/10/10 11/11/11 6/6/6 5/5/5\\nf 12/12/12 9/9/9 5/5/5 4/4/4\\nf 12/12/12 4/4/4 7/7/7 13/13/13 14/14/14\\nf 3/3/3 15/15/15 16/16/16 8/8/8\\nf 2/2/2 17/17/17 15/15/15 3/3/3\\nf 18/18/18 19/19/19 9/9/9 12/12/12 20/20/20\\nf 21/21/21 22/22/22 23/23/23 24/24/24\\nf 25/25/25 26/26/26 27/27/27 28/28/28 29/29/29\\nf 30/30/30 21/21/21 24/24/24 16/16/16 15/15/15\\nf 30/30/30 15/15/15 17/17/17 31/31/31\\nf 20/20/20 32/32/32 30/30/30 31/31/31 18/18/18\\nf 33/33/33 34/34/34 35/35/35 36/36/36\\nf 37/37/37 38/38/37 39/39/38 40/40/37 41/41/39\\nf 42/42/40 43/43/41 44/44/42 29/29/29 28/28/28\\nf 44/44/42 43/43/41 45/45/43 46/46/44\\nf 47/47/45 48/48/46 45/45/43 43/43/41\\nf 49/49/47 50/50/48 51/51/49 52/52/50 53/53/51 54/54/52\\nf 39/39/53 38/38/54 55/55/55 56/56/56 57/57/57\\nf 58/58/58 41/41/59 40/40/60 59/59/61 60/60/62\\nf 61/61/63 52/52/50 51/51/49 62/62/64 63/63/65 64/64/66\\nf 27/27/27 26/26/26 65/65/67 66/66/68\\nf 26/26/69 25/25/70 67/67/71 68/68/72 69/69/72\\nf 70/70/73 71/71/74 72/72/75 73/73/76 66/66/68 65/65/67\\nf 74/74/77 75/75/78 76/76/79 77/77/80 78/78/81 79/79/82 63/63/65 62/62/64\\nf 42/42/40 80/80/83 47/47/45 43/43/41\\nf 67/67/84 25/25/25 29/29/29 44/44/42 46/46/44\\nf 68/68/85 67/67/84 46/46/44 61/61/63\\nf 77/77/80 71/71/74 70/70/73 78/78/81\\nf 69/69/86 68/68/85 61/61/63 64/64/66 81/81/87\\nf 60/60/62 59/59/61 48/48/46 47/47/45\\nf 59/59/61 40/40/60 39/39/53 57/57/57\\nf 48/48/46 59/59/61 57/57/57 49/49/47 54/54/52\\nf 70/70/73 69/69/86 81/81/87 79/79/82 78/78/81\\nf 71/71/74 38/38/54 37/37/88 72/72/89\\nf 70/70/73 65/65/90 26/26/91 69/69/86\\nf 82/82/92 73/73/76 72/72/75 37/37/93\\nf 83/83/94 80/80/83 42/42/40 28/28/28 27/27/27 66/66/68 73/73/76 82/82/92\\nf 38/38/54 71/71/74 77/77/80 76/76/79 55/55/55\\nf 79/79/82 81/81/87 64/64/66 63/63/65\\nf 80/80/83 83/83/94 58/58/58 60/60/62 47/47/45\\nf 74/74/77 62/62/64 51/51/49 50/50/48\\nf 45/45/43 53/53/51 52/52/50 61/61/63 46/46/44\\nf 55/55/55 76/76/79 75/75/78 56/56/56\\nf 57/57/57 56/56/56 75/75/78 74/74/77 50/50/48 49/49/47\\nf 84/84/95 85/85/96 86/86/97 87/87/98 88/88/99\\nf 89/89/100 90/90/101 91/91/102 92/92/103\\nf 93/93/104 94/94/105 89/89/100 92/92/103\\nf 95/95/106 96/96/107 97/97/108 98/98/109\\nf 99/99/110 100/100/111 101/101/112 96/96/107 95/95/106\\nf 102/102/113 103/103/114 104/104/115 105/105/116\\nf 106/106/117 107/107/118 100/108/111 99/99/110\\nf 108/109/119 109/110/120 110/111/121 111/112/122\\nf 112/113/123 113/114/124 114/115/125 115/116/126\\nf 116/117/127 117/118/128 118/119/129 114/115/125 113/114/124\\nf 22/22/22 14/14/14 13/13/13 7/7/7 8/8/8 23/23/23\\nf 119/120/130 120/121/131 121/122/132 122/123/133\\nf 123/124/134 124/125/135 87/87/98 86/86/97\\nf 125/126/136 126/127/137 119/120/130 122/123/133\\nf 127/128/138 126/127/137 125/126/136 128/129/139\\nf 129/130/140 130/131/141 131/132/142 132/133/143\\nf 133/134/144 134/135/145 130/136/141 129/137/140\\nf 135/138/146 136/139/147 137/140/148 138/141/149\\nf 132/133/143 131/132/142 139/142/150 140/143/151\\nf 131/132/142 130/136/141 134/135/145 139/142/150\\nf 141/144/152 142/145/153 143/146/154 144/147/155\\nf 145/148/156 146/149/157 147/150/158 148/151/159 149/152/160 150/153/161\\nf 151/154/162 152/155/163 153/156/164 154/157/165 155/158/166 156/159/167\\nf 157/160/168 158/161/169 159/162/170 160/163/171 161/164/172 162/165/173 163/166/174 164/167/175\\nf 165/168/176 162/169/173 161/164/172 160/163/171 159/162/170 166/170/177\\nf 167/171/178 157/160/168 164/167/175 168/172/179\\nf 169/173/180 170/174/181 171/175/182 172/176/183\\nf 173/177/184 174/178/185 175/179/186 176/180/187\\nf 173/177/184 176/180/187 177/181/188 178/182/189\\nf 179/183/190 180/184/191 169/173/180 172/176/183\\nf 170/174/181 169/173/180 180/184/191 181/185/192\\nf 136/139/147 182/186/193 183/187/194 137/140/148\\nf 182/186/193 184/188/195 185/189/196 183/187/194\\nf 150/153/161 154/157/165 153/156/164 145/148/156\\nf 155/158/166 154/157/165 150/153/161 149/152/160\\nf 8/8/8 16/16/16 24/24/24 23/23/23\\nf 153/156/164 152/155/163 146/149/157 145/190/156\\nf 186/191/197 187/192/198 188/193/199 189/194/200\\nf 190/195/201 191/196/202 192/197/203 193/198/204\\nf 194/199/205 195/200/206 190/195/201 193/198/204\\nf 196/201/207 197/202/208 198/203/209 199/204/210 200/205/211\\nf 201/206/212 202/207/213 203/208/214 204/209/215\\nf 203/208/214 205/210/216 206/211/217 204/209/215\\nf 207/212/218 208/213/219 205/214/216 203/215/214\\nf 202/216/213 209/217/220 207/212/218 203/215/214\\nf 205/210/216 210/218/221 211/219/222 206/211/217\\nf 208/213/219 212/220/223 210/221/221 205/214/216\\nf 207/212/218 213/222/224 214/223/225 208/213/219\\nf 214/223/225 215/224/226 212/220/223 208/213/219\\nf 214/223/225 216/225/227 217/226/228 215/224/226\\nf 213/222/224 218/227/229 216/225/227 214/223/225\\nf 209/217/220 219/228/230 213/222/224 207/212/218\\nf 219/228/230 220/229/231 218/227/229 213/222/224\\nf 221/230/232 222/231/233 223/232/234 224/233/235\\nf 224/234/235 223/235/234 225/236/236 226/237/237\\nf 223/235/234 210/221/221 212/220/223 225/236/236\\nf 222/231/233 211/219/222 210/218/221 223/232/234\\nf 226/237/237 225/236/236 227/238/238 228/239/239\\nf 227/238/238 229/240/240 230/241/241 228/239/239\\nf 215/224/226 217/226/228 229/240/240 227/238/238\\nf 225/236/236 212/220/223 215/224/226 227/238/238\\nf 231/242/242 232/243/243 233/244/244 234/245/245\\nf 233/244/244 235/246/246 236/247/247 234/245/245\\nf 233/244/244 229/240/240 217/226/228 235/246/246\\nf 232/243/243 230/241/241 229/240/240 233/244/244\\nf 235/246/246 237/248/248 238/249/249 236/247/247\\nf 216/225/227 237/248/248 235/246/246 217/226/228\\nf 239/250/250 240/251/251 241/252/252 242/253/253\\nf 241/252/252 218/227/229 220/229/231 242/253/253\\nf 240/251/251 238/249/249 237/248/248 241/252/252\\nf 241/252/252 237/248/248 216/225/227 218/227/229\\nf 201/206/212 243/254/254 244/255/255 202/207/213\\nf 202/216/213 244/256/255 245/257/256 209/217/220\\nf 244/256/255 246/258/257 247/259/258 245/257/256\\nf 243/254/254 248/260/259 246/261/257 244/255/255\\nf 209/217/220 245/257/256 249/262/260 219/228/230\\nf 249/262/260 250/263/261 220/229/231 219/228/230\\nf 245/257/256 247/259/258 251/264/262 249/262/260\\nf 252/265/263 250/263/261 249/262/260 251/264/262\\nf 251/266/262 253/267/264 254/268/265 252/269/263\\nf 247/270/258 255/271/266 253/267/264 251/266/262\\nf 246/272/257 256/273/267 255/271/266 247/270/258\\nf 248/260/259 257/274/268 256/275/267 246/261/257\\nf 239/250/250 242/253/253 258/276/269 259/277/270\\nf 258/276/269 260/278/271 261/279/272 259/277/270\\nf 220/229/231 250/263/261 258/276/269 242/253/253\\nf 260/280/271 262/281/273 263/282/274 261/283/272\\nf 252/269/263 254/268/265 262/281/273 260/280/271\\nf 258/276/269 250/263/261 252/265/263 260/278/271\\nf 264/284/275 265/285/276 266/286/277 267/287/278\\nf 266/286/277 268/288/279 269/289/280 267/287/278\\nf 266/286/277 262/281/273 254/268/265 268/288/279\\nf 265/285/276 263/282/274 262/281/273 266/286/277\\nf 268/288/279 270/290/281 271/291/282 269/289/280\\nf 253/267/264 270/290/281 268/288/279 254/268/265\\nf 272/292/283 273/293/284 274/294/285 275/295/286\\nf 274/294/285 256/275/267 257/274/268 275/295/286\\nf 276/296/287 255/271/266 256/273/267 274/297/285\\nf 273/298/284 277/299/288 276/296/287 274/297/285\\nf 276/296/287 270/290/281 253/267/264 255/271/266\\nf 277/299/288 271/291/282 270/290/281 276/296/287\\nf 272/292/283 278/300/289 279/301/290 273/293/284\\nf 273/298/284 279/301/290 280/302/291 277/299/288\\nf 279/301/290 281/303/292 282/304/293 280/302/291\\nf 278/300/289 283/305/294 281/303/292 279/301/290\\nf 277/299/288 280/302/291 284/306/295 271/291/282\\nf 284/306/295 285/307/296 269/289/280 271/291/282\\nf 286/308/297 287/309/298 285/307/296 284/306/295\\nf 280/302/291 282/304/293 286/308/297 284/306/295\\nf 282/304/293 288/310/299 289/311/300 286/308/297\\nf 281/303/292 290/312/301 288/310/299 282/304/293\\nf 283/305/294 291/313/302 290/312/301 281/303/292\\nf 264/284/275 267/287/278 292/314/303 293/315/304\\nf 292/314/303 294/316/305 295/317/306 293/315/304\\nf 292/314/303 285/307/296 287/309/298 294/316/305\\nf 269/289/280 285/307/296 292/314/303 267/287/278\\nf 294/316/305 296/318/307 297/319/308 295/317/306\\nf 298/320/309 296/318/307 294/316/305 287/309/298\\nf 286/308/297 289/311/300 298/320/309 287/309/298\\nf 299/321/310 300/322/311 301/323/312 302/324/313\\nf 301/323/312 303/325/314 304/326/315 302/324/313\\nf 300/322/311 297/319/308 296/318/307 301/323/312\\nf 303/325/314 305/327/316 306/328/317 304/326/315\\nf 298/320/309 289/311/300 305/327/316 303/325/314\\nf 301/323/312 296/318/307 298/320/309 303/325/314\\nf 307/329/318 308/330/319 309/331/320 310/332/321\\nf 309/331/320 290/312/301 291/313/302 310/332/321\\nf 311/333/322 288/310/299 290/312/301 309/331/320\\nf 308/330/319 312/334/323 311/333/322 309/331/320\\nf 311/333/322 305/327/316 289/311/300 288/310/299\\nf 312/334/323 306/328/317 305/327/316 311/333/322\\nf 307/329/318 313/335/324 314/336/325 308/330/319\\nf 308/330/319 314/336/325 315/337/326 312/334/323\\nf 314/336/325 316/338/327 317/339/328 315/337/326\\nf 313/335/324 318/340/329 316/338/327 314/336/325\\nf 312/334/323 315/337/326 319/341/330 306/328/317\\nf 319/341/330 320/342/331 304/326/315 306/328/317\\nf 315/337/326 317/339/328 321/343/332 319/341/330\\nf 322/344/333 320/342/331 319/341/330 321/343/332\\nf 321/343/332 323/345/334 324/346/335 322/344/333\\nf 317/339/328 325/347/336 323/345/334 321/343/332\\nf 316/338/327 326/348/337 325/347/336 317/339/328\\nf 318/340/329 327/349/338 326/348/337 316/338/327\\nf 299/321/310 302/324/313 328/350/339 329/351/340\\nf 328/350/339 330/352/341 331/353/342 329/351/340\\nf 304/326/315 320/342/331 328/350/339 302/324/313\\nf 330/352/341 332/354/343 333/355/344 331/353/342\\nf 322/344/333 324/346/335 332/354/343 330/352/341\\nf 328/350/339 320/342/331 322/344/333 330/352/341\\nf 231/242/242 334/356/345 335/357/346 232/243/243\\nf 335/357/346 336/358/347 230/241/241 232/243/243\\nf 335/357/346 332/359/343 324/360/335 336/358/347\\nf 334/356/345 333/361/344 332/359/343 335/357/346\\nf 336/358/347 337/362/348 228/239/239 230/241/241\\nf 323/363/334 337/362/348 336/358/347 324/360/335\\nf 221/230/232 224/233/235 338/364/349 339/365/350\\nf 338/364/349 326/366/337 327/367/338 339/365/350\\nf 340/368/351 325/369/336 326/370/337 338/371/349\\nf 224/234/235 226/237/237 340/368/351 338/371/349\\nf 340/368/351 337/362/348 323/363/334 325/369/336\\nf 226/237/237 228/239/239 337/362/348 340/368/351\\nf 221/230/232 339/365/350 341/372/352 222/231/233\\nf 341/372/352 342/373/353 211/219/222 222/231/233\\nf 341/372/352 343/374/354 344/375/355 342/373/353\\nf 327/367/338 343/374/354 341/372/352 339/365/350\\nf 342/373/353 345/376/356 206/211/217 211/219/222\\nf 346/377/357 345/376/356 342/373/353 344/375/355\\nf 344/375/355 347/378/358 348/379/359 346/377/357\\nf 343/374/354 349/380/360 347/378/358 344/375/355\\nf 318/340/329 349/380/360 343/374/354 327/349/338\\nf 201/206/212 204/209/215 350/381/361 243/254/254\\nf 350/381/361 351/382/362 248/260/259 243/254/254\\nf 206/211/217 345/376/356 350/381/361 204/209/215\\nf 351/382/362 352/383/363 257/274/268 248/260/259\\nf 346/377/357 348/379/359 352/383/363 351/382/362\\nf 350/381/361 345/376/356 346/377/357 351/382/362\\nf 272/292/283 275/295/286 353/384/364 278/300/289\\nf 353/384/364 354/385/365 283/305/294 278/300/289\\nf 353/384/364 352/383/363 348/379/359 354/385/365\\nf 257/274/268 352/383/363 353/384/364 275/295/286\\nf 354/385/365 355/386/366 291/313/302 283/305/294\\nf 347/378/358 355/386/366 354/385/365 348/379/359\\nf 307/329/318 310/332/321 356/387/367 313/335/324\\nf 356/387/367 349/380/360 318/340/329 313/335/324\\nf 291/313/302 355/386/366 356/387/367 310/332/321\\nf 356/387/367 355/386/366 347/378/358 349/380/360\\nf 357/388/368 200/205/211 199/204/210 358/389/369\\nf 359/390/370 357/388/368 358/389/369 360/391/371\\nf 200/205/211 357/388/368 359/390/370 196/201/207\\nf 104/104/115 106/106/117 99/99/110 105/105/116\\nf 361/392/372 362/393/373 363/394/374 364/395/375 365/396/376\\nf 366/397/377 362/393/373 361/392/372 367/398/378 368/399/379\\nf 368/399/379 367/398/378 369/400/380 370/401/381\\nf 369/400/380 365/396/376 371/402/382 370/401/381\\nf 372/403/383 115/116/126 114/115/125 118/119/129\\nf 373/404/384 110/111/121 109/110/120 374/405/385\\nf 128/129/139 125/126/136 122/123/133 121/406/132 375/407/386\\nf 120/121/131 119/120/130 126/127/137 127/128/138 376/408/387\\nf 85/85/96 377/409/388 123/124/134 86/86/97\\nf 87/87/98 124/125/135 378/410/389 88/88/99\\nf 90/90/101 89/89/100 94/94/105 379/411/390 380/412/391\\nf 94/94/105 93/93/104 381/413/392 379/411/390\\nf 382/414/393 383/415/394 384/416/395 385/417/396\\nf 386/418/397 387/419/398 388/420/399 389/421/400\\nf 10/10/10 390/422/401 36/36/36 35/35/35 391/423/402 11/11/11\\nf 382/414/393 385/417/396 386/418/397 389/421/400\\nf 392/424/403 393/425/404 384/416/395 383/415/394\\nf 388/420/399 387/419/398 394/426/405 395/427/406\\nf 394/426/405 393/425/404 392/424/403 395/427/406\\nf 396/428/407 91/91/102 90/90/101 380/412/391\\nf 379/411/390 381/413/392 396/428/407 380/412/391\\nf 385/417/396 384/416/395 393/425/404 394/426/405 387/419/398 386/418/397\\nf 93/93/104 92/92/103 91/91/102 396/428/407 381/413/392\\nf 140/143/151 139/142/150 134/135/145 133/134/144\\nf 135/138/146 138/141/149 185/429/196 184/430/195\\nf 12/12/12 14/14/14 22/22/22 20/20/20\\nf 33/33/33 36/36/36 390/422/401 397/431/408 19/19/19 18/18/18\\nf 2/2/2 1/1/1 391/423/402 35/35/35 34/34/34\\nf 376/408/387 127/128/138 128/129/139 375/407/386\\nf 121/406/132 120/432/131 376/408/387 375/407/386\\nf 84/84/95 88/88/99 378/410/389 398/433/409\\nf 84/84/95 398/433/409 377/434/388 85/435/96\\nf 156/159/167 155/158/166 149/436/160 148/437/159\\nf 156/159/167 148/437/159 147/150/158 151/154/162\\nf 146/149/157 152/155/163 151/154/162 147/150/158\\nf 399/438/410 189/194/200 188/193/199 400/439/411\\nf 141/144/152 144/147/155 399/440/410 400/441/411\\nf 186/191/197 401/442/412 402/443/413 187/192/198\\nf 401/442/412 143/146/154 142/145/153 402/443/413\\nf 124/125/135 123/124/134 377/434/388 398/433/409 378/410/389\\nf 395/427/406 392/424/403 383/415/394 382/414/393 389/421/400 388/420/399\\nf 390/422/401 10/10/10 9/9/9 19/19/19 397/431/408\\nf 6/6/6 11/11/11 391/423/402 1/1/1\\nf 37/37/93 41/41/59 58/58/58 83/83/94 82/82/92\\nf 45/45/43 48/48/46 54/54/52 53/53/51\\nf 135/138/146 184/188/195 182/186/193 136/139/147\\nf 132/133/143 140/143/151 133/134/144 129/130/140\\nf 138/141/149 137/140/148 183/187/194 185/429/196\\nf 400/439/411 188/193/199 187/192/198 402/443/413 142/145/153 141/144/152\\nf 143/444/154 401/445/412 186/191/197 189/194/200 399/440/410 144/147/155\\nf 22/22/22 21/21/21 30/30/30 32/32/32 20/20/20\\nf 33/33/33 18/18/18 31/31/31 17/17/17 2/2/2 34/34/34\\nf 166/170/177 159/162/170 158/161/169 403/446/414\\nf 403/446/414 158/161/169 157/160/168 167/171/178 404/447/415\\nf 168/448/179 164/167/175 163/166/174 162/165/173 165/449/176\\nf 165/168/176 166/170/177 403/446/414 404/447/415 167/171/178 168/172/179\\nf 405/450/416 179/183/190 172/176/183 171/175/182\\nf 171/175/182 170/174/181 181/185/192 405/450/416\\nf 180/184/191 179/183/190 405/450/416 181/185/192\\nf 406/451/417 194/199/205 193/198/204 192/197/203\\nf 407/452/418 406/451/417 192/197/203 191/196/202\\nf 190/195/201 195/200/206 407/452/418 191/196/202\\nf 406/451/417 407/453/418 195/454/206 194/199/205\\nf 196/201/207 359/390/370 360/391/371 197/202/208\\nf 358/389/369 199/204/210 198/203/209 197/202/208 360/391/371\\nf 369/400/380 367/398/378 361/392/372 365/396/376\\nf 371/402/382 366/397/377 368/399/379 370/401/381\\nf 365/396/376 364/395/375 408/455/419 371/402/382\\nf 366/397/377 409/456/420 363/394/374 362/393/373\\nf 410/457/421 408/455/419 364/395/375 363/394/374 409/456/420\\nf 371/402/382 408/455/419 410/457/421 409/456/420 366/397/377\\nf 107/458/118 103/103/114 102/102/113 101/101/112 100/100/111\\nf 105/105/116 99/99/110 95/95/106 98/98/109\\nf 98/98/109 97/97/108 102/102/113 105/105/116\\nf 101/101/112 102/102/113 97/97/108 96/96/107\\nf 103/103/114 107/107/118 106/106/117 104/104/115\\nf 109/110/120 108/109/119 411/459/422 412/460/423 374/405/385\\nf 413/461/424 111/112/122 110/111/121 373/404/384\\nf 412/460/423 411/459/422 413/461/424 373/404/384 374/405/385\\nf 108/109/119 111/112/122 413/461/424 411/459/422\\nf 372/403/383 118/119/129 117/118/128 414/462/425\\nf 117/118/128 116/117/127 113/114/124 112/113/123 414/462/425\\nf 372/403/383 414/462/425 112/113/123 115/116/126\\nf 415/463/426 416/464/427 175/179/186 174/178/185\\nf 178/182/189 177/181/188 416/464/427 415/463/426\\nf 416/464/427 177/181/188 176/180/187 175/179/186\\nf 174/178/185 173/177/184 178/182/189 415/463/426\\nf 299/321/310 329/351/340 331/353/342 333/361/344 334/356/345 231/242/242 234/245/245 236/247/247 238/249/249 240/251/251 239/250/250 259/277/270 261/283/272 263/282/274 265/285/276 264/284/275 293/315/304 295/317/306 297/319/308 300/322/311\\no Beta_Joints.003_Beta_Joints.005_Beta_Joints.003_Beta_Joints.005\\nv 0.017116 -0.015615 0.909501\\nv -0.004493 0.052566 0.924418\\nv 0.041137 0.024663 0.903833\\nv 0.080227 -0.000000 0.892944\\nv 0.151462 0.070271 0.962825\\nv 0.159549 -0.003991 0.945010\\nv 0.109730 0.068176 0.980641\\nv 0.021633 0.081168 0.984964\\nv 0.078557 0.089367 1.013357\\nv -0.039395 -0.039077 0.946434\\nv -0.057583 0.073020 0.998059\\nv 0.088665 -0.063698 0.980823\\nv 0.020943 -0.068439 1.020097\\nv 0.042707 -0.050824 0.955465\\nv 0.118837 0.042193 1.054459\\nv 0.009183 0.075446 1.033777\\nv 0.013405 -0.022265 1.079782\\nv -0.012419 -0.042216 1.008041\\nv 0.139241 -0.003791 1.021770\\nv 0.074544 -0.038204 1.080667\\nv 0.035687 0.047946 1.087194\\nv 0.093861 0.027176 1.081918\\nv -0.026129 0.023678 1.029716\\nv 0.044246 0.049889 1.153491\\nv 0.046880 0.037945 0.939237\\nv 0.055272 -0.008120 0.930448\\nv 0.117535 -0.022693 0.906659\\nv 0.076681 0.005177 0.919818\\nv 0.140785 -0.025992 0.874290\\nv 0.117077 0.005746 0.906296\\nv 0.156130 0.017075 0.940650\\nv 0.060783 -0.010464 0.917759\\nv 0.139494 -0.047890 0.945713\\nv 0.084089 -0.055050 0.951879\\nv 0.162477 -0.012882 0.963609\\nv 0.080994 0.034334 0.929819\\nv 0.087821 0.064321 0.952706\\nv 0.057404 -0.048546 0.971688\\nv 0.054813 0.009618 1.007452\\nv 0.102120 -0.058674 0.984577\\nv 0.140635 -0.070556 1.014261\\nv 0.112768 -0.034296 1.041052\\nv 0.130725 -0.027502 1.008204\\nv 0.131257 0.043363 0.997500\\nv 0.129142 0.080888 0.996818\\nv 0.163287 0.050939 1.024084\\nv 0.109397 0.033258 1.042965\\nv 0.168087 -0.007251 1.043636\\nv -0.039690 0.029186 0.938147\\nv -0.031843 0.007633 0.881647\\nv -0.008399 -0.009287 0.917632\\nv -0.006221 0.021820 0.921050\\nv 0.022773 -0.037728 0.940534\\nv -0.013876 -0.049202 0.979277\\nv 0.052041 0.053560 0.992726\\nv 0.054059 -0.027918 0.966762\\nv 0.046908 0.008538 0.919596\\nv -0.004677 -0.071303 0.979510\\nv 0.027807 -0.031763 1.026366\\nv -0.007303 0.094061 1.001983\\nv 0.015177 0.068543 0.969082\\nv -0.029241 -0.008662 1.019326\\nv -0.055206 -0.006289 0.969972\\nv -0.029659 0.048466 1.008429\\nv -0.045744 -0.038552 1.041363\\nv -0.045573 0.070145 1.039677\\nv -0.006944 0.015904 1.060215\\nv 0.030088 -0.006624 0.928280\\nv 0.030793 -0.044523 1.155391\\nv 0.050429 0.057633 1.157928\\nv 0.105408 -0.000935 1.152960\\nv 0.170108 0.034138 1.397073\\nv 0.163531 0.013571 1.392427\\nv 0.155443 0.006058 1.436858\\nv 0.207593 0.062756 1.386087\\nv 0.195376 -0.005739 1.410912\\nv 0.153292 0.058889 1.447979\\nv 0.216566 0.063341 1.456660\\nv 0.200498 0.093647 1.430505\\nv 0.177729 0.025892 1.475847\\nv -0.054186 0.061451 1.384414\\nv -0.051738 0.017681 1.387164\\nv -0.063751 0.100911 1.381242\\nv -0.075472 0.085147 1.436507\\nv -0.079517 0.030882 1.361915\\nv -0.111583 0.090630 1.381528\\nv -0.050467 0.038803 1.431251\\nv -0.088561 0.011299 1.444324\\nv -0.117374 0.032911 1.384792\\nv -0.125087 0.063232 1.418683\\nv 0.060915 0.052947 1.500183\\nv 0.089628 -0.014471 1.521678\\nv 0.058177 -0.012313 1.487291\\nv 0.023919 0.016037 1.482998\\nv -0.000027 -0.011794 1.507102\\nv -0.006600 0.036674 1.526068\\nv 0.035650 -0.040928 1.530149\\nv 0.044338 -0.011266 1.589390\\nv 0.002838 0.002963 1.581816\\nv 0.029487 0.061031 1.569436\\nv 0.077660 0.013515 1.580703\\nv 0.088016 0.044944 1.548488\\nv -0.090931 0.089006 1.165345\\nv -0.090736 0.060010 1.127934\\nv -0.103941 0.119578 1.115365\\nv -0.127492 0.088318 1.178635\\nv -0.150718 0.058265 1.166979\\nv -0.139713 0.045462 1.116852\\nv -0.171216 0.082583 1.126838\\nv -0.141600 0.074144 0.816413\\nv -0.144790 0.113893 0.853597\\nv -0.114353 0.071874 0.840426\\nv -0.149606 0.073628 0.885339\\nv -0.174367 0.099822 0.831076\\nv -0.138705 0.047615 0.859066\\nv -0.176655 0.069452 0.850352\\nv -0.150183 0.051581 0.815526\\nv -0.155102 0.066437 0.834306\\nv -0.124936 0.054984 0.818519\\nv -0.152066 0.030991 0.824123\\nv 0.227940 0.072766 1.162378\\nv 0.267216 0.086837 1.148035\\nv 0.220557 0.049261 1.137598\\nv 0.243563 0.069246 1.193885\\nv 0.279662 0.056693 1.121566\\nv 0.267774 0.003721 1.133949\\nv 0.272350 0.021648 0.897909\\nv 0.312394 0.018201 0.851477\\nv 0.288699 -0.015279 0.861785\\nv 0.307585 -0.017027 0.902396\\nv 0.287692 -0.036408 0.838472\\nv 0.281136 -0.013461 0.878175\\nv 0.309404 -0.008783 0.854047\\nv 0.300908 -0.042493 0.870773\\nv 0.267570 -0.038757 0.831990\\nv 0.289053 -0.043492 0.829733\\nv 0.278158 -0.037147 0.810278\\nv 0.237321 -0.155350 0.491725\\nv 0.144252 -0.184269 0.548578\\nv 0.175718 -0.163201 0.474924\\nv 0.199695 -0.084899 0.489260\\nv 0.250080 -0.119394 0.552563\\nv 0.122258 -0.126348 0.518888\\nv 0.159107 -0.073707 0.533244\\nv 0.155129 -0.109454 0.483029\\nv 0.154575 -0.118627 0.590302\\nv 0.211592 -0.074052 0.537818\\nv 0.198806 -0.098433 0.591319\\nv 0.204210 -0.181168 0.579895\\nv -0.029610 -0.056130 0.474769\\nv -0.067207 -0.006996 0.481172\\nv 0.005696 -0.014619 0.464262\\nv -0.011915 0.042977 0.502254\\nv 0.049735 -0.007706 0.510504\\nv 0.018830 -0.070958 0.520203\\nv -0.042156 0.042405 0.548753\\nv -0.053232 -0.072246 0.548096\\nv -0.080105 -0.013613 0.543534\\nv 0.025692 0.019576 0.566404\\nv 0.014882 -0.045098 0.577318\\nv -0.031588 -0.006482 0.593977\\nv -0.006708 0.082018 0.098927\\nv 0.029103 0.084578 0.074533\\nv 0.004271 0.037770 0.048640\\nv 0.055517 0.046384 0.084551\\nv 0.024264 0.015848 0.107416\\nv 0.002204 0.051457 0.129015\\nv -0.025469 0.033651 0.086386\\nv 0.254121 -0.116985 0.054034\\nv 0.221056 -0.143498 0.084537\\nv 0.199635 -0.105488 0.074263\\nv 0.230645 -0.071206 0.074062\\nv 0.276251 -0.108042 0.105383\\nv 0.214298 -0.086343 0.122196\\nv 0.228534 -0.122679 0.130515\\nv 0.247733 -0.206017 0.000876\\nv 0.263171 -0.239479 0.012915\\nv 0.231782 -0.226269 0.028816\\nv 0.251518 -0.193274 0.049758\\nv 0.285709 -0.207271 0.012879\\nv 0.277935 -0.222310 0.049923\\nv 0.003637 -0.057961 0.038428\\nv -0.037438 -0.065807 0.051166\\nv -0.019539 -0.083507 0.019450\\nv -0.008278 -0.028708 0.022763\\nv -0.043483 -0.036713 0.024272\\nv 0.108127 -0.067525 0.930792\\nv 0.152344 -0.066013 0.987582\\nv 0.184827 0.010372 1.018514\\nv -0.075701 -0.002532 1.042484\\nv 0.106480 0.010946 1.152940\\nv 0.036964 0.022095 1.159695\\nv 0.045303 -0.028897 1.151991\\nv 0.015994 0.014410 0.895196\\nv 0.030308 0.050777 0.937912\\nv 0.183272 0.079760 1.398186\\nv 0.228993 0.019532 1.425452\\nv -0.113692 0.076324 1.092414\\nv -0.154232 0.120430 1.120990\\nv -0.139952 0.047109 0.846075\\nv -0.132133 0.027112 0.793967\\nv -0.147023 0.016881 0.807548\\nv -0.143579 0.034511 0.813307\\nv 0.222793 0.016080 1.154434\\nv 0.304053 0.042932 1.179070\\nv 0.266084 0.021959 1.200812\\nv 0.311981 0.030891 0.909563\\nv 0.339723 0.011013 0.887763\\nv 0.329587 -0.017170 0.873977\\nv -0.021035 -0.058424 -0.005120\\nv -0.057741 -0.044853 0.994091\\nv -0.047011 -0.000993 1.253488\\nv 0.015193 -0.091384 1.251282\\nv 0.055877 -0.012197 1.371336\\nv 0.121840 -0.095829 1.255754\\nv 0.166086 0.023821 1.264520\\nv 0.066004 0.103323 1.264904\\nvt 0.517632 0.106198\\nvt 0.356700 0.203928\\nvt 0.285908 0.068868\\nvt 0.069416 0.079611\\nvt 0.050000 0.360322\\nvt 0.000000 0.262652\\nvt 0.086862 0.404805\\nvt 0.277835 0.402268\\nvt 0.162453 0.522224\\nvt 0.538913 0.262077\\nvt 0.384182 0.400721\\nvt 0.785297 0.389231\\nvt 0.647347 0.528438\\nvt 0.691730 0.240222\\nvt 0.046468 0.674862\\nvt 0.309883 0.575587\\nvt 0.551271 0.781869\\nvt 0.565528 0.471567\\nvt 0.932809 0.533621\\nvt 0.780600 0.796902\\nvt 0.308307 0.822541\\nvt 0.061911 0.847594\\nvt 0.423516 0.549447\\nvt 0.987482 0.737691\\nvt 0.278570 0.900000\\nvt 0.233909 0.147634\\nvt 0.175000 0.000000\\nvt 0.225000 0.000000\\nvt 0.425000 0.000000\\nvt 0.502431 0.044159\\nvt 0.368528 0.227252\\nvt 0.261645 0.063330\\nvt 0.162652 0.168749\\nvt 0.039111 0.392807\\nvt 0.030845 0.074559\\nvt 0.477638 0.317831\\nvt 0.890886 0.087686\\nvt 0.977904 0.337164\\nvt 0.807505 0.340745\\nvt 0.657262 0.376906\\nvt 0.926036 0.492579\\nvt 0.291170 0.328093\\nvt 0.288565 0.487179\\nvt 0.551453 0.498885\\nvt 0.451937 0.636738\\nvt 0.701274 0.561671\\nvt 0.721451 0.674630\\nvt 0.629061 0.804790\\nvt 0.828799 0.720026\\nvt 0.109796 0.705748\\nvt 0.226543 0.659454\\nvt 0.126751 0.794249\\nvt 0.346267 0.824152\\nvt 0.857148 0.870485\\nvt 0.980385 0.247670\\nvt 0.943909 0.029635\\nvt 0.735448 0.092021\\nvt 0.029237 0.308098\\nvt 0.122962 0.134139\\nvt 0.213916 0.052014\\nvt 0.591998 0.305014\\nvt 0.736062 0.509927\\nvt 0.369353 0.596771\\nvt 0.499805 0.491498\\nvt 0.396499 0.303170\\nvt 0.636338 0.507506\\nvt 0.555556 0.689029\\nvt 0.228106 0.651007\\nvt 0.233749 0.517367\\nvt 0.864675 0.750742\\nvt 0.895463 0.468350\\nvt 0.995357 0.719962\\nvt 0.735852 0.760251\\nvt 0.130388 0.798647\\nvt 0.789256 0.903978\\nvt 0.967012 0.850000\\nvt 0.475000 0.000000\\nvt 0.455738 0.190210\\nvt 0.593234 0.098837\\nvt 0.252099 0.148722\\nvt 0.525000 0.000000\\nvt 0.603922 0.196106\\nvt 0.786634 0.333301\\nvt 0.587811 0.512081\\nvt 0.974968 0.410835\\nvt 0.799882 0.607465\\nvt 0.412150 0.555713\\nvt 0.204855 0.739596\\nvt 0.216851 0.458794\\nvt 0.509640 0.796479\\nvt 0.440035 0.213600\\nvt 0.716401 0.362311\\nvt 0.945986 0.210262\\nvt 0.104615 0.254245\\nvt 0.328482 0.559693\\nvt 0.866844 0.498633\\nvt 1.000000 0.464277\\nvt 0.497185 0.557604\\nvt 0.628026 0.764873\\nvt 0.012669 0.762319\\nvt 0.165281 0.781051\\nvt 0.103090 0.510871\\nvt 0.594907 0.767154\\nvt 1.000000 0.227605\\nvt 0.876593 0.400774\\nvt 0.794812 0.173692\\nvt 0.406343 0.163164\\nvt 0.522570 0.339959\\nvt 0.375904 0.462595\\nvt 0.686952 0.453862\\nvt 0.632041 0.796126\\nvt 0.480111 0.701357\\nvt 0.227416 0.684832\\nvt 0.022257 0.835910\\nvt 0.054347 0.555553\\nvt 0.084988 0.971897\\nvt 0.482458 0.295777\\nvt 0.697490 0.362420\\nvt 0.957004 0.233191\\nvt 0.402916 0.512797\\nvt 0.477682 0.768942\\nvt 0.784748 0.718996\\nvt 0.875000 1.000000\\nvt 0.961755 0.847284\\nvt 0.047718 0.245144\\nvt 0.241136 0.328923\\nvt 0.588879 0.259694\\nvt 0.409276 0.630238\\nvt 0.122837 0.588013\\nvt 0.657244 0.631037\\nvt 0.847447 0.803957\\nvt 1.000000 0.575205\\nvt 0.905418 0.328252\\nvt 0.156454 0.836879\\nvt 0.000000 0.444867\\nvt 0.188821 0.664838\\nvt 0.433641 0.203203\\nvt 0.684825 0.617807\\nvt 0.934059 0.487437\\nvt 0.865646 0.903036\\nvt 0.163225 0.078896\\nvt 0.149623 0.370863\\nvt 0.000000 0.185241\\nvt 0.370888 0.131194\\nvt 0.778405 0.205772\\nvt 0.374202 0.393910\\nvt 0.994819 0.527721\\nvt 0.816263 0.653436\\nvt 0.985332 0.020665\\nvt 0.976579 0.319364\\nvt 0.777825 0.364051\\nvt 0.614252 0.267062\\nvt 0.538103 0.749188\\nvt 0.426602 0.037635\\nvt 0.376629 0.430586\\nvt 0.087878 0.508206\\nvt 0.776218 0.411274\\nvt 0.992642 0.588214\\nvt 0.777094 0.870723\\nvt 0.339633 0.247205\\nvt 0.410434 0.662677\\nvt 0.013394 0.381527\\nvt 0.899526 0.280070\\nvt 0.595880 0.514306\\nvt 0.576036 0.173332\\nvt 0.039861 0.202895\\nvt 0.181431 0.310780\\nvt 0.027574 0.543833\\nvt 0.430621 0.476929\\nvt 0.278741 0.596484\\nvt 0.346636 0.272862\\nvt 0.542555 0.824705\\nvt 0.130871 0.592207\\nvt 0.111114 0.857109\\nvt 0.758835 0.607145\\nvt 0.959926 0.563757\\nvt 0.938498 0.817221\\nvt 0.012629 0.024742\\nvt 0.020601 0.285245\\nvt 0.372989 0.168293\\nvt 0.179266 0.380092\\nvt 0.390639 0.413440\\nvt 0.574494 0.442305\\nvt 0.096824 0.613803\\nvt 0.755726 0.574575\\nvt 0.734379 0.213419\\nvt 0.000000 0.573203\\nvt 0.317447 0.689148\\nvt 0.550029 0.730518\\nvt 0.749634 0.890600\\nvt 0.921762 0.580632\\nvt 0.041741 0.922496\\nvt 0.265975 0.883006\\nvt 0.074521 0.568295\\nvt 0.207185 0.384438\\nvt 0.185917 0.113775\\nvt 0.407559 0.467083\\nvt 0.592150 0.662670\\nvt 0.763689 0.202290\\nvt 0.599408 0.821823\\nvt 0.827900 0.490908\\nvt 0.777087 0.227090\\nvt 0.597892 0.455239\\nvt 0.409629 0.382769\\nvt 0.254038 0.129797\\nvt 0.191448 0.356932\\nvt 0.024043 0.542102\\nvt 0.300677 0.685935\\nvt 0.920855 0.576194\\nvt 0.677284 0.803850\\nvt 0.372234 0.812244\\nvt 0.429770 0.219444\\nvt 0.656873 0.366569\\nvt 0.464610 0.525155\\nvt 0.207057 0.698698\\nvt 0.909539 0.329293\\nvt 0.813741 0.702760\\nvt 0.395737 0.650827\\nvt 0.511421 0.733445\\nvt 0.625662 0.430529\\nvt 0.210082 0.482585\\nvt 0.027681 0.479715\\nvt 0.944849 0.025597\\nvt 0.978508 0.129645\\nvt 0.828409 0.230583\\nvt 0.955880 0.300933\\nvt 0.859329 0.400000\\nvt 0.000000 0.427488\\nvt 0.957971 0.459095\\nvt 0.478003 0.499739\\nvt 0.000000 0.518854\\nvt 0.974102 0.850112\\nvt 0.037794 0.900000\\nvt 0.388034 0.931038\\nvt 0.649125 0.900000\\nvt 0.931930 0.900000\\nvt 0.350717 0.927665\\nvt 0.722436 0.967439\\nvt 0.000000 0.579264\\nvt 0.973548 0.794324\\nvt 0.056398 0.871873\\nvt 0.380477 0.177888\\nvt 0.295299 0.358106\\nvt 0.074545 0.710795\\nvt 0.571895 0.881862\\nvt 0.977229 0.146831\\nvt 0.204045 0.219616\\nvt 0.586457 0.114272\\nvt 0.912571 0.794681\\nvt 0.042821 0.453795\\nvt 0.081397 0.834117\\nvt 0.935239 0.740281\\nvt 0.065204 0.076460\\nvt 0.141769 0.298184\\nvt 0.989745 0.501335\\nvt 0.927746 0.748281\\nvt 0.886016 0.429051\\nvt 0.102736 0.212798\\nvt 0.126984 0.520691\\nvt 0.101415 0.781871\\nvt 1.000000 0.567699\\nvt 0.402241 0.600714\\nvt 0.800472 0.265835\\nvt 0.771749 0.719443\\nvt 1.000000 0.751343\\nvt 0.030855 0.497367\\nvt 0.676425 0.415745\\nvt 0.198425 0.778147\\nvt 0.481717 0.665826\\nvt 0.781993 0.915905\\nvt 0.054851 0.357370\\nvt 0.483268 0.311587\\nvt 0.320459 0.491495\\nvt 0.177600 0.748141\\nvt 0.868311 0.702428\\nvt 0.000000 0.675732\\nvt 0.559595 0.764952\\nvt 0.938968 0.290990\\nvt 0.606790 0.215976\\nvt 1.000000 0.512752\\nvt 0.037942 0.326308\\nvt 0.000000 0.665919\\nvt 0.219344 0.183209\\nvt 0.599761 0.192630\\nvt 0.971166 0.498350\\nvt 0.738245 0.025436\\nvt 0.535235 0.402054\\nvt 0.450000 0.500000\\nvt 0.600000 0.500000\\nvt 0.475000 1.000000\\nvt 0.058696 0.982173\\nvt 1.000000 0.713740\\nvt 0.942999 0.093393\\nvt 0.800000 0.500000\\nvt 0.625000 1.000000\\nvt 1.000000 0.500000\\nvt 0.825000 1.000000\\nvt 0.200000 0.500000\\nvt 0.025000 1.000000\\nvt 0.000000 0.500000\\nvt 0.053936 0.117692\\nvt 0.225000 1.000000\\nvn -0.0626 -0.4206 -0.9051\\nvn -0.2841 0.5977 -0.7497\\nvn 0.4234 0.3220 -0.8468\\nvn -0.2263 0.0229 -0.9738\\nvn 0.5234 0.8214 -0.2265\\nvn 0.7903 -0.0767 -0.6079\\nvn 0.2895 0.9443 -0.1564\\nvn -0.1136 0.9695 -0.2172\\nvn 0.2475 0.9647 0.0905\\nvn -0.6068 -0.5881 -0.5348\\nvn -0.6407 0.7457 0.1831\\nvn 0.0369 -0.9828 0.1811\\nvn -0.3080 -0.9423 0.1313\\nvn -0.1553 -0.8566 -0.4920\\nvn 0.7455 0.4180 0.5192\\nvn -0.5413 0.7939 0.2770\\nvn -0.7419 -0.4657 0.4824\\nvn -0.4008 -0.8646 0.3029\\nvn 0.4936 -0.1837 0.8501\\nvn 0.4324 -0.8359 0.3380\\nvn -0.3994 0.7961 0.4546\\nvn 0.8083 0.4007 0.4313\\nvn -0.4746 0.3603 0.8031\\nvn -0.2941 0.7993 0.5241\\nvn 0.2046 0.7749 -0.5981\\nvn -0.1048 -0.4413 -0.8912\\nvn 0.2646 -0.7371 -0.6218\\nvn -0.2889 0.2903 -0.9123\\nvn 0.6435 -0.3708 -0.6696\\nvn 0.5091 0.5851 -0.6312\\nvn 0.8375 0.3822 -0.3906\\nvn -0.7131 -0.0643 -0.6981\\nvn 0.5849 -0.7622 -0.2773\\nvn -0.2260 -0.8983 -0.3767\\nvn 0.9810 -0.1773 0.0791\\nvn -0.4805 0.5562 -0.6780\\nvn -0.2978 0.8985 -0.3225\\nvn -0.7765 -0.6280 0.0523\\nvn -0.8846 0.1713 0.4337\\nvn 0.1429 -0.9764 -0.1622\\nvn 0.5014 -0.8641 -0.0438\\nvn -0.2776 -0.4237 0.8622\\nvn 0.9134 -0.2138 -0.3465\\nvn 0.8520 0.2142 -0.4777\\nvn 0.2851 0.9468 -0.1493\\nvn 0.8660 0.4799 0.1403\\nvn -0.2941 0.3866 0.8741\\nvn 0.8949 -0.1570 0.4177\\nvn -0.7674 0.4578 -0.4490\\nvn -0.5337 -0.0155 -0.8455\\nvn -0.2668 -0.7976 -0.5409\\nvn -0.0147 0.8519 -0.5236\\nvn 0.2111 -0.7291 -0.6510\\nvn -0.7707 -0.4743 -0.4255\\nvn 0.8430 0.4868 0.2287\\nvn 0.8848 -0.4630 -0.0531\\nvn 0.6848 -0.0028 -0.7287\\nvn -0.1848 -0.9592 -0.2139\\nvn 0.6227 -0.4812 0.6170\\nvn -0.1544 0.9876 -0.0285\\nvn 0.0197 0.9026 -0.4301\\nvn -0.9737 -0.0768 -0.2145\\nvn -0.9651 -0.2518 -0.0722\\nvn -0.8987 0.2977 -0.3221\\nvn -0.7440 -0.5157 0.4249\\nvn -0.6703 0.6401 0.3754\\nvn 0.1953 0.0954 0.9761\\nvn 0.1051 -0.5288 -0.8422\\nvn -0.6641 -0.6495 -0.3702\\nvn -0.1688 0.9430 -0.2868\\nvn 0.8536 -0.1036 -0.5105\\nvn -0.9625 0.2555 -0.0914\\nvn -0.6083 -0.3251 -0.7241\\nvn -0.7463 -0.6398 0.1837\\nvn 0.4848 0.2814 -0.8281\\nvn 0.2685 -0.9193 -0.2879\\nvn -0.8289 0.4426 0.3420\\nvn 0.6432 0.3960 0.6554\\nvn 0.2422 0.9579 0.1540\\nvn -0.1011 -0.2857 0.9530\\nvn 0.7779 0.1562 -0.6086\\nvn 0.6474 -0.6759 -0.3523\\nvn 0.4842 0.7911 -0.3737\\nvn 0.2111 0.6548 0.7257\\nvn 0.0424 -0.4335 -0.9002\\nvn -0.6009 0.6674 -0.4399\\nvn 0.8337 -0.0985 0.5434\\nvn -0.1531 -0.7188 0.6782\\nvn -0.7558 -0.5054 -0.4162\\nvn -0.8929 0.2037 0.4016\\nvn 0.3156 0.7296 -0.6068\\nvn 0.8606 -0.4728 -0.1892\\nvn 0.3039 -0.4354 -0.8474\\nvn -0.3109 0.1549 -0.9377\\nvn -0.7674 -0.4804 -0.4247\\nvn -0.8544 0.4861 -0.1839\\nvn -0.0988 -0.9949 -0.0218\\nvn 0.0966 -0.5069 0.8566\\nvn -0.6896 -0.1862 0.6999\\nvn -0.2129 0.8313 0.5134\\nvn 0.6388 -0.0249 0.7690\\nvn 0.8105 0.5655 0.1527\\nvn 0.7422 0.1648 0.6497\\nvn 0.8042 -0.5879 -0.0876\\nvn 0.4830 0.8130 -0.3251\\nvn -0.0769 0.2723 0.9591\\nvn -0.4808 -0.5325 0.6966\\nvn -0.2623 -0.8854 -0.3838\\nvn -0.9863 -0.0074 -0.1648\\nvn 0.1317 -0.1837 -0.9741\\nvn 0.1640 0.9664 0.1979\\nvn 0.9794 -0.0991 -0.1758\\nvn -0.0448 -0.0364 0.9983\\nvn -0.7188 0.5294 -0.4506\\nvn 0.2050 -0.9550 0.2146\\nvn -0.9260 -0.3744 0.0480\\nvn -0.4374 0.1593 -0.8850\\nvn -0.5539 0.7931 0.2533\\nvn 0.8956 0.2082 -0.3932\\nvn -0.3708 -0.9112 -0.1793\\nvn -0.8148 0.5696 0.1081\\nvn 0.2151 0.9504 -0.2248\\nvn -0.8382 0.1418 -0.5266\\nvn -0.3352 0.5121 0.7909\\nvn 0.4418 0.2406 -0.8642\\nvn 0.2195 -0.8217 -0.5259\\nvn -0.8759 0.3656 0.3148\\nvn 0.1344 0.4641 -0.8755\\nvn -0.5644 -0.5936 -0.5737\\nvn -0.0289 -0.7355 0.6769\\nvn -0.3231 -0.3795 -0.8669\\nvn -0.5873 0.4479 0.6741\\nvn 0.6249 0.7274 -0.2834\\nvn 0.3605 -0.7939 0.4896\\nvn 0.7907 -0.0379 -0.6110\\nvn 0.7966 -0.0366 -0.6034\\nvn 0.7827 -0.0440 -0.6209\\nvn 0.7292 -0.3902 -0.5622\\nvn -0.5718 -0.8035 0.1658\\nvn -0.1560 -0.5496 -0.8207\\nvn 0.2669 0.6708 -0.6920\\nvn 0.9565 0.1480 0.2514\\nvn -0.9819 0.0629 -0.1787\\nvn -0.4698 0.8818 0.0406\\nvn -0.4780 0.3380 -0.8107\\nvn -0.5504 0.1577 0.8199\\nvn 0.4387 0.8969 0.0558\\nvn 0.2037 0.4941 0.8452\\nvn 0.2924 -0.7485 0.5952\\nvn -0.1944 -0.6473 -0.7370\\nvn -0.7480 0.1644 -0.6431\\nvn 0.3578 0.0744 -0.9308\\nvn 0.0947 0.9028 -0.4194\\nvn 0.9630 0.1307 -0.2357\\nvn 0.5430 -0.8323 -0.1118\\nvn -0.3949 0.8746 0.2812\\nvn -0.5103 -0.8137 0.2785\\nvn -0.9729 0.0437 0.2271\\nvn 0.6390 0.5444 0.5434\\nvn 0.5009 -0.4702 0.7267\\nvn -0.2535 0.1170 0.9602\\nvn -0.4808 0.8544 0.1972\\nvn 0.3659 0.8781 -0.3083\\nvn -0.1781 -0.2804 -0.9432\\nvn 0.9955 -0.0747 -0.0589\\nvn 0.2995 -0.8629 0.4072\\nvn -0.1433 0.0937 0.9852\\nvn -0.9211 -0.3800 -0.0849\\nvn 0.4753 -0.2331 -0.8484\\nvn -0.3190 -0.9407 -0.1150\\nvn -0.9166 0.0069 -0.3997\\nvn -0.0185 0.9187 -0.3945\\nvn 0.9506 0.0080 0.3102\\nvn -0.4776 0.5334 0.6981\\nvn -0.1486 -0.4550 0.8780\\nvn -0.3770 0.4035 -0.8337\\nvn 0.1455 -0.9010 -0.4086\\nvn -0.9032 -0.4019 0.1505\\nvn -0.2599 0.7031 0.6619\\nvn 0.8387 0.3279 -0.4347\\nvn 0.5736 -0.3109 0.7578\\nvn 0.8866 -0.0775 0.4560\\nvn -0.5428 -0.2991 0.7848\\nvn -0.0170 -0.9693 -0.2453\\nvn 0.4669 0.8839 -0.0282\\nvn -0.8311 0.5556 -0.0248\\nvn 0.1106 -0.7990 -0.5911\\nvn 0.5275 -0.8054 0.2702\\nvn 0.7917 0.1260 0.5978\\nvn -0.6876 -0.1049 0.7185\\nvn 0.8207 0.0677 0.5674\\nvn -0.6116 0.0825 0.7869\\nvn -0.2357 -0.7217 0.6508\\nvn 0.3989 0.2592 -0.8796\\nvn 0.3236 0.7214 -0.6122\\nvn -0.3815 0.6990 -0.6048\\nvn 0.9159 -0.4013 -0.0063\\nvn 0.2320 -0.1393 -0.9627\\nvn -0.5547 0.8067 -0.2038\\nvn 0.2362 -0.1748 0.9558\\nvn -0.9483 0.3173 -0.0079\\nvn -0.9566 0.2911 -0.0127\\nvn -0.9558 0.2937 0.0144\\nvn -0.7923 -0.6101 -0.0028\\nvn 0.9324 0.0178 0.3609\\nvn 0.0736 -0.5138 0.8547\\nvn 0.1921 0.6968 0.6911\\nvn 0.9597 0.2225 0.1715\\nvn 0.6497 -0.7096 -0.2725\\nvn -0.0246 -0.1471 -0.9888\\nvn -0.6863 -0.7267 0.0292\\nvn -0.9930 0.0379 0.1121\\nvn -0.4195 -0.9076 0.0173\\nvn -0.0718 -0.4998 0.8631\\nvn 0.6023 -0.7979 0.0240\\nvn 0.9583 0.2469 0.1436\\nvn -0.0965 0.9951 0.0215\\nusemtl None\\ns 1\\nf 417/465/428 418/466/429 419/467/430\\nf 420/468/431 421/469/432 422/470/433\\nf 423/471/434 424/472/435 425/473/436\\nf 418/466/429 426/474/437 427/475/438\\nf 428/476/439 429/477/440 430/478/441\\nf 423/471/434 425/473/436 431/479/442\\nf 432/480/443 425/473/436 424/472/435\\nf 429/477/440 433/481/444 434/482/445\\nf 435/483/446 436/484/447 428/476/439\\nf 431/479/442 437/485/448 438/486/449\\nf 439/487/450 433/481/444 437/485/448\\nf 435/483/446 431/488/442 436/484/447\\nf 437/485/448 440/489/451 438/486/449\\nf 441/490/452 420/468/431 442/491/453\\nf 419/467/430 441/490/452 442/492/453\\nf 417/465/428 419/467/430 442/493/453\\nf 443/494/454 444/495/455 445/496/456\\nf 446/497/457 447/498/458 443/499/454\\nf 443/494/454 448/500/459 444/495/455\\nf 443/501/454 447/502/458 449/503/460\\nf 443/494/454 449/503/460 450/504/461\\nf 447/502/458 451/505/462 449/503/460\\nf 452/506/463 453/507/464 446/497/457\\nf 454/508/465 455/509/466 448/500/459\\nf 450/504/461 456/510/467 454/508/465\\nf 454/508/465 457/511/468 458/512/469\\nf 456/510/467 459/513/470 457/511/468\\nf 460/514/471 461/515/472 462/516/473\\nf 458/512/469 463/517/474 455/509/466\\nf 457/511/468 464/518/475 458/512/469\\nf 459/513/470 464/518/475 457/511/468\\nf 465/519/476 466/520/477 467/521/478\\nf 465/522/476 468/523/479 466/524/477\\nf 469/525/480 470/526/481 467/521/478\\nf 471/527/482 472/528/483 473/529/484\\nf 474/530/485 470/526/481 469/525/480\\nf 475/531/486 474/530/485 472/528/483\\nf 476/532/487 471/527/482 477/533/488\\nf 478/534/489 479/535/490 470/526/481\\nf 471/527/482 475/531/486 472/528/483\\nf 478/534/489 480/536/491 479/535/490\\nf 475/531/486 481/537/492 474/530/485\\nf 481/537/492 470/526/481 474/530/485\\nf 481/537/492 478/534/489 470/526/481\\nf 482/538/493 471/527/482 476/532/487\\nf 481/537/492 483/539/494 482/540/493\\nf 467/521/478 466/541/477 484/542/495\\nf 485/543/496 486/544/497 487/545/498\\nf 488/546/499 489/547/500 490/548/501\\nf 489/547/500 491/549/502 492/550/503\\nf 493/551/504 494/552/505 495/553/506\\nf 490/548/501 492/550/503 496/554/507\\nf 497/555/508 498/556/509 499/557/510\\nf 499/558/510 500/559/511 497/555/508\\nf 501/560/512 502/561/513 499/557/510\\nf 500/559/511 503/562/514 497/555/508\\nf 503/562/514 504/563/515 498/556/509\\nf 505/564/516 506/565/517 502/566/513\\nf 500/559/511 504/567/515 503/562/514\\nf 506/565/517 504/567/515 500/559/511\\nf 507/568/518 508/569/519 509/570/520\\nf 510/571/521 511/572/522 512/573/523\\nf 511/572/522 509/570/520 513/574/524\\nf 508/569/519 513/574/524 509/570/520\\nf 513/574/524 514/575/525 515/576/526\\nf 515/576/526 516/577/527 512/573/523\\nf 516/577/527 517/578/528 518/579/529\\nf 516/577/527 514/580/525 517/578/528\\nf 519/581/530 520/582/531 521/583/532\\nf 522/584/533 523/585/534 519/581/530\\nf 523/585/534 520/582/531 519/581/530\\nf 523/585/534 524/586/535 520/582/531\\nf 524/586/535 523/587/534 525/588/536\\nf 526/589/537 527/590/538 528/591/539\\nf 529/592/540 528/591/539 527/590/538\\nf 526/589/537 530/593/541 527/590/538\\nf 529/592/540 531/594/542 528/591/539\\nf 530/593/541 529/592/540 527/590/538\\nf 532/595/543 530/596/541 526/597/537\\nf 532/598/543 529/592/540 530/593/541\\nf 532/595/543 531/594/542 529/592/540\\nf 533/599/544 534/600/545 535/601/546\\nf 536/602/547 533/603/544 535/601/546\\nf 536/602/547 534/604/545 533/603/544\\nf 537/605/548 538/606/549 539/607/550\\nf 537/608/548 539/609/550 540/610/551\\nf 539/609/550 541/611/552 542/612/553\\nf 420/468/431 441/490/452 423/471/434 421/469/432\\nf 543/613/554 544/614/555 545/615/556\\nf 543/616/554 545/615/556 546/617/557\\nf 547/618/558 548/619/559 549/620/560\\nf 547/621/558 549/622/560 550/623/561\\nf 551/624/562 552/625/563 553/626/564\\nf 554/627/565 555/628/566 556/629/567\\nf 554/630/565 557/631/568 558/632/569\\nf 559/633/570 560/634/571 561/635/572\\nf 555/628/566 562/636/573 559/633/570\\nf 563/637/574 564/638/575 558/632/569\\nf 565/639/576 558/640/569 564/641/575\\nf 560/634/571 562/636/573 564/638/575\\nf 565/639/576 564/641/575 562/636/573\\nf 566/642/577 567/643/578 568/644/579\\nf 567/643/578 569/645/580 568/644/579\\nf 569/645/580 570/646/581 568/644/579\\nf 570/646/581 571/647/582 568/644/579\\nf 567/643/578 572/648/583 569/645/580\\nf 571/647/582 573/649/584 566/650/577\\nf 574/651/585 572/648/583 567/643/578\\nf 575/652/586 576/653/587 570/646/581\\nf 576/653/587 573/649/584 571/647/582\\nf 573/649/584 577/654/588 574/655/585\\nf 577/656/588 575/652/586 572/648/583\\nf 575/652/586 577/657/588 576/653/587\\nf 578/658/589 579/659/590 580/660/591\\nf 581/661/592 582/662/593 580/663/591\\nf 583/664/594 581/661/592 579/659/590\\nf 582/662/593 583/664/594 584/665/595\\nf 585/666/596 586/667/597 587/668/598\\nf 585/669/596 588/670/599 589/671/600\\nf 586/667/597 590/672/601 587/668/598\\nf 586/667/597 589/673/600 591/674/602\\nf 590/672/601 591/675/602 589/671/600\\nf 592/676/603 593/677/604 594/678/605\\nf 592/676/603 594/678/605 595/679/606\\nf 596/680/607 597/681/608 593/677/604\\nf 594/678/605 597/681/608 595/679/606\\nf 598/682/609 599/683/610 600/684/611\\nf 599/683/610 601/685/612 602/686/613\\nf 441/490/452 419/467/430 418/466/429\\nf 442/687/453 420/688/431 603/689/614\\nf 426/474/437 418/466/429 417/465/428\\nf 442/687/453 603/689/614 430/478/441\\nf 420/688/431 422/690/433 603/689/614\\nf 441/490/452 424/472/435 423/471/434\\nf 430/478/441 603/689/614 428/476/439\\nf 424/472/435 418/466/429 427/475/438\\nf 428/476/439 603/689/614 604/691/615\\nf 430/478/441 429/477/440 434/482/445\\nf 423/471/434 605/692/616 421/469/432\\nf 435/483/446 604/691/615 605/693/616\\nf 428/476/439 604/691/615 435/483/446\\nf 605/693/616 604/691/615 422/690/433\\nf 439/487/450 424/472/435 606/694/617\\nf 439/487/450 606/694/617 434/482/445\\nf 423/471/434 431/479/442 435/695/446\\nf 433/481/444 439/487/450 434/482/445\\nf 429/477/440 428/476/439 436/484/447\\nf 432/480/443 437/485/448 425/473/436\\nf 425/473/436 437/485/448 431/479/442\\nf 436/484/447 431/488/442 438/696/449\\nf 438/486/449 440/489/451 607/697/618\\nf 608/698/619 437/485/448 433/481/444\\nf 609/699/620 433/481/444 436/484/447\\nf 436/484/447 607/700/618 609/699/620\\nf 607/697/618 440/489/451 608/701/619\\nf 609/699/620 607/700/618 608/702/619\\nf 443/499/454 445/496/456 446/497/457\\nf 446/497/457 445/496/456 452/506/463\\nf 443/494/454 450/504/461 448/500/459\\nf 448/500/459 452/506/463 444/495/455\\nf 446/497/457 453/507/464 447/498/458\\nf 452/506/463 448/500/459 453/507/464\\nf 449/503/460 456/510/467 450/504/461\\nf 460/514/471 453/507/464 461/515/472\\nf 455/509/466 453/507/464 448/500/459\\nf 453/507/464 460/514/471 447/498/458\\nf 456/510/467 449/503/460 459/513/470\\nf 451/703/462 447/498/458 460/514/471\\nf 461/515/472 453/507/464 455/509/466\\nf 454/508/465 456/510/467 457/511/468\\nf 454/508/465 458/512/469 455/509/466\\nf 451/505/462 460/704/471 459/513/470\\nf 461/515/472 455/509/466 463/517/474\\nf 462/516/473 461/515/472 463/517/474\\nf 464/705/475 463/517/474 458/512/469\\nf 462/516/473 463/517/474 464/705/475\\nf 610/706/621 484/542/495 466/524/477\\nf 468/523/479 611/707/622 473/529/484\\nf 468/523/479 465/522/476 611/707/622\\nf 484/542/495 469/525/480 467/521/478\\nf 467/521/478 470/526/481 479/535/490\\nf 467/521/478 479/535/490 465/519/476\\nf 473/529/484 611/707/622 471/527/482\\nf 473/529/484 472/528/483 469/525/480\\nf 465/522/476 477/533/488 611/707/622\\nf 479/535/490 480/536/491 465/519/476\\nf 465/522/476 480/708/491 477/533/488\\nf 471/527/482 483/709/494 475/531/486\\nf 475/531/486 483/709/494 481/537/492\\nf 478/534/489 481/537/492 482/540/493\\nf 471/527/482 482/538/493 483/709/494\\nf 488/546/499 612/710/623 489/547/500\\nf 612/710/623 491/549/502 489/547/500\\nf 493/551/504 612/711/623 488/712/499\\nf 488/712/499 490/548/501 493/551/504\\nf 495/553/506 612/711/623 493/551/504\\nf 489/547/500 492/550/503 490/548/501\\nf 492/550/503 491/549/502 613/713/624\\nf 495/553/506 494/552/505 491/714/502\\nf 493/551/504 490/548/501 496/554/507\\nf 496/554/507 492/550/503 613/713/624\\nf 494/552/505 493/551/504 496/554/507\\nf 494/552/505 496/554/507 613/715/624\\nf 498/556/509 501/560/512 499/557/510\\nf 497/555/508 503/562/514 498/556/509\\nf 500/559/511 499/558/510 502/566/513\\nf 502/566/513 506/565/517 500/559/511\\nf 501/560/512 504/563/515 505/716/516\\nf 509/717/520 510/571/521 507/718/518\\nf 507/718/518 510/571/521 512/573/523\\nf 507/568/518 518/719/529 508/569/519\\nf 518/579/529 507/718/518 516/577/527\\nf 516/577/527 507/718/518 512/573/523\\nf 513/574/524 515/576/526 511/572/522\\nf 512/573/523 511/572/522 515/576/526\\nf 513/574/524 508/569/519 514/575/525\\nf 514/575/525 508/569/519 517/720/528\\nf 516/577/527 515/576/526 514/575/525\\nf 521/583/532 520/582/531 614/721/625\\nf 521/722/532 615/723/626 519/581/530\\nf 615/723/626 522/584/533 519/581/530\\nf 520/582/531 524/586/535 614/721/625\\nf 615/723/626 523/585/534 522/584/533\\nf 615/723/626 525/724/536 523/585/534\\nf 524/586/535 525/588/536 614/721/625\\nf 614/721/625 525/588/536 615/725/626\\nf 528/591/539 531/594/542 526/597/537\\nf 526/597/537 531/594/542 532/595/543\\nf 534/600/545 616/726/627 535/601/546\\nf 616/726/627 536/602/547 535/601/546\\nf 536/602/547 616/726/627 534/604/545\\nf 617/727/628 618/728/629 619/729/630\\nf 538/606/549 541/730/552 539/607/550\\nf 538/606/549 537/608/548 540/610/551\\nf 540/610/551 539/609/550 620/731/631\\nf 620/731/631 539/609/550 542/612/553\\nf 538/606/549 540/610/551 621/732/632\\nf 622/733/633 620/731/631 542/612/553\\nf 538/606/549 621/732/632 541/730/552\\nf 621/732/632 540/610/551 622/733/633\\nf 542/612/553 541/611/552 621/734/632\\nf 544/735/555 543/736/554 623/737/634\\nf 623/737/634 624/738/635 544/735/555\\nf 545/615/556 544/614/555 625/739/636\\nf 544/735/555 624/738/635 625/740/636\\nf 623/737/634 543/616/554 546/617/557\\nf 545/615/556 625/739/636 546/617/557\\nf 557/631/568 556/629/567 561/635/572\\nf 623/737/634 546/617/557 624/738/635\\nf 546/617/557 625/739/636 624/738/635\\nf 547/621/558 550/741/561 548/619/559\\nf 549/620/560 548/619/559 550/741/561\\nf 554/630/565 556/629/567 557/631/568\\nf 561/635/572 556/629/567 559/633/570\\nf 555/628/566 559/633/570 556/629/567\\nf 565/639/576 555/628/566 554/627/565\\nf 561/635/572 560/634/571 557/631/568\\nf 554/627/565 558/640/569 565/639/576\\nf 557/631/568 563/637/574 558/632/569\\nf 563/637/574 557/631/568 560/634/571\\nf 560/634/571 559/633/570 562/636/573\\nf 560/634/571 564/638/575 563/637/574\\nf 555/628/566 565/639/576 562/636/573\\nf 568/644/579 571/647/582 566/650/577\\nf 566/650/577 573/649/584 567/742/578\\nf 573/649/584 574/655/585 567/742/578\\nf 572/648/583 575/652/586 569/645/580\\nf 569/645/580 575/652/586 570/646/581\\nf 576/653/587 577/654/588 573/649/584\\nf 579/659/590 581/661/592 580/743/591\\nf 584/665/595 578/744/589 580/663/591\\nf 580/663/591 582/662/593 584/665/595\\nf 584/665/595 583/664/594 578/744/589\\nf 578/658/589 583/664/594 579/659/590\\nf 581/661/592 583/664/594 582/662/593\\nf 586/667/597 585/666/596 589/673/600\\nf 590/672/601 588/670/599 587/668/598\\nf 588/670/599 590/672/601 589/671/600\\nf 590/672/601 586/667/597 591/674/602\\nf 593/677/604 592/676/603 596/680/607\\nf 592/676/603 595/679/606 596/745/607\\nf 596/745/607 595/679/606 597/746/608\\nf 594/678/605 593/677/604 597/681/608\\nf 626/747/637 602/686/613 601/685/612\\nf 601/685/612 598/682/609 626/748/637\\nf 626/748/637 598/682/609 600/684/611\\nf 626/748/637 599/683/610 602/749/613\\nf 601/685/612 599/683/610 598/682/609\\nf 430/478/441 417/465/428 442/750/453\\nf 430/478/441 434/482/445 627/751/638 426/474/437\\nf 430/478/441 426/474/437 417/465/428\\nf 424/472/435 441/490/452 418/466/429\\nf 468/523/479 473/529/484 610/706/621 466/524/477\\nf 421/469/432 605/692/616 422/470/433\\nf 627/751/638 427/475/438 426/474/437\\nf 604/691/615 603/689/614 422/690/433\\nf 606/694/617 424/472/435 427/475/438\\nf 435/695/446 605/692/616 423/471/434\\nf 439/487/450 432/480/443 424/472/435\\nf 427/475/438 627/751/638 606/694/617\\nf 434/482/445 606/694/617 627/751/638\\nf 436/484/447 433/481/444 429/477/440\\nf 437/485/448 432/480/443 439/487/450\\nf 608/701/619 440/489/451 437/485/448\\nf 609/699/620 608/698/619 433/481/444\\nf 607/700/618 436/484/447 438/696/449\\nf 452/506/463 445/496/456 444/495/455\\nf 454/508/465 448/500/459 450/504/461\\nf 459/513/470 449/503/460 451/505/462\\nf 462/516/473 464/705/475 460/514/471\\nf 464/518/475 459/513/470 460/704/471\\nf 480/708/491 482/538/493 476/532/487 477/533/488\\nf 473/529/484 484/542/495 610/706/621\\nf 469/525/480 484/542/495 473/529/484\\nf 611/707/622 477/533/488 471/527/482\\nf 474/530/485 469/525/480 472/528/483\\nf 628/752/639 485/543/496 629/753/640 630/754/641\\nf 482/540/493 480/536/491 478/534/489\\nf 495/553/506 491/714/502 612/711/623\\nf 613/715/624 491/714/502 494/552/505\\nf 501/560/512 498/556/509 504/563/515\\nf 502/561/513 501/560/512 505/716/516\\nf 506/565/517 505/564/516 504/755/515\\nf 511/572/522 510/571/521 509/570/520\\nf 517/720/528 508/569/519 518/719/529\\nf 615/725/626 521/583/532 614/721/625\\nf 620/731/631 622/733/633 540/610/551\\nf 622/733/633 542/612/553 621/734/632\\nf 571/647/582 570/646/581 576/653/587\\nf 574/655/585 577/654/588 572/756/583\\nf 588/670/599 585/666/596 587/668/598\\nf 626/748/637 600/684/611 599/683/610\\nf 629/753/640 485/543/496 487/757/498 631/758/642 630/759/641\\nf 631/758/642 487/757/498 632/760/643 630/761/641\\nf 486/544/497 633/762/644 630/763/641 632/764/643 487/765/498\\nf 633/762/644 486/544/497 485/543/496 628/752/639 630/766/641" // This is your obj file 
	// objString = g_objects.droid[1][1].replaceAll("\\n", '\n')
// console.log( objString );

	obj_loader = new THREE.OBJLoader();

	//LOAD ANIMATION OBJECTS:
	objects.droid = [];
	objects.droid[0] = [];//droid.standing
	objects.droid[1] = [];//droid.jogging
	objects.droid[2] = [];//droid.butterfly.twirl
	objects.droid[3] = [];//droid.jumping.over
	objects.droid[4] = [];//droid.T default object
	objects.droid[5] = [];//droid.T default object

	for (let i = 0; i < g_objects.droid.length; i++)
	{
		for (let j = 0; j < g_objects.droid[i].length; j++)
		{
			objects.droid[i][j] = load_obj(g_objects.droid[i][j], textures[0][5], true);	//load object	1
			objects.droid[i][j].rotation.x = -90 * Math.PI / 180;						//tranform object to correct direction
			// scene.add( objects.droid[i][j] );
		}
	}
	scene.add( objects.droid[4][0] );		//add object to the scene

	// LOAD PLATFORM PIECE:
	objects.platform = [];
	objects.platform[0] = load_obj(g_objects.platform, textures[0][5], true);	//load object	1
	objects.platform[0].position.set(0, 3, 11)
	objects.platform[0].rotation.set(180 * Math.PI / 180, 0 * Math.PI / 180, 45 * Math.PI / 180)
	scene.add( objects.platform[0] );		//add object to the scene

	objects.platform[0].rhs = [];
	objects.platform[0].rhs[0] = load_obj(g_objects.platform, textures[1][5], true);	//load object	1
	objects.platform[0].rhs[0].position.set(0, 3, 11)
	objects.platform[0].rhs[0].rotation.set(180 * Math.PI / 180, 0 * Math.PI / 180, 45 * Math.PI / 180)
	//scene.add( objects.platform[0].rhs[0] );		//add object to the scene

	// 3D TRIANGLE COLLISION SETUP: 

	// console.log("TEST PRINT VERTICES: ");
	// console.log("vertices: ", g_vertices);
		
	objects.platform[0].triangles = [
		[ [g_vertices[0], g_vertices[1], g_vertices[2]], [g_vertices[3], g_vertices[4], g_vertices[5]], [g_vertices[6], g_vertices[7], g_vertices[8]] ],
		[ [g_vertices[9], g_vertices[10], g_vertices[11]], [g_vertices[12], g_vertices[13], g_vertices[14]], [g_vertices[15], g_vertices[16], g_vertices[17]] ]
	];

	let l_translate = [ objects.platform[0].position.x, objects.platform[0].position.y , objects.platform[0].position.z ]
	// let	l_rotate = [0, 0, 0];
	// let l_tp = [ l_translate[0] + l_rotate[0], l_translate[1] + l_rotate[1], l_translate[2] + l_rotate[2]];
	//console.log("l translate position: ", l_tp);
	collision_triangles_translation = [];	
	for (let i = 0; i < objects.platform[0].triangles.length; i++) {
		collision_triangles_translation[i] = [];
		for (let j = 0; j < objects.platform[0].triangles[i].length; j++) {
			collision_triangles_translation[i][j] = [];
			//translation and rotation formulas here:
			let l_ct = [ objects.platform[0].triangles[i][j][0], objects.platform[0].triangles[i][j][1], objects.platform[0].triangles[i][j][2] ]

			rotatexyz(l_ct, [0, 0, 0], objects.platform[0].rotation.x, objects.platform[0].rotation.y, objects.platform[0].rotation.z )

			for (let k = 0; k < objects.platform[0].triangles[i][j].length; k++) {

				collision_triangles_translation[i][j][k] = l_translate[k] + l_ct[k];
				//console.log("inner ", collision_triangles[i][j]);
			}
		}
	}
	// console.log("inner ", collision_triangles_translation);
	
	//screen_text[0][0].position.x

	// objects.platform[0].rhs[1] = objects.platform[0].clone(true)
	// objects.platform[0].rhs[1].rotation.set(270 * Math.PI / 180, 0, (Math.random() * 10) * Math.PI / 180)
	// objects.platform[0].rhs[1].position.set(0, 0, 17)
	// scene.add( objects.platform[0].rhs[1] );		//add object to the scene

	// objects.platform[0].rhs[2] = objects.platform[0].clone(true)
	// objects.platform[0].rhs[2].rotation.set(270 * Math.PI / 180, 0, (Math.random() * 10) * Math.PI / 180)
	// objects.platform[0].rhs[2].position.set(0, 0, 20)
	// scene.add( objects.platform[0].rhs[2] );		//add object to the scene

	// objects.platform[0].rhs[3] = objects.platform[0].clone(true)
	// objects.platform[0].rhs[3].rotation.set(270 * Math.PI / 180, 0, (Math.random() * 10) * Math.PI / 180)
	// objects.platform[0].rhs[3].position.set(0, 0, 23)
	// scene.add( objects.platform[0].rhs[3] );		//add object to the scene



	//LOAD FLOORS
	objects.floor = [];
	objects.floor[0] = [];
	for (let i = 0; i < g_objects.floor.length; i++)
	{
		for (let j = 0; j < g_objects.floor[i].length; j++)
		{
			objects.floor[i][j] = load_obj(g_objects.floor[i][j], textures[1][5], true);	//load object	3
			// objects.droid[i][j].rotation.x = -90 * Math.PI / 180;						//tranform object to correct direction
			// scene.add( objects.droid[i][j] );
		}
	}
	// objects.floor[0][0].position.set(-25, 0, 50);
	objects.floor[0][0].position.set(0, 0, 0);
	scene.add( objects.floor[0][0] );

	// HEIGHTMAPS:
	heightmap_load(g_objects.floor[0][0], height_map, heightmap_vertices)

	// objects.floor[0][0].rhs = [];
	// objects.floor[0][0].rhs[0] = objects.floor[0][0].clone(true);
	// objects.floor[0][0].position.set(25, 0, 0);
	// scene.add( objects.floor[0][0].rhs[0] );
	
	//LOAD trees:
	objects.tree = [];
	objects.tree[0] = [];
	
	objects.tree[0][0] = load_obj(g_objects.tree[0], textures[1][5], true);	//load object	7
	
	objects.tree[0][0].position.set(5, 0, 10);
	objects.tree[0][0].scale.x = 0.5;
	objects.tree[0][0].scale.y = 0.5;
	objects.tree[0][0].scale.z = 0.5;

	scene.add( objects.tree[0][0] );
	objects.tree[0][0].rhs = [];
	objects.tree[0][0].rhs[0] = objects.tree[0][0].clone(true);
	objects.tree[0][0].rhs[0].position.set(-5, 0, 10);
	scene.add( objects.tree[0][0].rhs[0] );

	//LOAD ASCII CHARECTERS
	objects.ascii = [];
	for (let i = 0; i < g_objects.ascii.length; i++)
	{
		objects.ascii[i] = load_obj(g_objects.ascii[i], textures[0][0], true);
		objects.ascii[i].rotation.x = -90 * Math.PI / 180;
		objects.ascii[i].rotation.z = 180 * Math.PI / 180;
	}

		//SET AND RENDER SCREEN_TEXT OBJECT:
	ascii.construct[0]();
	
	//LOAD TEST OBJECT:
	// scene.remove( cuber );
	cuber = objects.droid[1][0];
	cuber.position.set(-5, 0, 4);
	scene.add( cuber );

	//CREATE CAMERA_POINTER OBJECT:
	camera_pointer = load_obj(g_objects.cube, textures[0][0], true);
	// scene.add( camera_pointer );
	
	camera.position.set(0, player.height, -5);
	camera.lookAt(new THREE.Vector3(0,player.height,0));
	camera.rotation.x = 270;	// for looking up and down

	
	renderer = new THREE.WebGLRenderer();
	// renderer.setSize(1280, 720);
	renderer.setSize(window.innerWidth, window.innerHeight);

	renderer.shadowMap.enabled = true;
	renderer.shadowMap.type = THREE.BasicShadowMap;

	// renderer.domElement.style.zIndex = "-1";
	document.body.appendChild(renderer.domElement);

	animate();
}


function animate(){
	requestAnimationFrame(animate);
	
	// cuber.rotation.z += 0.01;

	mesh.rotation.x += 0.01;
	mesh.rotation.y += 0.02;
	crate.rotation.y += 0.01;
	
	camera_position = [camera.position.x, camera.position.y, camera.position.z];

	if(keyboard[87]){ // W key
		camera_position[0] -= Math.sin(camera.rotation.y) * player.speed;
		camera_position[2] -= -Math.cos(camera.rotation.y) * player.speed;
	}
	if(keyboard[83]){ // S key
		camera_position[0] += Math.sin(camera.rotation.y) * player.speed;
		camera_position[2] += -Math.cos(camera.rotation.y) * player.speed;
	}
	if(keyboard[65]){ // A key
		camera_position[0] += Math.sin(camera.rotation.y + Math.PI/2) * player.speed;
		camera_position[2] += -Math.cos(camera.rotation.y + Math.PI/2) * player.speed;
	}
	if(keyboard[68]){ // D key
		camera_position[0] += Math.sin(camera.rotation.y - Math.PI/2) * player.speed;
		camera_position[2] += -Math.cos(camera.rotation.y - Math.PI/2) * player.speed;
	}

	if(keyboard[37]){ // left arrow key
		camera.rotation.y -= player.turnSpeed;
	}
	if(keyboard[39]){ // right arrow key
		camera.rotation.y += player.turnSpeed;
	}
	if(keyboard[38]){ // up arrow key
		camera.rotation.x -= player.turnSpeed;
	}
	if(keyboard[40]){ // down arrow key
		camera.rotation.x += player.turnSpeed;
	}

	if(keyboard[100]){ // 4 arrow key
		camera.rotation.z += 1 * Math.PI / 180;
	}
	if(keyboard[102]){ // 6 arrow key
		camera.rotation.z -= 1 * Math.PI / 180;
	}
	if(keyboard[67]){ // C clicked
		//change camera variable:
		camera_switch_iter += 0.1;
		if (camera_switch_iter > 1.0) {
			camera_switch_iter = 0.0;
			camera_state += 1;
			if (camera_state > 2)
				camera_state = 0;
		}
	}

	let l_M;
	
	//ANIMATE AN OBJECT:
	animate_timer_end = new Date();
	if (animate_timer_end - animate_timer_start > animate_speed)					//if time reached
	{
		animate_timer_start = animate_timer_end;						//inherit new time
		animation_index += 1;
		if (animation_index >= objects.droid[animation_id].length)
			animation_index = 0;
		scene.remove( cuber );
		cuber = objects.droid[animation_id][animation_index];
		cuber.position.set(-5, 0, 4);
		scene.add( cuber );
	}

	//TERRAIN COLLISION DETECTION:
    //# Find quadrant player_pos lies in:
    height_offset = [0, 0, 0]

    h_player_pos = [ camera_position[0], camera_position[1], camera_position[2]]
    xy = terrain_bounds(h_player_pos, height_map, heightmap_vertices)
    if (xy) {
        if (xy[0] != -1 && xy[1] != -1) {
            height_offset = point_terrain(h_player_pos, xy, height_map, heightmap_vertices, [[0, 0, 0], [0, 0, 0], [0, 0, 0]])
			
            // height_offsets.append(height_offset)
		}
        // else
            // height_offsets.append(prev_height_offset)
	}
	objects.floor[0][0].position.y = -height_offset[1];

	//l_collision = l_collision || 0

	//OBJECT COLLISION DETECTION:
	let	l_collision = 0;

	// l_radius = Math.sqrt(Math.pow(cube.rhs[2].center[0] - cube.rhs[0].center[0], 2) + Math.pow(cube.rhs[2].center[1] - cube.rhs[0].center[1], 2)); 
	// if (l_radius < 0.1) {

		// CUBE COLLISION:
	let	l_collided;
	l_collided = collision_object(camera_position, [crate.position.x, crate.position.y, crate.position.z], 2.0);

	if (l_collided)
		crate.material.map = textures[0][3];
	else
		crate.material.map = textures[0][2];

	l_collision = l_collision || l_collided;// || l_radius2 <= 0.5;

		//TREE COLLISION:
	l_collision = l_collision || collision_object(camera_position, [objects.tree[0][0].rhs[0].position.x, objects.tree[0][0].rhs[0].position.y, objects.tree[0][0].rhs[0].position.z], 0.5);

	//WALL COLLISION DETECTION:
	l_collision = l_collision || collision_wall(camera_position, [crate.position.x, crate.position.y, crate.position.z], [objects.tree[0][0].position.x, objects.tree[0][0].position.y, objects.tree[0][0].position.z], 0.1);

	// SET CAMERA MOVEMENT POSITIONS OFFICIALLY:
	// if (l_radius > 2.0 && l_radius2 > 0.5 && l_wall_collided === 0)//if will not collide with object then move player camera object:
	animation_id = 1;
	if (!l_collision)
		camera.position.set(camera_position[0], camera_position[1], camera_position[2]);
	else {
		animation_id = 4;
		// animation_index = 0;
	}

	// RENDER SCREEN_TEXT:			ascii.js
	ascii.render[0]();

	// 3D TRIANGLE RAY COLLISION (REQUIRES ascii.render and camera.position):
	let l_ctt = collision_triangles_translation;//collision_triangles;

	let l_dp = [ screen_text[0][0].position.x, screen_text[0][0].position.y, screen_text[0][0].position.z ];
	let	l_p = [ camera.position.x, camera.position.y, camera.position.z ];

	let hit = rayIntersectsTriangle([l_p[0], l_p[1], l_p[2]], [l_dp[0] - l_p[0], l_dp[1] - l_p[1], l_dp[2] - l_p[2]], l_ctt[0][0], l_ctt[0][1], l_ctt[0][2]);
	let hit2 = rayIntersectsTriangle([l_p[0], l_p[1], l_p[2]], [l_dp[0] - l_p[0], l_dp[1] - l_p[1], l_dp[2] - l_p[2]], l_ctt[1][0], l_ctt[1][1], l_ctt[1][2]);

	// console.log("p: ", [l_p[0], l_p[1], l_p[2]])
	// console.log("d: ", [l_dp[0] - l_p[0], l_dp[0] - l_p[1], l_dp[0] - l_p[2]])
	//console.log("triangle", l_ctt);//collision_triangles);
	if (hit || hit2) {//if hit one of the 2 triangles that make up a square:
		scene.remove( objects.platform[0] );
		scene.remove( objects.platform[0].rhs[0] );
		scene.add( objects.platform[0].rhs[0] );
	}
	else {
		scene.remove( objects.platform[0] );
		scene.remove( objects.platform[0].rhs[0] );
		scene.add( objects.platform[0] );
	}

	//ANIMATION OBJECT MOVE TO INFRONT OF CAMERA:
	l_M = [0, -1.75, -0.1]
	rotatexyz(l_M, [0, 0, 0], 0, camera.rotation.y, 0 )

	cuber.position.x = camera.position.x + l_M[0];
	cuber.position.y = camera.position.y + l_M[1];
	cuber.position.z = camera.position.z + l_M[2];

	// cuber.rotation.x =  (90 * Math.PI / 180) + camera.rotation.x ;
	// cuber.rotation.y = camera.rotation.y;
	// if(keyboard[87] || keyboard[87] || keyboard[65] || keyboard[68]) // W key
		cuber.rotation.z = -camera.rotation.y + 0;//+ 180 * Math.PI / 180;

	//CAMERA ALLOW VERTICAL LOOK UP AND DOWNWARDS:
	camera_pointer.position.set(camera.position.x, camera.position.y, camera.position.z);
	//camera_pointer.rotation.x += 0.1;

	l_M = [0, 0, 1]
	rotatexyz(l_M, [0, 0, 0], camera.rotation.x, camera.rotation.y, 0 )

	camera_pointer.position.x += l_M[0];
	camera_pointer.position.y += l_M[1];
	camera_pointer.position.z += l_M[2];

	let temp = [camera.rotation.x, camera.rotation.y, camera.rotation.z]

	//THIRD PERSON CAMERA AND BEV CAMERA:
	if (camera_state === 1)
		l_M = [0, 0, -2]
	if (camera_state === 2) 
		l_M = [0, 4, -1]
	if (camera_state === 1 || camera_state === 2) {
		rotatexyz(l_M, [0, 0, 0], camera.rotation.x, camera.rotation.y, 0 )
		camera.position.x += l_M[0];
		camera.position.y += l_M[1];
		camera.position.z += l_M[2];
	}
	//TO LOOKAT LOCATION:
	if (camera_state === 0)	
		camera.lookAt(camera_pointer.position);
	else
		camera.lookAt(cuber.position);

	//TO RENDER THE SCENE THREEJS:
	renderer.render(scene, camera);

	//THIRD PERSON CAMERA AND BEV CAMEERA UNDO:
	if (camera_state === 1 || camera_state === 2) {
		camera.position.x -= l_M[0];
		camera.position.y -= l_M[1];
		camera.position.z -= l_M[2];
	}
	
	// TO RESET LOOKAT POSITION:
	camera.rotation.x = temp[0]
	camera.rotation.y = temp[1]
	camera.rotation.z = temp[2]

}

function keyDown(event){
	keyboard[event.keyCode] = true;
}

function keyUp(event){
	keyboard[event.keyCode] = false;
}

var mousedown = false
var	mx = -1, my = -1
var	touchdown = false
var tx = -1, ty = -1

function initMove(evt) {
	mousedown = true;
	mx = evt.clientX;
	my = evt.clientY;
}

//object.mousehook:
function	move(evt) {
	if (mousedown) {
		camera.rotation.y += (evt.clientX - mx) * 0.01  //player.turnSpeed;
		camera.rotation.x += (evt.clientY - my) * 0.01
		// for (let i = 0; i < 1; i++) {
			// // objects[i].mousehook[0]((evt.clientX - mx), (evt.clientY - my));
			// objects[i].angle[0] += (evt.clientX - mx) * Math.PI / 360;
			// objects[i].angle[1] += (evt.clientY - my) * Math.PI / 360;//180;

			// objects[i].angle[0] = cube.angle[0] % 360;
			// objects[i].angle[1] = cube.angle[1] % 360;
		// }
		mx = evt.clientX;
		my = evt.clientY;
	}
}

function stopMove() {
	mousedown = false;
}

function touchStart(event) {
	touchdown = true
	tx = event.touches[0].clientX
	ty = event.touches[0].clientY
}

function touchEnd(event) {
	touchdown = false
}

function touchMove(event) {
	if (touchdown) {
		let l_x = event.touches[0].clientX
		let l_y = event.touches[0].clientY

		camera.rotation.y += (l_x - tx) * 0.005  //player.turnSpeed;
		camera.rotation.x += (l_y - ty) * 0.005

		tx = l_x
		ty = l_y
	}
//  document.getElementById("demo").innerHTML = x + ", " + y;
}

function	reSize(event) {
	renderer.setSize(window.innerWidth, window.innerHeight);
}

window.addEventListener('keydown', keyDown);
window.addEventListener('keyup', keyUp);
// window.addEventListener('keyup', keyUp);

window.addEventListener('mousedown', initMove);
window.addEventListener('mousemove', move);
window.addEventListener('mouseup', stopMove);

window.addEventListener('touchstart', touchStart);
window.addEventListener('touchmove', touchMove);
window.addEventListener('touchend', touchEnd);

window.addEventListener("resize", reSize);
// ctx.canvas.width  = window.innerWidth;
// ctx.canvas.height = window.innerHeight;

window.onload = init;
