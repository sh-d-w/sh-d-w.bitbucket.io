
//ROTATION ALGORITHMS:
// Rotate a vertice
function rotatexyz(M, center, beta, theta, phi) {
	// Rotation matrix coefficients
	let		ct, st;
	let		x;
	let		y;
	let		z;

	//x
	x = M[0];
	y = M[1];
	z = M[2];
	ct = Math.cos(beta);
	st = Math.sin(beta);
	M[1] = ct * y - st * z;
	M[2] = st * y + ct * z;

	//y
	x = M[0];
	y = M[1];
	z = M[2];
	ct = Math.cos(theta);
	st = Math.sin(theta);
	M[0] = ct * x - st * z;
	M[2] = st * x + ct * z;

	//z
	x = M[0];
	y = M[1];
	z = M[2];
	ct = Math.cos(phi);
	st = Math.sin(phi);
	M[0] = ct * x - st * y;
	M[1] = st * x + ct * y;
}

//COLLISION DETECTION ALGORITHMS:
function	collision_object(p1, o1, p_buffer) {
// collision_object(camera_position, [objects.tree[0][0].position.x, objects.tree[0][0].position.y, objects.tree[0][0].position.z], 2.0);
	let l_radius = Math.sqrt(Math.pow(o1[0] - p1[0], 2) + Math.pow(o1[2] - p1[2], 2)); 

	return ( 1 * (l_radius < p_buffer) + 0);
}

function	collision_wall(p1, l1, l2, p_buffer) {
// collision_wall(camera_position, [crate.position.x, crate.position.y, crate.position.z], [objects.tree[0][0].position.x, objects.tree[0][0].position.y, objects.tree[0][0].position.z], 0.1)
	let lineLen = Math.sqrt(Math.pow(l1[0] - l2[0], 2) + Math.pow(l1[2] - l2[2], 2)); //dist(x1,y1, x2,y2);
	let d1	    = Math.sqrt(Math.pow(l1[0] - p1[0], 2) + Math.pow(l1[2] - p1[2], 2)); //dist(px,py, x1,y1);
	let d2      = Math.sqrt(Math.pow(l2[0] - p1[0], 2) + Math.pow(l2[2] - p1[2], 2)); //dist(px,py, x2,y2);

	return (1 * (d1+d2 >= lineLen-p_buffer && d1+d2 <= lineLen+p_buffer) + 0 );
	// if (d1+d2 >= lineLen-0.1 && d1+d2 <= lineLen+0.1) {
		// return 1;
	// }
	// else
		// return 0;
	// return (d1 + d2 >= lineLen - p_buffer && d1 + d2 <= lineLen + p_buffer);
}



//3d triangle collisions:
/* a = b - c */
function	vector(a, b, c) {
	a[0] = b[0] - c[0];
	a[1] = b[1] - c[1];
	a[2] = b[2] - c[2];
	return a
}

function	crossProduct(a,b,c) {
	a[0] = b[1] * c[2] - c[1] * b[2];
	a[1] = b[2] * c[0] - c[2] * b[0];
	a[2] = b[0] * c[1] - c[0] * b[1];
	return a
}

function	innerProduct(v,q) {

		return v[0] * q[0] + v[1] * q[1] + v[2] * q[2]
}

function	rayIntersectsTriangle(p, d, v0, v1, v2) {
//http://www.lighthouse3d.com/tutorials/maths/ray-triangle-intersection/
	let e1 = [0, 0, 0], e2 = [0, 0, 0], h = [0, 0, 0], s = [0, 0, 0], q = [0, 0, 0];
	let a, f, u, v;

	e1 = vector(e1,v1,v0);// 0,200,0
	e2 = vector(e2,v2,v0);// 200,200,0

	h = crossProduct(h,d,e2);//-800,800,(-73000)
	a = innerProduct(e1,h);//160000

	if (a > -0.00001 && a < 0.00001)
		return (false);

	f = 1 / a;
	s = vector(s,p,v0);
	u = f * (innerProduct(s,h));//(-0.4625)

	if (u < 0.0 || u > 1.0)
		return (false);
//	document.getElementById("debug").innerHTML = "TEST " + u;
//	console.log("debug: ", "TEST ", u);

	h = crossProduct(q,s,e1);
	v = f * innerProduct(d,q);

	if (v < 0.0 || u + v > 1.0)
		return (false);

	// at this stage we can compute t to find out where
	// the intersection point is on the line
	t = f * innerProduct(e2,q);

	if (t > 0.00001) // ray intersection
		return ([v, u, t]);//true

	else // this means that there is a line intersection
		 // but not a ray intersection
		 return (false);

}
